<?php

App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
App::uses('CakeTime', 'Utility');

/**
 * News Controller
 *
 * @property News               $News
 * @property Order              $Order
 * @property PaginatorComponent $Paginator
 */
class NewsController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array(
        'Session',
        'Paginator',
        'RequestHandler'
    );

    public $uses = array(
        'News',
        'Users.User',
        'Order'
    );
	
	public $helpers = array('String');

    public function beforeRender() {
        parent::beforeRender();
    }

    /**
     * index method
     *
     * @return void
     */
    public function index($course = null)
    {
        $this->Paginator->settings = array('order' => 'News.id DESC', 'limit' => 6);
        $this->set('news', $this->Paginator->paginate());
        $this->set("title_for_layout","Artigos");

    }

    public function tag($keyname = null)
    {
        $news = $this->News->Tagged->find('tagged', ['by' => $keyname, 'model' => 'News']);
        $this->set(compact('news'));

        $this->News->order = 'News.created DESC';
        $this->News->limit = 6;
        $recentNews = $this->News->find('all');

        $this->set(compact('recentNews'));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     *
     * @param string $slug
     *
     * @return void
     */
    public function view($slug = null)
    {
		/* All newss (even those are free) */
        // $news = $this->News->findBySlug($slug);
		$news = $this->News->find('first', array('recursive' => -1, 'conditions' => array('News.slug' => $slug), 'callbacks' => false));
		
		// var_dump($news);die;

        if (!$news) {
            throw new NotFoundException(__('News not found.'));
        }
        $this->set(compact('news'));

        /** RelatedNewss */
        // $this->News->order = 'News.created DESC';
        // $this->News->limit = 3;
		
		$relatedNews = array();
		// var_dump($this->News->find('first', array('conditions' => array('News.slug' => $slug), 'callbacks' => false)));die;
		
		if($news['News']['tags'] == ""){
			$relatedNews = $this->News->find('all', array('limit' => 6, 'order' => array('News.created' => 'DESC'), 'conditions' => array('News.slug <>' => $slug)));
		}else{
			$tags = explode(',', $news['News']['tags']);
			
			// var_dump($tags);die;
			
			if(count($tags) > 0){
				if(isset($tags[0]) && $tags[0] != ""){
					$relatedNews1 = $this->News->find('all', array('limit' => 6, 'order' => array('News.created' => 'DESC'), 'conditions' => array('News.slug <>' => $slug, 'News.tags LIKE "%'. $tags[0] .'%"')));

					if(count($relatedNews1) > 0){
						foreach($relatedNews1 as $relative){
							$relatedNews[count($relatedNews)+1] = $relative;
						}
					}
				}
				
				
				if(count($relatedNews) < 6 && isset($tags[1]) && $tags[1] != ""){
					$relatedNews2 = $this->News->find('all', array('limit' => 6, 'order' => array('News.created' => 'DESC'), 'conditions' => array('News.slug <>' => $slug, 'News.tags LIKE "%'. $tags[1] .'%"')));
				
					if(count($relatedNews2) > 0){
						foreach($relatedNews2 as $relative){
							$relatedNews[count($relatedNews)+1] = $relative;
						}
					}
				}
				
				if(count($relatedNews) < 6 && isset($tags[2]) && $tags[2] != ""){
					$relatedNews3 = $this->News->find('all', array('limit' => 6, 'order' => array('News.created' => 'DESC'), 'conditions' => array('News.slug <>' => $slug, 'News.tags LIKE "%'. $tags[2] .'%"')));
				
					if(count($relatedNews3) > 0){
						foreach($relatedNews3 as $relative){
							$relatedNews[count($relatedNews)+1] = $relative;
						}
					}
				}
				
				
			}
			
			if(count($relatedNews) == 0){
				$relatedNews = $this->News->find('all', array('limit' => 6-count($relatedNews), 'order' => array('News.created' => 'DESC'), 'conditions' => array('News.slug <>' => $slug)));
			}
		}
        $this->set(compact('relatedNews'));
        $this->set("title_for_layout","Artigos - ".$news["News"]["title"]);
        $this->set("meta_description_custom", (strlen($news['News']['text']) > 196) ? substr(strip_tags($news['News']['text']),0,196).'...' : strip_tags($news['News']['text']));
    }



    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index()
    {
        $news = array();
        $this->News->recursive = 0;
        if ($this->request->is('ajax') && ($this->request->query('q'))) {
            $conditions = ['fields'=> ['News.id', 'News.title'], 'conditions' => ['News.title LIKE' => '%' . $this->request->query('q') . '%']];
            $news = $this->News->find('all', $conditions);
            $news = Hash::extract($news, '{n}.News');
        } else {
            $this->Paginator->settings = array(
                'order' => 'News.id DESC'
            );
            $news = $this->Paginator->paginate();
        }
        $this->set([
            'news' => $news,
            '_serialize' => ['news']
        ]);
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     *
     * @param string $id
     *
     * @return void
     */
    public function admin_view($id = null)
    {
        if (!$this->News->exists($id)) {
            throw new NotFoundException(__d($this->cmsPluginName, 'News not found.'));
        }
        $options = array('conditions' => array('News.' . $this->News->primaryKey => $id));
        $news = $this->News->find('first', $options);
        $this->set('news', $news);
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add()
    {
        if ($this->request->is('post')) {
			$this->News->create();

			if ($this->News->save($this->request->data)) {
				$this->Session->setFlash(__d($this->cmsPluginName, 'The news has been saved successfully.'), 'alert', array(
					'plugin' => 'BoostCake',
					'class'  => 'alert-success'
				));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__d($this->cmsPluginName, 'The news could not be saved. Please, try again.'), 'alert', array(
					'plugin' => 'BoostCake',
					'class'  => 'alert-danger'
				));
			}
		}
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     *
     * @param string $id
     *
     * @return void
     */
    public function admin_edit($id = null)
    {
        if (!$this->News->exists($id)) {
            throw new NotFoundException(__d($this->cmsPluginName, 'News not found.'));
        }
        if ($this->request->is(array('post', 'put'))) {

            $data = $this->request->data;

            if ($this->News->saveAll($data)) {
                $this->Session->setFlash(__d($this->cmsPluginName, 'The news has been updated successfully.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-success'
                ));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__d($this->cmsPluginName, 'The news could not be updated. Please, correct any errors and try again.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-danger'
                ));
            }
        } else {
            $options             = array('conditions' => array('News.' . $this->News->primaryKey => $id));
            $this->request->data = $this->News->find('first', $options);
        }
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     *
     * @param string $id
     *
     * @return void
     */
    public function admin_delete($id = null)
    {
        $this->News->id = $id;
        $this->News->recursive = -1;

        $news = $this->News->findById($id);
        
        if (!$this->News->exists()) {
            throw new NotFoundException(__d($this->cmsPluginName, 'News not found.'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->News->delete()) {
            $this->Session->setFlash(__d($this->cmsPluginName, 'The news has been deleted successfully.'), 'alert', array(
                'plugin' => 'BoostCake',
                'class'  => 'alert-success'
            ));
        } else {
            $this->Session->setFlash(__d($this->cmsPluginName, 'The news could not be deleted. Please, try again.'), 'alert', array(
                'plugin' => 'BoostCake',
                'class'  => 'alert-danger'
            ));
        }

        return $this->redirect(array('action' => 'index'));
    }
}
