<?php

    App::uses('AppController', 'Controller');
    App::uses('HttpSocket', 'Network/Http');
    App::uses('CakeTime', 'Utility');
    App::uses('CakeEmail', 'Network/Email');

    /**
     * Movies Controller
     *
     * @property Movie              $Movie
     * @property Order              $Order
     * @property PaginatorComponent $Paginator
     * @property CarrinhoComponent  $Carrinho
     */
    class MoviesController extends AppController
    {

        /**
         * Components
         *
         * @var array
         */
        public $components = array(
            'Session',
            'Paginator',
            'RequestHandler',
            'Auth'
        );

        public $uses = array(
            'Movie',
            'Users.User',
            'Order'
        );

        public function beforeRender()
        {
            parent::beforeRender();
            $this->set($this->Movie->enumValues());
        }

        public function beforeFilter()
        {
            parent::beforeFilter();
            $this->set($this->Movie->enumValues());
            $this->Auth->deny('mymovies');
        }

        /**
         * index method
         *
         * @return void
         */
        public function index($ctrl = null, $slug = null)
        {
            App::import("helper", "String");
            $this->String = new StringHelper(new View(null));

            $this->set("title_for_layout", "Filmes");
            $conditions[] = array('Movie.status' => true);

            //conditions e title
            switch ($ctrl) {
                case 'courses':
                    $pageTitle = "Cursos";
                    $conditions[] = array('controller' => 'courses');
                    break;

                case 'modules':
                    $pageTitle = "Módulos";

                    $conditions[] = array('controller' => 'modules');

                    //busca as infos do curso para por na condicao
                    $curso = $this->Movie->find('first', array('conditions' => array('Movie.slug' => $this->params['slug'])));
                    if ($curso) {
                        $conditions[] = array('Movie.parent_id' => $curso['Movie']['id']);
                    } else {
                        //GAMBA :P
                        throw new NotFoundException(__('Curso não encontrado.'));
                        // $this->Session->setFlash('Curso não encontrado.', 'alert', array(
                        //     'plugin' => 'BoostCake',
                        //     'class'  => 'alert-danger'
                        // ));
                        // $this->redirect('/');
                        $conditions[] = array('Movie.parent_id <' => 0);
                    }

                    $this->set(compact('curso'));
                    break;

                case 'miscellaneous':
                    $pageTitle = "Vídeos Indicados";
                    $conditions[] = array('controller' => 'miscellaneous');
                    break;

                default:
                    $pageTitle = "Todos os filmes";
                    $conditions[] = array('controller' => 'movies');
                    break;
            }

            //order
            switch ($ctrl) {
                case 'lives':
                    $order = array('Movie.date' => 'ASC');
                    break;

                case 'modules':
                    $order = array('Movie.id' => 'ASC');
                    break;

                default:
                    $order = array('Movie.id' => 'DESC');
                    break;
            }

            //limit
            if (isset($this->params['named']['limit'])) {
                $limit = $this->params['named']['limit'];
            } else {
                $limit = 12;
            }

            $this->Paginator->settings = array('conditions' => $conditions, 'limit' => $limit, 'order' => $order);

            if ($ctrl != "miscellaneous") {
                $recentMovies = $this->Movie->find('all', array('conditions' => array('controller' => 'miscellaneous'), 'order' => 'Movie.created DESC', 'limit' => 6));
            } else {
                $recentMovies = array();
            }

            $movies = $this->Paginator->paginate();

            //verifica os cursos/modulos comprados
            if ($ctrl == "courses" || $ctrl == "modules") {
                $comprados = array();
                $curso_integral = false;

                if ($this->Auth->loggedIn()) {
                    if (is_array($movies) && count($movies) > 0) {

                        if ($ctrl == "modules" && isset($curso)) {

                            $order = $this->Order->find('first', array('recursive' => -1, 'conditions' => array('AND' => array('Order.foreign_id' => $curso['Movie']['id'], 'Order.user_id' => $this->Auth->user('id'), 'Order.status' => 3))));
                            if ($order) {
                                $curso_integral = true;
                            }
                        }

                        if ($curso_integral == false) {
                            foreach ($movies as $movie) {
                                $order = $this->Order->find('first', array('recursive' => -1, 'conditions' => array('AND' => array('Order.foreign_id' => $movie['Movie']['id'], 'Order.user_id' => $this->Auth->user('id'), 'Order.status' => 3))));
                                if ($order) {
                                    $comprados[] = $movie['Movie']['id'];
                                }
                            }
                        }

                    }
                }

                $com_modulos = array();
                foreach ($movies as $movie) {
                    $mv = $this->Movie->find('all', array('recursive' => -1, 'conditions' => array('Movie.parent_id' => $movie['Movie']['id'])));
                    if (count($mv) > 1) {
                        $com_modulos[] = $movie['Movie']['id'];
                    }
                }
                $this->set(compact('comprados', 'curso_integral', 'com_modulos'));
            }


            $this->set('movies', $movies);
            $this->set(compact('recentMovies', 'pageTitle', 'ctrl'));

            //define a view
            switch ($ctrl) {
                case 'courses':
                    $this->render('courses');
                    $this->set('body_class', 'cursos');
                    break;

                case 'modules':
                    $this->render('modules');
                    break;

                default:
                    $this->render('index');
                    break;
            }
        }

        public function tag($keyname = null)
        {
            $movies = $this->Movie->Tagged->find('tagged', ['by' => $keyname, 'model' => 'Movie']);
            $this->set(compact('movies'));

            $this->Movie->order = 'Movie.created DESC';
            $this->Movie->limit = 6;
            $recentMovies = $this->Movie->find('all');

            $this->set(compact('recentMovies'));
        }

        /**
         * view method
         *
         * @throws NotFoundException
         *
         * @param string $slug
         *
         * @return void
         */
        public function view($slug = null)
        {
            /* All movies (even those are free) */
            $movie = $this->Movie->findBySlug($slug);

            if (!$movie || $slug == null) {
                throw new NotFoundException(__('Vídeo não encontrado.'));
            }

            if ($movie['Movie']['controller'] == "related") {

                if (!$this->Auth->loggedIn()) {
                    $this->Session->setFlash('Permissão Negada', 'alert', array(
                        'plugin' => 'BoostCake',
                        'class'  => 'alert-danger'
                    ));
                    $this->redirect('/');
                }


                $this->loadModel('RelatedMovie');
                $relacionados = $this->RelatedMovie->find('all', array(
                        'fields'     => array('RelatedMovie.movie_id', 'Movie.parent_id'),
                        'conditions' => array(
                            'AND' => array(
                                'RelatedMovie.related_movie_id' => $movie['Movie']['id'],
                            )
                        )
                    )
                );
                $ids = array();
                foreach ($relacionados as $relacionado) {
                    $ids[] = $relacionado['RelatedMovie']['movie_id'];
                    $ids[] = $relacionado['Movie']['parent_id'];
                }

                $conditions = [
                    'conditions' => [
                        'Order.user_id'    => $this->Auth->user('id'),
                        'Order.status'     => 3,
                        'Order.foreign_id' => $ids,
                    ]
                ];
                $courses = $this->Order->find('all', $conditions);

                if (!$courses) {
                    $this->Session->setFlash('Permissão Negada', 'alert', array(
                        'plugin' => 'BoostCake',
                        'class'  => 'alert-danger'
                    ));
                    $this->redirect('/');
                }
            }

            if (!$movie || $movie['Movie']['status'] == 0) {
                throw new NotFoundException(__('Movie not found.'));
            }
            //sessao
            $this->Session->write('Movie.id', $movie['Movie']['id']);
            $this->Session->write('Movie.slug', $movie['Movie']['slug']);

            $embed = $this->Movie->getEmbed($movie);
            $this->set(compact('movie', 'embed'));

            if ($this->Auth->loggedIn()) {

                //verifica se o usuario comprou o video
                $this->Order->recursive = 2;
                $conditions = [
                    'conditions' => [
                        'Order.user_id' => $this->Auth->user('id'),
                        'Order.status'  => 3,
                        //'Order.paid >=' => CakeTime::toServer(strtotime('-30 days')),
                        'Movie.slug'    => $slug
                    ]
                ];
                $paidMovie = $this->Order->find('first', $conditions);

                //verifica se o cliente é assinante
                $user = $this->User->find('first', array('conditions' => array('User.id' => $this->Auth->user('id'))));
                if ($user && $user['User']['subscriber'] == 1 && $movie['Movie']['type'] != "donation") {
                    $paidMovie = true;
                }

                // if ($paidMovie) {
                $this->Movie->updateViewCounter();
                // }

                //verifica se o video está nos favoritos
                $this->loadModel('MovieFavorite');
                $favorite = $this->MovieFavorite->find('first', array('conditions' => array('AND' => array('movie_id' => $movie['Movie']['id'], 'user_id' => $this->Auth->user('id')))));
                if ($favorite) {
                    $this->set('favorite', true);
                } else {
                    $this->set('favorite', false);
                }
            } else {
                $paidMovie = false;
                $this->Movie->updateViewCounter();
            }

            $this->set(compact('paidMovie'));

            /** RelatedMovies */
            $this->Movie->order = 'Movie.created DESC';
            $this->Movie->limit = 6;


            $relatedMovies = $movies = $this->Movie->find('all', array('recursive' => -1, 'conditions' => array('Movie.id' => explode(',', $movie['Movie']['related_movies']))));
            $this->set(compact('relatedMovies'));
            $this->set("title_for_layout", "Filme - " . $movie["Movie"]["title"]);
            $this->set("meta_description_custom", (strlen($movie['Movie']['description']) > 196) ? substr($movie['Movie']['description'], 0, 196) . '...' : $movie['Movie']['description']);
        }

        public function mymovies()
        {
            $this->Order->recursive = 2;
            $conditions = [
                'conditions' => [
                    'Order.user_id'            => $this->Auth->user('id'),
                    'Order.status'             => 3,
                    'Order.reference NOT LIKE' => '%-assinatura',
                    //'Order.paid >=' => CakeTime::toServer(strtotime('-30 days')),
                    'Movie.status'             => true,
                    array('Movie.controller <>' => 'lives'),
                    array('Movie.controller <>' => 'courses'),
                    array('Movie.controller <>' => 'modules')
                ]
            ];
            $orders = $this->Order->find('all', $conditions);
            $this->set(compact('orders'));
        }

        public function mycourses()
        {
            $orders = array();

            $this->Order->recursive = 2;
            $conditions = [
                'conditions' => [
                    'Order.user_id'    => $this->Auth->user('id'),
                    'Order.status'     => 3,
                    //'Order.paid >=' => CakeTime::toServer(strtotime('-30 days')),
                    'Movie.controller' => 'courses'
                ]
            ];
            $courses = $this->Order->find('all', $conditions);

            if (is_array($courses) && count($courses) > 0) {
                foreach ($courses as $k => $course) {
                    $curs = $this->Movie->find('first', array('conditions' => array('Movie.id' => $course['Order']['foreign_id'])));
                    if ($curs) {
                        $orders[$course['Order']['foreign_id']]['course'] = $curs;
                        $modules = $this->Movie->find('all', array('conditions' => array('Movie.parent_id' => $course['Order']['foreign_id'])));
                        if ($modules) {
                            foreach ($modules as $module) {
                                $orders[$course['Order']['foreign_id']]['modules'][] = $module;
                            }
                        }
                    }
                }
            }

            $conditions = [
                'conditions' => [
                    'Order.user_id'    => $this->Auth->user('id'),
                    'Order.status'     => 3,
                    //'Order.paid >=' => CakeTime::toServer(strtotime('-30 days')),
                    'Movie.controller' => 'modules'
                ]
            ];
            $modules = $this->Order->find('all', $conditions);

            if (is_array($modules) && count($modules) > 0) {
                foreach ($modules as $k => $module) {
                    $mod = $this->Movie->find('first', array('conditions' => array('Movie.id' => $module['Order']['foreign_id'])));
                    if ($mod) {
                        $cur = $this->Movie->find('first', array('conditions' => array('Movie.id' => $mod['Movie']['parent_id'])));
                        $orders[$cur['Movie']['id']]['course'] = $cur;
                        $orders[$cur['Movie']['id']]['modules'][] = $mod;
                    }
                }
            }

            $this->set(compact('orders'));
        }

        /**
         * admin_index method
         *
         * @return void
         */
        public function admin_index($ctrl = null)
        {
            $controller = is_null($ctrl) ? 'movies' : $ctrl;

            $movies = array();
            $this->Movie->recursive = 0;

            if ($this->request->is('ajax')) {
                if ($this->request->query('q')) {

                    if ($controller == "modules") {
                        $conditions = ['fields' => ['Movie.id', 'Movie.title'], 'conditions' => ['Movie.title LIKE' => '%' . $this->request->query('q') . '%', 'controller' => array('related')]];
                    } else {
                        $conditions = ['fields' => ['Movie.id', 'Movie.title'], 'conditions' => ['Movie.title LIKE' => '%' . $this->request->query('q') . '%', 'controller' => array('miscellaneous', $controller)]];
                    }

                    $movies = $this->Movie->find('all', $conditions);
                    $movies = Hash::extract($movies, '{n}.Movie');

                    $this->set([
                        '_serialize' => ['movies']
                    ]);
                }

                if ($this->request->query('a')) {
                    App::import('Model', 'Tags.Tag');
                    $this->Tag = new Tag();
                    $this->Tag->recursive = 0;
                    $conditions2 = ['fields' => ['Tag.name', 'Tag.name'], 'conditions' => ['Tag.name LIKE' => '%' . $this->request->query('a') . '%']];
                    $tags = $this->Tag->find('all', $conditions2);
                    $tags = Hash::extract($tags, '{n}.Tag');

                    $this->set([
                        '_serialize' => ['tags']
                    ]);


                    $this->set([
                        'tags' => $tags
                    ]);
                }
            } else {
                //$this->Paginator->settings = array('limit' => 2);
                $conditions2[] = array('controller' => $controller);
                if (isset($this->params['named']['curso']) && $this->params['named']['curso'] != "") {
                    $conditions2[] = array('Movie.parent_id' => $this->params['named']['curso']);
                }

                $movies = $this->Paginator->paginate($conditions2);
            }

            $this->set([
                'movies' => $movies
            ]);

            $movieType = ucfirst(substr($controller, 0, -1));
            $movieRoute = $controller;

            $this->set(compact('movieType', 'movieRoute'));
        }

        /**
         * admin_view method
         *
         * @throws NotFoundException
         *
         * @param string $id
         *
         * @return void
         */
        public function admin_view($id = null)
        {
            if (!$this->Movie->exists($id)) {
                throw new NotFoundException(__d($this->cmsPluginName, 'Movie not found.'));
            }
            $options = array('conditions' => array('Movie.' . $this->Movie->primaryKey => $id));
            $movie = $this->Movie->find('first', $options);
            $embed = $this->Movie->getEmbed($movie);
            $this->set('movie', $movie);
            $this->set('embed', $embed);
            //$movieType = $movie['Movie']['course']? "Course" : "Movie";
            //$movieRoute = $movie['Movie']['course']? "courses" : "movies";

            $movieRoute = $movie['Movie']['controller'];
            $movieType = ucfirst(substr($movieRoute, 0, -1));

            $this->set(compact('movieType', 'movieRoute'));
        }

        /**
         * admin_add method
         *
         * @return void
         */
        public function admin_add($ctrl = null)
        {
            if ($this->request->is('post')) {

                $this->Movie->create();

                if (isset($this->params['named']['curso']) && $this->params['named']['curso'] != "") {
                    $this->request->data['Movie']['parent_id'] = $this->params['named']['curso'];
                } else {
                    $this->request->data['Movie']['parent_id'] = 0;
                }

                if ($this->Movie->saveAll($this->request->data)) {
                    if ($this->request->data['Movie']['RelatedMovie'] != "") {
                        $this->loadModel('RelatedMovie');
                        $this->RelatedMovie->query("DELETE FROM related_movies WHERE movie_id = " . $this->Movie->id);
                        foreach (explode(',', $this->request->data['Movie']['RelatedMovie']) as $related) {
                            $this->RelatedMovie->save(array('id' => null, 'movie_id' => $this->Movie->id, 'related_movie_id' => $related));
                        }
                    }

                    $movieRoute = $this->request->data['Movie']['controller'];

                    $this->Session->setFlash(__d('users', 'The ' . substr($movieRoute, 0, -1) . ' has been saved successfully.'), 'alert', array(
                        'plugin' => 'BoostCake',
                        'class'  => 'alert-success'
                    ));

                    if (isset($this->params['named']['curso']) && $this->params['named']['curso'] != "") {
                        return $this->redirect(array('controller' => ucfirst($movieRoute), 'action' => 'index', 'curso' => $this->params['named']['curso']));
                    } else {
                        return $this->redirect(array('controller' => ucfirst($movieRoute), 'action' => 'index'));
                    }
                } else {
                    $movieRoute = $this->request->data['Movie']['controller'];

                    $this->Session->setFlash(__d('users', 'The ' . substr($movieRoute, 0, -1) . ' could not be saved. Please, try again.'), 'alert', array(
                        'plugin' => 'BoostCake',
                        'class'  => 'alert-danger'
                    ));
                }
            }
            $categories = $this->Movie->Category->find('list');
            $related_movies = $this->Movie->RelatedMovie->find('list');

            $controller = is_null($ctrl) ? 'movies' : $ctrl;

            if ($controller == 'movies') {
                $this->request->data['Movie']['via_assinatura'] = true;
                $this->request->data['Movie']['via_venda_avulso'] = true;
            } elseif ($controller == 'lives') {
                $this->request->data['Movie']['via_assinatura'] = true;
                $this->request->data['Movie']['via_venda_avulso'] = false;
            } elseif ($controller == 'courses') {
                $this->request->data['Movie']['via_assinatura'] = false;
                $this->request->data['Movie']['via_venda_avulso'] = true;
            } elseif ($controller == 'miscellaneous') {
                $this->request->data['Movie']['via_assinatura'] = false;
                $this->request->data['Movie']['via_venda_avulso'] = false;
            }

            $movieType = ucfirst(substr($controller, 0, -1));
            $movieRoute = $controller;

            $this->set(compact('categories', 'related_movies', 'movieType', 'movieRoute', 'controller'));
        }

        /**
         * admin_edit method
         *
         * @throws NotFoundException
         *
         * @param string $id
         *
         * @return void
         */
        public function admin_edit($id = null)
        {

            App::import("helper", "String");
            $this->String = new StringHelper(new View(null));

            if (!$this->Movie->exists($id)) {
                throw new NotFoundException(__d('users', 'Movie not found.'));
            }
            if ($this->request->is(array('post', 'put'))) {
                $data = $this->request->data;

                //feio demais
                if (isset($data['Movie']['RelatedMovieTmp'])) {
                    $data['Movie']['RelatedMovie'] = str_replace($data['Movie']['RelatedMovieTmp'] . ",", "", $data['Movie']['RelatedMovie']);
                }


                if ($this->Movie->saveAll($data)) {

                    if (isset($data['Movie']['RelatedMovie']) && $data['Movie']['RelatedMovie'] != "") {
                        $this->loadModel('RelatedMovie');
                        $this->RelatedMovie->query("DELETE FROM related_movies WHERE movie_id = " . $this->Movie->id);
                        foreach (explode(',', $data['Movie']['RelatedMovie']) as $related) {
                            $this->RelatedMovie->save(array('id' => null, 'movie_id' => $this->Movie->id, 'related_movie_id' => $related));
                        }
                    }

                    $movieRoute = $data['Movie']['controller'];
                    $movieType = ucfirst(substr($movieRoute, 0, -1));

                    $this->Session->setFlash(__d('users', 'The ' . substr($movieRoute, 0, -1) . ' has been updated successfully.'), 'alert', array(
                        'plugin' => 'BoostCake',
                        'class'  => 'alert-success'
                    ));

                    if (isset($data['Movie']['parent_id']) && $data['Movie']['parent_id'] > 0) {
                        return $this->redirect(array('controller' => ucfirst($movieRoute), 'action' => 'index', 'curso' => $data['Movie']['parent_id']));
                    } else {
                        return $this->redirect(array('controller' => ucfirst($movieRoute), 'action' => 'index'));
                    }
                } else {
                    $movieRoute = $data['Movie']['controller'];
                    $movieType = ucfirst(substr($movieRoute, 0, -1));

                    $this->Session->setFlash(__d('users', 'The ' . substr($movieRoute, 0, -1) . ' could not be updated. Please, correct any errors and try again.'), 'alert', array(
                        'plugin' => 'BoostCake',
                        'class'  => 'alert-danger'
                    ));
                }
            } else {
                $options = array('conditions' => array('Movie.' . $this->Movie->primaryKey => $id));
                $data = $this->Movie->find('first', $options);

                $movieRoute = $controller = $data['Movie']['controller'];
                $movieType = ucfirst(substr($movieRoute, 0, -1));

                $this->request->data = $data;
            }

            if ($this->request->data['Movie']['date'] != "" && $this->request->data['Movie']['date'] != "") {
                $this->request->data['Movie']['date'] = $this->String->dataFormatada("d/m/Y", $this->request->data['Movie']['date']);
            }

            if ($this->request->data['Movie']['related_movies'] != "") {
                $movies = $this->Movie->find('all', array('recursive' => -1, 'fields' => array('Movie.id', 'Movie.title'), 'conditions' => array('Movie.id' => explode(',', $this->request->data['Movie']['related_movies']))));
                $re = array();

                foreach ($movies as $k => $movie) {
                    $re[$k]['id'] = $movie['Movie']['id'];
                    $re[$k]['title'] = $movie['Movie']['title'];
                }

                $this->request->data['Movie']['RelatedMovie'] = json_encode($re);
                //gamba - remover qndo encontrar solucao
                $this->request->data['Movie']['RelatedMovieTmp'] = json_encode($re);
            }

            $categories = $this->Movie->Category->find('list');
            $this->set(compact('categories', 'movieType', 'movieRoute', 'controller'));
        }

        /**
         * admin_delete method
         *
         * @throws NotFoundException
         * @param string $id
         * @return boolean
         */
        public function admin_delete_price($id = null)
        {
            App::import('Model', 'MoviePrice');
            $this->MoviePrice = new MoviePrice();
            $this->MoviePrice->id = $id;
            if (!$this->MoviePrice->exists()) {
                if (!$this->request->is('ajax')) {
                    throw new NotFoundException(__('Registro Inválido.'));
                } else {
                    die('false');
                }
            }

            $this->request->onlyAllow('post', 'delete');
            if ($this->MoviePrice->delete()) {
                if (!$this->request->is('ajax')) {
                    $this->Session->setFlash(__('Registro deletado.'), 'success_message');
                    $this->redirect($this->referer());
                } else {
                    die('true');
                }
            }

            if (!$this->request->is('ajax')) {
                $this->Session->setFlash(__('Registro não pode ser deletado.'), 'error_message');
                $this->redirect(array('action' => 'index'));
            } else {
                die('false');
            }
        }

        /**
         * admin_delete method
         *
         * @throws NotFoundException
         *
         * @param string $id
         *
         * @return void
         */
        public function admin_delete($id = null)
        {
            $this->Movie->id = $id;
            $this->Movie->recursive = -1;

            $movie = $this->Movie->findById($id);
            $movieRoute = $movie['Movie']['controller'];

            if (!$this->Movie->exists()) {
                throw new NotFoundException(__d('users', 'Movie not found.'));
            }
            $this->request->onlyAllow('post', 'delete');
            if ($this->Movie->delete()) {
                $this->Session->setFlash(__d('users', 'The movie has been deleted successfully.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-success'
                ));
            } else {
                $this->Session->setFlash(__d('users', 'The movie could not be deleted. Please, try again.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-danger'
                ));
            }

            return $this->redirect(array('controller' => $movieRoute, 'action' => 'index'));
        }

        public function mobile($type = 'compra', $movie_id = null, $off = 'false', $movie_price)
        {
            if (!$this->Movie->exists($movie_id)) {
                throw new NotFoundException(__d('users', 'Movie not found.'));
            }

            $this->set('movie_price', $movie_price);
            if ($this->Auth->loggedIn()) {

                if ($movie_id != null) {
                    $movie = $this->Movie->findById($movie_id);
                    $this->set('movie', $movie);
                    $this->set('off', $off);
                    $this->set('movie_price', $movie_price);
                }

                if ($type == 'assinatura') {
                    $this->render('assinatura');
                } else {
                    $this->render('compra');
                }

            } else {
                $this->Session->setFlash(__d('users', 'Efetua login para prosseguir.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-danger'
                ));
                $this->redirect(array('plugin' => 'users', 'controller' => 'users', 'action' => 'login'));
            }
        }
    }
