<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('AuthComponent', 'Controller/Component');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $components = array(
        // 'DebugKit.Toolbar' => array('history' => 10),
        // 'RequestHandler',
        'Session',
        'Auth',
        'Facebook.Connect' => array('model' => 'User', 'plugin' => 'Users'),
        'Cookie'
    );

    public $helpers = array(
        'Session',
        'Html'      => array('className' => 'BoostCake.BoostCakeHtml'),
        'Form'      => array('className' => 'BoostCake.BoostCakeForm'),
        'Paginator' => array('className' => 'BoostCake.BoostCakePaginator'),
        'Facebook.Facebook'
    );

    public function beforeFilter()
    {
        // Dinamycally enable the DebugKit Toolbar component.
        // if (Configure::read('debug') > 0) {
        //     $this->components[] = 'DebugKit.Toolbar';
        // }
        $this->cmsSettings();
        $this->cmsVariables();

        if (!isset($this->viewVars['variables']['content']['body_class'])) {
            $this->set('body_class',
                sprintf(
                    '%s %s %s %s',
                    strtolower($this->name),
                    $this->action,
                    $this->request->params['action'],
                    @$this->request->params['pass'][0]
                )
            );
        }

        $this->set('cookieHelper', $this->Cookie);

       // $facebook = $this->Connect->user();
        //$this->set('facebook', $facebook);
        $this->set('facebook_session', $this->Session->read('FB.Me'));
        $this->set('user', $this->Auth->user());

        /*if($this->Auth->User()){
            $user = $this->Auth->User();
            if(isset($user['facebook_post']) && $user['facebook_post'] == false){
                $this->set('publica_no_face', 'true');
                $this->Session->write('Auth.User.facebook_post', '1');
                $this->loadModel('Users.User');
                $this->User->save(array('id' => $user['id'], 'facebook_post' => 1), false);
            }
        }*/

        $this->loadModel('Movie');
        //begin count_ao_vivos
        $count_ao_vivos = $this->Movie->find('count', array(
                                            'recursive' => -1,
                                            'conditions' => array('or' => array(
                                              array('and' => array(
                                                        'controller' => 'lives', 
                                                        'date >' => date('Y-m-d'), 
                                                    )
                                                ),
                                                array('and' => array(
                                                        'date' => date('Y-m-d'), 
                                                        'hour_end >= ' => date('H:i:s')
                                                    )
                                                ),
                                            )
                                      )));
        //begin count_cursos
        $count_cursos = $this->Movie->find('first', array(
                                                'recursive' => -1,
                                                'conditions' => array(
                                                    array('and' => array(
                                                            'controller' => 'courses', 
                                                            'status' => true
                                                        )
                                                    )
                                                ),
                                                'order' => 'Movie.id DESC'
                                            )
                                        );
        //end count_cursos

        //set count_cursos
        $this->set(compact('count_cursos', 'count_ao_vivos'));
    }

    public function beforeFacebookSave(){
        /*if (empty($this->Connect->authUser['User']['id'])) {
            $this->Connect->authUser['User']['id'] = $this->Connect->user('id');
        }*/

        $user = $this->User->find('first', array('conditions' => array('User.email' => $this->Connect->user('email'))));
        if($user){
            $this->Connect->authUser['User']['id']       = $user['User']['id'];
            if($user['User']['password'] != ""){
                $this->Connect->authUser['User']['password'] = $user['User']['password'];
            }
        }else{
            $this->Connect->authUser['User']['id']       = null;            
            $this->Connect->authUser['User']['username'] = $this->Connect->user('username');
        }

        $this->Connect->authUser['User']['name'] = $this->Connect->user('name');
        $this->Connect->authUser['User']['email'] = $this->Connect->user('email');
        $this->Connect->authUser['User']['tos'] = true;
        $this->Connect->authUser['User']['email_verified'] = true;
        $this->Connect->authUser['User']['active'] = true;
        return true; //Must return true or will not save.
    }

    public function beforeRender()
    {
        $this->_setErrorLayout();
        $this->seoSettings();
        if ($this->request->is('ajax')) {
            $this->layout = 'ajax';
        }
    }

    public function cmsVariables() {
        $adminSettings = Configure::read('Admin');

        foreach ($adminSettings as $key => $value) {
            $_key = 'cms' . Inflector::camelize($key);
            $this->set($_key, $value);
            $this->$_key = $value;
        }

    }

    public function cmsSettings()
    {

        if (isset($this->request->params['prefix']) && $this->request->params['prefix'] == 'admin') {
            AuthComponent::$sessionKey = 'Auth.Admin';// solution from http://bit.ly/1pMoolz

            $this->Auth->loginAction    = array(
                'plugin'     => 'Admin',
                'controller' => 'CmsUsers',
                'action'     => 'login'
            );
            $this->Auth->loginRedirect  = array(
                'plugin'     => 'Admin',
                'controller' => 'CmsUsers',
                'action'     => 'index'
            );
            $this->Auth->logoutRedirect = array(
                'plugin'       => false,
                'solutionscms' => false,
                'controller'   => 'home',
                'action'       => 'index',
                'home'
            );
            $this->Auth->authenticate   = array(
                'Form' => array(
                    'userModel' => 'CmsUser',
                )
            );
            $this->Auth->authError      = 'Você não tem permissão para acessar esta página!';
            $this->Auth->flash          = array(
                'params'  => array(
                    'class' => 'alert alert-warning'
                ),
                'key'     => 'auth',
                'element' => 'default'
            );
            $this->Auth->allow('login');
        } else {
            AuthComponent::$sessionKey = 'Auth.User';// solution from http://bit.ly/1pMoolz
            $this->Auth->allow();
        }

    }

    public function seoSettings() {
        $meta_keywords = '';
        $meta_description = 'O Instituto Tolerância é um portal de educação à distância que disponibiliza cursos, aulas, palestras, documentários, debates, com transmissão “ao vivo” e por filmes.';
        $project_prefix_title = 'Instituto Tolerância';

        $this->set(compact('meta_keywords'));
        $this->set(compact('meta_description'));
        $this->set(compact('project_prefix_title'));
    }

    /**
     * Método pra chamar dinamicamente o layout do Admin caso seja uma açãoo administrativa.
     */
    protected function _setErrorLayout()
    {

        if ($this->name == 'CakeError') {
            $this->layout = 'error';
            return;
        }

        if ($this->request->prefix == $this->cmsRoutingPrefix) {
            $this->layout = "{$this->cmsPluginName}.default";
            if (strpos($this->request->action, 'login')) {
                $this->layout = "{$this->cmsPluginName}.login";
            }
        }

        /* if this is 404 error
        {
            $this->layout = 'error';
        }*/

    }

}
