<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * MovieFavorites Controller
 *
 * @property MovieFavorite		$MovieFavorite
 * @property PaginatorComponent $Paginator
 */
class MovieFavoritesController extends AppController
{
    /**
     * Components
     *
     * @var array
     */
    public $components = array();

    public function mymovies()
    {
        $this->MovieFavorite->recursive = 2;
        $conditions = [
            'conditions' => [
                'MovieFavorite.user_id' => $this->Auth->user('id'),
            ]
        ];
        $favorites = $this->MovieFavorite->find('all', $conditions);
        $this->set(compact('favorites'));
    }

    /**
     * add method
     *
     * @return void
     */
    public function salvar(){

        $this->layout = false;
        $this->render(false);

        if ($this->request->is('post')) {

            if($this->data['movie_id'] == null){
                die(json_encode(array('status' => false)));
            }

            $user_id = $this->Auth->user('id');
            $movie_id = $this->data['movie_id'];

            $retorno = array();
            $this->MovieFavorite->create();
            $favorite = $this->MovieFavorite->find('first', array('conditions' => array('movie_id' => $movie_id, 'user_id' => $user_id)));
            
            if($favorite){
                $this->MovieFavorite->query("DELETE FROM movie_favorites where user_id = {$user_id} AND movie_id = {$movie_id}");
                $retorno = array('message' => 'delete', 'status' => true);
            }else{
                $save = array('id' => null, 'movie_id' => $movie_id, 'user_id' => $user_id);
                $this->MovieFavorite->set($save);
                if ($this->MovieFavorite->save()) {
                    $retorno = array('message' => 'insert', 'status' => true);
                }else{
                    $retorno = array('status' => false);
                }
            }

            die(json_encode($retorno));

        }

    }
}
