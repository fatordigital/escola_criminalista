<?php

App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
App::uses('CakeTime', 'Utility');

/**
 * Novidades Controller
 *
 * @property Novidade           $Novidade
 * @property Order              $Order
 * @property PaginatorComponent $Paginator
 */
class NovidadesController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array(
        'Session',
        'Paginator',
        'RequestHandler'
    );

    public $uses = array(
        'Novidade'
    );
	
	public $helpers = array('String');

    public function beforeRender() {
        parent::beforeRender();
    }

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index()
    {
        $novidades = array();
        $this->Novidade->recursive = 0;
        if ($this->request->is('ajax') && ($this->request->query('q'))) {
            $conditions = ['fields'=> ['Novidade.id', 'Novidade.title'], 'conditions' => ['Novidade.title LIKE' => '%' . $this->request->query('q') . '%']];
            $novidades = $this->Novidade->find('all', $conditions);
            $novidades = Hash::extract($novidades, '{n}.Novidade');
        } else {
            $novidades = $this->Paginator->paginate();
        }
        $this->set([
            'novidades' => $novidades,
            '_serialize' => ['novidades']
        ]);
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     *
     * @param string $id
     *
     * @return void
     */
    public function admin_view($id = null)
    {
        if (!$this->Novidade->exists($id)) {
            throw new NotFoundException(__d($this->cmsPluginName, 'Novidade não encontrada.'));
        }
        $options = array('conditions' => array('Novidade.' . $this->Novidade->primaryKey => $id));
        $novidade = $this->Novidade->find('first', $options);
        $this->set('novidade', $novidade);
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add()
    {
        if ($this->request->is('post')) {
			$this->Novidade->create();

			if ($this->Novidade->save($this->request->data)) {
				$this->Session->setFlash(__d($this->cmsPluginName, 'O registro foi salvo com sucesso.'), 'alert', array(
					'plugin' => 'BoostCake',
					'class'  => 'alert-success'
				));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__d($this->cmsPluginName, 'O registro não pode ser salvo. Tente novamente.'), 'alert', array(
					'plugin' => 'BoostCake',
					'class'  => 'alert-danger'
				));
			}
		}
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     *
     * @param string $id
     *
     * @return void
     */
    public function admin_edit($id = null)
    {
        if (!$this->Novidade->exists($id)) {
            throw new NotFoundException(__d($this->cmsPluginName, 'Registro não encontrado.'));
        }
        if ($this->request->is(array('post', 'put'))) {

            $data = $this->request->data;

            if ($this->Novidade->save($data)) {
                $this->Session->setFlash(__d($this->cmsPluginName, 'O registro foi atualizado com sucesso.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-success'
                ));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__d($this->cmsPluginName, 'O registro não pode ser atualizado. Verifique os campos em destaque, e tente novamente.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-danger'
                ));
            }
        } else {
            $options             = array('conditions' => array('Novidade.' . $this->Novidade->primaryKey => $id));
            $this->request->data = $this->Novidade->find('first', $options);
        }
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     *
     * @param string $id
     *
     * @return void
     */
    public function admin_delete($id = null)
    {
        $this->Novidade->id = $id;
        $this->Novidade->recursive = -1;

        $novidades = $this->Novidade->findById($id);
        
        if (!$this->Novidade->exists()) {
            throw new NotFoundException(__d($this->cmsPluginName, 'Registro não encontrado.'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Novidade->delete()) {
            $this->Session->setFlash(__d($this->cmsPluginName, 'O registro foi deletado com sucesso.'), 'alert', array(
                'plugin' => 'BoostCake',
                'class'  => 'alert-success'
            ));
        } else {
            $this->Session->setFlash(__d($this->cmsPluginName, 'O registro não pode ser deletado. Tente novamente.'), 'alert', array(
                'plugin' => 'BoostCake',
                'class'  => 'alert-danger'
            ));
        }

        return $this->redirect(array('action' => 'index'));
    }
}
