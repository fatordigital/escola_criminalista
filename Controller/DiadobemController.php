<?php

App::uses('AppController', 'Controller','File', 'Utility');
App::uses('CakeEmail', 'Network/Email');

/**
 * Contacts Controller
 *
 * @property Contact            $Contact
 * @property PaginatorComponent $Paginator
 */
class DiadobemController extends AppController
{
    public $uses = [
        'Diadobem',
    ];

    /**
     * Components
     *
     * @var array
     */
    public $components = array();

    /**
     * index method
     *
     * @return void
     */
    public function index()
    {        
        $this->set('body_class', 'interna soucontra171');
        $this->set("title_for_layout","Dia Do Bem");

        // busca eventos cadastrados
        $eventos = $this->Diadobem->find('all', array('conditions' => array('ativo' => 1)));
        $this->set('eventos', $eventos);
    }

    /**
     * add method
     *
     * @return void
     */
    public function add()
    {
        if ($this->request->is('post')):

			if($this->request->data['DiaDobem']['teste']== ""):

                if(isset($this->request->data['DiaDobem']['imagem']['name'])&&$this->request->data['DiaDobem']['imagem']['name']!=''):
                    $image = explode('.',$this->request->data['DiaDobem']['imagem']['name']);
                    $data = array(
                        'entidade' => $this->request->data['DiaDobem']['entidade'],
                        'cidade'   => $this->request->data['DiaDobem']['cidade'],
                        'descricao'=> $this->request->data['DiaDobem']['descricao'],
                        'horario'  => $this->request->data['DiaDobem']['horario'],
                        'contato'  => $this->request->data['DiaDobem']['contato'],
                        'imagem'   => md5($this->request->data['DiaDobem']['imagem']['name']).".".$image[1],
                        'dt_create'=> date('Y-m-d'),
                    );

                    $dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'files' .DS;
                      // enviada a imagen
                    $arquivo = new File($this->request->data['DiaDobem']['imagem']['tmp_name']);
                    $arquivo->copy($dir.$data['imagem']);
                    $arquivo->close();
                else:
                    $data = array(
                        'entidade' => $this->request->data['DiaDobem']['entidade'],
                        'cidade'   => $this->request->data['DiaDobem']['cidade'],
                        'descricao'=> $this->request->data['DiaDobem']['descricao'],
                        'horario'  => $this->request->data['DiaDobem']['horario'],
                        'contato'  => $this->request->data['DiaDobem']['contato'],
                        'dt_create'=> date('Y-m-d h:m:s'),
                    );

                endif; 

                $this->Diadobem->create();
                $this->Diadobem->set($data);                
                $validationErrors = $this->Diadobem->invalidFields();
                if($this->Diadobem->save()):

                    
                    $email = new CakeEmail('smtp');
                    $email->template('diadobem')
                        ->emailFormat('html')
                        ->viewVars(
                                array(
                                        'name' => $this->request->data['DiaDobem']['entidade'],
                                        'cidade' => $this->request->data['DiaDobem']['cidade'],
                                        'horario' => $this->request->data['DiaDobem']['horario'],
                                        'contato' => $this->request->data['DiaDobem']['contato'],
                                        'descricao' => $this->request->data['DiaDobem']['descricao']
                                    )
                            )
                        ->to('contato@casadatolerancia.com.br') 
                        ->bcc('rosalba.monteiro@fatordigital.com.br')                       
                        ->subject('Contato pelo formulario Dia do Bem')
                        ->send();


                    $this->Session->setFlash(__('O seu contato foi enviado com sucesso.'), 'alert', array(
                        'plugin' => 'BoostCake',
                        'class'  => 'alert-success'
                    ));
                    $this->request->data = array();
                else:
                    $this->Session->setFlash(__('Não foi possível salvar a sua mensagem. Tente novamente ou entre em contato por outros meios.'), 'alert', array(
                        'plugin' => 'BoostCake',
                        'class'  => 'alert-danger'
                    ));
                endif;

            endif;

        endif;

        $this->redirect(['action' => 'index']);
        $this->set("title_for_layout","Dia Do Bem");

    }

   
}
