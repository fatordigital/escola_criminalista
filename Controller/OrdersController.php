<?php

App::uses('AppController', 'Controller');
App::uses('Codes', 'Plugin/PagSeguro/Vendor');
App::uses('CakeTime', 'Utility');
App::uses('CakeEmail', 'Network/Email');

/**
 * Orders Controller
 *
 * @property Order $Order
 * @property PaginatorComponent     $Paginator
 * @property CarrinhoComponent      $Carrinho
 * @property NotificacaoComponent   $Notificacao
 */
class OrdersController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array(
        'Paginator',
        'RequestHandler',
        'PagSeguro.Carrinho',
        'PagSeguro.Notificacao',
        'Auth'
    );

    public $uses = [
        'Order',
        'Users.User',
        'Movie',
    ];

     public function beforeFilter() {
        parent::beforeFilter();
        //$this->Auth->deny('buy');
    }

    public function buy($movie_id = null, $params_assintatura = 'false', $off = 'false', $movie_price = 'false')
    {
		$movie_id_tmp = $movie_id;
        if ($movie_id === null) {
            $this->Session->setFlash(__d('users', "Parâmetros inválidos."), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-warning'
                ));
            $this->redirect('/');
            return;
        }

        if (!$this->Auth->loggedIn()) {
            $this->Session->write('Compra.Movie.id', $movie_id);
            $this->Session->write('Compra.Movie.movie_price', $movie_price);
            $this->redirect(array('plugin' => false, 'controller' => 'users', 'action' => 'login'));
            //$this->redirect($this->Auth->redirectUrl());
            return;
        }

        //se for assinatura, busca o primeiro produto que encontrar, para ter um vinculo com apneas um video
        if($movie_id == "assinaturaictd"){
            $assin_movie = $this->Movie->find('first', array('recursive' => -1, 'fields' => array('Movie.id'), 'order' => array('Movie.id' => 'ASC'), 'conditions' => array('AND' => array('Movie.price >' => 0, 'Movie.type <>' => 'free'))));
            if($assin_movie){
                $movie_id = $assin_movie['Movie']['id']; 
            }
        }

        $movie = $this->Movie->findById($movie_id);

        if ($this->request->is(array('post', 'put')) && isset($this->request->data['Order']['price'])) {
            $this->request->data['Order']['price'] = str_replace("R$ ", "", $this->request->data['Order']['price']);
            // print(str_replace(",", ".", $this->request->data['Order']['price']));
            // print('<br />');
            // print($this->request->data['Order']['price']);
            // print('<br />');
            // print($movie['Movie']['price']);
            // print('<br />');

            // var_dump($this->request->data['Order']['price']);
            // var_dump($movie['Movie']['price']);
            // var_dump(str_replace(",", ".", str_replace(".", "", $this->request->data['Order']['price'])));
            // var_dump(str_replace(",", ".", $movie['Movie']['price']));
            // die;



            // var_dump(str_replace(",", ".", str_replace(".", "", $this->request->data['Order']['price'])) < str_replace(",", ".", $movie['Movie']['price']));
            // die;

            //number_format($valor, $prec, ",", ".")

            $this->request->data['Order']['price'] = str_replace(",", ".", str_replace(".", "", $this->request->data['Order']['price']));
            // var_dump($this->request->data['Order']);
            //var_dump($teste);
            //var_dump(number_format($teste, 2, ",", "."));
            // die;

            if($this->request->data['Order']['price'] == "" || str_replace(",", ".", str_replace(".", "", $this->request->data['Order']['price'])) < str_replace(",", ".", $movie['Movie']['price'])){
                $this->Session->setFlash(__d('users', "Você deve inserir um valor maior que o valor mínimo do vídeo."), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-warning'
                ));
                $this->redirect(array('plugin' => false, 'controller' => 'movies', 'action' => 'view', $movie['Movie']['slug']));
            }else{
                $movie['Movie']['price'] = str_replace(",", ".", $this->request->data['Order']['price']);
            }
        }

        if( $movie['Movie']['price'] <= 0 && $movie_id_tmp != "assinaturaictd" ){
             $this->Session->setFlash(__d('users', "Parâmetros inválidos."), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-warning'
                ));
             $this->redirect('/');
            return;
        }
        if($movie_price != 'false' && count($movie['MoviePrice']) > 0){
            foreach ($movie['MoviePrice'] as $key => $price) {
                if($price['id'] == $movie_price){
                    $movie['Movie']['price'] = str_replace(",", ".", $price['price']);
                }
            }
        }

        $this->User->recursive = 2;
        $user = $this->User->findByEmail($this->Auth->user('email'));
        //var_dump($this->Auth->user());

        //var_dump($user);die;

        if( $user['User']['phone'] == "" || $user['User']['phone'] == "(00) 0000-0000" ){
            $this->Session->write('Compra.Movie.id', $movie_id);
            $this->Session->setFlash(__d('users', "Seu número de telefone é inválido. Preencha novamente."), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-warning'
                ));
            $this->redirect(array('plugin' => false, 'controller' => 'users', 'action' => 'index'));
        }

        if (!$movie) {
            throw new NotFoundException(__('Movie not found.'));
        }
		
		if($movie['Movie']['type'] == "donation"){
            //clique solidario
            $this->Carrinho->setCredenciais('contatofaesp@gmail.com', '75C4CCF9AE014DECB3919208D1CE058B');
               $this->Carrinho->setUrlRetorno(Router::url(['plugin' => false, 'controller' => 'orders', 'action' => 'payment_return_donation'], true));
        }elseif($movie['Movie']['type'] == "paid"){
            //pago
            $this->Carrinho->setCredenciais('paula@casadatolerancia.com.br', '16FB106ED0E24F4BA9B432CF80832621');
               $this->Carrinho->setUrlRetorno(Router::url(['plugin' => false, 'controller' => 'orders', 'action' => 'payment_return_paid'], true));
        }else{
             throw new NotFoundException(__('Tipo desconhecido.'));
        }

        $this->Carrinho->setQuantidadeUso(1);


      //  $this->Carrinho->setUrlRetorno(Router::url(['plugin' => false, 'controller' => 'orders', 'action' => 'payment_return'], true));
		if($movie_id_tmp == "assinaturaictd"){
             $movie['Movie']['id'] = "assinaturaictd";
         }

        if($user['User']['subscriber'] == true && $movie['Movie']['controller'] == "courses" && $movie['Movie']['desconto'] > 0){
            $movie['Movie']['price'] -= $movie['Movie']['desconto'];
        }

        if($movie['Movie']['controller'] == "courses" && $movie['Movie']['off'] > 0 && $off == 'true'){
            $movie['Movie']['price'] -= $movie['Movie']['price']*($movie['Movie']['off']/100);
        }

        $this->Carrinho->adicionarItem(
            $movie['Movie']['id'],
            utf8_decode($movie['Movie']['title']),
            $movie['Movie']['price'],
            0 // peso
        );

        $data = [
                    'user_id' => $user['User']['id'],
                    'total' => $movie['Movie']['price'],
                    'model' => 'Movie',
                    'payment_method_type' => 0,
                    'status' => 1,
                    'foreign_id' => $movie['Movie']['id'],
                    'created' => CakeTime::toServer(null),
                    'updated' => CakeTime::toServer(null),
                    'paid' => CakeTime::toServer(null),
                ];

        $telefone   = preg_replace("/[^0-9]/", "", $user['User']['phone']);
        $tel_ddd    = substr($telefone, 0, 2);
        $tel_numero = substr($telefone, 2, strlen($telefone));

        if($tel_ddd < 11 || strlen($tel_numero) < 8){
            $this->Session->write('Compra.Movie.id', $movie_id);
            $this->Session->setFlash(__d('users', "Seu número de telefone é inválido. Preencha novamente."), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-warning'
                ));
            $this->redirect(array('plugin' => false, 'controller' => 'users', 'action' => 'index'));
        }

        if($this->Order->save($data, false)){

            $this->Carrinho->setReferencia("{$this->Order->id}-{$user['User']['id']}-Movie-{$movie['Movie']['id']}");

            // definindo o contato do comprador
            $this->Carrinho->setContatosComprador(
                $user['User']['first_name'],
                $user['User']['email'],
                $tel_ddd,
                $tel_numero
            );

        $this->Carrinho->setTipoFrete(null);
        $this->Carrinho->setValorTotalFrete('0.00');

        // e finalmente se os dados estivere corretos, redirecionando ao Pagseguro
        //tratamaneto para verificar se a compra é recorrente ou não (sim, gamba)
        if($params_assintatura === 'true'){
            if ($result = $this->Carrinho->finalizaCompra(true)) {
                $this->redirect($result);
            }
        }else{
            if ($result = $this->Carrinho->finalizaCompra(false)) {
                $this->redirect($result);
            }
            }
        }

        die;

    }

    public function payment_return_donation()
    {

        $idTransacao = $this->params['url']['transaction_id'];

        CakeLog::info(var_export($this->request, true));

        $this->payment_return($idTransacao, 'donation');

        die;

    }

    public function payment_return_paid()
    {   

        $idTransacao = $this->params['url']['transaction_id'];

        CakeLog::info(var_export($this->request, true));

        $this->payment_return($idTransacao, 'paid');

        die;

    }

    private function payment_return($idTransacao, $type){
        //$this->Carrinho->setCredenciais('cleberalves.info@gmail.com', 'D22944748BD046BFAA64AD79A82CF16D');

        if($type == 'donation'){
            //clique solidario
            $this->Carrinho->setCredenciais('contatofaesp@gmail.com', '75C4CCF9AE014DECB3919208D1CE058B');    
        }elseif($type == 'paid'){
            //pago
            $this->Carrinho->setCredenciais('paula@casadatolerancia.com.br', '16FB106ED0E24F4BA9B432CF80832621');
        }
        

        if ($this->Carrinho->obterInformacoesTransacao($idTransacao)) {
            $dadosUsuario = $this->Carrinho->obterDadosUsuario();
            //debug($dadosUsuario);

            $statusTransacao = $this->Carrinho->obterStatusTransacao();
            //debug($statusTransacao);

            $dadosPagamento = $this->Carrinho->obterDadosPagamento();
            //debug($dadosPagamento);

            $dataTransacao = $this->Carrinho->obterDataTransacao();
            //debug($dataTransacao);

            $valores = $this->Carrinho->obterValores();
            //debug($valores);

            CakeLog::write('payment_return', var_export($statusTransacao, true));
            CakeLog::write('payment_return', var_export($dadosPagamento, true));
            CakeLog::write('payment_return', var_export($dataTransacao, true));
            CakeLog::write('payment_return', var_export($valores, true));

            $referencia = $valores['referencia'];
            list($pedido_id, $user_id, $model, $foreign_id) = explode('-', $referencia);

            $email = new CakeEmail('smtp');
            $email->template('assinatura')
                ->emailFormat('html')
                ->viewVars(
                        array(
                                'statusTransacao' => var_export($statusTransacao, true),
                                'dadosPagamento' => var_export($dadosPagamento, true),
                                'obterValores' => var_export($this->Carrinho->obterValores(), true)
                            )
                    )
                ->to('cleberalves.info@gmail.com')
                ->bcc('roberto@fatordigital.com.br')
                ->subject('Debug Assinatura: ' . $user_id)
                ->send();
            if($statusTransacao['id'] == 4){
                $statusTransacao['id'] = 3;
            }

            if (!$this->Order->findByTransactionId($idTransacao)) {
                $this->Order->create();
                $this->Order->id = $pedido_id;
                $data = [
                    'user_id' => $user_id,
                    'transaction_id' => $idTransacao,
                    'payment_method_type' => $dadosPagamento['tipo_id'],
                    'reference' => $referencia,
                    'total' => $valores['valorTotal'],
                    'taxes' => $valores['valorTaxa'],
                    'status' => $statusTransacao['id'],
                    'model' => $model,
                    'foreign_id' => $foreign_id,
                ];

                if ($statusTransacao['id'] == 3 || $statusTransacao['id'] == 4) {
                    $data['paid'] = CakeTime::toServer(null);

                    //GAMBA -
                    //Se o codigo do produto for igual a 'assinaturaictd', a transação é do tipo 'ASSINATURA'
                    //Então atualizo a flag do usuario, que descrima o assinante
                    $eh_assintura = false;
                    if(isset($valores['produtos'][0]) && $valores['produtos'][0]['id'] == "assinaturaictd"){
                        $eh_assintura = true;
                        if ($statusTransacao['id'] == 3 || $statusTransacao['id'] == 4) {
                            $upd_user['User']['id']         = $user_id;
                            $upd_user['User']['subscriber'] = true;
                        }else{
                            $upd_user['User']['id']         = $user_id;
                            $upd_user['User']['subscriber'] = false;
                        }
                        $this->User->save($upd_user, false);
                    }
                }

                $this->Order->set($data);

                if ($this->Order->save()) {

                    //$movie = $this->Movie->find('first', array('conditions' => array('Movie.id' => $foreign_id)));
                    
                    if($statusTransacao['id'] == 1){
                        $this->Session->setFlash('O status do seu pedido encontra-se como "Aguardando Pagamento".', 'alert', array(
                            'plugin' => 'BoostCake',
                            'class' => 'alert-success'
                        ));
                    }elseif($statusTransacao['id'] == 2){
                        $this->Session->setFlash('A aprovação do seu pedido está em processo de análise da instituição financeira.', 'alert', array(
                            'plugin' => 'BoostCake',
                            'class' => 'alert-success'
                        ));
                    }elseif($statusTransacao['id'] == 3 || $statusTransacao['id'] == 4){
                        $this->Session->setFlash('Seu pedido foi registrado com sucesso.', 'alert', array(
                            'plugin' => 'BoostCake',
                            'class' => 'alert-success'
                        ));
                    }elseif($statusTransacao['id'] == 7){
                        $this->Session->setFlash('Houve um problema com o seu pedido, e o mesmo não pode ser efetuado. Tente Novamente.', 'alert', array(
                            'plugin' => 'BoostCake',
                            'class' => 'alert-danger'
                        ));
                    }

                    if($eh_assintura){
                        if ($statusTransacao['id'] == 3 || $statusTransacao['id'] == 4) {
                            $this->redirect(Router::url(['plugin' => 'users', 'controller' => 'users', 'action' => 'confirmacao_assinatura'], true));
                        }else{
                            $this->redirect(Router::url(['plugin' => 'users', 'controller' => 'users', 'action' => 'confirmacao_assinatura'], true));
                        }
                    }else{
                        $this->redirect(Router::url(['plugin' => 'users', 'controller' => 'users', 'action' => 'confirmacao_compra'], true));
                    }

                    // if($movie){
                    //     if($movie['Movie']['controller'] == 'lives'){
                    //         $this->redirect(Router::url(['plugin' => false, 'controller' => 'Lives', 'action' => 'mymovies'], true));
                    //     }
                    //     $this->redirect(Router::url(['plugin' => false, 'controller' => 'Movies', 'action' => 'mymovies'], true));
                    // }else{
                    //     $this->redirect(Router::url(['plugin' => false, 'controller' => 'Movies', 'action' => 'mymovies'], true));
                    // }

                } else {
                    CakeLog::error($data);
                    $this->Session->setFlash('Houve um problema com seu pedido', 'alert', array(
                        'plugin' => 'BoostCake',
                        'class' => 'alert-danger'
                    ));
                    $this->redirect(Router::url(['plugin' => 'Users', 'controller' => 'Users', 'action' => 'index'], true));
                }

            };


        }

        die;
    }

    public function listener_donation()
    {
        $tipo = $this->request->data['notificationType'];
        $codigo = $this->request->data['notificationCode'];

        CakeLog::info(var_export($this->request, true));
        CakeLog::write('listener', var_export($this->request, true));

        $this->Notificacao->setCredenciais('contatofaesp@gmail.com', '75C4CCF9AE014DECB3919208D1CE058B');    

        if ( $this->Notificacao->obterDadosTransacao($tipo, $codigo) ) {

            // // retorna o status da transação
            // $statusTransacao = $this->Notificacao->obterStatusTransacao();

            // // retorna o id da transação para consulta e/ou atualização
            // $codigoTransacao = $this->Notificacao->obterCodigoTransacao();

            // $this->Order->recursive = -1;
            // $ordem = $this->Order->findByTransactionId($codigoTransacao);

            // if ($ordem) {

            //     // if ($statusTransacao['id'] == 3 || $statusTransacao['id'] == 4) {

            //         $data = [
            //             'id' => $ordem['Order']['id'],
            //             'status' => $statusTransacao['id'],
            //             'paid' => CakeTime::toServer(null),
            //         ];

            //         $this->Order->set($data);

            //         if (!$this->Order->save()) {
            //             CakeLog::error($data);
            //         }
            //     // }
            // }


            $status["Aguardando pagamento"] = 1;
            $status["Em análise"]           = 2;
            $status["Paga"]                 = 3;
            $status["Disponível"]           = 4;
            $status["Em disputa"]           = 5;
            $status["Devolvida"]            = 6;
            $status["Cancelada"]            = 7;
        
            CakeLog::write('listener', "=== obterDadosTransacao ==");
            CakeLog::write('listener', var_export($this->Notificacao->obterDadosTransacao($tipo, $codigo), true));

            // retorna o status da transação
            $statusTransacao = $this->Notificacao->obterStatusTransacao();          
            CakeLog::write('listener', "=== statusTransacao ==");
            CakeLog::write('listener', $status[$statusTransacao]);
            $statusTransacao['id'] = $status[$statusTransacao];

            $referencia = $this->Notificacao->obterReferencia();            
            list($pedido_id, $user_id, $model, $foreign_id) = explode('-', $referencia);
            $this->Order->recursive = -1;
            $ordem = $this->Order->findById($pedido_id);
            
            CakeLog::write('listener', "=== ordem ==");
            CakeLog::write('listener', var_export($ordem, true));

            $valores = $this->Notificacao->obterValores();
            $transaction_id = $this->Notificacao->obterCodigoTransacao();

            CakeLog::write('listener', "=== valores ==");
            CakeLog::write('listener', var_export($valores, true));

            CakeLog::write('listener', "=== transaction_id ==");
            CakeLog::write('listener', var_export($transaction_id, true));

            $dadosPagamento = $this->Notificacao->obterDadosPagamento();
            CakeLog::write('listener', "=== dadosPagamento ==");
            CakeLog::write('listener', var_export($dadosPagamento, true));

            if ($ordem) {

                if($statusTransacao['id'] == 4){
                    $statusTransacao['id'] = 3;
                }
                //if ($statusTransacao['id'] == 3 || $statusTransacao['id'] == 4) {

                    $data = [
                        'id' => $ordem['Order']['id'],
                        'status' => $statusTransacao['id'],
                        'reference' => $referencia,
                        'total' => $valores['valorTotal'],
                        'taxes' => $valores['valorTaxa'],
                        'transaction_id' => $transaction_id,
                        'payment_method_type' => $dadosPagamento['tipo_id'],
                        'paid' => CakeTime::toServer(null),
                    ];

                    CakeLog::write('listener', "=== data ==");
                    CakeLog::write('listener', var_export($data, true));

                    $this->Order->set($data);

                    if (!$this->Order->save()) {
                        CakeLog::error($data);
                    }
                    if(isset($valores['produtos'][0]) && $valores['produtos'][0]['id'] == "assinaturaictd"){
                        if ($statusTransacao['id'] == 3 || $statusTransacao['id'] == 4) {
                            $upd_user['User']['id']         = $user_id;
                            $upd_user['User']['subscriber'] = true;
                        }else{
                            $upd_user['User']['id']         = $user_id;
                            $upd_user['User']['subscriber'] = false;
                        }
                        $this->User->save($upd_user, false);
                    }
                //}
            }
        }

        die;
    }

    public function listener_paid()
    {
        $tipo = $this->request->data['notificationType'];
        $codigo = $this->request->data['notificationCode'];

        CakeLog::info(var_export($this->request, true));
        CakeLog::write('listener', var_export($this->request, true));

        $this->Notificacao->setCredenciais('paula@casadatolerancia.com.br', '16FB106ED0E24F4BA9B432CF80832621');

        if ( $this->Notificacao->obterDadosTransacao($tipo, $codigo) ) {

            // // retorna o status da transação
            // $statusTransacao = $this->Notificacao->obterStatusTransacao();

            // // retorna o id da transação para consulta e/ou atualização
            // $codigoTransacao = $this->Notificacao->obterCodigoTransacao();

            // $this->Order->recursive = -1;
            // $ordem = $this->Order->findByTransactionId($codigoTransacao);

            // if ($ordem) {

            //     // if ($statusTransacao['id'] == 3 || $statusTransacao['id'] == 4) {

            //         $data = [
            //             'id' => $ordem['Order']['id'],
            //             'status' => $statusTransacao['id'],
            //             'paid' => CakeTime::toServer(null),
            //         ];

            //         $this->Order->set($data);

            //         if (!$this->Order->save()) {
            //             CakeLog::error($data);
            //         }
            //     // }
            // }

            $status["Aguardando pagamento"] = 1;
            $status["Em análise"]           = 2;
            $status["Paga"]                 = 3;
            $status["Disponível"]           = 4;
            $status["Em disputa"]           = 5;
            $status["Devolvida"]            = 6;
            $status["Cancelada"]            = 7;
        
            CakeLog::write('listener', "=== obterDadosTransacao ==");
            CakeLog::write('listener', var_export($this->Notificacao->obterDadosTransacao($tipo, $codigo), true));

            // retorna o status da transação
            $statusTransacao = $this->Notificacao->obterStatusTransacao();          
            CakeLog::write('listener', "=== statusTransacao ==");
            CakeLog::write('listener', $status[$statusTransacao]);
            $statusTransacao['id'] = $status[$statusTransacao];

            $referencia = $this->Notificacao->obterReferencia();            
            list($pedido_id, $user_id, $model, $foreign_id) = explode('-', $referencia);
            $this->Order->recursive = -1;
            $ordem = $this->Order->findById($pedido_id);
            
            CakeLog::write('listener', "=== ordem ==");
            CakeLog::write('listener', var_export($ordem, true));

            $valores = $this->Notificacao->obterValores();
            $transaction_id = $this->Notificacao->obterCodigoTransacao();

            CakeLog::write('listener', "=== valores ==");
            CakeLog::write('listener', var_export($valores, true));

            CakeLog::write('listener', "=== transaction_id ==");
            CakeLog::write('listener', var_export($transaction_id, true));

            $dadosPagamento = $this->Notificacao->obterDadosPagamento();
            CakeLog::write('listener', "=== dadosPagamento ==");
            CakeLog::write('listener', var_export($dadosPagamento, true));

            if ($ordem) {

                if($statusTransacao['id'] == 4){
                    $statusTransacao['id'] = 3;
                }
                // if ($statusTransacao['id'] == 3 || $statusTransacao['id'] == 4) {

                    $data = [
                        'id' => $ordem['Order']['id'],
                        'status' => $statusTransacao['id'],
                         'reference' => $referencia,
                        'total' => $valores['valorTotal'],
                        'taxes' => $valores['valorTaxa'],
                        'transaction_id' => $transaction_id,
                        'payment_method_type' => $dadosPagamento['tipo_id'],
                        'paid' => CakeTime::toServer(null),
                    ];

                    CakeLog::write('listener', "=== data ==");
                    CakeLog::write('listener', var_export($data, true));

                    $this->Order->set($data);

                    if (!$this->Order->save()) {
                        CakeLog::error($data);
                    }
                    if(isset($valores['produtos'][0]) && $valores['produtos'][0]['id'] == "assinaturaictd"){
                        if ($statusTransacao['id'] == 3 || $statusTransacao['id'] == 4) {
                            $upd_user['User']['id']         = $user_id;
                            $upd_user['User']['subscriber'] = true;
                        }else{
                            $upd_user['User']['id']         = $user_id;
                            $upd_user['User']['subscriber'] = false;
                        }
                        $this->User->save($upd_user, false);
                    }
                // }
            }
        }

        die;
    }


    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index()
    {
        $this->Order->recursive  = 2;
        $orders = $this->Paginator->paginate(array('transaction_id <>' => null));
        $this->set(compact('orders'));

        $status = [
            '1' => 'Aguardando Pagamento',
            '2' => 'Em Análise',
            '3' => 'Paga',
            '4' => 'Disponível',
            '5' => 'Em Disputa',
            '6' => 'Devolvida',
            '7' => 'Cancelada',
        ];

        $this->set(compact('status'));
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     *
     * @param string $id
     *
     * @return void
     */
    public function admin_view($id = null)
    {
        $this->Order->id = $id;
        if (!$this->Order->exists()) {
            throw new NotFoundException(__d('admin', 'Invalid cms user'), 'alert', array(
                'plugin' => 'BoostCake',
                'class'  => 'alert-warning'
            ));
        }
        $this->set('order', $this->Order->read(null, $id));

        $status = [
            '1' => 'Aguardando Pagamento',
            '2' => 'Em Análise',
            '3' => 'Paga',
            '4' => 'Disponível',
            '5' => 'Em Disputa',
            '6' => 'Devolvida',
            '7' => 'Cancelada',
        ];

        $paymentMethod = [
            '0' => 'Desconhecido',
            '1' => 'Cartão de Crédito',
            '2' => 'Boleto',
            '3' => 'Débito online (TEF)',
            '4' => 'Saldo PagSeguro',
            '5' => 'Oi Paggo',
            '7' => 'Depósito em conta',
        ];

        $this->set(compact('status', 'paymentMethod'));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     *
     * @param string $id
     *
     * @return void
     */
    public function admin_edit($id = null)
    {
        $this->Order->id = $id;
        if (!$this->Order->exists()) {
            throw new NotFoundException(__d('admin', 'System User not found'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {

            $cmsUser = $this->request->data;
            if (empty($cmsUser['Order']['password'])) {
                unset($cmsUser['Order']['password']);
            }

            if ($this->Order->save($cmsUser)) {
                $this->Session->setFlash(__d('admin', 'The System User has been saved'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-success'
                ));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__d('admin', 'The System User could not be saved. Please, try again.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-danger'
                ));
            }
        } else {
            $this->request->data = $this->Order->read(null, $id);
            unset($this->request->data['Order']['password']);

        }
    }

    /**
     * admin_delete method
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     *
     * @param string $id
     *
     * @return void
     */
    public function admin_delete($id = null)
    {
        $this->request->onlyAllow('post');

        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Order->id = $id;
        if (!$this->Order->exists()) {
            throw new NotFoundException(__d('admin', 'System User not found'));
        }
        if ($this->Order->delete()) {
            $this->Session->setFlash(__d('admin', 'System User deleted'), 'alert', array(
                'plugin' => 'BoostCake',
                'class'  => 'alert-success'
            ));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__d('admin', 'System User was not deleted'), 'alert', array(
            'plugin' => 'BoostCake',
            'class'  => 'alert-danger'
        ));
        $this->redirect(array('action' => 'index'));
    }

    public function inscreva($movie_id = null, $movie_price = 'false')
    {
        $movie_id_tmp = $movie_id;
        if ($movie_id === null) {
            $this->Session->setFlash(__d('users', "Parâmetros inválidos."), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-warning'
                ));
            $this->redirect('/');
            return;
        }

        if (!$this->Auth->loggedIn()) {
            $this->Session->write('Compra.Movie.id', $movie_id);
            $this->Session->write('Compra.Movie.movie_price', $movie_price);
            $this->Session->write('Compra.Movie.inscreva', true);
            $this->redirect(array('plugin' => false, 'controller' => 'users', 'action' => 'login'));
            //$this->redirect($this->Auth->redirectUrl());
            return;
        }

        $movie = $this->Movie->findById($movie_id);

        if ($movie['Movie']['price'] > 0) {
            $this->Session->setFlash(__d('users', "Parâmetros inválidos."), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-warning'
                ));
            $this->redirect('/');
            return;
        }

        $this->User->recursive = 2;
        $user = $this->User->findByEmail($this->Auth->user('email'));
        //var_dump($this->Auth->user());

        //var_dump($user);die;

        if (!$movie) {
            throw new NotFoundException(__('Movie not found.'));
        }
        
        $this->Carrinho->setQuantidadeUso(1);



        $data = [
                    'user_id' => $user['User']['id'],
                    'total' => $movie['Movie']['price'],
                    'model' => 'Movie',
                    'payment_method_type' => 0,
                    'status' => 3,
                    'foreign_id' => $movie['Movie']['id'],
                    'transaction_id' => 'INSCRIÇÃO',
                    'reference' => 'INSCRIÇÃO',
                    'created' => CakeTime::toServer(null),
                    'updated' => CakeTime::toServer(null),
                    'paid' => CakeTime::toServer(null),
                ];

        if($this->Order->save($data, false)){
            $this->Session->setFlash(__d('admin', 'Inscrição concluída com sucesso.'), 'alert', array(
                'plugin' => 'BoostCake',
                'class'  => 'alert-success'
            ));
            $this->redirect('/cursos/modulos/' . $movie['Movie']['slug']);
            die();
        }else{
            $this->Session->setFlash(__d('users', "Ocorreu um problema na hora da Inscrição. Tente novamente."), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-warning'
                ));
            $this->redirect('/cursos/modulos/' . $movie['Movie']['slug']);
            die();
        }

        die;

    }
}
