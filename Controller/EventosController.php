<?php

App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
App::uses('CakeTime', 'Utility');

/**
 * Orders Controller
 *
 * @property Order $Order
 * @property PaginatorComponent     $Paginator
 * @property CarrinhoComponent      $Carrinho
 * @property NotificacaoComponent   $Notificacao
 */
class EventosController extends AppController
{
    /**
     * Components
     *
     * @var array
     */
    public $components = array(
        'Paginator',
        'RequestHandler',
    );

    public $helpers = array('String');

    public $uses = [
        'Diadobem',
        'Order'
    ];

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index()
    {
        $eventos = $this->Paginator->paginate(array('id <>' => null,));
        $this->set(compact('eventos'));
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     *
     * @param string $id
     *
     * @return void
     */
    public function admin_view($id = null)
    {
        $this->Diadobem->id = $id;
        if (!$this->Diadobem->exists()) {
            throw new NotFoundException(__d('admin', 'Invalid cms user'), 'alert', array(
                'plugin' => 'BoostCake',
                'class'  => 'alert-warning'
            ));
        }
        $this->set('evento', $this->Diadobem->read(null, $id));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     *
     * @param string $id
     *
     * @return void
     */
    public function admin_edit($id = null)
    {
        if (!$this->Diadobem->exists($id)) {
            throw new NotFoundException(__d($this->cmsPluginName, 'Registro não encontrado.'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $data = $this->request->data;
            if ($this->Diadobem->save($data)) {
                $this->Session->setFlash(__d($this->cmsPluginName, 'O registro foi atualizado com sucesso.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-success'
                ));

                // se troca de imagem
                if($this->request->data['Diadobem']['image']['name']):

                    $options = array('conditions' => array('Diadobem.' . $this->Diadobem->primaryKey => $id));
                    $this->request->mydata = $this->Diadobem->find('first', $options);                    

                    if(($this->request->mydata['Diadobem']['imagem']!="")&&($this->request->mydata['Diadobem']['imagem']!=NULL)):
                        $evento = $this->request->mydata['Diadobem']['imagem'];
                    else:
                        $image = explode('.',$this->request->data['Diadobem']['image']['name']);
                        $evento = md5($this->request->data['DiaDobem']['imagem']['name']).".".$image[1];
                        $data2 = array(
                            'imagem'   => $evento,
                        );
                        $this->Diadobem->save($data2);
                    endif;

                    //$path = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'upload';
                    $dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'files' .DS;

                    // enviada a imagen
                    $arquivo = new File($this->request->data['Diadobem']['image']['tmp_name']);
                    $arquivo->copy($dir.$evento);
                    $arquivo->close();                    

                else:
                    $options = array('conditions' => array('Diadobem.' . $this->Diadobem->primaryKey => $id));
                    $this->request->mydata = $this->Diadobem->find('first', $options);
                    $evento = $this->request->mydata['Diadobem']['imagem'];                    
                endif;


                $this->set('evento', $evento);               

                //return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__d($this->cmsPluginName, 'O registro não pode ser atualizado. Verifique os campos em destaque, e tente novamente.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-danger'
                ));
            }
        } else {
            $options = array('conditions' => array('Diadobem.' . $this->Diadobem->primaryKey => $id));
            $this->request->data = $this->Diadobem->find('first', $options);
            $evento = $this->request->data['Diadobem']['imagem'];
            $this->set('evento', $evento);
        }
    }

    /**
     * admin_delete method
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     *
     * @param string $id
     *
     * @return void
     */
    public function admin_delete($id = null)
    {
        $this->request->onlyAllow('post');

        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Diadobem->id = $id;
        if (!$this->Diadobem->exists()) {
            throw new NotFoundException(__d('admin', 'System Event not found'));
        }
        if ($this->Diadobem->delete()) {
            $this->Session->setFlash(__d('admin', 'System Event deleted'), 'alert', array(
                'plugin' => 'BoostCake',
                'class'  => 'alert-success'
            ));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__d('admin', 'System Event was not deleted'), 'alert', array(
            'plugin' => 'BoostCake',
            'class'  => 'alert-danger'
        ));
        $this->redirect(array('action' => 'index'));
    }   

    public function admin_ativar(){

        if ($this->request->is('post')):
            if($this->request->data['Diadobem']['status']==0):
                $id = $this->request->data['Diadobem']['id'];
                $this->Diadobem->updateAll(
                    array('ativo' => 1),
                    array('id' => $id)
                );
            else:
                $id = $this->request->data['Diadobem']['id'];
                $this->Diadobem->updateAll(
                    array('ativo' => 0),
                    array('id' => $id)
                );
            endif;
            $this->redirect(array('action' => 'admin_index'));
        endif;


    }

}
