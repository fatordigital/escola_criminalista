<?php

App::uses('AppController', 'Controller');

/**
 * Categories Controller
 *
 * @property Category           $Category
 * @property PaginatorComponent $Paginator
 */
class CategoriesController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array(
        'Session',
        'Paginator',
        'RequestHandler'
    );

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index()
    {
        // $this->Category->recursive = 0;
        $this->set('categories', $this->Paginator->paginate());
        $categories = $this->Category->find('all');
        $this->set('categories', $categories);
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     *
     * @param string $id
     *
     * @return void
     */
    public function admin_view($id = null)
    {
        if (!$this->Category->exists($id)) {
            throw new NotFoundException(__d('admin', 'Category not found.'));
        }
        $options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
        $this->set('category', $this->Category->find('first', $options));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add()
    {
        if ($this->request->is('post')) {
            $this->Category->create();
            if ($this->Category->save($this->request->data)) {
                $this->Session->setFlash(__d('admin', 'The category has been saved successfully.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-success'
                ));

                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__d('admin', 'The category could not be saved. Please, try again.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-danger'
                ));
            }
        }
        $parentCategories = $this->Category->ParentCategory->find('list');
        $this->set(compact('parentCategories'));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     *
     * @param string $id
     *
     * @return void
     */
    public function admin_edit($id = null)
    {
        if (!$this->Category->exists($id)) {
            throw new NotFoundException(__d('admin', 'Category not found.'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Category->save($this->request->data)) {
                $this->Session->setFlash(__d('admin', 'The category has been updated successfully.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-success'
                ));

                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__d('admin', 'The category could not be updated. Please, correct any errors and try again.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-danger'
                ));
            }
        } else {
            $options             = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
            $this->request->data = $this->Category->find('first', $options);
        }
        $parentCategories = $this->Category->ParentCategory->find('list');
        $this->set(compact('parentCategories'));
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     *
     * @param string $id
     *
     * @return void
     */
    public function admin_delete($id = null)
    {
        $this->Category->id = $id;
        if (!$this->Category->exists()) {
            throw new NotFoundException(__d('admin', 'Category not found.'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Category->delete()) {
            $this->Session->setFlash(__d('admin', 'The category has been deleted successfully.'), 'alert', array(
                'plugin' => 'BoostCake',
                'class'  => 'alert-success'
            ));
        } else {
            $this->Session->setFlash(__d('admin', 'The category could not be deleted. Please, try again.'), 'alert', array(
                'plugin' => 'BoostCake',
                'class'  => 'alert-danger'
            ));
        }

        return $this->redirect(array('action' => 'index'));
    }
}
