<?php

App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
App::uses('CakeTime', 'Utility');

/**
 * Lives Controller
 *
 * @property Live              $Live
 * @property Order              $Order
 * @property PaginatorComponent $Paginator
 * @property CarrinhoComponent  $Carrinho
 */
class LivesController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array(
        'Session',
        'Paginator',
        'RequestHandler',
		'Auth'
    );

    public $uses = array(
        'Movie',
        'Users.User',
        'Order'
    );

    public function beforeRender() {
        parent::beforeRender();
        $this->set($this->Movie->enumValues());		
    }
	
	 public function beforeFilter() {
        parent::beforeFilter();
        $this->set($this->Movie->enumValues());
        $this->Auth->deny('mymovies');
    }

    /**
     * index method
     *
     * @return void
     */
    public function index($course = null)
    {
		$conditions = array('or' => array(
                                  array('and' => array(
                                            'controller' => 'lives', 
                                            'date >' => date('Y-m-d'), 
											'status' => true,
                                        )
                                    ),
                                    array('and' => array(
                                            // 'controller' => 'lives', 
											'status' => true,
                                            'date' => date('Y-m-d'), 
                                            'hour_end >= ' => date('H:i:s')
                                        )
                                    ),
                            )
                          
                        );
        
		$this->Paginator->settings = array('conditions' => $conditions, 'limit' => 12, 'order' => array('Movie.date' => 'ASC'));
        $this->set('movies', $this->Paginator->paginate());

        $recentLives = $this->Movie->find('all', array('conditions' => $conditions, 'limit' => 6));

        $this->set(compact('recentLives', 'pageTitle'));
        $this->set("title_for_layout","Ao vivo");
    }

    public function tag($keyname = null)
    {
        $movies = $this->Movie->Tagged->find('tagged', ['by' => $keyname, 'model' => 'Movie']);
        $this->set(compact('movies'));

        $this->Movie->order = 'Movie.created DESC';
        $this->Movie->limit = 6;
        $recentLives = $this->Movie->find('all');

        $this->set(compact('recentLives'));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     *
     * @param string $slug
     *
     * @return void
     */
    public function view($slug = null)
    {
         /* All movies (even those are free) */
        $movie = $this->Movie->findBySlug($slug);

        if (!$movie || $slug == null) {
            throw new NotFoundException(__('V�deo n�o encontrado.'));
        }
        $embed = $this->Movie->getEmbed($movie);
        $this->set(compact('movie', 'embed'));

        if ($this->Auth->loggedIn()) {

            //verifica se o usuario comprou o video
            $this->Order->recursive = 2;
            $conditions = [
                'conditions' => [
                    'Order.user_id' => $this->Auth->user('id'),
                    'Order.status' => 3,
                    //'Order.paid >=' => CakeTime::toServer(strtotime('-30 days')),
                    'Movie.slug' => $slug
                ]
            ];
            $paidMovie = $this->Order->find('first', $conditions);

            //verifica se o usuario eh assinante
            if($movie['Movie']['controller'] != "modules"){
                $user = $this->User->find('first', array('conditions' => array('User.id' => $this->Auth->user('id'))));
                if($user && $user['User']['subscriber'] == 1 && $movie['Movie']['subscriber'] == 1){
                    $paidMovie = true;
                }
            }elseif(!$paidMovie){
                //se o usuario comprou o curso que o modulo faz parte, tamb�m poder� visualizar
                $conditions = [
                    'conditions' => [
                        'Order.user_id' => $this->Auth->user('id'),
                        'Order.status' => 3,
                        'Order.foreign_id' => $movie['Movie']['parent_id']
                    ]
                ];
                $paidMovie = $this->Order->find('first', $conditions);
            }

            if ($paidMovie) {
                $this->Movie->updateViewCounter();
            }
        } else {
            $paidMovie = false;
            $this->Movie->updateViewCounter();
        }

        $this->set(compact('paidMovie'));

        /** RelatedMovies */
        $this->Movie->order = 'Movie.created DESC';
        $this->Movie->limit = 6;
        if($movie['Movie']['controller'] == "modules" && $movie['Movie']['related_movies'] != ""){
            $relatedMovies =  $this->Movie->find('all', array('recursive' => -1, 'conditions' => array('Movie.id' => explode(',', $movie['Movie']['related_movies']))));    
        }else{
            $relatedMovies = array();
        }
        $this->set(compact('relatedMovies'));
        $this->set("meta_description_custom", (strlen($movie['Movie']['description']) > 196) ? substr($movie['Movie']['description'],0,196).'...' : $movie['Movie']['description']);

        //define a view
        if($movie['Movie']['controller'] == "modules"){
            $this->render('module');
        }else{
            $this->render('view');
        }
    }

    public function mymovies()
    {
		$this->Order->recursive = 2;
        $conditions = [
            'conditions' => [
                'AND' => array(
					'Order.user_id' => $this->Auth->user('id'),
					'Order.status' => 3,
					array('Movie.controller' => array('modules', 'courses')),
				),
                // 'Order.paid >=' => CakeTime::toServer(strtotime('-30 days')),
            ],
			'group' => 'Movie.id'
        ];
		
        $orders = $this->Order->find('all', $conditions);
        $this->set(compact('orders'));
    }
}
