<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link          http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    public $components = array(
        // 'RequestHandler',
    );

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array('Movie','Novidade', 'News');

    public function admin_dashboard() {

    }

    public function home() {	
        $this->Session->write('Compra.Movie.id', null);

        //busca ultimos videos
        $recentMovies = $this->Movie->find('all', array('recursive' => 1, 'conditions' => array('Movie.controller' => array('controller' => 'movies')), 'limit' => 3, 'order' => 'Movie.created DESC'));
        
        //busca ultimas novidades
        $novidades = $this->Novidade->find('all', array('recursive' => -1, 'limit' => 3, 'order' => array('Novidade.created' => 'DESC')));

        //busca ultimas news (inscritos)
        $news = $this->News->find('all', array('recursive' => -1, 'limit' => 2, 'order' => array('News.created' => 'DESC')));

        $this->set(compact('recentMovies', 'novidades', 'news'));
        $this->set('body_class', 'principal');
        
        $this->set("title_for_layout","Instituto Tolerância");
    }

    public function cliquesolidario() {
        $this->Session->write('Compra.Movie.id', null);
        $this->set('body_class', 'interna sobre');

        $recentMovies = $this->Movie->find('all', array('conditions' => array('Movie.type' => 'donation'), 'order' => 'Movie.created DESC', 'limit' => 6));
        $sectionTitle = "Vídeos com Clique Solidário";
        $this->set(compact('recentMovies', 'sectionTitle'));
        $this->set("title_for_layout","Clique solidário");
    }

    public function sobre() {
        $this->Session->write('Compra.Movie.id', null);
        $this->set('body_class', 'interna sobre');
        $this->set("title_for_layout","Sobre");
    }

    public function soucontra171() {
        $this->Session->write('Compra.Movie.id', null);
        $this->set('body_class', 'interna soucontra171');
        $this->set("title_for_layout","#SouContra171");
    }
     public function porque() {
        $this->Session->write('Compra.Movie.id', null);
        $this->set('body_class', 'interna por-que');
        $this->set("title_for_layout","Por que?");
    }

    public function aovivo() {
        $this->Session->write('Compra.Movie.id', null);
        $this->set('body_class', 'interna ao-vivo');
        $this->set("title_for_layout","Ao vivo");
    }

    public function assine() {
        $this->Session->write('Compra.Movie.id', null);
        $this->set('body_class', 'interna assine');
        $this->set("title_for_layout","Assine");
    }

    public function cursos() {
        $this->Session->write('Compra.Movie.id', null);
        $this->set('body_class', 'interna cursos');
        $this->set("title_for_layout","Cursos");
    }
	 public function processo_penal_tolerante() {
        $this->Session->write('Compra.Movie.id', null);
        $this->set('body_class', 'interna sobre penal_tolerante');
        $this->set("title_for_layout","Processo Penal Tolerante");
    }

}
