<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Contacts Controller
 *
 * @property Contact            $Contact
 * @property PaginatorComponent $Paginator
 */
class ContactsController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array();

    /**
     * index method
     *
     * @return void
     */
    public function index()
    {
        $this->redirect(['action' => 'add']);
        $this->set("title_for_layout","Contato");
    }

    /**
     * add method
     *
     * @return void
     */
    public function add()
    {
		// die('EM MANUTENCAO');
        if ($this->request->is('post')) {
			if($this->request->data['Contact']['teste'] == ""){
				$this->Contact->create();
				$this->Contact->set($this->request->data);
				$validationErrors = $this->Contact->invalidFields();

				if ($this->Contact->save()) {

					$email = new CakeEmail('smtp');
					$email->template('contato')
						->emailFormat('html')
						->viewVars(
								array(
										'name' => $this->request->data['Contact']['name'],
										'email' => $this->request->data['Contact']['email'],
										'phone' => $this->request->data['Contact']['phone'],
										'message' => $this->request->data['Contact']['message']
									)
							)
						->to('contato@escoladecriminalistas.com.br')
                        ->replyTo($this->request->data['Contact']['email'])
                        ->bcc('cleber.alves@fatordigital.com.br')
						->subject('Contato pelo site de ' . $this->request->data['Contact']['name'])
						->send();


					$this->Session->setFlash(__('O seu contato foi enviado com sucesso.'), 'alert', array(
						'plugin' => 'BoostCake',
						'class'  => 'alert-success'
					));
					$this->request->data = array();

				} else {
					$this->Session->setFlash(__('Não foi possível salvar a sua mensagem. Tente novamente ou entre em contato por outros meios.'), 'alert', array(
						'plugin' => 'BoostCake',
						'class'  => 'alert-danger'
					));
				}
			}
        }
        $this->set("title_for_layout","Contato");
    }

    /**
     * solutionscms_index method
     *
     * @return void
     */
    public function solutionscms_index()
    {
        $this->DataTable->paginate = array('Contact');
        // $this->Contact->recursive = 0;
        // $this->set('contacts', $this->Paginator->paginate());
        $contacts = $this->Contact->find('all');
        $this->set('contacts', $contacts);
    }

    /**
     * solutionscms_view method
     *
     * @throws NotFoundException
     *
     * @param string $id
     *
     * @return void
     */
    public function solutionscms_view($id = null)
    {
        if (!$this->Contact->exists($id)) {
            throw new NotFoundException(__d('SolutionsCMS', 'Contact not found.'));
        }
        $options = array('conditions' => array('Contact.' . $this->Contact->primaryKey => $id));
        $this->set('contact', $this->Contact->find('first', $options));
    }

    /**
     * solutionscms_add method
     *
     * @return void
     */
    public function solutionscms_add()
    {
        if ($this->request->is('post')) {
            $this->Contact->create();
            if ($this->Contact->save($this->request->data)) {
                $this->Session->setFlash(__d('SolutionsCMS', 'The contact has been saved successfully.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-success'
                ));

                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__d('SolutionsCMS', 'The contact could not be saved. Please, try again.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-danger'
                ));
            }
        }
    }

    /**
     * solutionscms_edit method
     *
     * @throws NotFoundException
     *
     * @param string $id
     *
     * @return void
     */
    public function solutionscms_edit($id = null)
    {
        if (!$this->Contact->exists($id)) {
            throw new NotFoundException(__d('SolutionsCMS', 'Contact not found.'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Contact->save($this->request->data)) {
                $this->Session->setFlash(__d('SolutionsCMS', 'The contact has been updated successfully.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-success'
                ));

                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__d('SolutionsCMS', 'The contact could not be updated. Please, correct any errors and try again.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-danger'
                ));
            }
        } else {
            $options             = array('conditions' => array('Contact.' . $this->Contact->primaryKey => $id));
            $this->request->data = $this->Contact->find('first', $options);
        }
    }

    /**
     * solutionscms_delete method
     *
     * @throws NotFoundException
     *
     * @param string $id
     *
     * @return void
     */
    public function solutionscms_delete($id = null)
    {
        $this->Contact->id = $id;
        if (!$this->Contact->exists()) {
            throw new NotFoundException(__d('SolutionsCMS', 'Contact not found.'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Contact->delete()) {
            $this->Session->setFlash(__d('SolutionsCMS', 'The contact has been deleted successfully.'), 'alert', array(
                'plugin' => 'BoostCake',
                'class'  => 'alert-success'
            ));
        } else {
            $this->Session->setFlash(__d('SolutionsCMS', 'The contact could not be deleted. Please, try again.'), 'alert', array(
                'plugin' => 'BoostCake',
                'class'  => 'alert-danger'
            ));
        }

        return $this->redirect(array('action' => 'index'));
    }
}
