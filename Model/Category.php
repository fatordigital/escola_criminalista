<?php
App::uses('AppModel', 'Model');

/**
 * Category Model
 *
 * @property Category $ParentCategory
 * @property Category $ChildCategory
 * @property Movie    $Movie
 */
class Category extends AppModel
{

    public $cacheQueries = true;

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'title';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(

    );

    public $actsAs = array(
        'Admin.Sluggable' => array(
            'label'     => 'title', // Field that will be slugged
            'slug'      => 'slug', // Field that will be used for the slug
            'separator' => '-', //
            'overwrite' => true // Does the slug is auto generated when field is saved no matter what
        )
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'ParentCategory' => array(
            'className'    => 'Category',
            'foreignKey'   => 'parent_id',
            'conditions'   => '',
            'fields'       => '',
            'order'        => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'ChildCategory' => array(
            'className'    => 'Category',
            'foreignKey'   => 'parent_id',
            'dependent'    => false,
            'conditions'   => '',
            'fields'       => '',
            'order'        => '',
            'limit'        => '',
            'offset'       => '',
            'exclusive'    => '',
            'finderQuery'  => '',
            'counterQuery' => ''
        ),
        'Movie'         => array(
            'className'    => 'Movie',
            'foreignKey'   => 'category_id',
            'dependent'    => false,
            'conditions'   => '',
            'fields'       => '',
            'order'        => '',
            'limit'        => '',
            'offset'       => '',
            'exclusive'    => '',
            'finderQuery'  => '',
            'counterQuery' => ''
        )
    );

}
