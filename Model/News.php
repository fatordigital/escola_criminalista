<?php
App::uses('AppModel', 'Model');
// App::uses('VimeoVideos', 'Vimeo.Model');
require_once APP . 'Vendor' . DS . 'Vimeo' . DS . 'vimeo.php';

/**
 * Movie Model
 *
 * @property Category $Category
 */
class News extends AppModel
{

    public function afterFind($results = array(), $primary = false)
    {

        if (!isset($results[0]['Movie']) || !isset($results[0]['Movie']['id']) || !isset($results[0]['Movie']['source'])) {
            return $results;
        }

        $thumb = array();
        foreach ($results as $key => $result) {
            if ($result['Movie']['source'] == 'youtube') {
                $data           = json_decode($result['Movie']['api_data']);
                $depth_1        = 'media$group';
                $depth_2        = 'media$thumbnail';
                $obj_thumbs     = $data->entry->$depth_1->$depth_2;
                $thumb['big']   = (array)$obj_thumbs[2];
                $thumb['small'] = (array)$obj_thumbs[0];
            } else if ($result['Movie']['source'] == 'vimeo') {
                $data   = unserialize($result['Movie']['api_data']);
                $thumbs = (array)($data[0]->video[0]->thumbnails->thumbnail);
                //$t = max(array_keys($thumbs));

                if (isset($thumbs[3])) {
                    $thumb['xbig']        = (array)$thumbs[3];
                    $thumb['xbig']['url'] = $thumb['xbig']['_content'];
                }

                ;$thumb['big']        = (array)$thumbs[1];
                $thumb['big']['url'] = $thumb['big']['_content'];

                $thumb['small']        = (array)$thumbs[1];
                $thumb['small']['url'] = $thumb['small']['_content'];

                unset($thumb['big']['_content'], $thumb['small']['_content']);
            }
            $results[$key]['Movie']['thumb'] = $thumb;
            $thumb                           = array();
        }

        return $results;
    }

    public function beforeSave($options = array())
    {

        if (!isset($this->data[$this->alias]['link'])) {
            return true;
        }

        if (strpos($this->data[$this->alias]['link'], 'youtube') !== false) {
            $this->handleYoutubeData();
            $this->data[$this->alias]['source'] = 'youtube';
        } else if (strpos($this->data[$this->alias]['link'], 'vimeo') !== false) {
            $this->handleVimeoData();
            $this->data[$this->alias]['source'] = 'vimeo';
        }

        return true;

    }


    public function getEmbed($movie = array())
    {
        if (!empty($movie) && empty($this->data)) {
            $this->data = $movie;
        }

        $embed = null;
        if (strpos($movie[$this->alias]['link'], 'youtube') !== false) {
            $url = $movie[$this->alias]['link'];
            parse_str(parse_url($url, PHP_URL_QUERY), $query);

            $embed                             = $this->getEmbedYoutube($query['v']);
            $movie[$this->alias]['embed'] = $embed;
        } else if (strpos($movie[$this->alias]['link'], 'vimeo') !== false) {
            $embed                             = $this->getEmbedVimeo($movie[$this->alias]['link']);
            $movie[$this->alias]['embed'] = $embed;
        }

        return $embed;

    }



    public $actsAs = array(
        'Admin.Sluggable' => array(
            'label'     => 'title', // Field that will be slugged
            'slug'      => 'slug', // Field that will be used for the slug
            'separator' => '-', //
            'overwrite' => true // Does the slug is auto generated when field is saved no matter what
        ),
        'Tags.Taggable',
		'Upload.Upload' => array(
            'image' => array(
                'thumbnailMethod' => 'php',
                'thumbnailSizes' => array(
                    'thumb' => '290w'
                )
            )
        )
    );

    public $cacheQueries = true;

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'title';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'id'               => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'title'             => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
            )
        ),
        // 'text'             => array(
            // 'noempty' => array(
                // 'rule' => array('notEmpty'),
            // )
        // ),
		 'image'             => array(
           // 'file' => array(
				// 'rule' => 'isFileUpload',
				// 'message' => 'A imagem não é um arquivo valido.',
				// "allowEmpty" => true
			// ),
			'ext' => array(
				'rule' => array('isValidExtension', array('jpg', 'png', 'gif'), false),
				'message' => 'Extensão inválida. Extensões permitidas: jpg, png, ou gif',
				"allowEmpty" => true
			)
        ),
    );

}
