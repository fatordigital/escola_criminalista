<?php
App::uses('AppModel', 'Model');

/**
 * Contact Model
 *
 */
class Diadobem extends AppModel
{


    /**
     * Display field
     *
     * @var string
     */
    //public $displayField = 'name';
    public $useTable = 'diadobem';
    public $name = 'Diadobem';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'email' => array(
            'email' => array(
                'rule' => array('email'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );
}