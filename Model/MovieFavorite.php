<?php
App::uses('AppModel', 'Model');

/**
 * MovieFavorite Model
 *
 */
class MovieFavorite extends AppModel
{

	//The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Movie' => array(
            'className' => 'Movie',
            'foreignKey' => 'movie_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
	
}
