<?php
App::uses('AppModel', 'Model');

/**
 * Novidade Model
 *
 * @property Category $Category
 */
class Novidade extends AppModel
{

    public function beforeSave($options = array()){
        return true;

    }

    public $actsAs = array(
        'Admin.Sluggable' => array(
            'label'     => 'title', // Field that will be slugged
            'slug'      => 'slug', // Field that will be used for the slug
            'separator' => '-', //
            'overwrite' => true // Does the slug is auto generated when field is saved no matter what
        ),
        'Upload.Upload' => array(
            'image' => array(
                    'thumbnailMethod' => 'php',
                    'thumbnailSizes' => array(
                        'thumb' => '350x150',
                        'face' => '250h'
                    )
                )
        )
    );

    public $cacheQueries = true;

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'title';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            ),
        ),
        'title' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
            )
        ),
        'text' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
            )
        ),
    	'image' => array(
           // 'file' => array(
				// 'rule' => 'isFileUpload',
				// 'message' => 'A imagem não é um arquivo valido.',
				// "allowEmpty" => true
			// ),
			'ext' => array(
				'rule' => array('isValidExtension', array('jpg', 'png', 'gif'), false),
				'message' => 'Extensão inválida. Extensões permitidas: jpg, png, ou gif',
				"allowEmpty" => true
			)
        ),
    );

}
