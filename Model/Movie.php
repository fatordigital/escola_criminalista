<?php
App::uses('AppModel', 'Model');
// App::uses('VimeoVideos', 'Vimeo.Model');
require_once APP . 'Vendor' . DS . 'Vimeo' . DS . 'vimeo.php';

/**
 * Movie Model
 *
 * @property Category $Category
 */
class Movie extends AppModel
{

    public function afterFind($results = array(), $primary = false)
    {
        App::import("helper", "String");
        $this->String = new StringHelper(new View(null));

        if (!isset($results[0]['Movie']) || !isset($results[0]['Movie']['id']) || !isset($results[0]['Movie']['source'])) {
            return $results;
        }

        $thumb = array();
        foreach ($results as $key => $result) {
            if ($result['Movie']['source'] == 'youtube') {
                if(isset($result['Movie']['api_data']) && $result['Movie']['api_data'] != ""){
                    $data           = json_decode($result['Movie']['api_data']);
                    
                    $depth_1        = 'media$group';
                    $depth_2        = 'media$thumbnail';
                    if(isset($data->items[0]->snippet->thumbnails)){
                        if (isset($data->items[0]->snippet->thumbnails->standard)) {
                            $thumb['xbig'] = (array)$data->items[0]->snippet->thumbnails->standard;
                        }
                        $thumb['big']   = (array)$data->items[0]->snippet->thumbnails->high;
                        if(isset($data->items[0]->snippet->thumbnails->small)){
                            $thumb['small'] = (array)$data->items[0]->snippet->thumbnails->small; 
                        }else{
                            $thumb['small'] = (array)$data->items[0]->snippet->thumbnails->default; 
                        }

                        // $obj_thumbs = $data->items[0];
						
						// $url = parse_str(parse_url($result['Movie']['link'], PHP_URL_QUERY), $query);
						// $thumb['xbig'] = str_replace('UKY3scPIMd8', $query['v'], $thumb['xbig']);
						// $thumb['big'] = str_replace('UKY3scPIMd8', $query['v'], $thumb['big']);
						// $thumb['small'] = str_replace('UKY3scPIMd8', $query['v'], $thumb['small']);						
                    }
                }                
            } else if ($result['Movie']['source'] == 'vimeo') {
                $data   = unserialize($result['Movie']['api_data']);
                $thumbs = (array)($data[0]->video[0]->thumbnails->thumbnail);
                //$t = max(array_keys($thumbs));

                if (isset($thumbs[3])) {
                    $thumb['xbig']        = (array)$thumbs[3];
                    $thumb['xbig']['url'] = $thumb['xbig']['_content'];
                }

                ;$thumb['big']        = (array)$thumbs[1];
                $thumb['big']['url'] = $thumb['big']['_content'];

                $thumb['small']        = (array)$thumbs[1];
                $thumb['small']['url'] = $thumb['small']['_content'];

                unset($thumb['big']['_content'], $thumb['small']['_content']);
            }
            $results[$key]['Movie']['thumb'] = $thumb;
            $thumb                           = array();
        }

        if (!empty($results)) {
            foreach ($results as $k => &$r) {
                if(isset($r[$this->alias])){
                    if($r[$this->alias]['date'] != "" && $r[$this->alias]['date'] != ""){
                        @$r[$this->alias]['date'] = $this->String->dataFormatada("d/m/Y", $r[$this->alias]['date']);
                    }

                    if (isset($r[$this->alias]['related_movies']) && $r[$this->alias]['related_movies'] != "") {
                        $r[$this->alias]['related_movies'] = json_decode($r[$this->alias]['related_movies']);
                    }
                }
            }
        }

        return $results;
    }

    public function beforeSave($options = array())
    {

        App::import("helper", "String");
        $this->String = new StringHelper(new View(null));

        if (isset($this->data[$this->alias]['date']) && $this->data[$this->alias]['date'] != "") {
            $this->data[$this->alias]['date'] = $this->String->dataFormatada("Y-m-d", $this->data[$this->alias]['date']);
        } else {
            $this->data[$this->alias]['date'] = null;
        }

        if (isset($this->data[$this->alias]['RelatedMovie']) && $this->data[$this->alias]['RelatedMovie'] != "") {
            $this->data[$this->alias]['related_movies'] = json_encode($this->data[$this->alias]['RelatedMovie']);
        }

        if (isset($this->data[$this->alias]['type']) && $this->data[$this->alias]['type'] == "donation") {
            $this->data[$this->alias]['via_assinatura'] = false;
        }

        if(isset($this->data[$this->alias]['controller']) && ($this->data[$this->alias]['controller'] == "courses" || $this->data[$this->alias]['controller'] == "modules")){
            $this->data[$this->alias]['type'] = 'paid';
        }

        if(isset($this->data[$this->alias]['link'])){
            if (strpos($this->data[$this->alias]['link'], 'youtube') !== false) {
                $this->handleYoutubeData();
                $this->data[$this->alias]['source'] = 'youtube';
            } else if (strpos($this->data[$this->alias]['link'], 'vimeo') !== false) {
                $this->handleVimeoData();
                $this->data[$this->alias]['source'] = 'vimeo';
            }
        }        

        if (!isset($this->data[$this->alias]['link'])) {
            return true;
        }

        return true;

    }

    private function handleVimeoData()
    {
        $url      = $this->data[$this->alias]['link'];
        $query    = (parse_url($url));
        $video_id = str_replace('/', '', $query['path']);

        $vimeo      = new phpVimeo(
            '262ad30ced3f199d460737c7ed165e55b26b2dca',
            '5bfc8bdedb76d16ac53971c8233fb423a1360e51',
            'fc19d2a72f9f67de47990dfef5b16c80',
            'c4766cd38bfd1608c476e4cbd8a549a7b6452b1d'
        );
        $video_info = $vimeo->call('vimeo.videos.getInfo', ['video_id' => $video_id]);

        $this->data[$this->alias]['api_data'] = serialize([$video_info]);
        $this->data[$this->alias]['length']   = $video_info->video[0]->duration;

    }

    public function handleYoutubeData()
    {
        $url = $this->data[$this->alias]['link'];
        parse_str(parse_url($url, PHP_URL_QUERY), $query);

        $HttpSocket = new HttpSocket(array(
            'ssl_verify_peer' => false
        ));

        $url = "https://gdata.youtube.com/feeds/api/videos/{$query['v']}?v=2&format=5&alt=json";
        $url = "https://www.googleapis.com/youtube/v3/videos?id={$query['v']}&key=AIzaSyBEhPW4-2s-RV_ENFd2wHbw7Fkl1SQT3LY&part=snippet,contentDetails,statistics,status";

        $results    = $HttpSocket->get($url);

        $data    = json_decode($results->body);

        if (count($data->items)) {
            $depth_1 = 'media$group';
            $depth_2 = 'yt$duration';
            $this->data[$this->alias]['api_data'] = $results->body;
            $duration = new DateInterval($data->items[0]->contentDetails->duration);
            $this->data[$this->alias]['length'] = ((int)$duration->i * 60) + $duration->s;
        } else {
            return false;
        }
    }

    public function getEmbed($movie = array())
    {
        if (!empty($movie) && empty($this->data)) {
            $this->data = $movie;
        }

        $embed = null;
        if (strpos($movie[$this->alias]['link'], 'youtube') !== false) {
            $url = $movie[$this->alias]['link'];
            parse_str(parse_url($url, PHP_URL_QUERY), $query);

            $embed                             = $this->getEmbedYoutube($query['v']);
            $movie[$this->alias]['embed'] = $embed;
        } else if (strpos($movie[$this->alias]['link'], 'vimeo') !== false) {
            $embed                             = $this->getEmbedVimeo($movie[$this->alias]['link']);
            $movie[$this->alias]['embed'] = $embed;
        }

        return $embed;

    }

    public function getEmbedVimeo($url)
    {
        $endpoint = 'http://vimeo.com/api/oembed.json?url=';
        $endpoint .= urlencode($url);

        $HttpSocket = new HttpSocket();
        $result     = $HttpSocket->get($endpoint);

        $metadata = (array) json_decode($result->body);

        return array('metadata' => $metadata, 'html' => $metadata['html']);
    }

    public function getEmbedYoutube($video_id)
    {

        $endpoint = "https://www.youtube.com/v/{$video_id}?fs=1&hd=1&rel=0";

        $html = <<<HTML
<object width="640" height="360" id="video-youtube">
    <param name="movie" value="{$endpoint}" />
    <param name="wmode" value="transparent">
    <embed src="{$endpoint}"
      type="application/x-shockwave-flash"
      allowfullscreen="true"
      width="640" height="360">
    </embed>
</object>
HTML;

        return array('endpoint' => $endpoint, 'html' => $html);
    }

    public function getEmbedYoutubeV2($video_id)
    {

        $endpoint = 'https://www.youtube.com/embed/';
        $endpoint .= ($video_id);

        $HttpSocket = new HttpSocket(array(
            'ssl_verify_peer' => false
        ));

        $html = $HttpSocket->get($endpoint);

        return array('endpoint' => $endpoint, 'html' => $html);
    }

    public function updateViewCounter() {
        // Counter update
        $this->read(null, ($this->data[$this->alias]['id']));
        // $this->set('view_count', ($this->data[$this->alias]['view_count']+1));
        $count = $this->field('view_count');
        $this->save(array('id' => $this->data[$this->alias]['id'], 'view_count' => $count+1), false);
    }

    public $actsAs = array(
        'Admin.Enum' => array(
            'type'   => array('free' => 'Gratuito', 'paid' => 'Pago', 'donation' => 'Clique Solidário'),
            'source' => array('youtube' => 'YouTube', 'vimeo' => 'Vimeo')
        ),
        'Admin.Sluggable' => array(
            'label'     => 'title', // Field that will be slugged
            'slug'      => 'slug', // Field that will be used for the slug
            'separator' => '-', //
            'overwrite' => true // Does the slug is auto generated when field is saved no matter what
        ),
        'Tags.Taggable',
        'Upload.Upload' => array(
            'image' => array(
                    'thumbnailMethod' => 'php',
                    'thumbnailSizes' => array(
                        'vga' => '800w',
                        'thumb' => '400w'
                    )
                )
        )
    );

    public $cacheQueries = true;

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'title';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'id' => array(
                'numeric' => array(
                'rule' => array('numeric'),
            ),
        ),
        'category_id'      => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'link' => array(
            'url' => array(
                'rule' => array('url'),
                'message' => 'URL Inválida',
				'allowEmpty' => true,
            ),
            'vimeo' => array(
                'rule' => array('custom', '/.(vimeo|youtube)./'),
                'message' => 'Link tem que ser do Vimeo ou do Youtube.',
				'allowEmpty' => true,
            ),
        ),
        'price'            => array(
            'money' => array(
                'rule' => array('money'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'comments'         => array(
            'boolean' => array(
                'rule' => array('boolean'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'restriction'      => array(
            'boolean' => array(
                'rule' => array('boolean'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'facebook_posting' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Category' => array(
            'className'    => 'Category',
            'foreignKey'   => 'category_id',
            'conditions'   => '',
            'fields'       => '',
            'order'        => '',
            'counterCache' => true
        )
    );

    public $hasMany = array(
        'RelatedMovie' => array(
            'className' => 'RelatedMovie',
            'foreignKey' => 'movie_id',
            'limit' => 5,
            'order' => array('RelatedMovie.created' => 'DESC'),
            'finderQuery' => ''
        ),
        'MoviePrice' => array(
            'className' => 'MoviePrice',
            'foreignKey' => 'movie_id',
            'dependent' => false,
        ),
    );

}
