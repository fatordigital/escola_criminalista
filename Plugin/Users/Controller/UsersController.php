<?php

App::uses('UsersAppController', 'Users.Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends UsersAppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array(
        'Session',
        'Paginator',
        'RequestHandler',
        'Reports',
        'Auth' => array(
            'loginAction'    => array(
                'plugin' => false,
                'controller' => 'users',
                'action'     => 'login'
            ),
            'loginRedirect'  => array(
                'plugin'     => 'users',
                'controller'   => 'users',
                'action'       => 'home'
            ),
            'logoutRedirect' => array(
                'plugin'       => false,
                'solutionscms' => false,
                'controller'   => 'pages',
                'action'       => 'home'
            ),
            'authError'      => 'Você não tem permissão para acessar esta página!',
            'authenticate'   => array(
                'Form' => array(
                    'userModel' => 'User',
                    'fields' => array(
                     //   'username' => 'email'
                    )
                ),
            ),
            'flash' => array(
                'params' => array(
                    'class'  => 'alert alert-warning'
                ),
                'key' => 'auth',
                'element' => 'default'
            )
        ),
    );

    public $uses = ['Users.User', 'Users.Address', 'Orders', 'Estado'];

     public $helpers = array(
        'Form',
        'Session',
        'Facebook.Facebook'
    );

    public function beforeRender() {
        parent::beforeRender();
    }

    public function beforeFilter() {
        parent::beforeFilter();

        if($this->params['params']['admin'] != true){
            $facebook = $this->Connect->user();
            $this->set('facebook', $facebook);    
        }
        
        $this->set($this->User->enumValues());
        $this->Auth->deny();
        $this->Auth->allow('login');
        $this->Auth->allow('add');
        $this->Auth->allow('logout');
        $this->Auth->allow('esqueci');
        $this->Auth->allow('reset');
    }

     public function beforeFacebookSave(){
        /*if (empty($this->Connect->authUser['User']['id'])) {
            $this->Connect->authUser['User']['id'] = $this->Connect->user('id');
        }*/

        $user = $this->User->find('first', array('conditions' => array('User.email' => $this->Connect->user('email'))));
        if($user){
            $this->Connect->authUser['User']['id']       = $user['User']['id'];
            if($user['User']['password'] != ""){
                $this->Connect->authUser['User']['password'] = $user['User']['password'];
            }
        }else{
            $this->Connect->authUser['User']['id']       = null;            
            $this->Connect->authUser['User']['username'] = $this->Connect->user('username');
        }

        $this->Connect->authUser['User']['first_name'] = $this->Connect->user('first_name')." ".$this->Connect->user('last_name');
        $this->Connect->authUser['User']['email'] = $this->Connect->user('email');
        $this->Connect->authUser['User']['tos'] = true;
        $this->Connect->authUser['User']['email_verified'] = true;
        return true; //Must return true or will not save.
    }

    /**
     * index method
     *
     * @return void
     */
    public function index()
    {   
        // App::import('Model', 'Users.User');
        // $this->User = new User();
        // $this->User->create();
        // $this->User->recursive = 2;

        // App::import('Model', 'Users.Address');
        // $this->Address = new Address();
        // $this->Address->create();    
        
        $user = $this->User->findById($this->Auth->user('id'));
        unset($user['User']['password']);

		if (!$this->User->exists($this->Auth->user('id'))) {
            throw new NotFoundException(__d('admin', 'User not found.'));
        }

        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['User']['id'] = $this->Auth->user('id');

            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__d('admin', 'Os dados do usuário foram atualizados com sucesso.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-success'
                ));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__d('admin', 'Os dados do usuário não poderam ser atualizado. Verifique os campos em destaque.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-danger'
                ));
            }

        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $this->Auth->user('id')));
            $this->request->data = $this->User->find('first', $options);
            //if (isset($this->request->data['Address'][0])) $this->request->data['Address'] = $this->request->data['Address'][0];
            unset($this->request->data['User']['password']);
        }

        // debug($this->User->validationErrors);
        //$this->Address->invalidate('cep','lala');

        //$estados = $this->Estado->find('list', array('fields' => 'uf, nome'));

        //$this->request->data = $user;
        $this->set(compact('user'));
    }
	
	/**
     * index method
     *
     * @return void
     */
    public function subscriber()
    {   
        // App::import('Model', 'Users.User');
        // $this->User = new User();
        // $this->User->create();
        // $this->User->recursive = 2;

        // App::import('Model', 'Users.Address');
        // $this->Address = new Address();
        // $this->Address->create();    
        
        $user = $this->User->findById($this->Auth->user('id'));
        unset($user['User']['password']);

		if (!$this->User->exists($this->Auth->user('id'))) {
            throw new NotFoundException(__d('admin', 'User not found.'));
        }

        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['User']['id'] = $this->Auth->user('id');

            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__d('admin', 'Os dados do usuário foram atualizados com sucesso.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-success'
                ));
                return $this->redirect( array('plugin' => false, 'controller' => 'orders','action' => 'buy', 'assinaturaictd', 'true'));
            } else {
                $this->Session->setFlash(__d('admin', 'Os dados do usuário não poderam ser atualizado. Verifique os campos em destaque.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-danger'
                ));
            }

        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $this->Auth->user('id')));
            $this->request->data = $this->User->find('first', $options);
            //if (isset($this->request->data['Address'][0])) $this->request->data['Address'] = $this->request->data['Address'][0];
            unset($this->request->data['User']['password']);
        }

        // debug($this->User->validationErrors);
        //$this->Address->invalidate('cep','lala');

        //$estados = $this->Estado->find('list', array('fields' => 'uf, nome'));

        //$this->request->data = $user;
        $this->set(compact('user'));
    }
	
    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null)
    {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__d('admin', 'User not found.'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add()
    {
        // App::import('Model','User');
        // $this->User = new User();

        if($this->Auth->user('id')){
            $this->Session->setFlash(__d('users', 'Agora você está logado com sucesso.'), 'alert', array(
                'plugin' => 'BoostCake',
                'class' => 'alert-success'
            ));
            $this->redirect(array('plugin' => 'users', 'controller' => 'users', 'action' => 'home'));
        }

        if ($this->request->is('post') && isset($this->request->data['User'])) {
            // $this->User->create();

            $data = $this->request->data;
           // $this->User->set($this->request->data);

            //$this->set('invalidFields', $this->User->invalidFields());
            //$this->set('invalidFields', $this->Address->invalidFields());

            if ($this->User->save($data)) {

                // $this->Address->create();

                // $data['Address']['user_id'] = $this->User->getInsertID();

                // if ($this->Address->save($data)) {
                    $data['User']['id'] = $this->User->id;
                    $data['User']['subscriber'] = false;
                    $this->Auth->login($data['User']);
                    $this->Session->setFlash(__('Seu usuário foi salvo com sucesso, e agora você já está logado.'), 'alert', array(
                        'plugin' => 'BoostCake',
                        'class' => 'alert-success'
                    ));
                    //$this->redirect($this->Auth->redirectUrl());

                    //if($data['User']['assinar'] == 2){
                      //  $this->redirect(array('plugin' => 'users', 'controller' => 'users', 'action' => 'home'));
                    //}
					
                    $compra_movie_id = $this->Session->read('Compra.Movie.id');
                    if($compra_movie_id != null){
                        $this->loadModel('Movie');
                        $movie = $this->Movie->find('first', array('conditions' => array('Movie.id' => $compra_movie_id)));
                        if($movie){
                            // if($movie['Movie']['controller'] == 'lives'){
                            //     $this->redirect(array('plugin' => false, 'controller' => 'lives', 'action' => 'view', $movie['Movie']['slug']));     
                            // }else{
                            //     $this->redirect(array('plugin' => false, 'controller' => 'movies', 'action' => 'view', $movie['Movie']['slug'])); 
                            // }
                            $this->redirect(array('plugin' => false, 'controller' => 'orders', 'action' => 'buy', $movie['Movie']['id']));
                        }else{
                            // $this->redirect( array('plugin' => false, 'controller' => 'orders','action' => 'buy', 'assinaturaictd', 'true'));
                        }
                    }else{
                        // $this->redirect( array('plugin' => false, 'controller' => 'orders','action' => 'buy', 'assinaturaictd', 'true'));
                    }

                    $movie_id = $this->Session->read('Movie.id');
                    if($movie_id != null){
                        $this->loadModel('Movie');
                        $movie = $this->Movie->find('first', array('conditions' => array('Movie.id' => $movie_id)));
                        if($movie){
                            $this->redirect(array('plugin' => false, 'controller' => 'movies', 'action' => 'view', $movie['Movie']['slug']));
                        }
                    }
                    
                    $this->redirect(array('plugin' => false, 'controller' => 'movies', 'action' => 'mymovies'));

                // } else {
                //     $this->Session->setFlash(__('O endereço não pode ser salvo. Verifique os campos em destaque.'), 'alert', array(
                //         'plugin' => 'BoostCake',
                //         'class' => 'alert-danger'
                //     ));
                // }
            } else {                
                // $this->set('validationErrors', $this->User->invalidFields());
                $this->Session->setFlash(__('Seu usuário não pode ser salvo. Verifique os campos em destaque.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-danger'
                ));
            }
        }

        $estados = $this->Estado->find('list', array('fields' => 'uf, nome'));

        $this->set(compact('estados'));
    }

    public function login() {
	
		// if(stripos($this->referer(), 'users/add')){
		// 	$this->Session->write('User.forcar_assine', true);
		// }

        //var_dump($this->referer());

        if($this->Auth->user()){

            /*if($this->request->data['User']['assinar'] == 2){
                $this->Session->setFlash(__d('users', 'Agora você está logado com sucesso.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-success'
                ));
                $this->redirect(Router::url(['plugin' => 'users', 'controller' => 'users', 'action' => 'confirmacao_cadastro'], true));
            }*/

            Cache::write('facebook', false);
            if($this->Session->read('Compra.Movie.id') != null){
                App::import('Model', 'Movie');
                $this->Movie = new Movie();
                $movie = $this->Movie->find('first', array('conditions' => array('Movie.id' => $this->Session->read('Compra.Movie.id'))));
                if($movie){
                    // if($movie['Movie']['controller'] == 'lives'){
                    //     $this->redirect(array('plugin' => false, 'controller' => 'lives', 'action' => 'view', $movie['Movie']['slug']));     
                    // }else{
                    //     $this->redirect(array('plugin' => false, 'controller' => 'movies', 'action' => 'view', $movie['Movie']['slug'])); 
                    // }

                    if($this->Session->read('Compra.Movie.inscreva') == true){
                        $this->redirect(array('plugin' => false, 'controller' => 'orders', 'action' => 'inscreva', $movie['Movie']['id']));
                    }else{
                        if($this->Session->read('Compra.Movie.movie_price') != 'false'){
                            $this->redirect(array('plugin' => false, 'controller' => 'orders', 'action' => 'buy', $movie['Movie']['id'], 'false', 'false', $this->Session->read('Compra.Movie.movie_price')));
                        }else{
                            $this->redirect(array('plugin' => false, 'controller' => 'orders', 'action' => 'buy', $movie['Movie']['id']));
                        }
                    }
                }else{
                    $this->redirect(array('plugin' => 'users', 'controller' => 'users', 'action' => 'home'));
                }

            }else{
                 $this->Session->setFlash(__d('users', 'Agora você está logado com sucesso.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-success'
                ));
				// if(stripos($this->referer(), 'users/add')){
					// $this->redirect(array('plugin' => false, 'controller' => 'pages', 'action' => 'assine'));
				// }
                $this->redirect(array('plugin' => false, 'controller' => 'users', 'action' => 'home'));
            }
		}

        // Dispatch the event before everything.
        $Event = new CakeEvent(
            'Users.Controller.Users.beforeLogin',
            $this,
            array('data' => $this->request->data)
        );

        $this->getEventManager()->dispatch($Event);

        if ($Event->isStopped()) {
            return;
        }

        if ($this->request->is('post')) {

            if (strpos($this->request->data['User']['username'], '@') !== false) {
                // $this->Auth->authenticate['Form']['fields']['username'] = 'email';
            }

            if ($this->Auth->login()) {
                $Event = new CakeEvent(
                    'Users.Controller.Users.afterLogin',
                    $this,
                    array(
                        'data' => $this->request->data,
                        'isFirstLogin' => !$this->Auth->user('last_login')
                    )
                );
                $this->getEventManager()->dispatch($Event);

                $this->{$this->modelClass}->id = $this->Auth->user('id');
                $this->{$this->modelClass}->saveField('last_login', date('Y-m-d H:i:s'));

                if ($this->here == $this->Auth->loginRedirect) {
                    $this->Auth->loginRedirect = '/';
                }

                if($this->Session->read('Compra.Movie.id') != null){
                    App::import('Model', 'Movie');
                    $this->Movie = new Movie();
                    $movie = $this->Movie->find('first', array('conditions' => array('Movie.id' => $this->Session->read('Compra.Movie.id'))));
                    if($movie){
                        // if($movie['Movie']['controller'] == 'lives'){
                        //     $this->redirect(array('plugin' => false, 'controller' => 'lives', 'action' => 'view', $movie['Movie']['slug']));     
                        // }else{
                        //     $this->redirect(array('plugin' => false, 'controller' => 'movies', 'action' => 'view', $movie['Movie']['slug'])); 
                        // }
                        $this->redirect(array('plugin' => false, 'controller' => 'orders', 'action' => 'buy', $movie['Movie']['id'])); 
                    }else{
                        $this->redirect(array('plugin' => false, 'controller' => 'users', 'action' => 'home'));
                    }

                }else{
                    $this->Session->setFlash(__d('users', 'Agora você está logado com sucesso.'), 'alert', array(
                        'plugin' => 'BoostCake',
                        'class' => 'alert-success'
                    ));
					
					// if($this->Session->read('User.forcar_assine') == true && $this->Auth->user('subscriber') != 1){
					// 	$this->Session->write('User.forcar_assine', false);
					// 	$this->redirect(array('plugin' => 'users', 'controller' => 'users', 'action' => 'subscriber'));
					// }
                    $this->redirect(array('plugin' => false, 'controller' => 'users', 'action' => 'home'));
                }

            } else {

                $this->Session->setFlash(__d('users', "Houve um problema na sua autenticação. Tente novamente."), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-warning'
                ));
            }
        }
    }

    /**
     * Common logout action
     *
     * @return void
     */
    public function logout() {
		$this->Session->destroy();
		$this->Session->delete('FB');
		$this->Session->delete('Auth');
		$this->Session->delete('User');
		//$this->Session->delete('Usuario');
        //$this->Session->delete('FB.Me');
        $this->Auth->logout();
        $this->redirect('/');
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null)
    {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__d('admin', 'User not found.'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__d('admin', 'Os dados do usuário foram atualizados com sucesso'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-success'
                ));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__d('admin', 'Os dados do usuário não poderam ser atualizado. Verifique os campos em destaque.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-danger'
                ));
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
            unset($this->request->data['User']['password']);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null)
    {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__d('admin', 'User not found.'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->User->delete()) {
            $this->Session->setFlash(__d('admin', 'The user has been deleted successfully.'), 'alert', array(
                'plugin' => 'BoostCake',
                'class' => 'alert-success'
            ));
        } else {
            $this->Session->setFlash(__d('admin', 'The user could not be deleted. Please, try again.'), 'alert', array(
                'plugin' => 'BoostCake',
                'class' => 'alert-danger'
            ));
        }
        return $this->redirect(array('action' => 'index'));
    }


    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index()
    {
        $this->User->recursive = 0;
		
        $this->set('users', $this->Paginator->paginate(array('email <>' => null)));
		
		//exportar? 
		if(isset($this->params->params['named']['acao']) && $this->params->params['named']['acao'] == "exportar"){
            $this->Reports->xls($this->User->find('all', array('recursive' => -1, 'fields' => array('User.first_name', 'User.email'), 'conditions' => array('email <>' => null), 'callbacks' => false)), 'Usuários');
		}
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null)
    {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__d('admin', 'User not found.'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add()
    {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__d('admin', 'The user has been saved successfully.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-success'
                ));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__d('admin', 'The user could not be saved. Please, try again.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-danger'
                ));
            }
        }
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null)
    {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__d('admin', 'User not found.'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__d('admin', 'The user has been updated successfully.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-success'
                ));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__d('admin', 'The user could not be updated. Please, correct any errors and try again.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-danger'
                ));
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
            unset($this->request->data['User']['password']);
        }
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null)
    {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__d('admin', 'User not found.'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->User->delete()) {
            $this->Session->setFlash(__d('admin', 'The user has been deleted successfully.'), 'alert', array(
                'plugin' => 'BoostCake',
                'class' => 'alert-success'
            ));
        } else {
            $this->Session->setFlash(__d('admin', 'The user could not be deleted. Please, try again.'), 'alert', array(
                'plugin' => 'BoostCake',
                'class' => 'alert-danger'
            ));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function esqueci(){
        if($this->Auth->user()){
            $this->Session->setFlash(__d('users', $this->Auth->user('first_name').', você já está logado.'), 'alert', array(
                            'plugin' => 'BoostCake',
                            'class' => 'alert-success'
                        ));
            $this->redirect(array('plugin' => false, 'controller' => 'users', 'action' => 'home'));
        }
        if ($this->request->is('post')) {

            $user = $this->User->findByEmail($this->request->data['User']['email']);
            if (isset($user['User']['id'])) {

                $this->User->id = $user['User']['id'];
                $activationKey = md5(uniqid());
                $this->User->saveField('activation_key', $activationKey);

                $email = new CakeEmail('smtp');
                $email->viewVars(array('url' =>  Router::url(array('plugin' => 'users', 'controller' => 'users', 'action' => 'reset', $user['User']['email'], $activationKey,), true), 'nome' => $user['User']['first_name']));
                $email->template('esqueci_senha');
                $email->emailFormat('html');
                $email->to($user['User']['email']);
                $email->subject("Alteração de Senha");

                if ($email->send()) {
                     $this->Session->setFlash(__('As instruções para recuperar sua senha foram enviadas para seu e-mail.'), 'alert', array(
                        'plugin' => 'BoostCake',
                        'class'  => 'alert-success'
                    ));
                    $this->request->data = array();
                } else {
                    $this->Session->setFlash(__('Ocorreu um erro ao tentar resetar sua senha, tente novamente.'), 'alert', array(
                        'plugin' => 'BoostCake',
                        'class'  => 'alert-danger'
                    ));
                }
            } elseif (empty($this->request->data['User']['email'])) {
                $this->User->invalidate('email', 'Campo de preenchimento obrigatório');
            } else {
                $this->User->invalidate('email', 'O email informado não foi encontrado.');
            }
        }
    }

    public function reset($email = null, $key = null) {
        if ($email == null || $key == null) {
            $this->Session->setFlash(__('Paramentros inválidos'), 'alert', array(
                        'plugin' => 'BoostCake',
                        'class'  => 'alert-danger'
                    ));
            $this->redirect(array('action' => 'login'));
        }

        $user = $this->User->find('first', array(
            'conditions' => array(
                'User.email' => $email,
                'User.activation_key' => $key,
            )
        ));
        if (!isset($user['User']['id'])) {
            $this->Session->setFlash(__('Paramentros inválidos'), 'alert', array(
                        'plugin' => 'BoostCake',
                        'class'  => 'alert-danger'
                    ));
            $this->redirect(array('action' => 'login'));
        }

        if ($this->request->is('post')) {
            if (!empty($this->request->data['User']['password'])) {
                $this->User->id = $user['User']['id'];
                $user['User']['password'] = $this->request->data['User']['password'];
                $user['User']['activation_key'] = md5(uniqid());
                if ($this->User->save($user, false)) {
                    $this->Session->setFlash(__('Sua senha foi alterada com sucesso!'), 'alert', array(
                        'plugin' => 'BoostCake',
                        'class'  => 'alert-success'
                    ));
                    $this->redirect(array('action' => 'login'));
                } else {
                    $this->Session->setFlash(__('Ocorreu um erro ao tentar alterar a senha, tente novamente.'), 'alert', array(
                        'plugin' => 'BoostCake',
                        'class'  => 'alert-danger'
                    ));
                }
            } else {
                $this->User->invalidate('password', 'Campo de preenchimento obrigatório');
            }
        }

        $this->set(compact('usuario', 'email', 'key'));   
    }

    public function confirmacao_cadastro(){

    }

    public function confirmacao_assinatura(){
        
    }

    public function confirmacao_compra(){
        
    }

    public function home(){
        
    }
}
