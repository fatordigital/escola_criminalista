<?php

/**
 * @var $this View
 */
$this->Html->addCrumb(__d('users', 'Users'));

?>

<div class="row">
    <div class="col-md-12">
        <h2 class="page-title"><?php echo __d('users', 'Admin Index User'); ?></h2>
        <?php echo $this->Html->getCrumbList(
            array(
                'class' => 'breadcrumb',
                'separator' => '<i class="icon-angle-right"></i>',
                'escape' => false
            ),
            'Home'
        );
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <?php echo $this->Session->flash(); ?>

        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>&nbsp;<?php echo __d('users', 'Admin Index the User information'); ?>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                        <a href="javascript:;" class="reload"></a>
                        <a href="javascript:;" class="remove"></a>
                    </div>
                </div>
            </div>

            <div class="portlet-body">

                <div class="table-toolbar">
                    <?php echo $this->Html->link(
                        sprintf('%s <i class="fa fa-plus"></i>', __d('users', 'Add New')),
                        array('action' => 'add'),
                        array(
                            'escape' => false,
                            'class' => 'btn green'
                        )
                    ); ?>
                    <?php echo $this->Html->link('<i class="icon-external-link bigger-110"></i>Exportar Relatório', array('acao' => 'exportar') , array('class' => 'btn btn-info btn-small btn-export', 'escape' => false)); ?>
                </div>

                <table class="table table-hover table-bordered table-striped table-condensed flip-content"
                       data-model="User">
                    <thead>
                    <tr>
                        <th><?php echo $this->Paginator->sort('first_name', __d('users', 'Full Name')); ?></th>
                        <th><?php echo $this->Paginator->sort('birthday', __d('users', 'Data de Nascimento')); ?></th>
                        <th><?php echo $this->Paginator->sort('email', __d('users', 'E-mail')); ?></th>
                        <th><?php echo $this->Paginator->sort('username', __d('users', 'Usuário')); ?></th>
                        <th><?php echo $this->Paginator->sort('subscriber', __d('users', 'Assinante')); ?></th>
                        <th><?php echo $this->Paginator->sort('created', __d('users', 'Criado')); ?></th>
                        <th><?php echo $this->Paginator->sort('last_login', __d('users', 'Último Login')); ?></th>
                        <th class="actions"><?php echo __d('users', 'Actions'); ?></th>
                    </tr>
                    </thead>

                    <?php foreach ($users as $user): ?>
                        <tr>
                            <td><?php echo h($user['User']['first_name']); ?>&nbsp;</td>
                            <td align="center"><?php echo $this->Time->format('d/m/Y', $user['User']['birthday']); ?>&nbsp;</td>
                            <td><?php echo h($user['User']['email']); ?>&nbsp;</td>
                            <td><?php echo h($user['User']['username']); ?>&nbsp;</td>
                            <td align="center"><?php echo ($user['User']['subscriber']) ? "<span class='active'>Sim</span>" : "<span class='inative'>Não</span>"; ?>&nbsp;</td>
                            <td align="center"><?php echo $this->Time->format('d/m/Y H:i:s', $user['User']['created']); ?>&nbsp;</td>
                            <td align="center"><?php echo $user['User']['last_login'] ? $this->Time->format('d/m/Y H:i:s', $user['User']['last_login']) : 'Nunca logado' ?>&nbsp;</td>
                            <td class="actions">
                                <?php echo $this->Html->link(

                                    sprintf('<i class="fa fa-info"></i>&nbsp;&nbsp;%s</a>', __d('users', 'View')),
                                    array('action' => 'view', $user['User']['id']),
                                    array(
                                        'escape' => false,
                                        'data-id' => $user['User']['id'],
                                        'class' => 'btn btn-sm blue btn-editable',
                                        'original-title' => __d('users', 'View')
                                    )
                                ); ?>
                                <?php echo $this->Html->link(sprintf('<i class="fa fa-edit"></i>&nbsp;&nbsp;%s</a>', __d('users', 'Edit')),
                                    array(
                                        'action' => 'edit',
                                        $user['User']['id']
                                    ),
                                    array(
                                        'escape' => false,
                                        'data-id' => $user['User']['id'],
                                        'class' => 'btn btn-sm green btn-editable',
                                        'original-title' => __d('users', 'Edit')
                                    )
                                ); ?>
                                <?php echo $this->Form->postLink(
                                    sprintf('<i class="fa fa-times"></i>&nbsp;&nbsp;%s</a>', __d('users', 'Delete')),
                                    array(
                                        'action' => 'delete',
                                        $user['User']['id']),
                                    array(
                                        'escape' => false,
                                        'class' => 'btn btn-sm red btn-removable',
                                    ),
                                    __d('users', 'Are you sure you want to delete # %s?', $user['User']['id'])
                                );

                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                </table>

                <?php if($this->params['paging']['User']['pageCount'] > 1): ?>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center clear">
                            <ul class="pagination customizada">
                                <?php echo $this->Paginator->prev( '«', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li' ) ); ?>
                                <?php echo $this->Paginator->numbers(['currentClass' => 'active', 'tag' => 'li']); ?>
                                <?php  echo $this->Paginator->next( '»', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li' ) ); ?>
                            </ul>
                        </div>
                    </div>
                    <p style="width: 100%; text-align: center;"><?php echo $this->Paginator->counter(array(
                        'format' => 'Página {:page} de {:pages}, exibindo {:current} registros de um total de {:count} registros.'
                    )); ?>
                    </p>
                <?php endIf; ?>

            </div>
        </div>
    </div>
</div>
