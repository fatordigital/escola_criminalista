<?php $this->Html->script("$cmsPluginName.assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js", ['inline' => false]); ?>
<?php $this->Html->css("$cmsPluginName.assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css", ['inline' => false]); ?>
<?php

/**
 * @var $this View
 */
$this->Html->addCrumb(__d('users', 'Users'));

$this->Html->addCrumb(__d('users', 'Admin Add User', $this->Form->value('User.id')));

?>
<div class="row">
    <div class="col-md-12">
        <h2 class="page-title"><?php echo __d('users', 'Admin Add User'); ?></h2>
        <?php echo $this->Html->getCrumbList(
            array(
                'class' => 'breadcrumb',
                'separator' => '<i class="icon-angle-right"></i>',
                'escape' => false
            ),
            'Home'
        );
        ?>    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <?php echo $this->Session->flash(); ?>
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <?php echo __d('users', 'Admin Add the User information'); ?>
                </div>
            </div>

            <div class="portlet-body form users">
                <div class="form-body">
                    <?php echo $this->Form->create(
                        'User',
                        array(
                            'inputDefaults' => array(
                                'div' => 'form-group',
                                'wrapInput' => 'col col-md-9',
                                'label' => array(
                                    'class' => 'col col-md-3 control-label'
                                ),
                                'class' => 'form-control',
                                'novalidate' => 'novalidate'
                            ),
                            'class' => 'form-horizontal',
                        )
                    ); ?>

                    <?php
                    echo $this->Form->input('first_name',
                        array(
                            'label' => array(
                                'text' => 'Primeiro Nome'
                            )
                        )
                    );
                    echo $this->Form->input('last_name',
                        array(
                            'label' => array(
                                'text' => 'Sobreome'
                            )
                        )
                    );
                    echo $this->Form->input('birthday',
                        array(
                            'label' => array(
                                'text' => 'Data de Nascimento'
                            ),
                            'class' => ''
                        )
                    );
                    echo $this->Form->input('gender',
                        array(
                            'label' => array(
                                'text' => 'Sexo'
                            )
                        )
                    );
                    echo $this->Form->input('email',
                        array(
                            'label' => array(
                                'text' => 'Email'
                            )
                        )
                    );
                    echo $this->Form->input('username',
                        array(
                            'label' => array(
                                'text' => 'Usuário'
                            )
                        )
                    );
                    echo $this->Form->input('password',
                        array(
                            'label' => array(
                                'text' => 'Senha'
                            )
                        )
                    );
                    echo $this->Form->input('subscriber',
                        array(
                            'class' => 'form-control make-switch',
                            'before' => '<label class="col-md-3 control-label">Assinante</label>',
                            'label' => false,
                            'checkboxDiv' => false
                        )
                    );
                    ?>

                    <div class="form-actions">

                        <?php echo $this->Form->submit(__d('users', 'Submit'), array(
                            'div' => false,
                            'class' => 'btn blue',
                            'escape' => false,
                        )); ?>
                        <?php echo $this->Form->postLink(
                            __d('users', 'Delete %s', '<i class="fa fa-times"></i>'),
                            array(
                                'action' => 'delete',
                                $this->Form->value('User.id')
                            ),
                            array('class' => 'btn red', 'escape' => false),
                            __d('users', 'Are you sure you want to delete %s # %s?', __d('users', 'User'), $this->Form->value('User.id'))
                        ); ?>

                    </div>

                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
