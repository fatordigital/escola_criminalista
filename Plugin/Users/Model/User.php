<?php

App::uses('UsersAppModel', 'Users.Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

/**
 * User Model
 *
 * @property Subscription $Subscription
 */
class User extends UsersAppModel
{

    public $actsAs = array(
        'Admin.Enum' => array(
            'gender'   => array('M' => 'Masculino', 'F' => 'Feminino'),
        )
    );

    public $virtualFields = array(
        'full_name' => 'CONCAT(User.first_name, " ", User.last_name)'
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    public function beforeSave($options = array())
    {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new SimplePasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash($this->data[$this->alias]['password']);
        }

		if (isset($this->data[$this->alias]['email'])) {
            $this->data[$this->alias]['username'] = $this->data[$this->alias]['email'];
        }
        return true;
    }

    // public $hasMany = array(
    //     'Address' => array(
    //         'className' => 'Address',
    //         'foreignKey' => 'user_id',
    //         'limit' => '',
    //         'order' => '',
    //         'finderQuery' => '',
    //         'dependent' => false
    //     )
    // );

    public $validate = array(
        'first_name' => array(
            'valido' => array(
               'rule'=>array('custom', '/[A-Za-zÀ-Üà-ü ] [A-Za-zÀ-Üà-ü ]/'),
                'message' => 'O nome deve ser composto'
            ),
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo de preenchimento obrigatório.',
            ),
        ),
        // 'last_name' => array(
        //     'notempty' => array(
        //         'rule' => array('notempty'),
        //         'message' => 'Campo de preenchimento obrigatório.',
        //     ),
        // ),
        'email' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo de preenchimento obrigatório.',
            ),
            'email' => array(
                'rule' => array('email'),
                'message' => 'Formato de e-mail inválido.',
            ),
            'unico' => array(
                'rule' => array('isUnique'),
                'message' => 'Esse e-mail já está cadastrado. Caso ele seja seu, <a href="http://www.casadatolerancia.com.br/users/login">clique aqui</a> e faça seu login para prosseguir.</a>',
                'on' => 'create'
            ),
        ),
        'password' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo de preenchimento obrigatório.',
                'on' => 'create'
            ),
        ),
        'confirm' => array(
            'rule' => 'confirmPassword',
            'message' => 'As senhas não conferem.',
            'on' => 'create'),
        // 'cpf' => array(
        //     'notempty' => array(
        //         'rule' => array('notempty'),
        //         'message' => 'Campo de preenchimento obrigatório.',
        //     ),
        //     'unico' => array(
        //         'rule' => array('isUnique'),
        //         'message' => 'Este cpf já está cadastrado, insira outro cpf!',
        //     ),
        // ),
        'phone' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo de preenchimento obrigatório.',
            ),
        ),
        // 'termo' => array(
        //     'notempty' => array(
        //         'rule' => array('notempty'),
        //         'message' => 'Campo de preenchimento obrigatório.',
        //     ),
        // ),
        'facebook_id' => array(
             'unico' => array(
                'rule' => array('isUnique'),
                'message' => 'Este e-mail já está cadastrado. Insira outro e-mail!',
                'on' => 'create'
            ),
        ),
    );

    public function confirmPassword($password = null)
    {
        if ((isset($this->data[$this->alias]['password']) && isset($password['confirm']))
            && !empty($password['confirm'])
            && ($this->data[$this->alias]['password'] === $password['confirm'])
        ) {
            return true;
        }
        return false;
    }

}
