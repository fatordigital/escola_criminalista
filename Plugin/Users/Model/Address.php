<?php
App::uses('UsersAppModel', 'Users.Model');

/**
 * Address Model
 *
 * @property User $User
 */
class Address extends UsersAppModel
{

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'user_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'number' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Digite um número válido',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'zipcode' => array(
           'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo de preenchimento obrigatório.',
            ),
        ),
        'street' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo de preenchimento obrigatório.',
            ),
        ),
         'complement' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo de preenchimento obrigatório.',
            ),
        ),
          'district' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo de preenchimento obrigatório.',
            ),
        ),
        'city' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo de preenchimento obrigatório.',
            ),
        ),
        'state' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo de preenchimento obrigatório.',
            ),
        ),
    );

    public function beforeSave($options = array())
    {
        if (isset($this->data[$this->alias]['complement'])) {
            $this->data[$this->alias]['extra'] = $this->data[$this->alias]['complement'];
        }
        return true;
    }

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}
