<header>
    <h1>Faça seu login</h1>
</header>
<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
    <h2>Preencha seu usuário e senha para entrar.</h2>

    <?php

    echo $this->Session->flash();
    echo $this->Session->flash('auth');

    echo $this->Form->create('User');

    echo $this->Form->input('username', ['label' => 'Usuário']);
    echo $this->Form->input('password', ['label' => 'Senha']);

    echo $this->Form->submit('Enviar', ['class' => 'btn btn-default']);

    echo $this->Form->end();

    ?>
</div>
