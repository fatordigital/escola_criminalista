# LANGUAGE translation of CakePHP Application
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2014-03-05 04:41-0300\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: Controller/CmsUsersController.php:46
msgid "Invalid username or password, try again"
msgstr ""

#: Controller/CmsUsersController.php:87
msgid "Invalid cms user"
msgstr ""

#: Controller/CmsUsersController.php:107
msgid "The System User has been saved successfully."
msgstr ""

#: Controller/CmsUsersController.php:113
msgid "The system user could not be saved. Please, try again."
msgstr ""

#: Controller/CmsUsersController.php:134;181
msgid "System User not found"
msgstr ""

#: Controller/CmsUsersController.php:144
msgid "The System User has been saved"
msgstr ""

#: Controller/CmsUsersController.php:150
msgid "The System User could not be saved. Please, try again."
msgstr ""

#: Controller/CmsUsersController.php:184
msgid "System User deleted"
msgstr ""

#: Controller/CmsUsersController.php:190
msgid "System User was not deleted"
msgstr ""

#: Controller/CmsUsersController.php:218
msgid "The System User %s has been created with the following password %s"
msgstr ""

#: View/CmsUsers/admin_add.ctp:5
#: View/CmsUsers/admin_edit.ctp:5
#: View/CmsUsers/admin_index.ctp:5
msgid "Users"
msgstr ""

#: View/CmsUsers/admin_add.ctp:6
msgid "New User"
msgstr ""

#: View/CmsUsers/admin_add.ctp:13
msgid "New System User"
msgstr ""

#: View/CmsUsers/admin_add.ctp:34
msgid "Fill in the new user details"
msgstr ""

#: View/CmsUsers/admin_add.ctp:60
#: View/CmsUsers/admin_edit.ctp:64
msgid "Full Name"
msgstr ""

#: View/CmsUsers/admin_add.ctp:65
#: View/CmsUsers/admin_edit.ctp:69
#: View/CmsUsers/admin_login.ctp:20;25
msgid "Username"
msgstr ""

#: View/CmsUsers/admin_add.ctp:72
#: View/CmsUsers/admin_edit.ctp:77
#: View/CmsUsers/admin_login.ctp:32;37
msgid "Password"
msgstr ""

#: View/CmsUsers/admin_add.ctp:80
#: View/CmsUsers/admin_edit.ctp:84
msgid "E-mail"
msgstr ""

#: View/CmsUsers/admin_add.ctp:86
msgid "Submit"
msgstr ""

#: View/CmsUsers/admin_edit.ctp:6
msgid "Editing %s User"
msgstr ""

#: View/CmsUsers/admin_edit.ctp:13
msgid "Editing System User"
msgstr ""

#: View/CmsUsers/admin_edit.ctp:35
msgid "Edit the System User information"
msgstr ""

#: View/CmsUsers/admin_edit.ctp:73
msgid "Leave blank if you do not want to change"
msgstr ""

#: View/CmsUsers/admin_edit.ctp:90
msgid "Update"
msgstr ""

#: View/CmsUsers/admin_edit.ctp:97
msgid "Delete %s"
msgstr ""

#: View/CmsUsers/admin_edit.ctp:103
msgid "Are you sure you want to delete %s # %s?"
msgstr ""

#: View/CmsUsers/admin_edit.ctp:103
msgid "System User"
msgstr ""

#: View/CmsUsers/admin_index.ctp:6
msgid "Listing"
msgstr ""

#: View/CmsUsers/admin_index.ctp:11
#: View/Elements/sidebar.ctp:44
msgid "System Users"

#: View/CmsUsers/admin_index.ctp:39
msgid "View the registered users on the system"
msgstr ""

#: View/CmsUsers/admin_index.ctp:54
msgid "Add New"
msgstr ""

#: View/CmsUsers/admin_index.ctp:83
msgid "View"
msgstr ""

#: View/CmsUsers/admin_index.ctp:98
msgid "Edit"
msgstr ""

#: View/CmsUsers/admin_index.ctp:113
msgid "Delete"
msgstr ""

#: View/CmsUsers/admin_index.ctp:125
msgid "Are you sure you want to delete the \"%s\" user?"
msgstr ""

#: View/CmsUsers/admin_login.ctp:12
msgid "Login to your account"
msgstr ""

#: View/CmsUsers/admin_login.ctp:52
msgid "Login %s"
msgstr ""

#: View/Elements/sidebar.ctp:33
msgid "Dashboard"
msgstr ""

#: View/Elements/sidebar.ctp:53
msgid "List System Users"
msgstr ""

#: View/Elements/sidebar.ctp:66
msgid "Create System User"
msgstr ""

#: View/Elements/sidebar.ctp:90
msgid "List"
msgstr ""

#: View/Elements/sidebar.ctp:103
msgid "Create"
msgstr ""

