<?php

if (!Configure::check('Admin.name')) {
    Configure::write('Admin.name', 'SolutionsCMS');
}
if (!Configure::check('Admin.author')) {
    Configure::write('Admin.author', 'NEO Solutions');
}
if (!Configure::check('Admin.pluginName')) {
    Configure::write('Admin.pluginName', 'Admin');
}
if (!Configure::check('Admin.displayName')) {
    Configure::write('Admin.displayName', 'SolutionsCMS');
}
if (!Configure::check('Admin.routingName')) {
    Configure::write('Admin.routingName', 'solutionscms');
}
if (!Configure::check('Admin.routingPrefix')) {
    Configure::write('Admin.routingPrefix', 'admin');
}

CakePlugin::load('Upload');