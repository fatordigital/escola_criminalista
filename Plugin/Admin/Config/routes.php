<?php

$routePrefix = strtolower(Configure::read('Admin.routingPrefix'));
$routeName = strtolower(Configure::read('Admin.routingName'));
$pluginName = Configure::read('Admin.pluginName');

Router::connect(
    "/{$routeName}/CmsUsers/:action/*",
    array($routePrefix => true, 'plugin' => $pluginName, 'prefix' => $routePrefix, 'controller' => 'CmsUsers', 'action' => 'index')
);
Router::connect(
    "/{$routeName}/cms_users/logout",
    array($routePrefix => true, 'plugin' => $pluginName, 'prefix' => $routePrefix, 'controller' => 'CmsUsers', 'action' => 'logout')
);
Router::connect(
    "/{$routeName}/CmsUsers",
    array($routePrefix => true, 'plugin' => $pluginName, 'prefix' => $routePrefix, 'controller' => 'CmsUsers', 'action' => 'index')
);
Router::connect(
    "/{$routeName}/blocks/*",
    array($routePrefix => true, 'plugin' => $pluginName, 'controller' => 'blocks', 'action' => 'index')
);
Router::connect(
    "/{$routeName}/blocks/:action/*",
    array($routePrefix => true, 'plugin' => $pluginName, 'controller' => 'blocks')
);
Router::connect(
    "/{$routeName}/blog/:controller/:action/*",
    array($routePrefix => true, 'plugin' => 'Blog')
);

Router::connect(
    "/{$routeName}/blog/blog_posts",
    array($routePrefix => true, 'plugin' => 'Blog', 'controller' => 'blog_posts')
);
Router::connect(
    "/{$routeName}/blog/blog_tags",
    array($routePrefix => true, 'plugin' => 'Blog', 'controller'   => 'blog_post_tags')
);

Router::connect(
    "/{$routeName}/sliders",
    array($routePrefix => true, 'plugin' => $pluginName, 'controller' => 'sliders', 'action' => 'index')
);

/*LIVE*/
Router::connect(
    "/{$routeName}/Lives",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'index', 'lives')
);
Router::connect(
    "/{$routeName}/lives",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'index', 'lives')
);
Router::connect(
    "/{$routeName}/Lives/add",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'add', 'lives')
);
Router::connect(
    "/{$routeName}/lives/add",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'add', 'lives')
);
Router::connect(
    "/{$routeName}/lives/view/:id",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'view'),
    array('pass' => array('id'))
);
Router::connect(
    "/{$routeName}/lives/edit/:id",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'edit'),
    array('pass' => array('id'))
);
Router::connect(
    "/{$routeName}/lives/delete/:id",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'delete'),
    array('pass' => array('id'))
);
/*LIVE*/


/*DIVERSOS*/
Router::connect(
    "/{$routeName}/Miscellaneous",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'index', 'miscellaneous')
);
Router::connect(
    "/{$routeName}/miscellaneous",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'index', 'miscellaneous')
);
Router::connect(
    "/{$routeName}/Miscellaneous/add",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'add', 'miscellaneous')
);
Router::connect(
    "/{$routeName}/miscellaneous/add",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'add', 'miscellaneous')
);
Router::connect(
    "/{$routeName}/miscellaneous/view/:id",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'view'),
    array('pass' => array('id'))
);
Router::connect(
    "/{$routeName}/miscellaneous/edit/:id",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'edit'),
    array('pass' => array('id'))
);
Router::connect(
    "/{$routeName}/miscellaneous/delete/:id",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'delete'),
    array('pass' => array('id'))
);
/*DIVERSOS*/


/*MODULOS*/
Router::connect(
    "/{$routeName}/Modules",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'index', 'modules')
);
Router::connect(
    "/{$routeName}/modules",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'index', 'modules')
);
Router::connect(
    "/{$routeName}/Modules/index/*",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'index', 'modules')
);
Router::connect(
    "/{$routeName}/modules/index/*",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'index', 'modules')
);
Router::connect(
    "/{$routeName}/Modules/add",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'add', 'modules')
);
Router::connect(
    "/{$routeName}/modules/add",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'add', 'modules')
);
Router::connect(
    "/{$routeName}/Modules/add/*",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'add', 'modules')
);
Router::connect(
    "/{$routeName}/modules/add/*",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'add', 'modules')
);
Router::connect(
    "/{$routeName}/modules/view/:id",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'view'),
    array('pass' => array('id'))
);
Router::connect(
    "/{$routeName}/modules/edit/:id",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'edit'),
    array('pass' => array('id'))
);
Router::connect(
    "/{$routeName}/modules/delete/:id",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'delete'),
    array('pass' => array('id'))
);
/*MODULOS*/

/*RELACIONADOS*/
Router::connect(
    "/{$routeName}/Related",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'index', 'related')
);
Router::connect(
    "/{$routeName}/related",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'index', 'related')
);
Router::connect(
    "/{$routeName}/Related/index/*",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'index', 'related')
);
Router::connect(
    "/{$routeName}/related/index/*",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'index', 'related')
);
Router::connect(
    "/{$routeName}/Related/add",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'add', 'related')
);
Router::connect(
    "/{$routeName}/related/add",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'add', 'related')
);
Router::connect(
    "/{$routeName}/Related/add/*",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'add', 'related')
);
Router::connect(
    "/{$routeName}/related/add/*",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'add', 'related')
);
Router::connect(
    "/{$routeName}/related/view/:id",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'view'),
    array('pass' => array('id'))
);
Router::connect(
    "/{$routeName}/related/edit/:id",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'edit'),
    array('pass' => array('id'))
);
Router::connect(
    "/{$routeName}/related/delete/:id",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'delete'),
    array('pass' => array('id'))
);
/*RELACIONADOS*/

Router::connect(
    "/{$routeName}/Courses",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'index', 'courses')
);
Router::connect(
    "/{$routeName}/Courses/add",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'add', 'courses')
);
Router::connect(
    "/{$routeName}/courses/add",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'add', 'courses')
);
Router::connect(
    "/{$routeName}/courses/add/:curso",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'add', 'courses')
);
Router::connect(
    "/{$routeName}/Courses/add/:curso",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'add', 'courses')
);
Router::connect(
    "/{$routeName}/courses/view/:id",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'view'),
    array('pass' => array('id'))
);
Router::connect(
    "/{$routeName}/Courses/view/:id",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'view'),
    array('pass' => array('id'))
);
Router::connect(
    "/{$routeName}/courses/edit/:id",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'edit'),
    array('pass' => array('id'))
);
Router::connect(
    "/{$routeName}/Courses/edit/:id",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'edit'),
    array('pass' => array('id'))
);
Router::connect(
    "/{$routeName}/courses/delete/:id",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'delete'),
    array('pass' => array('id'))
);
Router::connect(
    "/{$routeName}/Courses/delete/:id",
    array($routePrefix => true, 'controller' => 'movies', 'action' => 'delete'),
    array('pass' => array('id'))
);

Router::connect(
    "/{$routeName}/sliders/:action/*",
    array($routePrefix => true, 'plugin' => $pluginName, 'controller' => 'sliders')
);
Router::connect(
    "/{$routeName}/options",
    array($routePrefix => true, 'plugin' => $pluginName, 'controller' => 'options', 'action' => 'index')
);
Router::connect(
    "/{$routeName}/options/:action/*",
    array($routePrefix => true, 'plugin' => $pluginName, 'controller' => 'options')
);


/** Custom Route for Dashboard. And lives on App, outside of the Plugin. */
Router::connect(
    "/{$routeName}",
    array(
        'plugin'       => false,
        $routePrefix => true,
        'controller'   => 'Pages',
        'action' => 'dashboard'
    )
);

/**
 * Down here we enable all actions outside the plugin be served with
 * the prefix, being the same defined on core
 * It's the expected magic to the prefix be the same name as the plugin itself.
 */

Router::connect(
    "/{$routeName}/:controller",
    array(
        // 'plugin'       => $pluginName,
        $routePrefix => true
    )
);

Router::connect(
    "/{$routeName}/:controller/:action",
    array(
        // 'plugin'       => $pluginName,
        $routePrefix => true
    )
);

Router::connect(
    "/{$routeName}/:controller/:action/*",
    array(
        // 'plugin'       => $pluginName,
        $routePrefix => true
    )
);