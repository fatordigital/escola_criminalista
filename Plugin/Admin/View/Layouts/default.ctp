<?php /** @var $this View */ ?>

<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 2.3.2
Version: 1.4
Author: KeenThemes
Website: http://www.keenthemes.com/preview/?theme=metronic
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <title><?php echo $title_for_layout ?> :: <?php echo $cmsDisplayName; ?></title>

    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <script>
        var WEBROOT = '<?php echo $this->webroot; ?>';
    </script>

    <?php
        echo $this->Html->meta('icon', "$cmsPluginName.favicon.ico");
        echo $this->fetch('meta');
        echo $this->fetch('css');
    ?>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <?php
    echo $this->Html->css(array(
        '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all',
        "$cmsPluginName.assets/plugins/font-awesome/css/font-awesome.min",
        "$cmsPluginName.assets/plugins/bootstrap/css/bootstrap.min",
        "$cmsPluginName.assets/plugins/uniform/css/uniform.default",
    ));
    ?>
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
    <?php $this->fetch('page_level_css'); ?>
    <!-- END PAGE LEVEL PLUGIN STYLES -->

    <!-- BEGIN THEME STYLES -->
    <?php
    echo $this->Html->css(array(
        "$cmsPluginName.assets/css/style-metronic",
        "$cmsPluginName.assets/css/style",
        "$cmsPluginName.assets/css/style-responsive",
        "$cmsPluginName.assets/css/plugins",
        "$cmsPluginName.assets/css/themes/grey",
        "$cmsPluginName.assets/css/custom",
    ))
    ?>
    <!-- END THEME STYLES -->

    <!-- BEGIN PAGE LEVEL STYLES -->
    <?php echo $this->Html->css("$cmsPluginName.assets/css/pages/tasks", array('media' => 'screen')); ?>
    <?php echo $this->Html->css("$cmsPluginName.assets/css/print", array('media' => 'print')); ?>
    <!-- END PAGE LEVEL STYLES -->

</head>
<body class="page-header-fixed page-sidebar-fixed page-footer-fixed">

    <?php echo $this->element("$cmsPluginName.header"); ?>

    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <?php echo $this->element("$cmsPluginName.sidebar"); ?>

        <!-- BEGIN PAGE -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-content-body">
                    <?php echo $this->fetch('content'); ?>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTAINER -->

    <!-- BEGIN FOOTER -->
    <div class="footer">
        <div class="footer-inner">
            <?php echo date('Y') ?> &copy; <?php echo $cmsDisplayName ?> by <?php echo $cmsAuthor ?>
        </div>
        <div class="footer-tools">
            <span class="go-top">
                <i class="fa fa-angle-up"></i>
            </span>
        </div>
    </div>
    <!-- END FOOTER -->


    <!-- BEGIN JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) -->

    <!-- BEGIN CORE PLUGINS -->
    <?php echo $this->Html->script(array(
        "$cmsPluginName.assets/plugins/jquery-1.10.2.min",
        "$cmsPluginName.assets/plugins/jquery-migrate-1.2.1.min",
        "$cmsPluginName.assets/plugins/bootstrap/js/bootstrap.min",
        "$cmsPluginName.assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min",
        "$cmsPluginName.assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min",
        "$cmsPluginName.assets/plugins/bootstrap-sessiontimeout/jquery.sessionTimeout.min",
    ));
    ?>
    <!--[if lt IE 9]>
    <?php echo $this->Html->script(array(
        "$cmsPluginName.assets/plugins/excanvas.min",
        "$cmsPluginName.assets/plugins/respond.min",
    ));
    ?>
    <![endif]-->

    <?php echo $this->Html->script(array(
        "$cmsPluginName.assets/plugins/jquery-slimscroll/jquery.slimscroll.min",
        "$cmsPluginName.assets/plugins/jquery.blockui.min",
        "$cmsPluginName.assets/plugins/jquery.cookie.min",
        "$cmsPluginName.assets/plugins/uniform/jquery.uniform.min",
    ));
    ?>
    <!-- END CORE PLUGINS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <?php echo $this->Html->script(array(
        "$cmsPluginName.assets/scripts/core/app",
        "$cmsPluginName.assets/scripts/custom/index",
        "$cmsPluginName.assets/scripts/custom/tasks",
    ));
    ?>
    <!-- END PAGE LEVEL SCRIPTS -->

    <script>
        jQuery(document).ready(function() {
            App.init(); // initlayout and core plugins
            $.sessionTimeout({
                title: 'Session Timeout Notification',
                message: 'Sua sessão está quase expirando. Você será desconectado em 2 minutos',
                keepAliveUrl: '<?php echo $this->here; ?>',
                redirUrl: '<?php echo $this->Html->url(['controller' => 'CmsUsers', 'action' => 'login']); ?>',
                logoutUrl: '<?php echo $this->Html->url(['controller' => 'CmsUsers', 'action' => 'logout']); ?>',
                warnAfter: <?php echo ini_get('session.gc_maxlifetime') * 1000 - ( 2 * 10000 ); ?>, // warn before 2 minutes left
                redirAfter: <?php echo ini_get('session.gc_maxlifetime') * 1000; ?> // redirect when session is over.
            });
        });
    </script>

    <?php echo $this->fetch('script'); ?>

    <!-- History.js -->
    <?php echo $this->Html->script('//browserstate.github.io/history.js/scripts/bundled/html4+html5/jquery.history.js'); ?>

    <!-- END JAVASCRIPTS -->


</body>
</html>