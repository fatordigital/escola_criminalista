<?php
/** @var $this View */
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <title><?php echo $title_for_layout; ?></title>

    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <base href="<?php echo $this->webroot . Inflector::underscore($cmsPluginName); ?>/" />

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <?php
    echo $this->Html->css(array(
        "$cmsPluginName.assets/plugins/font-awesome/css/font-awesome.min",
        "$cmsPluginName.assets/plugins/bootstrap/css/bootstrap.min",
        "$cmsPluginName.assets/plugins/uniform/css/uniform.default",
    ));
    ?>
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL STYLES -->
    <?php
    echo $this->Html->css(array(
        "$cmsPluginName.assets/css/pages/login-soft",
    ));
    ?>
    <!-- END PAGE LEVEL STYLES -->

    <!-- BEGIN THEME STYLES -->
    <?php
    echo $this->Html->css(array(
        "$cmsPluginName.assets/css/style-metronic",
        "$cmsPluginName.assets/css/style",
        "$cmsPluginName.assets/css/style-responsive",
        "$cmsPluginName.assets/css/plugins",
        "$cmsPluginName.assets/css/themes/default",
        "$cmsPluginName.assets/css/pages/login-soft",
        "$cmsPluginName.assets/css/custom",
    ));
    ?>
    <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
    <h2 class="page-title"><?php echo $cmsDisplayName ?></h2>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    <?php echo $this->fetch('content'); ?>
</div>
<!-- END LOGIN -->

<!-- BEGIN COPYRIGHT -->
<div class="copyright">
    <?php echo date('Y') ?> &copy; <?php echo $cmsDisplayName ?> by <?php echo $cmsAuthor ?>
</div>
<!-- END COPYRIGHT -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

<!-- BEGIN CORE PLUGINS -->


<!--[if lt IE 9]>
<?php echo $this->Html->script(array(
    "$cmsPluginName.assets/plugins/excanvas.min",
    "$cmsPluginName.assets/plugins/respond.min",
));
?>
<![endif]-->

<?php echo $this->Html->script(array(
    "$cmsPluginName.assets/plugins/jquery-1.10.2.min",
    "$cmsPluginName.assets/plugins/jquery-migrate-1.2.1.min",
    "$cmsPluginName.assets/plugins/bootstrap/js/bootstrap.min",
    "$cmsPluginName.assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min",
    "$cmsPluginName.assets/plugins/jquery-slimscroll/jquery.slimscroll.min",
    "$cmsPluginName.assets/plugins/jquery.blockui.min",
    "$cmsPluginName.assets/plugins/jquery.cookie.min",
    "$cmsPluginName.assets/plugins/uniform/jquery.uniform.min",
));
?>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<?php echo $this->Html->script(array(
    "$cmsPluginName.assets/plugins/jquery-validation/dist/jquery.validate.min",
    "$cmsPluginName.assets/plugins/backstretch/jquery.backstretch.min",
    "$cmsPluginName.assets/plugins/select2/select2.min",
));
?>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<?php echo $this->Html->script(array(
    "$cmsPluginName.assets/scripts/app",
    "$cmsPluginName.assets/scripts/login-soft",
));
?>
<!-- END PAGE LEVEL SCRIPTS -->

<script>
    jQuery(document).ready(function() {
        App.init();
        Login.init();
    });
</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>