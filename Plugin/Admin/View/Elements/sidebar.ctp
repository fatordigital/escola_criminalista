<?php
/** @var $this View */
?>

<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu">
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler hidden-phone">
                </div>
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            </li>
            <li class="sidebar-search-wrapper">

                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                <form class="sidebar-search" action="extra_search.html" method="POST">
                    <div class="form-container">
                        <div class="input-box">
                            <a href="javascript:;" class="remove"></a>
                            <input type="text" placeholder="Search..."/>
                            <input type="button" class="submit" value=" "/>
                        </div>
                    </div>
                </form>
                <!-- END RESPONSIVE QUICK SEARCH FORM -->

            </li>
            <li class="start">
                <?php echo $this->Html->link(
                    '<i class="icon-home"></i> <span class="title">' . __d('admin', 'Dashboard') . '</span><span class="selected"></span>',
                    array(
                        'plugin'     => false,
                        'prefix'     => $cmsRoutingPrefix,
                        'controller' => 'Pages',
                        'action'     => 'dashboard'
                    ), array('escape' => false)); ?>
            </li>
            <li>
                <a href="javascript:;">
                    <span class="title"><i
                            class="fa fa-user"></i> <?php echo __d('admin', 'System Users') ?>
                    </span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <?php
                        echo $this->Html->link(
                            __d('admin', 'List System Users'),
                            array(
                                'plugin'     => $cmsPluginName,
                                'prefix'     => $cmsRoutingPrefix,
                                'controller' => 'CmsUsers',
                                'action'     => 'index',
                            )
                        );
                        ?>
                    </li>
                    <li>
                        <?php
                        echo $this->Html->link(
                            __d('admin', 'Create System User'),
                            array(
                                'plugin'     => $cmsPluginName,
                                'prefix'     => $cmsRoutingPrefix,
                                'controller' => 'CmsUsers',
                                'action'     => 'add',
                            )
                        );
                        ?>
                    </li>
                </ul>
            </li>
        </ul>
        <!--END SIDEBAR MENU-->
    </div>
</div>
<!--END SIDEBAR-->
