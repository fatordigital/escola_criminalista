<?php /** @var $this View */ ?>

<!-- BEGIN HEADER -->
<div class="header navbar navbar-inverse navbar-fixed-top">

    <!-- BEGIN TOP NAVIGATION BAR -->
    <div class="header-inner">

        <!-- BEGIN LOGO -->
        <a class="navbar-brand" href="<?php echo $this->Html->Url('/fatorcms', true); ?>">
            <!--<img src="assets/img/logo.png" alt="logo" class="img-responsive"/>-->
            &nbsp;&nbsp;&nbsp;<span><?php echo $cmsDisplayName; ?></span>
        </a>
        <!-- END LOGO -->

        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <?php echo $this->Html->image("$cmsPluginName.assets/img/menu-toggler.png"); ?>
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->

        <!-- BEGIN TOP NAVIGATION MENU -->
        <ul class="nav navbar-nav pull-right">
            <!-- BEGIN USER LOGIN DROPDOWN -->
            <li class="dropdown user">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                   data-close-others="true">
                    <!-- Avatar -->

        <span class="username">
             <?php if (isset($loggedInUser) && isset($loggedInUser['name'])) { echo $loggedInUser['name']; } ?>
        </span>
                    <i class="fa fa-angle-down"></i>
                </a>

                <ul class="dropdown-menu">
                    <li>
                        <?php echo $this->Html->link(
                            sprintf('<i class="fa fa-key"></i> %s</a>', __d('SolutionsCMS', 'Log Out')),
                            array(
                                'controller' => 'cms_users',
                                'action'     => 'logout'
                            ),
                            array('escape' => false)
                        ); ?>
                    </li>
                </ul>
            </li>
            <!-- END USER LOGIN DROPDOWN -->
        </ul>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
<div class="clearfix"></div>
