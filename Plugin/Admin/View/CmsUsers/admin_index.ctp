<?php
/**
 * @var $this View
 */
$this->Html->addCrumb(__d('users', 'Users'));
$this->Html->addCrumb(__d('users', 'Listing'));
?>

<div class="row">
    <div class="col-md-12">
        <h2 class="page-title"><?php echo __d('users', 'System Users'); ?></h2>
        <?php echo $this->Html->getCrumbList(
            array(
                'class'     => 'breadcrumb',
                'separator' => '<i class="icon-angle-right"></i>',
                'escape'    => false
            ),
            'Home'
        );
        ?>
    </div>
    <div class="clearfix"></div>
</div>

<div class="row">
    <div class="col-md-12">
        <?php echo $this->Session->flash(); ?>
    </div>
    <div class="clearfix"></div>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="portlet box blue">

            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>&nbsp;<?php echo __d('users', 'View the registered users on the system'); ?>

                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                        <a href="javascript:;" class="reload"></a>
                        <a href="javascript:;" class="remove"></a>
                    </div>
                </div>
            </div>

            <div class="portlet-body">

                <div class="table-toolbar">
                    <?php echo $this->Html->link(
                        sprintf('%s <i class="fa fa-plus"></i>', __d('users', 'Add New')),
                        array('action' => 'add'),
                        array(
                            'escape' => false,
                            'class'  => 'btn green ajaxify'
                        )
                    ); ?>
                </div>

                <table class="table table-hover table-bordered table-striped table-condensed flip-content"
                       data-model="CmsUser">
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Usuário</th>
                        <th>E-mail</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($cmsUsers) :
                        foreach ($cmsUsers as $cmsUser) : ?>
                            <tr>
                                <td><?php echo h($cmsUser['CmsUser']['name']); ?></td>
                                <td><?php echo h($cmsUser['CmsUser']['username']); ?></td>
                                <td><?php echo h($cmsUser['CmsUser']['email']); ?></td>
                                <td>
                                    <?php echo $this->Html->link(
                                            sprintf('<i class="fa fa-info"></i>&nbsp;&nbsp;%s</a>', __d('users', 'View')),
                                            array(
                                                'action' => 'view',
                                                $cmsUser['CmsUser']['id']
                                            ),
                                            array(
                                                'escape'         => false,
                                                'data-id'        => $cmsUser['CmsUser']['id'],
                                                'class'          => 'btn btn-sm blue btn-editable ajaxify',
                                                'data-gravity'   => 's',
                                                'original-title' => 'Visualizar'
                                            )
                                        )
                                        . str_repeat('&nbsp;', 8)
                                        . $this->Html->link(
                                            sprintf('<i class="fa fa-edit"></i>&nbsp;&nbsp;%s</a>', __d('users', 'Edit')),
                                            array(
                                                'action' => 'edit',
                                                $cmsUser['CmsUser']['id']
                                            ),
                                            array(
                                                'escape'         => false,
                                                'data-id'        => $cmsUser['CmsUser']['id'],
                                                'class'          => 'btn btn-sm green btn-editable ajaxify',
                                                'data-gravity'   => 's',
                                                'original-title' => 'Editar'
                                            )
                                        )
                                        . str_repeat('&nbsp;', 8)
                                        . $this->Form->postLink(
                                            sprintf('<i class="fa fa-times"></i>&nbsp;&nbsp;%s</a>', __d('users', 'Delete')),
                                            array(
                                                'action' => 'delete',
                                                $cmsUser['CmsUser']['id']
                                            ),
                                            array(
                                                'escape'         => false,
                                                'data-id'        => $cmsUser['CmsUser']['id'],
                                                'class'          => 'btn btn-sm red btn-removable ajaxify',
                                                'data-gravity'   => 's',
                                                'original-title' => 'Remove'
                                            ),
                                            __d('users', 'Are you sure you want to delete the "%s" user?', $cmsUser['CmsUser']['name'])
                                        ); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>

            </div>
        </div>

        <!--<?php echo $this->DataTable->render(
			'CmsUser' ,
			array(
				'class'           => 'dynamic styled with-prev-next' ,
				'data-data-table' => '{"sAjaxSource":"' . $this->webroot . 'solutionscms/cms_users?model=CmsUser","bServerSide":true}' ,
				// 'tbody'           => $cmsUsers
			)
			/*array(
				'sAjaxSource' => $this->webroot . 'solutionscms/cms_users?model=CmsUser' ,
				'bServerSide' => TRUE
			)*/
		); ?>-->
    </div>
    <div class="clearfix"></div>
</div>
