<?php /** @var $this View */ ?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-reorder"></i> Vendo dados do Usuário: <?php echo $cmsUser['CmsUser']['username']; ?>
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" class="config">
            </a>
            <a href="javascript:;" class="reload">
            </a>
            <a href="javascript:;" class="remove">
            </a>
        </div>
    </div>
    <?php //var_dump($cmsUser); die; ?>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form class="form-horizontal" role="form">
            <div class="form-body">

                <h3 class="form-section">Informações do Usuário</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Usuário:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $cmsUser['CmsUser']['username']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Nome:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $cmsUser['CmsUser']['name']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">E-mail:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $cmsUser['CmsUser']['email']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Criado:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $this->Time->format('d/m;Y', $cmsUser['CmsUser']['created']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>

            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-3 col-md-9">
                            <?php echo $this->Html->link(
                                __d('users', 'Go back'),
                                array('action' => 'index'),
                                array('escape' => false, 'class' => 'btn default')
                            ); ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>
