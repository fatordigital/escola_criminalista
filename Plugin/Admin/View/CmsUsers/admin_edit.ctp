<?php
/**
 * @var $this View
 */
$this->Html->addCrumb(__d('users', 'Users'));
$this->Html->addCrumb(__d('users', 'Editing %s User', $this->Form->value('CmsUser.name')));
?>


<div class="row">

    <div class="col-md-12">
        <h2 class="page-title"><?php echo __d('users', 'Editing System User'); ?></h2>
        <?php echo $this->Html->getCrumbList(
            array(
                'class' => 'breadcrumb',
                'separator' => '<i class="icon-angle-right"></i>',
                'escape' => false
            ),
            'Home'
        );
        ?>
    </div>
</div>

<div class="row">
    <div class="col col-md-12">

        <?php echo $this->Session->flash(); ?>

        <div class="portlet box blue">

            <div class="portlet-title">
                <div class="caption">
                    <?php echo __d('users', 'Edit the System User information'); ?>
                </div>
            </div>

            <div class="portlet-body form">

                <div class="form-body">

                    <?php echo $this->Form->create(
                        'CmsUser',
                        array(
                            'inputDefaults' => array(
                                'div' => 'form-group',
                                'wrapInput' => 'col-md-4',
                                'label' => array(
                                    'class' => 'col col-md-3 control-label'
                                ),
                                'class' => 'form-control',
                                'novalidate' => 'novalidate'
                            ),
                            'class' => 'form-horizontal',
                        )
                    ); ?>

                    <?php
                    echo $this->Form->hidden('id');

                    echo $this->Form->input('name', array(
                        'label' => array(
                            'text' => __d('users', 'Full Name')
                        )
                    ));
                    echo $this->Form->input('username', array(
                        'label' => array(
                            'text' => __d('users', 'Username')
                        )
                    ));
                    echo $this->Form->input('password', array(
                        'placeholder' => __d('users', 'Leave blank if you do not want to change'),
                        'beforeInput' => '<i class="fa fa-lock"></i>',
                        'wrapInput' => 'input-icon col-md-4',
                        'label' => array(
                            'text' => __d('users', 'Password')
                        )
                    ));
                    echo $this->Form->input('email', array(
                        'beforeInput' => '<i class="fa fa-envelope"></i>',
                        'wrapInput' => 'input-icon col-md-4',
                        'label' => array(
                            'text' => __d('users', 'E-mail')
                        )
                    ));
                    ?>

                    <div class="form-actions">
                        <?php echo $this->Form->submit(__d('users', 'Update'), array(
                            'div' => false,
                            'class' => 'btn blue',
                            'escape' => false,
                        )); ?>

                        <?php echo $this->Form->postLink(
                            __d('users', 'Delete %s', '<i class="fa fa-times"></i>'),
                            array(
                                'action' => 'delete',
                                $this->Form->value('CmsUser.id')
                            ),
                            array('class' => 'btn red', 'escape' => false),
                            __d('users', 'Are you sure you want to delete %s # %s?', __d('users', 'System User'), $this->Form->value('CmsUser.id'))
                        ); ?>


                    </div>

                    <?php echo $this->Form->end(); ?>

                </div>

            </div>

        </div>
    </div>
</div>