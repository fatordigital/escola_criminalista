<?php
/**
 * @var $this View
 */
$this->Html->addCrumb(__d('users', 'Users'));
$this->Html->addCrumb(__d('users', 'New User'));
?>


<div class="row">

    <div class="col-md-12">
        <h2 class="page-title"><?php echo __d('users', 'New System User'); ?></h2>
        <?php echo $this->Html->getCrumbList(
            array(
                'class'     => 'breadcrumb',
                'separator' => '<i class="icon-angle-right"></i>',
                'escape'    => false
            ),
            'Home'
        );
        ?>
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <?php echo $this->Session->flash(); ?>

        <div class="portlet box blue">

            <div class="portlet-title">
                <div class="caption"><?php echo __d('users', 'Fill in the new user details'); ?></div>
            </div>

            <div class="portlet-body form">

                <div class="form-body">
                    <?php echo $this->Form->create(
                        'CmsUser',
                        array(
                            'inputDefaults' => array(
                                'div'        => 'form-group',
                                'wrapInput'  => 'col-md-4',
                                'label'      => array(
                                    'class' => 'col-md-3 control-label'
                                ),
                                'class'      => 'form-control',
                                'novalidate' => 'novalidate'
                            ),
                            'class'         => 'form-horizontal',
                            'role'          => 'form'
                        )
                    ); ?>

                    <?php
                    echo $this->Form->input('name', array(
                        'label' => array(
                            'text' => __d('users', 'Full Name')
                        )
                    ));
                    echo $this->Form->input('username', array(
                        'label' => array(
                            'text' => __d('users', 'Username')
                        )
                    ));
                    echo $this->Form->input('password', array(
                        'beforeInput' => '<i class="fa fa-lock"></i>',
                        'wrapInput' => 'input-icon col-md-4',
                        'label' => array(
                            'text' => __d('users', 'Password')
                        ),
                        'required' => true,
                    ));
                    echo $this->Form->input('email', array(
                        'beforeInput' => '<i class="fa fa-envelope"></i>',
                        'wrapInput' => 'input-icon col-md-4',
                        'label' => array(
                            'text' => __d('users', 'E-mail')
                        )
                    ));
                    ?>

                    <div class="form-actions">
                        <?php echo $this->Form->submit(__d('users', 'Submit'), array(
                            'div'    => false,
                            'class'  => 'btn blue',
                            'escape' => false,
                        )); ?>
                    </div>

                    <?php echo $this->Form->end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>