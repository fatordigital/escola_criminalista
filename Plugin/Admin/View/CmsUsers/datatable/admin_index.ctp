<?php
/**
 * @var $this View
 */

foreach($dtResults as $result) {
	$this->dtResponse['aaData'][] = array(
		$result['CmsUser']['username'],
		$result['CmsUser']['email'],
		$this->Html->link(
			__( '<i class="icon-eye-open"></i>') ,
			array( 'action' => 'view' , $result['CmsUser']['id'] ) ,
			array( 'escape' => FALSE , 'class' => 'button small grey tooltip' , 'data-gravity' => 's' , 'original-title' => 'Visualizar' )
		)
			. str_repeat('&nbsp;' , 8 )
			. $this->Html->link(
			__( '<i class="icon-pencil"></i>' ) ,
			array( 'action' => 'edit' , $result['CmsUser']['id'] ) ,
			array( 'escape' => FALSE , 'class' => 'button small grey tooltip' , 'data-gravity' => 's' , 'original-title' => 'Editar' )
		),
	);
}