<?php /** @var $this View */ ?>

<?php echo $this->Form->create('CmsUsers', array(
    'inputDefaults' => array(
        'div' => 'form-group',
        'wrapInput' => false,
        'class' => 'form-vertical login-form register-form',
        'novalidate' => 'novalidate'
    )
)); ?>

    <h3 class="form-title"><?php echo __d('users', 'Login to your account') ?></h3>

    <?php echo $this->Session->flash(); ?>
    <?php echo $this->Session->flash('auth'); ?>

    <?php echo $this->Form->input('CmsUser.username', array(
        'label' => array(
            'class' => 'control-label visible-ie8 visible-ie9',
            'text' =>  __d('users', 'Username'),
        ),
        'class' => 'form-control placeholder-no-fix',
        'wrapInput' => 'input-icon',
        'beforeInput' => '<i class="fa fa-user"></i>',
        'placeholder' => __d('users', 'Username'),
        'autocomplete' => 'on'
    )); ?>

    <?php echo $this->Form->input('CmsUser.password', array(
        'label' => array(
            'class' => 'control-label visible-ie8 visible-ie9',
            'text' =>  __d('users', 'Password'),
        ),
        'class' => 'form-control placeholder-no-fix',
        'wrapInput' => 'input-icon',
        'beforeInput' => '<i class="fa fa-lock"></i>',
        'placeholder' => __d('users', 'Password'),
        'autocomplete' => 'on'
    )); ?>

    <div class="form-actions">

        <?php /*echo $this->Form->input('CmsUsers.remember', array(
            'type' => 'checkbox',
            'label' => array(
                'text' => __d('admin', 'Remember me'),
            ),
            'class' => false,
            'div' => false
        )); */?>

        <?php echo $this->Form->button(__d('users', 'Login %s', '<i class="m-icon-swapright m-icon-white"></i>'), array(
            'div' => false,
            'class' => 'btn blue pull-right',
            'escape' => false,
        )); ?>

    </div>

<?php echo $this->Form->end(); ?>