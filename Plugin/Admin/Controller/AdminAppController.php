<?php

App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');

class AdminAppController extends AppController
{

    public $availableControllers;

    public $helpers = array(
        'Session',
        'Html'      => array('className' => 'BoostCake.BoostCakeHtml'),
        'Form'      => array('className' => 'BoostCake.BoostCakeForm'),
        'Paginator' => array('className' => 'BoostCake.BoostCakePaginator'),
    );

    public $components = array(
        // 'DebugKit.Toolbar',
        'Session',
        'Auth' => array(
            'loginAction'    => array(
                'controller' => 'CmsUsers',
                'action'     => 'login'
            ),
            'loginRedirect'  => array(
                'plugin'     => false,
                'controller' => '',
                'action'     => 'index'
            ),
            'logoutRedirect' => array(
                'plugin'       => false,
                'solutionscms' => false,
                'controller'   => 'pages',
                'action'       => 'display',
                'home'
            ),
            'authError'      => 'Você não tem permissão para acessar esta página!',
            'authenticate'   => array(
                'Form' => array(
                    'userModel' => 'CmsUser',
                ),
            ),
            'flash' => array(
                'params' => array(
                    'class'  => 'alert alert-warning'
                ),
                'key' => 'auth',
                'element' => 'default'
            )
        ),
    );

    public function beforeRender()
    {
        parent::beforeRender();
    }

    public function beforeFilter()
    {
        parent::beforeFilter();

        if (is_object($this->Auth)) {
            $this->Auth->allow([
                'solutionscms_letmein',
            ]);
            if ($this->Auth->user('id') === null) {
                $this->set('loggedInUser', $this->CmsUser->findByUsername($this->Auth->user()['CmsUser']['username']));
            } else {
                $this->set('loggedInUser', $this->Auth->user());
            }
        }

        // if (Configure::read('debug') > 0) {
            // $this->components[] = 'DebugKit.Toolbar';
        // }

        // $this->Security->csrfCheck = false;
        // $this->Security->authCheck = false;
        // $this->Security->blackHoleCallback = 'securityErrorHandle';

        // Allow the login action over the AuthComponent
        // $this->Auth->allow( 'login' );
        // $this->Auth->allow( 'add' );

        $this->discoverAvailableControllers();

    }

    public function cmsSettings()
    {

        if (isset($this->request->params['prefix']) && $this->request->params['prefix'] == 'admin') {
            AuthComponent::$sessionKey = 'Auth.Admin';// solution from http://bit.ly/1pMoolz

            $this->Auth->loginAction    = array(
                'plugin'     => 'Admin',
                'controller' => 'CmsUsers',
                'action'     => 'login'
            );
            $this->Auth->loginRedirect  = array(
                'plugin'     => 'Admin',
                'controller' => 'CmsUsers',
                'action'     => 'index'
            );
            $this->Auth->logoutRedirect = array(
                'plugin'       => false,
                'solutionscms' => false,
                'controller'   => 'pages',
                'action'       => 'display',
                'home'
            );
            $this->Auth->authenticate   = array(
                'Form' => array(
                    'userModel' => 'CmsUser',
                )
            );
            $this->Auth->authError      = 'Você não tem permissão para acessar esta página!';
            $this->Auth->flash          = array(
                'params'  => array(
                    'class' => 'alert alert-warning'
                ),
                'key'     => 'auth',
                'element' => 'default'
            );
            $this->Auth->allow('login');
        }

    }


    public function securityErrorHandle($type)
    {
        var_dump($type);
        die;
    }

    // Loads the controllers available under the SolutionsCMS directory.
    protected function discoverAvailableControllers()
    {
        $dir         = new Folder('../Plugin/SolutionsCMS/Controller');
        $controllers = $dir->find('^(?!SolutionsCMS).*\.php');

        foreach ($controllers as $controller) {
            $this->availableControllers[] = explode('Controller', $controller, 2)[0];
        }

        $this->set('availableControllers', $this->availableControllers);

    }

}
