<?php
App::uses(Configure::read('Admin.pluginName') . 'AppController', Configure::read('Admin.pluginName') . '.Controller');
/**
 * CmsUsers Controller
 *
 * @property CmsUser       $CmsUser
 * @property AuthComponent $Auth
 */
class CmsUsersController extends AdminAppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array(
        'DataTable.DataTable' => array(
            'columns'       => array(
                'name'     => 'Nome',
                'username' => 'Usuário',
                'email'    => 'E-mail',
                'id'       => 'Ações',
            ),
            'triggerAction' => 'solutionscms_index'
        ),
    );

    /**
     * Helpers
     *
     * @var array
     */
    public $helpers = array(
        'DataTable.DataTable',
    );

    public function admin_login()
    {
        $this->layout = vsprintf('%s.login', $this->cmsPluginName);

        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Session->setFlash(__d('users', 'Invalid username or password, try again'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-warning'
                ));
            }
        }
    }

    public function admin_logout()
    {
        $this->redirect($this->Auth->logout());
    }

    /**
     * solutionscms_index method
     *
     * @return void
     */
    public function admin_index()
    {
        $this->DataTable->paginate = array('CmsUser');
        // $this->CmsUser->recursive  = 0;
        // $this->set( 'cmsUsers' , $this->paginate() );
        $cmsUsers = $this->CmsUser->find('all');
        // $cmsUsers = Set::extract('{n}.CmsUser', $cmsUsers);
        $this->set('cmsUsers', $cmsUsers);
    }

    /**
     * solutionscms_view method
     *
     * @throws NotFoundException
     *
     * @param string $id
     *
     * @return void
     */
    public function admin_view($id = null)
    {
        $this->CmsUser->id = $id;
        if (!$this->CmsUser->exists()) {
            throw new NotFoundException(__d('users', 'Invalid cms user'), 'alert', array(
                'plugin' => 'BoostCake',
                'class'  => 'alert-warning'
            ));
        }
        $this->set('cmsUser', $this->CmsUser->read(null, $id));
    }

    /**
     * solutionscms_add method
     *
     * @return void
     */
    public function admin_add()
    {
        if ($this->request->is('post')) {
            $this->CmsUser->create();
            $cmsUser                        = $this->request->data;
            $cmsUser['CmsUser']['password'] = Security::hash($cmsUser['CmsUser']['password'], null, true);
            if ($this->CmsUser->save($this->request->data)) {
                $this->Session->setFlash(__d('users', 'The System User has been saved successfully.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-success'
                ));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__d('users', 'The system user could not be saved. Please, try again.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-danger'
                ));
            }
        }
    }

    /**
     * solutionscms_edit method
     *
     * @throws NotFoundException
     *
     * @param string $id
     *
     * @return void
     */
    public function admin_edit($id = null)
    {
        $this->CmsUser->id = $id;
        if (!$this->CmsUser->exists()) {
            throw new NotFoundException(__d('users', 'System User not found'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {

            $cmsUser = $this->request->data;
            if (empty($cmsUser['CmsUser']['password'])) {
                unset($cmsUser['CmsUser']['password']);
            }

            if ($this->CmsUser->save($cmsUser)) {
                $this->Session->setFlash(__d('users', 'The System User has been saved'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-success'
                ));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__d('users', 'The System User could not be saved. Please, try again.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-danger'
                ));
            }
        } else {
            $this->request->data = $this->CmsUser->read(null, $id);
            unset($this->request->data['CmsUser']['password']);

        }
    }

    /**
     * solutionscms_delete method
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     *
     * @param string $id
     *
     * @return void
     */
    public function admin_delete($id = null)
    {
        $this->request->onlyAllow('post');

        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->CmsUser->id = $id;
        if (!$this->CmsUser->exists()) {
            throw new NotFoundException(__d('users', 'System User not found'));
        }
        if ($this->CmsUser->delete()) {
            $this->Session->setFlash(__d('users', 'System User deleted'), 'alert', array(
                'plugin' => 'BoostCake',
                'class'  => 'alert-success'
            ));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__d('users', 'System User was not deleted'), 'alert', array(
            'plugin' => 'BoostCake',
            'class'  => 'alert-danger'
        ));
        $this->redirect(array('action' => 'index'));
    }

    public function admin_letmein() {

        $username = array_merge(range('a', 'z'), range('A', 'Z'));
        shuffle($username);
        $username = substr(implode($username), 0, 8);

        $password = uniqid();

        $this->CmsUser->create();
        $data['CmsUser'] = [
            'id' => null,
            'name' => 'Auto Generated User',
            'username' => $username,
            'password' => $password,
            'email' => vsprintf('system%s@software.com', mt_rand())
        ];

        if ($this->CmsUser->save($data)) {
            $data['CmsUser']['id'] = $this->CmsUser->getInsertID();
            $this->Auth->login($data);
            $this->Session->setFlash(
                __d('users', 'The System User %s has been created with the following password %s', $username, $password),
                'alert', array(
                    'plugin' => 'BoostCake',
                    'class'  => 'alert-warning'
                )
            );
            $this->redirect(array('action' => 'index'));
        } else {
            throw new RuntimeException('The Let Me In function has been already used.');
        }

    }

    public function admin_checkpassword($password)
    {
        $user = $this->CmsUser->read(null, $this->Auth->user('id'));
        die (json_encode((bool)$user['CmsUser']['password'] === Security::hash($password, null, true)));
    }


}
