<?php

/**
 * @var $this View
 */
$this->Html->addCrumb(__d('users', 'Orders'));

?>

<div class="row">
    <div class="col-md-12">
        <h2 class="page-title"><?php echo __d('users', 'Admin Index Order'); ?></h2>
        <?php echo $this->Html->getCrumbList(
            array(
                'class' => 'breadcrumb',
                'separator' => '<i class="icon-angle-right"></i>',
                'escape' => false
            ),
            'Home'
        );
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <?php echo $this->Session->flash(); ?>

        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>&nbsp;<?php echo __d('users', 'Admin Index the Order information'); ?>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                        <a href="javascript:;" class="reload"></a>
                        <a href="javascript:;" class="remove"></a>
                    </div>
                </div>
            </div>

            <div class="portlet-body">



                <table class="table table-hover table-bordered table-striped table-condensed flip-content"
                       data-model="User">
                    <thead>
                    <tr>

                        <th><?php echo $this->Paginator->sort('first_name'); ?></th>
                        <th><?php echo $this->Paginator->sort('title'); ?></th>
                        <th><?php echo $this->Paginator->sort('total'); ?></th>
                        <th><?php echo $this->Paginator->sort('status'); ?></th>
                        <th><?php echo $this->Paginator->sort('created'); ?></th>
                        <th class="actions"><?php echo __d('users', 'Actions'); ?></th>
                    </tr>
                    </thead>

                    <?php foreach ($orders as $order): ?>
                        <tr>
                            <td><?php echo h($order['User']['first_name']); ?>&nbsp;</td>
                            <td><?php echo h(($order['Movie']['title'] != "") ? $order['Movie']['title'] : "Assinatura"); ?>&nbsp;</td>
                            <td><?php echo h($this->Number->currency($order['Order']['total'])); ?>&nbsp;</td>
                            <td><?php echo h($status[$order['Order']['status']]); ?>&nbsp;</td>
                            <td><?php echo h($order['User']['created']); ?>&nbsp;</td>
                            <td class="actions">
                                <?php echo $this->Html->link(
                                    sprintf('<i class="fa fa-info"></i>&nbsp;&nbsp;%s</a>',__d('users', 'View')),
                                    array('action' => 'view', $order['Order']['id']),
                                    array(
                                        'escape' => false,
                                        'data-id' => $order['Order']['id'],
                                        'class' => 'btn btn-sm blue btn-editable',
                                        'original-title' =>__d('users', 'View')
                                    )
                                ); ?>

                            </td>
                        </tr>
                    <?php endforeach; ?>

                </table>

                  <?php if($this->params['paging']['Order']['pageCount'] > 1): ?>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center clear">
                            <ul class="pagination customizada">
                                <?php echo $this->Paginator->prev( '«', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li' ) ); ?>
                                <?php echo $this->Paginator->numbers(['currentClass' => 'active', 'tag' => 'li']); ?>
                                <?php  echo $this->Paginator->next( '»', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li' ) ); ?>
                            </ul>
                        </div>
                    </div>
                    <p style="width: 100%; text-align: center;"><?php echo $this->Paginator->counter(array(
                        'format' => 'Página {:page} de {:pages}, exibindo {:current} registros de um total de {:count} registros.'
                    )); ?>
                    </p>
                <?php endIf; ?>

            </div>
        </div>
    </div>
</div>
