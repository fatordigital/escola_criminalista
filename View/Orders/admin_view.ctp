<?php /** @var $this View */ ?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-reorder"></i> Vendo dados do Pedido: <?php echo $order['Order']['id']; ?>
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" class="config">
            </a>
            <a href="javascript:;" class="reload">
            </a>
            <a href="javascript:;" class="remove">
            </a>
        </div>
    </div>
    <?php // var_dump($order); die; ?>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form class="form-horizontal" role="form">
            <div class="form-body">

                <h3 class="form-section">Informações do Pedido</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID da Transação:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $order['Order']['transaction_id']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Forma de Pagamento:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $paymentMethod[ $order['Order']['payment_method_type']]; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Referência:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $order['Order']['reference']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Status:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $status[ $order['Order']['status']]; ?>
                                </p>
                            </div>
                        </div>
                    </div>

                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Valor:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $order['Order']['total']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Taxas:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $order['Order']['taxes']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Criado:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $this->Time->format('d/m/Y', $order['Order']['created']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Pago:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $this->Time->format('d/m/Y', $order['Order']['paid']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <h3 class="form-section">Informações do Usuário</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Nome completo:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $order['User']['first_name']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">E-mail:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $order['User']['email']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <!--/row-->
                </div>
                <h3 class="form-section">Informações do Filme</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Título:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $order['Movie']['title']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Link:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $this->Text->autoLink($order['Movie']['link']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <!--/row-->
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-3 col-md-9">
                            <?php echo $this->Html->link(
                                __d('users', 'Go back'),
                                array('action' => 'index'),
                                array('escape' => false, 'class' => 'btn default')
                            ); ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>