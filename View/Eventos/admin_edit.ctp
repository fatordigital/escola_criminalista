<?php

/**
 * @var $this View
 */


$this->Html->addCrumb('Eventos');
$this->Html->addCrumb(__d('users', 'Editar %s Evento', $this->Form->value('Diadobem.entidade')));

?>

<?php $this->Html->script("$cmsPluginName.assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js", ['inline' => false]); ?>
<?php $this->Html->css("$cmsPluginName.assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css", ['inline' => false]); ?>

<?php $this->Html->script("$cmsPluginName.assets/plugins/ckeditor/ckeditor", ['inline' => false]); ?>
<div class="row">
    <div class="col-md-12">
        <h2 class="page-title">Editar Eventos</h2>
        <?php echo $this->Html->getCrumbList(
            array(
                'class'     => 'breadcrumb',
                'separator' => '<i class="icon-angle-right"></i>',
                'escape'    => false
            ),
            'Home'
        );
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <?php echo $this->Session->flash(); ?>
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                     Editar Informações do Evento
                </div>
            </div>

            <div class="portlet-body form movies">

                <?php echo $this->Form->create(
                    'Diadobem',
                    array(
                        'inputDefaults' => array(
                            'div'        => 'form-group',
                            'wrapInput'  => 'col col-md-9',
                            'label'      => array(
                            'class' => 'col col-md-3 control-label'
                            ),
                            'class'      => 'form-control',
                            'novalidate' => 'novalidate'
                        ),
                        'class'         => 'form-horizontal',
                        'type'          => 'file',
                    )
                ); ?>

                <div class="form-body">

                    <?php
                    echo $this->Form->hidden('id');
                    echo $this->Form->input('entidade', ['label' => 'Entidade','rows' => '1']);
                    echo $this->Form->input('cidade', ['label' => 'Cidade']);
                    echo $this->Form->input('descricao', ['label' => 'Descrição', 'class' => 'ckeditor']);
                    echo $this->Form->input('horario', ['label' => 'Horário']);
                    echo $this->Form->input('contato', ['label' => 'Dados de contato']);
                    echo $this->Form->input('image', ['label' => 'Imagem', 'type' => 'file']);
                    ?>
                    <?php if($evento != ""): ?>
                    <div class="form-group content-image-news">
                        <label class="col col-md-3 control-label"></label>
                        <div class="col col-md-9">
                            <img src="<?php echo $this->Html->Url('/files/'.$evento, true); ?>" alt="" width="150" />
                           
                        </div>
                    </div>
                    <?php 
                        endIf;
                    ?>
                    
                    <div class="form-actions">

                        <?php echo $this->Form->submit(__d('users', 'Submit'), array(
                            'div'    => false,
                            'class'  => 'btn blue',
                            'escape' => false,
                        )); ?>


                    </div>
                </div>
                <?php echo $this->Form->end(); ?>

            </div>
        </div>
    </div>
</div>

