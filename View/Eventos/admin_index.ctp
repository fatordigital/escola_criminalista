<?php

/**
 * @var $this View
 */
$this->Html->addCrumb(__d('users', 'Eventos'));

?>

<div class="row">
    <div class="col-md-12">
        <h2 class="page-title"><?php echo __d('users', 'Admin Index Order'); ?></h2>
        <?php echo $this->Html->getCrumbList(
            array(
                'class' => 'breadcrumb',
                'separator' => '<i class="icon-angle-right"></i>',
                'escape' => false
            ),
            'Home'
        );
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <?php echo $this->Session->flash(); ?>

        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>&nbsp;<?php echo __d('users', 'Admin Index the Order information'); ?>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                        <a href="javascript:;" class="reload"></a>
                        <a href="javascript:;" class="remove"></a>
                    </div>
                </div>
            </div>

            <div class="portlet-body">



                <table class="table table-hover table-bordered table-striped table-condensed flip-content"
                       data-model="User">
                    <thead>
                    <tr>
                        <th><?php echo $this->Paginator->sort('Código'); ?></th>
                        <th><?php echo $this->Paginator->sort('Nome/ Entidade'); ?></th>
                        <th><?php echo $this->Paginator->sort('Cidade'); ?></th>
                        <th><?php echo $this->Paginator->sort('Horário'); ?></th>
                        <th><?php echo $this->Paginator->sort('Contato'); ?></th>
                        <th><?php echo $this->Paginator->sort('Status'); ?></th>
                        <!--<th><?php echo $this->Paginator->sort('Data de Registro'); ?></th>-->
                        <th class="actions"><?php echo __d('users', 'Actions'); ?></th>
                    </tr>
                    </thead>

                    <?php foreach ($eventos as $evento): ?>
                        <tr>
                            <td><?php echo h($evento['Diadobem']['id']); ?>&nbsp;</td>
                            <td><?php echo h($evento['Diadobem']['entidade']); ?>&nbsp;</td>
                            <td><?php echo h($evento['Diadobem']['cidade']); ?>&nbsp;</td>
                            <td><?php echo h($evento['Diadobem']['horario']); ?>&nbsp;</td>
                            <td><?php echo h($evento['Diadobem']['contato']); ?>&nbsp;</td>
                            <td><?php if($evento['Diadobem']['ativo']==1): echo h('ativo'); else: echo h('inativo'); endif; ?>&nbsp;</td>
                            <!--<td><?php echo h(date("d/m/Y h:m:s", strtotime($evento['Diadobem']['dt_create']))); ?>&nbsp;</td>-->
                            <td class="actions">
                                <?php echo $this->Html->link(
                                    sprintf('<i class="fa fa-info"></i>&nbsp;&nbsp;%s</a>',__d('users', 'Ativar/Desativar')),
                                    array('action' => 'view', $evento['Diadobem']['id']),
                                    array(
                                        'escape' => false,
                                        'data-id' => $evento['Diadobem']['id'],
                                        'class' => 'btn btn-sm blue btn-editable',
                                        'original-title' =>__d('users', 'View')
                                    )
                                ); ?>
                                <?php echo $this->Html->link(sprintf('<i class="fa fa-edit"></i>&nbsp;&nbsp;%s</a>', __d('users', 'Edit')),

                                    array(
                                        'action' => 'edit',
                                        $evento['Diadobem']['id']
                                    ),
                                    array(
                                        'escape'         => false,
                                        'data-id'        => $evento['Diadobem']['id'],
                                        'class'          => 'btn btn-sm green btn-editable ajaxify',
                                        'original-title' => __d('users', 'Edit')
                                    )
                                ); ?>
                                <?php echo $this->Form->postLink(
                                    sprintf('<i class="fa fa-times"></i>&nbsp;&nbsp;%s</a>', __d('users', 'Delete')),
                                    array(
                                        'action' => 'delete',
                                        $evento['Diadobem']['id']),
                                    array(
                                        'escape'         => false,
                                        'class'          => 'btn btn-sm red btn-removable',
                                        'original-title' => __d('users', 'Delete')
                                    ),
                                    __d('users', 'Are you sure you want to delete # %s?', $evento['Diadobem']['id'])
                                );
                                ?>

                            </td>
                        </tr>
                    <?php endforeach; ?>

                </table>

                  <?php if($this->params['paging']['Diadobem']['pageCount'] > 1): ?>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center clear">
                            <ul class="pagination customizada">
                                <?php echo $this->Paginator->prev( '«', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li' ) ); ?>
                                <?php echo $this->Paginator->numbers(['currentClass' => 'active', 'tag' => 'li']); ?>
                                <?php  echo $this->Paginator->next( '»', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li' ) ); ?>
                            </ul>
                        </div>
                    </div>
                    <p style="width: 100%; text-align: center;"><?php echo $this->Paginator->counter(array(
                        'format' => 'Página {:page} de {:pages}, exibindo {:current} registros de um total de {:count} registros.'
                    )); ?>
                    </p>
                <?php endIf; ?>

            </div>
        </div>
    </div>
</div>
