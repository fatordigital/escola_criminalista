<?php /** @var $this View */ ?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-reevento"></i> Vendo dados do Pedido: <?php echo $evento['Diadobem']['id']; ?>
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" class="config">
            </a>
            <a href="javascript:;" class="reload">
            </a>
            <a href="javascript:;" class="remove">
            </a>
        </div>
    </div>
    <?php // var_dump($evento); die; ?>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form class="form-horizontal" role="form" method="post" action="../ativar">
            <div class="form-body">                
                <h3 class="form-section">Informações do Evento</h3>
                <?php if($evento['Diadobem']['ativo']==0): ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <input type="hidden" name="data[Diadobem][id]" value="<?php echo $evento['Diadobem']['id']; ?>">
                            <input type="hidden" name="data[Diadobem][status]" value="<?php echo $evento['Diadobem']['ativo']; ?>">
                            <button type="submit" class="btn pull-right">Ativar evento</button>
                        </div>
                    </div>
                <? else: ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <input type="hidden" name="data[Diadobem][id]" value="<?php echo $evento['Diadobem']['id']; ?>">
                            <input type="hidden" name="data[Diadobem][status]" value="<?php echo $evento['Diadobem']['ativo']; ?>">
                            <button type="submit" class="btn pull-right">Desativar evento</button>
                        </div>
                    </div>
                <? endif; ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Entidade / Nome:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $evento['Diadobem']['entidade']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Cidade</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $evento['Diadobem']['cidade']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <!--/row-->
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Horário:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $evento['Diadobem']['horario']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Dados de Contato</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $evento['Diadobem']['contato']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <!--/row-->
                </div>
                <div class="row">   
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Descrição:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $evento['Diadobem']['descricao']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Dados de Contato</label>
                            <div class="col-md-9">
                                <figure style="width:200px; height: 200px; overflow:hidden; bevento: 1px solid #ddd">
                                   <?php if(isset($evento['Diadobem']['imagem'])&&$evento['Diadobem']['imagem']!=""): ?>
                                        <img src="<?php echo $this->Html->Url('/files/'.$evento['Diadobem']['imagem'], true); ?>" class="img-responsive" width="200" >
                                   <?php endif; ?>
                                </figure>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <!--/row-->
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-3 col-md-9">
                            <?php echo $this->Html->link(
                                __d('users', 'Go back'),
                                array('action' => 'index'),
                                array('escape' => false, 'class' => 'btn default')
                            ); ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>