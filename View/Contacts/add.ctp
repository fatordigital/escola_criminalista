<?php /** @var $this View */ ?>

<?php /*
<section class="entre-em-contato hidden-sm hidden-xs">
    <div class="container">
        <div class="col-lg-8">
            <h1>Nós queremos a sua ajuda para construir uma casa sempre melhor. <span>Faça parte!</span></h1>
        </div>
    </div>
</section>
*/ ?>

<?php echo $this->Session->flash(); ?>
<section class="formulario-contato destaque" style="padding-top: 50px;">
    <div class="container">
        <div class="col-lg-4">
            <h2>
                Entre em contato<br>
            </h2>
            <p>
                por aqui, pelo e-mail 
                <?php echo $this->Html->link('contato@escoladecriminalistas.com.br',
                    'mailto:contato@escoladecriminalistas.com.br', ['title' => 'Envie um e-mail para contato@escoladecriminalistas.com.br']); ?>
                    <br>ou pelo telefone (051) 9903-7703
            </h2>
            </p>
        </div>

        <?php echo $this->Form->create('Contact', [
            'inputDefaults' => [
                'required'  => 'required',
                'div'       => false,
                'wrapInput' => false,
                'label'     => false
            ]
        ]); ?>

        <div class="col-lg-4">
			<?php echo $this->Form->input('teste', array('class' => 'teste_input', 'required' => false)); ?>
            <?php echo $this->Form->input('name', ['placeholder' => 'nome']); ?>
            <?php echo $this->Form->input('email', ['placeholder' => 'e-mail']); ?>
            <?php echo $this->Form->input('phone', ['placeholder' => 'telefone']); ?>

            <div class="row-fluid">
                <div class="container-fluid">
                    <div class="col-xs-6">
                        <?php /* echo $this->Form->select('state',
                            @$estados,
                            ['empty'   => 'Selecione seu estado']
                        );*/ ?>
                    </div>
                    <div class="col-xs-6">
                        <?php /*echo $this->Form->select('city',
                            @$cidades,
                            ['empty' => 'Selecione sua cidade']
                        ); */?>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

        </div>

        <div class="col-lg-4">
            <?php echo $this->Form->textarea('message', ['placeholder' => 'Escreva sua mensagem aqui']); ?>
        </div>

        <?php echo $this->Form->submit('Enviar', ['class' => 'btn btn-default']); ?>

        <?php echo $this->Form->end(); ?>

    </div>
</section>
