<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Emails.text
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>

<div>
	<table width="100%" align="center">
		<tbody>
			<tr>
				<td>
					<table width="650" align="center">
						<tbody>
							<tr>
								<td>
									<table width="650">
										<tbody>
											<tr>
												<td>
													<p style="font-size: 18px; line-height: 16px; margin: 5px 0 15px; color: #2F2F2F;">Ol&aacute;, <?php echo $nome; ?></p>
													<p style="font-size: 14px; color: #2F2F2F;">Para alterar sua senha acesse neste link: <?php echo $url; ?>.</p>
													<p style="font-size: 14px; color: #2F2F2F;">Se voc&ecirc; n&atilde;o quiser alterar sua senha desconsidere este email.</p>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</div>