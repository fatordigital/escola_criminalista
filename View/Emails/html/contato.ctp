<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Emails.text
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>

<div>
	<table width="100%" align="center">
		<tbody>
			<tr>
				<td>
					<table width="650" align="center">
						<tbody>
							<tr>
								<td>
									<table width="650">
										<tbody>
											<tr>
												<td>Nome: </td>
												<td><?php echo $name; ?></td>
											</tr>
											<tr>
												<td>E-mail: </td>
												<td><?php echo $email; ?></td>
											</tr>
											<tr>
												<td>Telefone: </td>
												<td><?php echo $phone; ?></td>
											</tr>
											<tr>
												<td>Mensagem: </td>
												<td><?php echo $message; ?></td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</div>