<?php /** @var $this View */ ?>

<section class="topo-pagina">
    <div class="container">
        <div class="row-fluid">
            <header>
                <div class="col-lg-3">
                    <h1>Faça seu login</h1>
                </div>
            </header>
        </div>
    </div>
</section>

<section class="destaque minhas-info">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <h2>Preencha seu e-mail e senha para entrar.</h2>

                <?php
                    echo $this->Session->flash();
                    echo $this->Session->flash('auth');

                    echo $this->Form->create('User');

                    echo $this->Form->input('username', ['label' => 'E-mail']);
                    echo $this->Form->input('password', ['label' => 'Senha']);
                ?>
               
                <?php echo $this->Html->link('Esqueceu sua senha?',
                    ['plugin' => 'users', 'controller' => 'users', 'action' => 'esqueci'],
                    ['style' => 'float: right; margin-top: 6px;']); ?>
                <?php
                    echo $this->Form->submit('Enviar', ['class' => 'btn btn-default']);

                    echo $this->Form->end();
                ?>
            </div>
            <div class="col-lg-4 col-lg-offset-4 col-md-6 col-sm-6 col-xs-12 hidden-xs">
                <h2>Não é cadastrado?</h2>

                <?php echo $this->Html->link('Faça seu cadastro!',
                    ['plugin' => 'users', 'controller' => 'users', 'action' => 'add'],
                    ['class' => 'btn btn-primary btn-lg btn-block']); ?>

                <hr/>

                <h2>Ou cadastre-se usando o Facebook</h2>
                <?php
                if(!isset($facebook_session) || $facebook_session['id'] == ""){ 
                    echo $this->Facebook->login(
                        array(
                            'custom' => true,
                            'perms' => 'email,publish_stream',
                            'img' => 'facebook-grande.png'
                        )
                    );
                }
                ?>

            </div>

        </div>
    </div>
</section>