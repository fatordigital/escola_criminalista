<?php /** @var $this View */ ?>

<section class="topo-pagina">
    <div class="container">
        <div class="row-fluid">
            <header>
                <div class="col-lg-3">
                    <h1>Restauração de senha:</h1>
                </div>
            </header>
        </div>
    </div>
</section>

<section class="destaque minhas-info">
    <div class="container">

        <?php echo $this->Session->flash(); ?>
        
        <?php echo $this->Form->create('User', [
            'inputDefaults' => array(
                'div' => 'form-group',
                'wrapInput' => '',
                'label' => array(
                    'class' => 'control-label'
                ),
                'class' => 'form-control'
            ),
            'class' => '',
            'role' => 'form',
            'url' => array('plugin' => 'users', 'controller' => 'users', 'action' => 'esqueci')
        ]); ?>

        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <?php

                    echo $this->Form->input('User.email', [
                        'label' => 'E-mail'
                    ]);

                ?>
            </div>
        </div>

        <div class="form-actions">
            <?php echo $this->Form->submit('Enviar', ['class' => 'btn btn-default']); ?>
        </div>

    </div>

    <?php echo $this->Form->end(); ?>

</section>