<section class="topo-pagina">
    <div class="container">
        <div class="row-fluid">
            <header>
                <div class="col-lg-12">
                    <h1>Bem vindo, <span><?php echo $this->Session->read('Auth.User.first_name'); ?></span></h1>
                </div>
            </header>
        </div>
    </div>
</section>

<section class="destaque minhas-info">
    <div class="container">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="area-restrita">
                <h2>Essa é sua área restrita. Escolha abaixo o que deseja acessar:</h2>
                <?php echo $this->Html->link('Meus Dados',
                            ['plugin' => false, 'controller' => 'users', 'action' => 'index'],
                            ['escape' => false]
                        ); ?>
                <p>Nessa tela você poderá atualizar suas informações de cadastro, como endereço ou outros dados pessoais.</p>
                <?php echo $this->Html->link('Meus Vídeos',
                            ['plugin' => false, 'controller' => 'movies', 'action' => 'mymovies'],
                            ['escape' => false]
                        ); ?>
                <p>Aqui você encontrará uma lista dos filmes que você comprou, devidamente organizados por ordem cronológica. Caso você seja um assinante, essa lista estará vazia, pois todos os filmes pagos estarão à sua disposição no site!</p>
                <?php echo $this->Html->link('Vídeos Favoritos',
                            ['plugin' => false, 'controller' => 'movie_favorites', 'action' => 'mymovies'],
                            ['escape' => false]
                        ); ?>
                <p>Todos os vídeos da Casa que você marcar como “favorito” serão listados aqui nessa página. Tudo para facilitar a organização!</p>
                 <?php echo $this->Html->link('Agenda',
                            ['plugin' => false, 'controller' => 'lives', 'action' => 'mymovies'],
                            ['escape' => false]
                        ); ?>
                <p>Aqui estarão presentes todos os eventos ao vivo que você adquiriu, organizados de acordo com a data em que ele acontecerá!</p>
            </div>
        </div>
    </div>
</section>