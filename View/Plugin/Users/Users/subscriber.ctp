﻿<?php /** @var $this View */ ?>

<?php $this->Html->scriptStart(['inline' => false]); ?>
$(function() {
    var cellphoneMask = function(phone, e, currentField, options){
        return phone.match(/^(\(?11\)? ?9(5[0-9]|6[0-9]|7[01234569]|8[0-9]|9[0-9])[0-9]{1})/g) ?
        '(00) 00000-0000' : '(00) 0000-0000';
    };
    $("#UserMobile").mask(cellphoneMask);
    $("#UserPhone").mask($("#UserPhone").data('mask'));
    $("#UserSsn").mask($("#UserSsn").data('mask'));
    $("#Address0Zipcode").mask($("#Address0Zipcode").data('mask'));
});
<?php echo $this->Html->scriptEnd(); ?>

<?php //echo $this->element('users_menu'); ?>

<section class="topo-pagina">
    <div class="container">
        <div class="row-fluid">
            <header>
                <div class="col-lg-12 col-md-12 text-center">
                    <h1 class="hidden-sm hidden-xs"><?php echo $this->Html->image('imagens/passo-01.gif', ['alt' => 'Passo 01']); ?></h1>
                    <h1 class="hidden-lg hidden-md">Faça seu Cadastro</h1>
                </div>
            </header>
        </div>
    </div>
</section>

<section class="destaque minhas-info">
    <div class="container">

        <?php echo $this->Session->flash(); ?>
        <?php echo $this->Session->flash('auth'); ?>

        <?php echo $this->Form->create('User', [
            'inputDefaults' => array(
                'div' => 'form-group',
                'wrapInput' => '',
                'label' => array(
                    'class' => 'control-label'
                ),
                'class' => 'form-control'
            ),
            'class' => '',
            'role' => 'form'
        ]); 
        ?>
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <p class="detalhe">Tornando-se um assinante você tem acesso a conteúdos exclusivos da Casa da Tolerância. </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
				<?php
					echo $this->Form->hidden('User.id');
                   echo $this->Form->input('User.first_name', [
                        'label' => 'Nome Completo'
                    ]);
                ?>
                <?php
                   echo $this->Form->input('User.email', [
                        'label' => 'E-mail <span class="ex">(Exemplo: seunome@seuemail.com.br)</span>',
                        'escape' => false,
                        'type'  => 'text',
			'error' => false
                    ]);
?>
<div class="form-group">
<?php
if ($this->Form->isFieldError('email')) {
echo $this->Form->error('email', array(),
array('escape' => false)); 
}
                ?>
</div>
                <?php
                   echo $this->Form->input('User.phone', [
                        'label' => 'Telefone <span class="ex">(Ex.: (51) 3344-5566)</span>',
                        'escape' => false,
                        'type'  => 'text',
                        'data-mask' => '(00) 0000-0000'
                    ]);
                ?>

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                        <input type="submit" value="Assinar (R$ 19,90)" class="btn-assine-edit">
                        <div class="detalhe text-center">
                            <div class="menor">
                                Vamos redirecionar você de imediato para o <span>PagSeguro/UOL</span> para que possa finalizar o processo e validar as informações com a instituição financeira. Esse processo garantirá toda a <span class="vermelho">segurança</span> da transação.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
                <div class="desc-assine">
                    <h2>Assinatura da Casa da Tolerância</h2>
                    <p class="text-center"><?php echo $this->Html->image('imagens/apenas-1990.png', ['alt' => 'Por apenas R$ 19,90', 'class' => 'valor-assinatura']); ?></p>
                    <h3>Importante</h3>
                    <p>Após o processo de pagamento, sua assinatura poderá levar até 1 dia útil para ser habilitada, de acordo com a aprovação da instituição financeira. Para agilizar esse processo entre em contato conosco pelo telefone: <span>(51) 3232-4749</span></p>
                    <?php echo $this->Html->image('imagens/pag-seguro-assine.png', ['alt' => 'PagSeguro', 'class' => 'pagseguro-assine']); ?>
                </div>
                <div class="desc-assine">
                    <h3>Problemas durante o cadastro?</h3>
                    <p>Caso você encontre problemas durante seu cadastro, você pode <?php echo $this->Html->link('clicar aqui', ['plugin' => '', 'controller' => '', 'action' => '']); ?> para acessar nossa área de contato e enviar as informações para que possamos auxiliá-lo! </p>
                </div>
            </div>

        </div>

    </div>

    <?php echo $this->Form->end(); ?>

</section>