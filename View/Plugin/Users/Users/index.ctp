<?php /** @var $this View */ ?>

<?php $this->Html->scriptStart(['inline' => false]); ?>
$(function() {
    var cellphoneMask = function(phone, e, currentField, options){
        return phone.match(/^(\(?11\)? ?9(5[0-9]|6[0-9]|7[01234569]|8[0-9]|9[0-9])[0-9]{1})/g) ?
        '(00) 00000-0000' : '(00) 0000-0000';
    };
    //$("#UserMobile").mask(cellphoneMask);
    //$("#UserPhone").mask($("#UserPhone").data('mask'));
    $("#UserSsn").mask($("#UserSsn").data('mask'));
    $("#Address0Zipcode").mask($("#Address0Zipcode").data('mask'));

    var SPMaskBehavior = function (val) {
      return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    spOptions = {
      onKeyPress: function(val, e, field, options) {
          field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
    };

    $('#UserPhone').mask(SPMaskBehavior, spOptions);
});
<?php echo $this->Html->scriptEnd(); ?>

<?php echo $this->element('users_menu'); ?>

<section class="topo-pagina">
    <div class="container">
        <div class="row-fluid">
            <header>
                <div class="col-lg-3">
                    <h1>Meus dados</h1>
                </div>
            </header>
        </div>
    </div>
</section>

<section class="destaque minhas-info">
    <div class="container">

        <?php echo $this->Session->flash(); ?>
        <?php echo $this->Session->flash('auth'); ?>

        <?php echo $this->Form->create('User', [
            'inputDefaults' => array(
                'div' => 'form-group',
                'wrapInput' => '',
                'label' => array(
                    'class' => 'control-label'
                ),
                'class' => 'form-control'
            ),
            'class' => '',
            'role' => 'form'
        ]); ?>

        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                <h2>Informações Pessoais</h2>
                <?php
                   echo $this->Form->hidden('User.id');
                   echo $this->Form->input('User.first_name', [
                        'label' => 'Nome Completo'
                    ]);
                ?>
                <?php
                   echo $this->Form->input('User.email', [
                        'label' => 'E-mail <span class="ex">(Exemplo: seunome@seuemail.com.br)</span>',
                        'escape' => false,
                        'type'  => 'text'
                    ]);
                ?>
                <?php
                   echo $this->Form->input('User.phone', [
                        'label' => 'Telefone <span class="ex">(Apenas números. Ex.: 5133445566)</span>',
                        'escape' => false,
                        'type'  => 'text'
                    ]);
                ?>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <?php
                            echo $this->Form->input('User.password', [
                                'label' => 'Digite uma nova senha'
                            ]);
                        ?>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <?php
                            echo $this->Form->input('User.confirm', [
                                'label' => 'Redigite essa senha',
                                'type'  => 'password'
                            ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-actions">
            <?php echo $this->Form->submit('Enviar', ['class' => 'btn btn-default']); ?>
        </div>

    </div>

    <?php echo $this->Form->end(); ?>

</section>
