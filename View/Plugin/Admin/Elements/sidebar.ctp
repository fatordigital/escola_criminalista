<?php
/** @var $this View */
?>

<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu">
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler hidden-phone">
                </div>
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            </li>
            <li class="start">
                <?php echo $this->Html->link(
                    '<i class="icon-home"></i> <span class="title">' . __d('users', 'Dashboard') . '</span><span class="selected"></span>',
                    array(
                        'plugin'     => false,
                        'prefix'     => $cmsRoutingPrefix,
                        'controller' => 'Pages',
                        'action'     => 'dashboard'
                    ), array('escape' => false, 'class' => 'ajaxify start')); ?>
            </li>
            <li>
                <a href="javascript:;">
                    <span class="title"><i
                            class="fa fa-list"></i> <?php echo __d('users', 'Movies') ?>
                    </span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <?php
                        echo $this->Html->link(
                            __d('users', 'List Movies'),
                            array(
                                'plugin'     => false,
                                'prefix'     => $cmsRoutingPrefix,
                                'controller' => 'Movies',
                                'action'     => 'index',
                            ),
                            ['class' => 'ajaxify']
                        );
                        ?>
                    </li>
                    <li>
                        <?php
                        echo $this->Html->link(
                            __d('users', 'Add New Movie'),
                            array(
                                'plugin'     => false,
                                'prefix'     => $cmsRoutingPrefix,
                                'controller' => 'Movies',
                                'action'     => 'add',
                            ),
                            ['class' => 'ajaxify']
                        );
                        ?>
                    </li>
                    <li>
                        <?php
                        echo $this->Html->link(
                            __d('users', 'Movie Categories'),
                            array(
                                'plugin'     => false,
                                'prefix'     => $cmsRoutingPrefix,
                                'controller' => 'Categories',
                                'action'     => 'index',
                            ),
                            ['class' => 'ajaxify']
                        );
                        ?>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <span class="title"><i
                            class="fa fa-list"></i> <?php echo __d('users', 'Courses') ?>
                    </span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <?php
                        echo $this->Html->link(
                            __d('users', 'List Courses'),
                            array(
                                'plugin'     => false,
                                'prefix'     => $cmsRoutingPrefix,
                                'controller' => 'Courses',
                                'action'     => 'index',
                            ),
                            ['class' => 'ajaxify']
                        );
                        ?>
                    </li>
                    <li>
                        <?php
                        echo $this->Html->link(
                            __d('users', 'Add New Course'),
                            array(
                                'plugin'     => false,
                                'prefix'     => $cmsRoutingPrefix,
                                'controller' => 'Courses',
                                'action'     => 'add',
                            ),
                            ['class' => 'ajaxify']
                        );
                        ?>
                    </li>
					<li>
                        <?php
                        echo $this->Html->link(
                            __d('users', 'List Related'),
                            array(
                                'plugin'     => false,
                                'prefix'     => $cmsRoutingPrefix,
                                'controller' => 'Related',
                                'action'     => 'index',
                            ),
                            ['class' => 'ajaxify']
                        );
                        ?>
                    </li>
                </ul>
            </li>
			<li>
                <a href="javascript:;">
                    <span class="title"><i
                            class="fa fa-list"></i> <?php echo __d('users', 'Live') ?>
                    </span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <?php
                        echo $this->Html->link(
                            __d('users', 'List Live'),
                            array(
                                'plugin'     => false,
                                'prefix'     => $cmsRoutingPrefix,
                                'controller' => 'Lives',
                                'action'     => 'index',
                            ),
                            ['class' => 'ajaxify']
                        );
                        ?>
                    </li>
                    <li>
                        <?php
                        echo $this->Html->link(
                            __d('users', 'Add New Live'),
                            array(
                                'plugin'     => false,
                                'prefix'     => $cmsRoutingPrefix,
                                'controller' => 'Lives',
                                'action'     => 'add',
                            ),
                            ['class' => 'ajaxify']
                        );
                        ?>
                    </li>

                </ul>
            </li>
             <li>
                <a href="javascript:;">
                    <span class="title"><i
                            class="fa fa-list"></i> <?php echo __d('users', 'Miscellaneous') ?>
                    </span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <?php
                        echo $this->Html->link(
                            __d('users', 'List Miscellaneous'),
                            array(
                                'plugin'     => false,
                                'prefix'     => $cmsRoutingPrefix,
                                'controller' => 'Miscellaneous',
                                'action'     => 'index',
                            ),
                            ['class' => 'ajaxify']
                        );
                        ?>
                    </li>
                    <li>
                        <?php
                        echo $this->Html->link(
                            __d('users', 'Add New Miscellaneous'),
                            array(
                                'plugin'     => false,
                                'prefix'     => $cmsRoutingPrefix,
                                'controller' => 'Miscellaneous',
                                'action'     => 'add',
                            ),
                            ['class' => 'ajaxify']
                        );
                        ?>
                    </li>

                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <span class="title"><i
                            class="fa fa-list"></i> <?php echo __d('users', 'News') ?>
                    </span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <?php
                        echo $this->Html->link(
                            __d('users', 'List News'),
                            array(
                                'plugin'     => false,
                                'prefix'     => $cmsRoutingPrefix,
                                'controller' => 'News',
                                'action'     => 'index',
                            ),
                            ['class' => 'ajaxify']
                        );
                        ?>
                    </li>
                    <li>
                        <?php
                        echo $this->Html->link(
                            __d('users', 'Add New News'),
                            array(
                                'plugin'     => false,
                                'prefix'     => $cmsRoutingPrefix,
                                'controller' => 'News',
                                'action'     => 'add',
                            ),
                            ['class' => 'ajaxify']
                        );
                        ?>
                    </li>

                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <span class="title"><i
                            class="fa fa-list"></i> Novidades
                    </span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <?php
                        echo $this->Html->link(
                            'Listar Novidades',
                            array(
                                'plugin'     => false,
                                'prefix'     => $cmsRoutingPrefix,
                                'controller' => 'Novidades',
                                'action'     => 'index',
                            ),
                            ['class' => 'ajaxify']
                        );
                        ?>
                    </li>
                    <li>
                        <?php
                        echo $this->Html->link(
                            'Adicionar Novidades',
                            array(
                                'plugin'     => false,
                                'prefix'     => $cmsRoutingPrefix,
                                'controller' => 'Novidades',
                                'action'     => 'add',
                            ),
                            ['class' => 'ajaxify']
                        );
                        ?>
                    </li>

                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <span class="title"><i
                            class="fa fa-list"></i> <?php echo __d('users', 'Eventos') ?>
                    </span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <?php
                        echo $this->Html->link(
                            __d('users', 'List Eventos'),
                            array(
                                'plugin'     => false,
                                'prefix'     => $cmsRoutingPrefix,
                                'controller' => 'Eventos',
                                'action'     => 'index',
                            ),
                            ['class' => 'ajaxify']
                        );
                        ?>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <span class="title"><i
                            class="fa fa-list"></i> <?php echo __d('users', 'Orders') ?>
                    </span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <?php
                        echo $this->Html->link(
                            __d('users', 'List Orders'),
                            array(
                                'plugin'     => false,
                                'prefix'     => $cmsRoutingPrefix,
                                'controller' => 'Orders',
                                'action'     => 'index',
                            ),
                            ['class' => 'ajaxify']
                        );
                        ?>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <span class="title"><i
                            class="fa fa-list"></i> <?php echo __d('users', 'Users') ?>
                    </span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <?php
                        echo $this->Html->link(
                            __d('users', 'List Users'),
                            array(
                                'plugin'     => 'users',
                                'prefix'     => $cmsRoutingPrefix,
                                'controller' => 'users',
                                'action'     => 'index',
                            ),
                            ['class' => 'ajaxify']
                        );
                        ?>
                    </li>
                    <li>
                        <?php
                        echo $this->Html->link(
                            __d('users', 'Add New User'),
                            array(
                                'plugin'     => 'users',
                                'prefix'     => $cmsRoutingPrefix,
                                'controller' => 'users',
                                'action'     => 'add',
                            ),
                            ['class' => 'ajaxify']
                        );
                        ?>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <span class="title"><i
                            class="fa fa-user"></i> <?php echo __d('users', 'System Users') ?>
                    </span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <?php
                        echo $this->Html->link(
                            __d('users', 'List System Users'),
                            array(
                                'plugin'     => $cmsPluginName,
                                'prefix'     => $cmsRoutingPrefix,
                                'controller' => 'CmsUsers',
                                'action'     => 'index',
                            ),
                            ['class' => 'ajaxify']
                        );
                        ?>
                    </li>
                    <li>
                        <?php
                        echo $this->Html->link(
                            __d('users', 'Create System User'),
                            array(
                                'plugin'     => $cmsPluginName,
                                'prefix'     => $cmsRoutingPrefix,
                                'controller' => 'CmsUsers',
                                'action'     => 'add',
                            ),
                            ['class' => 'ajaxify']
                        );
                        ?>
                    </li>
                </ul>
            </li>
        </ul>
        <!--END SIDEBAR MENU-->
    </div>
</div>
<!--END SIDEBAR-->
