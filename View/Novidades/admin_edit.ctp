<?php

/**
 * @var $this View
 */

$this->Html->addCrumb('Novidades');
$this->Html->addCrumb(__d('users', 'Editar %s Novidade', $this->Form->value('Novidade.title')));

?>

<?php $this->Html->script("$cmsPluginName.assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js", ['inline' => false]); ?>
<?php $this->Html->css("$cmsPluginName.assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css", ['inline' => false]); ?>

<?php $this->Html->script("$cmsPluginName.assets/plugins/ckeditor/ckeditor", ['inline' => false]); ?>

<div class="row">
    <div class="col-md-12">
        <h2 class="page-title">Editar Novidade</h2>
        <?php echo $this->Html->getCrumbList(
            array(
                'class'     => 'breadcrumb',
                'separator' => '<i class="icon-angle-right"></i>',
                'escape'    => false
            ),
            'Home'
        );
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <?php echo $this->Session->flash(); ?>
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                     Editar Informações da Novidade
                </div>
            </div>

            <div class="portlet-body form movies">

                <?php echo $this->Form->create(
                    'Novidade',
                    array(
                        'inputDefaults' => array(
                            'div'        => 'form-group',
                            'wrapInput'  => 'col col-md-9',
                            'label'      => array(
                                'class' => 'col col-md-3 control-label'
                            ),
                            'class'      => 'form-control',
                            'novalidate' => 'novalidate'
                        ),
                        'class'         => 'form-horizontal',
                        'type'       	=> 'file',
                    )
                ); ?>

                <div class="form-body">

                    <?php
					echo $this->Form->hidden('id');
                    echo $this->Form->input('title', ['label' => 'Titulo']);
                    echo $this->Form->input('text', ['label' => 'Conteúdo', 'class' => 'ckeditor']);
					echo $this->Form->input('link', ['label' => 'Link']);
                    echo $this->Form->input('image', ['label' => 'Imagem', 'type' => 'file']);
					?>
					<?php
						if($this->data['Novidade']['image'] != ""):
					?>
					<div class="form-group content-image-news">
						<label class="col col-md-3 control-label"></label>
						<div class="col col-md-9">
							<img src="<?php echo $this->Html->Url('/files/novidade/image/'.$this->data['Novidade']['id'].'/'.$this->data['Novidade']['image'], true); ?>" alt="" width="150" />
							<?php echo $this->Form->input('Novidade.image.remove', array('type' => 'checkbox', 'label' => 'Remover imagem existente')); ?>
						</div>
					</div>
					<?php 
						endIf;
					?>
					
                    <div class="form-actions">

                        <?php echo $this->Form->submit(__d('users', 'Submit'), array(
                            'div'    => false,
                            'class'  => 'btn blue',
                            'escape' => false,
                        )); ?>


                    </div>
                </div>
                <?php echo $this->Form->end(); ?>

            </div>
        </div>
    </div>
</div>
