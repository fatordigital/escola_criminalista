<?php

/**
 * @var $this View
 */

$this->Html->addCrumb('Novidades');
$this->Html->addCrumb('Adicionar Novidade');

?>

<?php $this->Html->script("$cmsPluginName.assets/plugins/jquery-tags-input/jquery.tagsinput.min.js", ['inline' => false]); ?>
<?php $this->Html->css("$cmsPluginName.assets/plugins/jquery-tags-input/jquery.tagsinput.css", ['inline' => false]); ?>

<?php $this->Html->script("$cmsPluginName.assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js", ['inline' => false]); ?>
<?php $this->Html->css("$cmsPluginName.assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css", ['inline' => false]); ?>

<?php $this->Html->script("$cmsPluginName.assets/plugins/select2/select2.min.js", ['inline' => false]); ?>
<?php $this->Html->css("$cmsPluginName.assets/plugins/select2/select2.css", ['inline' => false]); ?>
<?php $this->Html->css("$cmsPluginName.assets/plugins/select2/select2-metronic.css", ['inline' => false]); ?>

<?php $this->Html->script("$cmsPluginName.assets/plugins/ckeditor/ckeditor", ['inline' => false]); ?>

<div class="row">
    <div class="col-md-12">
        <h2 class="page-title">Adicionar Novidade</h2>
        <?php echo $this->Html->getCrumbList(
            array(
                'class'     => 'breadcrumb',
                'separator' => '<i class="icon-angle-right"></i>',
                'escape'    => false
            ),
            'Home'
        );
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <?php echo $this->Session->flash(); ?>
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    Adicionar Informações da Novidade
                </div>
            </div>

            <div class="portlet-body form movies">

                <?php echo $this->Form->create(
                    'Novidade',
                    array(
                        'inputDefaults' => array(
                            'div'        => 'form-group',
                            'wrapInput'  => 'col col-md-9',
                            'label'      => array(
                                'class' => 'col col-md-3 control-label'
                            ),
                            'class'      => 'form-control',
                            'novalidate' => 'novalidate'
                        ),
                        'class'         => 'form-horizontal',
                        'type'       	=> 'file',
                    )
                ); ?>

                <div class="form-body">

                    <?php
                        echo $this->Form->input('title', ['label' => 'Título']);
						echo $this->Form->input('text', ['label' => 'Conteúdo', 'class' => 'ckeditor', 'required' => false]);
						echo $this->Form->input('link', ['label' => 'Link']);
                        echo $this->Form->input('image', ['label' => 'Imagem', 'type' => 'file']);
                    ?>

                    <div class="form-actions">

                        <?php echo $this->Form->submit(__d('users', 'Submit'), array(
                            'div'    => false,
                            'class'  => 'btn blue',
                            'escape' => false,
                        )); ?>


                    </div>


                </div>
                <?php echo $this->Form->end(); ?>

            </div>
        </div>
    </div>
</div>
