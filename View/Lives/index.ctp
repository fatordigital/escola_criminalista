<?php echo $this->Html->scriptStart(['inline' => false]) ?>

function modalPagamento(id) {
    $.blockUI({
        css: {
            border: 'none',
            padding: '5px',
            width: '600px',
            backgroundColor: '#FFFFFF',
            '-webkit-border-radius': '5px',
            '-moz-border-radius': '5px',
        },
    message: $('#modalblock-'+id)
    });
}


function fechar() {
    $.unblockUI();
}

$(document).ready(function(){
    $('.btn_fechar').click(function() {
        fechar();
    });
    
});


<?php echo $this->Html->scriptEnd() ?>

<?php
    $bg = array('aovivo01.jpg', 'aovivo02.jpg', 'aovivo03.jpg' );
    $i = rand(0, count($bg)-1);

    
    if (isset($_SESSION['back']) && $_SESSION['back'] == $i) {
        while($_SESSION['back'] == $i){
            $i = rand(0, count($bg)-1);            
        }
        $_SESSION['back'] = $i; 
    }else{
        $_SESSION['back'] = $i; 
    }
      
    $selectedBg = $bg[$_SESSION['back']];
?>

<style>
   section.ao-vivo-banner {
        background: url("../imagens/capas/<?php echo $selectedBg; ?>") no-repeat scroll center 0 #FFFFFF !important;
    }
</style> 
<!-- <section class="ao-vivo-banner hidden-sm hidden-xs">
    <div class="container">
        <div class="col-lg-7">
            <header>
                <h1>Acompanhe às transmissões <span>ao vivo</span></h1>
            </header>
            <p>O Instituto Casa da Tolerância possui um estúdio para as transmissões ao vivo</p>
        </div>
    </div>
</section> -->
<section class="destaque aulas-ao-vivo">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px;">
            <p>Caso haja problema na recepção do sinal, informamos que o curso será gravado e ficará disponível posteriormente.</p>
        </div>
        <?php if(count($movies) > 0): ?>
            <?php $i = 1; ?>
            <?php foreach ($movies as $movie) : ?>

                <?php if ($i == 0 || $i % 3 == 0) : ?>
                    <div class="row-fluid">
                <?php endif; ?>

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 box">
                    <article>
                        <?php //IMAGEM DESTAQUE DO MODULO ?>
                        <?php if($movie['Movie']['image'] != ""): ?>
                            <img src="<?php echo $this->Html->Url('/files/movie/image/'.$movie['Movie']['id'].'/vga_'.$movie['Movie']['image'], true); ?>" alt="<?php echo $movie['Movie']['title']; ?>" class="img-responsive preview" style="margin-bottom: 10px;" />
                        <?php endIf; ?>
                        <header>
                            <?php if($movie['Movie']['date'] != ""): ?>
                                <span>
                                    <?php echo $this->String->diasemana($this->String->DataFormatada('w',$movie['Movie']['date'])); ?>,
                                    <br>
                                    <?php echo $this->String->DataFormatada('d',$movie['Movie']['date']); ?> de 
                                    <?php echo $this->String->mes($this->String->DataFormatada('m',$movie['Movie']['date'])); ?>
                                </span>
                            <?php endIf; ?>
                            <h2><?php echo $movie['Movie']['title']; ?></h2>
                            <?php if($movie['Movie']['price'] > 0): ?>
                                <p><?php echo $this->Number->currency($movie['Movie']['price']); ?></p>
                            <?php endIf; ?>
                        </header>

                        <p>
                            <?php
                                if(strlen($movie['Movie']['description']) > 255){
                                 echo substr($movie['Movie']['description'],0,255).'...';
                                }else{
                                 echo substr($movie['Movie']['description'],0,255);
                                }
                            ?>
                        </p>
                        
                        <?php if(1 == 2 && $movie['Movie']['type'] != 'free'): ?>
                            <a href="javascript:modalPagamento(<?php echo $movie['Movie']['id']; ?>)" class="btn btn-default comprar" title="Comprar">Comprar</a>
                        <?php endIf; ?>

                        <?php echo $this->Html->link('Saiba mais/Assistir',
                                    ['controller' => 'lives', 'action' => 'view', $movie['Movie']['slug']],
                                    ['escape' => false, 'class' => 'btn btn-default saiba-mais']
                                ); ?>
                    </article>
                </div>

                <?php if ($i % 3 == 0) : ?>
                    <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php $i++; ?>

                <div id="modalblock-<?php echo $movie['Movie']['id']; ?>" class="modalblock" style="display: none">                
                    <a href="javascript:void(0);" class="btn_fechar">
                        <?php echo $this->Html->image('imagens/btn_fechar.png', ['alt' => 'Fechar', 'id' => 'btnFechar']); ?>
                    </a>

                    <div id="topomodal">
                        <h1>Conteúdo ao vivo: <span><?php echo $movie['Movie']['title'] ?></span></h1>
                    </div>
                    <div id="centromodal">
                        <b>Importante</b><br />
                        Antes de finalizar a compra, por favor, verifique se sua conexão é capaz de suportar o conteúdo ao vivo.<br>
                        O teste pode ser feito <a href="http://icdt.com.br/aovivo/" title="Clicando aqui">clicando aqui</a>, caso não suporte, recomendamos que você contate seu provedor de serviços de internet.
                    </div>
                    <div id="rodapemodal">
                        <b>Compre com o pagseguro</b>

                        <?php echo $this->Html->image('imagens/rodape_pagseguro.jpg', ['alt' => 'Pagseguro']); ?>
                        <div align="right">
                            <?php echo $this->Form->postLink('Comprar',
                                array(
                                    'controller' => 'movies',
                                    'action'     => 'buy',
                                    $movie['Movie']['id']
                                ),
                                array(
                                    'class' => 'btn btn-default',
                                    'title' => 'Comprar'
                                )
                            ); ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <p>Nenhum vídeo encontrado.</p>
        <?php endif; ?>

        <?php if($this->params['paging']['Movie']['pageCount'] > 1): ?>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center clear">
                    <ul class="pagination customizada">
                        <?php echo $this->Paginator->prev(
                            $this->Html->image('imagens/voltar-pagina.png', ['alt' => 'Voltar Página']),
                            ['escape' => false]
                        ); ?>
                        <?php echo $this->Paginator->numbers(['currentClass' => 'active']); ?>
                        <?php echo $this->Paginator->next(
                            $this->Html->image('imagens/avancar-pagina.png', ['alt' => 'Avançar Página']),
                            ['escape' => false]
                        ); ?>
                    </ul>
                </div>
            </div>
        <?php endIf; ?>
    </div>
</section>
