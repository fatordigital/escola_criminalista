<?php echo $this->element('users_menu'); ?>

<?php echo $this->Session->flash(); ?>

<section class="topo-pagina">
    <div class="container">
        <div class="row-fluid">
            <header>
                <div class="col-lg-3">
                    <h1>Agenda</h1>
                </div>
            </header>
        </div>
    </div>
</section>

<section class="destaque aulas-ao-vivo">
    <div class="container">

        <?php if(isset($orders) && count($orders) > 0): ?>

            <?php $i = 1; ?>
            <?php foreach ($orders as $order) : ?>

                <?php if ($i == 0 || $i % 3 == 0) : ?>
                    <div class="row-fluid">
                <?php endif; ?>

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 box">
                    <article>
                        <header>
                            <?php if($order['Movie']['date'] != ""): ?>
                                <span>
                                    <?php echo $this->String->diasemana($this->String->DataFormatada('w',$order['Movie']['date'])); ?>,
                                    <br>
                                    <?php echo $this->String->DataFormatada('d',$order['Movie']['date']); ?> de 
                                    <?php echo $this->String->mes($this->String->DataFormatada('m',$order['Movie']['date'])); ?>
                                </span>
                            <?php endIf; ?>
                            <h2><?php echo $order['Movie']['title']; ?></h2>
							<?php if($order['Movie']['price'] > 0): ?>
								<p><?php echo $this->Number->currency($order['Movie']['price']); ?></p>
							<?php endIf; ?>
                        </header>

                        <p><?php echo $order['Movie']['description']; ?></p>
                        
                        <?php echo $this->Html->link('Saiba mais',
                                    ['controller' => 'lives', 'action' => 'view', $order['Movie']['slug']],
                                    ['escape' => false, 'class' => 'btn btn-default saiba-mais']
                                ); ?>
                    </article>
                </div>

                <?php if ($i % 3 == 0) : ?>
                    <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php $i++; ?>
            <?php endforeach; ?>

        <?php else: ?>

            <p>Nenhum Vídeo Encontrado</p>
            
        <?php endif; ?>

    </div>
</section>