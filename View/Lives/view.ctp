<?php /** @var $this View */ ?>
<?php echo $this->Html->scriptStart(['inline' => false]) ?>

function modalPagamento() {
    $.blockUI({
        css: {
            border: 'none',
            padding: '5px',
            width: '600px',
            top: '25%',
            backgroundColor: '#FFFFFF',
            '-webkit-border-radius': '5px',
            '-moz-border-radius': '5px',
        },
    message: $('#modalblock')

    });
}


function modalPagamentoCustom(id) {
    $.blockUI({
        css: {
            border: 'none',
            padding: '5px',
            width: '600px',
            top: '25%',
            backgroundColor: '#FFFFFF',
            '-webkit-border-radius': '5px',
            '-moz-border-radius': '5px',
        },
    message: $("#modalblock"+id)

    });
}

function modalAssinatura(){
    $.blockUI({
        css: {
            border: 'none',
            padding: '5px',
            width: '600px',
            top: '25%',
            backgroundColor: '#FFFFFF',
            '-webkit-border-radius': '5px',
            '-moz-border-radius': '5px'
        },
    message: $('#modalblockassinante'),

    });
}

function fechar() {
    $.unblockUI();
}

 $('.btnFechar').click(function() {
    fechar();
});

$('document').ready(function(){
    $('.ler-mais').click(function(){
        if($(this).attr('rel') == 'hidden'){
            $('section.filme-detalhe .descricao-filme').animate({height:'100%'});
            $('.fadeout').fadeOut();
            $(this).attr('rel', 'show');
        }else{
            $('section.filme-detalhe .descricao-filme').animate({height:'100'});
            $('.fadeout').fadeIn();
            $(this).attr('rel', 'hidden');
        }   
    });



    setTimeout(function () {
       if(!$("#ifPlayer").length){
        window.location.reload();
       }
        
    }, 60000);
});

<?php echo $this->Html->scriptEnd() ?>

<section class="filme-detalhe">
    <div class="container">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="o-filme">
                <figure>
                    <?php 
                        $aovivo_hora_atual = date('H');
                        $dia_atual = date('Y-m-d');
                        $aovivo_hora_inicio = $this->String->DataFormatada('H',$movie['Movie']['hour_start']);
                        $aovivo_hora_termino = $this->String->DataFormatada('H',$movie['Movie']['hour_end']);
                    ?>
                    <div class="video-container">
                        <?php if ($paidMovie): ?>
                            <?php if( ($dia_atual == $movie['Movie']['date']) && ($aovivo_hora_atual >= ($aovivo_hora_inicio-1)) && ($aovivo_hora_atual <= ($aovivo_hora_termino+1)) ): ?>
                                <iframe width="640" height="360" frameborder="0" id="ifPlayer" name="ifPlayer" src="http://www.z1on.com/player/5535/?src=iframe&embed=1&controlbar=1"></iframe>
                            <?php else: ?>
                                <?php 
									if($this->Session->check('Auth.User')){
										echo $this->Html->image('imagens/ao-vivo-espera.png', ['alt' => 'Aguarde', 'class' => 'fullsize-images img-responsive']);
									}else{
										echo $this->Html->image('imagens/ao-vivo-espera-off.png', ['alt' => 'Cadastre-se', 'class' => 'fullsize-images img-responsive']);
									}
								 ?>
                            <?php endIf; ?>
                        <?php else: ?>
                            <?php echo $this->Html->scriptStart(['inline' => false]) ?>
                                $(document).ready(function() {
                                    <?php if ($movie['Movie']['restriction'] == true && $movie['Movie']['type'] == "free" && !$this->Session->check('Auth.User')) : ?>
                                    
                                        $.blockUI({
                                            css: {
                                                border: 'none',
                                                padding: '5px',
                                                width: '960px',
                                                height: '445px',
                                                top: '25%',
                                                left: '20%',                                        
                                                backgroundColor: '#FFFFFF',
                                                '-webkit-border-radius': '5px',
                                                '-moz-border-radius': '5px',
                                            },
                                            overlayCSS:  { 
                                                backgroundColor: '#5E1A17',
                                                opacity: 0.85
                                            },
                                            message: $('#modalfree')

                                        });
                                        //setTimeout($.unblockUI, 10000);

                                        $('#btnSemCadastro').click(function() {
                                            fechar();
                                        });
                                    <?php else : ?>
                                    if(!isMobile()){
                                        $('.btnComprar').click(function() {
                                            modalPagamento();
                                            return false;
                                        });

                                         $('.btnComprarCustom').click(function() {
                                            modalPagamentoCustom($(this).attr('rel'));
                                            return false;
                                        });


                                        $('#btnAssinar').click(function() {
                                            modalAssinatura();
                                            return false;
                                        });
                                    }
                                    <?php endif; ?>
                                });
                            <?php echo $this->Html->scriptEnd() ?>
                            
                            <?php if ($movie['Movie']['type'] == "free") : ?>
								
								<?php if( ($dia_atual == $movie['Movie']['date']) && ($aovivo_hora_atual >= ($aovivo_hora_inicio-1)) && ($aovivo_hora_atual <= ($aovivo_hora_termino+1)) ): ?>
                                    <iframe width="640" height="360" frameborder="0" id="ifPlayer" name="ifPlayer" src="http://www.z1on.com/player/5535/?src=iframe&embed=1&controlbar=1"></iframe>
                                <?php else: ?>
									<?php 
										if($this->Session->check('Auth.User')){
											echo $this->Html->image('imagens/ao-vivo-espera.png', ['alt' => 'Aguarde', 'class' => 'fullsize-images img-responsive']);
										}else{
											echo $this->Html->image('imagens/ao-vivo-espera-off.png', ['alt' => 'Cadastre-se', 'class' => 'fullsize-images img-responsive']);
										}
									 ?>
                                <?php endIf; ?>
                            <?php else: ?>
                                <?php echo $this->Html->image('imagens/ao-vivo-espera.png', ['alt' => 'Aguarde', 'class' => 'fullsize-images img-responsive']); ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                        <span>
                            <?php echo $this->Html->image("imagens/{$movie['Movie']['type']}-top-right.png", ['alt' => "{$types[$movie['Movie']['type']]}"]); ?>
                        </span>
                </figure>
                <!--<a href="#" class="favoritos" title="Adicionar aos favoritos">Adicionar aos favoritos</a>
                <a href="#" class="apagar-luz" title="Apagar luz">Apagar luz</a>-->

                <header>
                    <h1><?php echo $movie['Movie']['title'] ?></h1>
                    <span class="visualizacoes">Data de evento: <?php echo $this->String->DataFormatada('d/m/Y', $movie['Movie']['date']); ?> - <?php echo $movie['Movie']['hour_start'] ?></span>
                </header>
                <?php if($movie['Movie']['description'] != ""): ?>
                    <div class="descricao-filme">
                        <?php echo $movie['Movie']['description'] ?>

                        <div class="fadeout"></div>
                    </div>
                <?php endIf; ?>
                <?php if(strlen($movie['Movie']['description']) > 255): ?>
                    <?php echo $this->Html->link(
                        'Continuar lendo ' . $this->Html->image('imagens/continuar-lendo.gif', ['alt' => 'Continuar lendo']),
                        'javascript:;',
                        ['class' => 'ler-mais', 'escape' => false, 'rel' => 'hidden']
                    ); ?>
                <?php endIf; ?>
                <hr />
                <div class="dados-filme">
                    Data: <?php echo $this->String->DataFormatada('d/m',$movie['Movie']['date']); ?> - <?php echo $this->String->DataFormatada('H:i',$movie['Movie']['hour_start']); ?> - <?php echo $this->String->DataFormatada('H:i',$movie['Movie']['hour_end']); ?>
                </div>

                <?php
                if(!$paidMovie && $movie['Movie']['type'] != 'free'): ?>
                    <div class="comprar">
						<div class="adquirir-cursos">
							<div class="row">
								
								<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
									<div class="observacao text-right">
										Adquira acesso!
									</div>
								</div>
							
								<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center">
									<div class="valor-total-curso">
										<?php $pricee = explode('.', $movie['Movie']['price']); ?>
										<span class="superior">R$</span><?php echo $pricee[0]; ?><span>,<?php echo $pricee[1]; ?></span>
									</div>
								</div>
								
								<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
									<?php echo $this->Html->link('Comprar', array('controller' => 'movies', 'action' => 'mobile', 'compra', $movie['Movie']['id']), array('class' => 'btn btn-default comprar-curso btnComprar')); ?>
								</div>
							</div>
						</div>


                        <?php if($movie['Movie']['show_movie_prices'] == true && count($movie['MoviePrice']) > 0): ?>
                            <?php foreach($movie['MoviePrice']  as $price): ?>
                                <?php if($price['price'] > 0): ?>
									<div class="adquirir-cursos" style="margin-top: 20px;">
										<div class="row">
											<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
												<div class="observacao text-right">
													Comprar como: <?php echo $price['title']; ?>
												</div>
											</div>
											
											<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center">
												<div class="valor-total-curso">
													<?php $pricee = explode('.', $price['price']); ?>
													<span class="superior">R$</span><?php echo $pricee[0]; ?><span>,<?php echo $pricee[1]; ?></span>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
												<?php echo $this->Html->link('Comprar', array('controller' => 'movies', 'action' => 'mobile', 'compra', $movie['Movie']['id'], 'false', $price['id']), array('class' => 'btn btn-default comprar-curso btnComprarCustom', 'rel' => $price['id'])); ?>
											</div>

											<div id="modalblock<?php echo $price['id']; ?>" class="modalblock" style="display: none">
												<a href="javascript:void(0);" class="btnFechar">
													<?php echo $this->Html->image('imagens/btn_fechar.png', ['alt' => 'Fechar', 'id' => 'btnFechar']); ?>
												</a>
												<?php echo $this->element('modal_compra', array('movie' => $movie, 'movie_price' => $price['id'])); ?>
											</div>
										</div>
									</div>
                                <?php endIf; ?>
                            <?php endForeach; ?>
                        <?php endIf; ?>
                    </div>
                <?php endIf; ?>
            </div>
        </div>
        <?php if( ($aovivo_hora_atual >= ($aovivo_hora_inicio-1)) && ($aovivo_hora_atual <= ($aovivo_hora_termino+1)) ): ?>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 clear">
                <?php if($movie['Movie']['comments'] == true): ?>
                    <div id="disqus_thread"></div>
                    <script type="text/javascript">
                        /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
                        var disqus_shortname = 'casadetoleranciaaovivo'; // required: replace example with your forum shortname

                        /* * * DON'T EDIT BELOW THIS LINE * * */
                        (function() {
                            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                        })();
                    </script>
                    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                    <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
                <?php endIf; ?>
            </div>
        <?php endIf; ?>
    </div>

    <div id="modalfree" class="modal_cadastre" style="display: none;">
        <h1>Cadastre-se!</h1>
        <div class="message">
            O Instituto Tolerância é um ambiente diferenciado, disponível a<br />
            qualquer pessoa e com muito conteúdo de qualidade<br />
            distribuído gratuitamente. Apenas pedimos que cadastre-se.
        </div>

        <div id="fazercadastro">
            <?php echo $this->Html->link('Quero fazer meu cadastro e assistir aos vídeos!',
                    ['plugin' => 'users', 'controller' => 'users', 'action' => 'add'],
                    ['class' => 'btn btn-default', 'title' => 'Quero fazer meu cadastro e assistir aos vídeos!']); ?>
        </div>
        <div id="semcadastro">
            <?php echo $this->Form->button('Quero continuar sem cadastro',
                array(
                    'id' => 'btnSemCadastro',
                    'class' => 'btn btn-default',
                    'title' => 'Quero continuar sem cadastro',
                    'type' => 'button'
                )
            ); ?>

        </div>
        <div id="fazerlogin">
        <?php echo $this->Html->link('Já sou cadastrado e quero fazer meu login',
            array(
                'plugin'     => 'users',
                'controller' => 'users',
                'action'     => 'login'
            )); ?>
        </div>

    </div>

    <div id="modalblock" class="modalblock" style="display: none">
        <a href="javascript:void(0);" class="btnFechar">
            <?php echo $this->Html->image('imagens/btn_fechar.png', ['alt' => 'Fechar', 'id' => 'btnFechar']); ?>
        </a>
        <?php echo $this->element('modal_compra', array('movie' => $movie)); ?>
    </div>

    <div id="modalblockassinante" class="modalblock" style="display: none">
        <a href="javascript:void(0);" class="btnFechar">
            <?php echo $this->Html->image('imagens/btn_fechar.png', ['alt' => 'Fechar', 'id' => 'btnFechar']); ?>
        </a>
        <?php echo $this->element('modal_assinatura', array('movie' => $movie)); ?>
    </div>
</section>


<style type="text/css">
.modal_cadastre h1{
    font-family: Roboto, sans-serif;
    color:#671C19;
    font-size: 50px;
    font-weight: bold;
}

.modal_cadastre .message{
    margin:60px 0 0 0;
    font-color:#763B39;
    font-size: 27px;
}
.modal_cadastre #fazercadastro .btn{
    background-color:#9B2926;
    font-size:14px;
    padding:20px 20px;
    border-radius: 10px;
    margin:60px 0 0 140px;
    color:#fff;
}
.modal_cadastre #semcadastro .btn{
    background-color:#D4D4D4;
    font-size:14px;
    padding:20px 20px;
    border-radius: 10px;
    margin:60px 0 0 -100px;
    color:#9B2926;
}

.modal_cadastre #fazerlogin a{
    color:#9A2826;
    margin:0 0 0 155px;
}

.valor-total-curso {
	font-size: 60px;
}
.observacao {
	font-size: 20px;
}
.adquirir-cursos a.comprar-curso{
	font-size: 23px;
}

</style>