<?php /** @var $this View */ ?>
<?php echo $this->Html->scriptStart(['inline' => false]) ?>

function modalPagamento() {
    $.blockUI({
        css: {
            border: 'none',
            padding: '5px',
            width: '600px',
            top: '25%',
            backgroundColor: '#FFFFFF',
            '-webkit-border-radius': '5px',
            '-moz-border-radius': '5px',
        },
    message: $('#modalblock')

    });
}

function modalAssinatura(){
    $.blockUI({
        css: {
            border: 'none',
            padding: '5px',
            width: '600px',
            top: '25%',
            backgroundColor: '#FFFFFF',
            '-webkit-border-radius': '5px',
            '-moz-border-radius': '5px'
        },
    message: $('#modalblockassinante'),

    });
}

function fechar() {
    $.unblockUI();
}

 $('.btnFechar').click(function() {
    fechar();
});

$('document').ready(function(){
    $('.ler-mais').click(function(){
        if($(this).attr('rel') == 'hidden'){
            $('section.filme-detalhe .descricao-filme').animate({height:'100%'});
            $('.fadeout').fadeOut();
            $(this).attr('rel', 'show');
        }else{
            $('section.filme-detalhe .descricao-filme').animate({height:'100'});
            $('.fadeout').fadeIn();
            $(this).attr('rel', 'hidden');
        }   
    });

    setTimeout(function () {
       if(!$("#ifPlayer").length && !$('.video-container iframe').length){
        window.location.reload();
       }
        
    }, 60000);
});

<?php echo $this->Html->scriptEnd() ?>

<section class="filme-detalhe">
    <div class="container">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="o-filme">
                <figure>
                    <?php 
                        $aovivo_hora_atual = date('H');
                        $dia_atual = date('Y-m-d');
                        $movie['Movie']['date'] = $this->String->DataFormatada('Y-m-d',$movie['Movie']['date']);
                        $aovivo_hora_inicio = $this->String->DataFormatada('H',$movie['Movie']['hour_start']);
                        $aovivo_hora_termino = $this->String->DataFormatada('H',$movie['Movie']['hour_end']);
                    ?>
                    <div class="video-container">
                        <?php if ($paidMovie): ?>
                            <?php if( ($dia_atual == $movie['Movie']['date']) && ($aovivo_hora_atual >= ($aovivo_hora_inicio-1)) && ($aovivo_hora_atual <= ($aovivo_hora_termino+1)) ): ?>
                                <iframe width="640" height="360" frameborder="0" id="ifPlayer" name="ifPlayer" src="http://www.z1on.com/player/5535/?src=iframe&embed=1&controlbar=1"></iframe>
                            <?php else: ?>
                                <?php 
                                    if(
                                            isset($movie['Movie']['thumb']) 
                                            && 
                                            (
                                                ($aovivo_hora_atual >= ($aovivo_hora_termino+1) && $dia_atual == $movie['Movie']['date']) 
                                                || 
                                                $dia_atual > $movie['Movie']['date']
                                            )
                                        ){
                                        echo $embed['html'];
                                        // if(file_get_contents($movie['Movie']['thumb']['xbig']['url']) == false){
                                        //     echo $this->Html->image(str_replace("_1280.jpg", "_640.jpg", $movie['Movie']['thumb']['xbig']['url']), ['class' => 'fullsize-images img-responsive']);
                                        // }else{
                                        //     echo $this->Html->image($movie['Movie']['thumb']['xbig']['url'], ['class' => 'fullsize-images img-responsive']);
                                        // }
                                    }else{
                                        if($this->Session->check('Auth.User')){
                                            echo $this->Html->image('imagens/ao-vivo-espera.png', ['alt' => 'Aguarde', 'class' => 'fullsize-images img-responsive']);
                                        }else{
                                            echo $this->Html->image('imagens/ao-vivo-espera-off.png', ['alt' => 'Cadastre-se', 'class' => 'fullsize-images img-responsive']);
                                        }
                                    }
								 ?>
                            <?php endIf; ?>
                        <?php else: ?>
                            <?php echo $this->Html->scriptStart(['inline' => false]) ?>
                                $(document).ready(function() {
                                    if(!isMobile()){
                                        $('#btnComprar').click(function() {
                                            modalPagamento();
                                            return false;
                                        });

                                        $('#btnAssinar').click(function() {
                                            modalAssinatura();
                                            return false;
                                        });
                                    }
                                });
                            <?php echo $this->Html->scriptEnd() ?>
                                
                            <?php
                                if($this->Session->check('Auth.User')){
                                    echo $this->Html->image('imagens/ao-vivo-espera.png', ['alt' => 'Aguarde', 'class' => 'fullsize-images img-responsive']);
                                }else{
                                    echo $this->Html->image('imagens/ao-vivo-espera-off.png', ['alt' => 'Cadastre-se', 'class' => 'fullsize-images img-responsive']);
                                }
                            ?>
                            <?php //echo $this->Html->image('imagens/ao-vivo-espera.png', ['alt' => 'Aguarde', 'class' => 'fullsize-images img-responsive']); ?>

                        <?php endif; ?>
                    </div>
                    <span>
                        <?php echo $this->Html->image("imagens/{$movie['Movie']['type']}-top-right.png", ['alt' => "{$types[$movie['Movie']['type']]}"]); ?>
                    </span>
                </figure>

                <header>
                    <h1><?php echo $movie['Movie']['title'] ?></h1>
                    <span class="visualizacoes">Data de evento: <?php echo $this->String->DataFormatada('d/m/Y', $movie['Movie']['date']); ?> - <?php echo $movie['Movie']['hour_start'] ?></span>
                </header>
                <?php if($movie['Movie']['description'] != ""): ?>
                    <div class="descricao-filme">
                        <?php echo $movie['Movie']['description'] ?>

                        <div class="fadeout"></div>
                    </div>
                <?php endIf; ?>
                <?php if(strlen($movie['Movie']['description']) > 255): ?>
                    <?php echo $this->Html->link(
                        'Continuar lendo ' . $this->Html->image('imagens/continuar-lendo.gif', ['alt' => 'Continuar lendo']),
                        'javascript:;',
                        ['class' => 'ler-mais', 'escape' => false, 'rel' => 'hidden']
                    ); ?>
                <?php endIf; ?>
                <hr />
                <div class="dados-filme">
                    Data: <?php echo $this->String->DataFormatada('d/m',$movie['Movie']['date']); ?> - <?php echo $this->String->DataFormatada('H:i',$movie['Movie']['hour_start']); ?> - <?php echo $this->String->DataFormatada('H:i',$movie['Movie']['hour_end']); ?>
                </div>

                <?php
                    $exibir_btn = false;
                    if($movie['Movie']['date'] > $dia_atual){
                        $exibir_btn = true;
                    }elseif($dia_atual == $movie['Movie']['date'] && $aovivo_hora_atual < $aovivo_hora_inicio){
                         $exibir_btn = true;
                    }

                    if($exibir_btn && !$paidMovie && $movie['Movie']['type'] != 'free'): ?>
                    <div class="comprar">
                        <div class="row">
                                <?php if($movie['Movie']['price'] > 0): ?>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="preco">
                                    <strong>Valor</strong><br>
                                    <?php if($movie['Movie']['price'] > 0): ?>
                                        <span><?php echo $this->Number->currency($movie['Movie']['price']); ?></span>
                                    <?php endIf; ?>
                                </div>
                            </div>

                            <?php if($movie['Movie']['via_venda_avulso'] == true): ?>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <?php echo $this->Html->link('Comprar', array('controller' => 'movies', 'action' => 'mobile', 'compra', $movie['Movie']['id']), array('id' => 'btnComprar',
                                        'class' => 'btn btn-default')); ?>
                                </div>
                            <?php endIf; ?>
                                 <?php else: ?>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="preco">
                                            <span>Inscri��o Gratuita</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <?php echo $this->Html->link('Inscreva-se', array('controller' => 'movies', 'action' => 'mobile', 'compra', $movie['Movie']['id']), array('id' => 'btnComprar',
                                                'class' => 'btn btn-default')); ?>
                                    </div>
                                <?php endIf; ?>
                            </div>
                        </div>
                <?php endIf; ?>
            </div>
        </div>
        <?php if( ($aovivo_hora_atual >= ($aovivo_hora_inicio-1)) && ($aovivo_hora_atual <= ($aovivo_hora_termino+1)) ): ?>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 clear">
                <?php if($movie['Movie']['comments'] == true): ?>
                    <div id="disqus_thread"></div>
                    <script type="text/javascript">
                        /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
                        var disqus_shortname = 'casadetoleranciaaovivo'; // required: replace example with your forum shortname

                        /* * * DON'T EDIT BELOW THIS LINE * * */
                        (function() {
                            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                        })();
                    </script>
                    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                    <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
                <?php endIf; ?>
            </div>
        <?php endIf; ?>
    </div>

    <div id="modalblock" class="modalblock" style="display: none">
        <a href="javascript:void(0);" class="btnFechar">
            <?php echo $this->Html->image('imagens/btn_fechar.png', ['alt' => 'Fechar', 'id' => 'btnFechar']); ?>
        </a>
        <?php echo $this->element('modal_compra', array('movie' => $movie)); ?>
    </div>

</section>

<?php if($paidMovie && isset($relatedMovies) && count($relatedMovies) > 0): ?>

    <section class="destaque comentarios-relacionados">
        <div class="container">           
            <div class="col-lg-4 col-md-8 col-sm-12 col-xs-12">
                <h2>Filmes Relacionados</h2>

                <?php foreach ($relatedMovies as $relatedMovie) : ?>

                    <article class="videos-relacionados col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <figure>
                            <?php echo $this->Html->image($relatedMovie['Movie']['thumb']['big']['url'], ['alt' => $relatedMovie['Movie']['title'], 'class' => 'img-responsive fullsize-images']); ?>
                            <span>
                                <?php echo $this->Html->image("imagens/{$relatedMovie['Movie']['type']}-bottom-right.png", ['alt' => $relatedMovie['Movie']['title'], 'class' => 'img-responsive categoria-video']); ?>
                            </span>
                        </figure>
                        <header>
                            <?php echo $this->Html->link($relatedMovie['Movie']['title'],
                                ['controller' => 'movies', 'action' => 'view', $relatedMovie['Movie']['slug']],
                                ['escape' => false]
                            ); ?>
                        </header>
                        <p><?php echo $this->Text->truncate($relatedMovie['Movie']['description']); ?></p>
                    </article>

                <?php endforeach; ?>

            </div>
        </div>
    </section>

<?php endIf; ?>

<style type="text/css">
.modal_cadastre h1{
    font-family: Roboto, sans-serif;
    color:#671C19;
    font-size: 50px;
    font-weight: bold;
}

.modal_cadastre .message{
    margin:60px 0 0 0;
    font-color:#763B39;
    font-size: 27px;
}
.modal_cadastre #fazercadastro .btn{
    background-color:#9B2926;
    font-size:14px;
    padding:20px 20px;
    border-radius: 10px;
    margin:60px 0 0 140px;
    color:#fff;
}
.modal_cadastre #semcadastro .btn{
    background-color:#D4D4D4;
    font-size:14px;
    padding:20px 20px;
    border-radius: 10px;
    margin:60px 0 0 -100px;
    color:#9B2926;
}

.modal_cadastre #fazerlogin a{
    color:#9A2826;
    margin:0 0 0 155px;
}

</style>