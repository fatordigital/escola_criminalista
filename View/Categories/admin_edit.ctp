<?php



/**
 * @var $this View
 */


$this->Html->addCrumb(__('Categories'));

$this->Html->addCrumb(__('Admin Edit %s Category', $this->Form->value('Category.id')));

?>
<div class="row">
    <div class="col-md-12">
        <h2 class="page-title"><?php echo __('Admin Edit Category'); ?></h2>
        <?php echo $this->Html->getCrumbList(
            array(
                'class' => 'breadcrumb',
                'separator' => '<i class="icon-angle-right"></i>',
                'escape' => false
            ),
            '<i class="icon-home"></i> Home'
        );
        ?>    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <?php echo $this->Session->flash(); ?>
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <?php echo __('Admin Edit the Category information'); ?>
                    18
                </div>
            </div>

            <div class="portlet-body form categories">
                <div class="form-body">
                    <?php echo $this->Form->create(
                        'Category',
                        array(
                            'inputDefaults' => array(
                                'div' => 'form-group',
                                'wrapInput' => 'col-md-4',
                                'label' => array(
                                    'class' => 'col col-md-3 control-label'
                                ),
                                'class' => 'form-control',
                                'novalidate' => 'novalidate'
                            ),
                            'class' => 'form-horizontal',
                        )

                    ); ?>

                    <?php
                    echo $this->Form->hidden('id');
                    echo $this->Form->input('parent_id', [
                        'label' => 'Categoria Mãe',
                        'options' => [
                            null => 'Sem categoria-mãe',
                            $parentCategories
                        ]
                    ]);
                    echo $this->Form->input('title', [
                        'label' => [
                            'text' => 'Título'
                        ]
                    ]);
                    ?>

                    <div class="form-actions">

                        <?php echo $this->Form->submit(__('Submit'), array(
                            'div' => false,
                            'class' => 'btn blue',
                            'escape' => false,
                        )); ?>
                        <?php echo $this->Form->postLink(
                            __('Delete %s', '<i class="fa fa-times"></i>'),
                            array(
                                'action' => 'delete',
                                $this->Form->value('Category.id')
                            ),
                            array('class' => 'btn red', 'escape' => false),
                            __('Are you sure you want to delete %s # %s?', __('System User'), $this->Form->value('Category.id'))
                        ); ?>

                    </div>

                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
