<?php



/**
 * @var $this View
 */


$this->Html->addCrumb(__d('users', 'Categories'));

?>

<div class="row">
    <div class="col-md-12">
        <h2 class="page-title"><?php echo __d('users', 'Admin Index Category'); ?></h2>
        <?php echo $this->Html->getCrumbList(
            array(
                'class'     => 'breadcrumb',
                'separator' => '<i class="icon-angle-right"></i>',
                'escape'    => false
            ),
            'Home'
        );
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <?php echo $this->Session->flash(); ?>

        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>&nbsp;<?php echo __d('users', 'Admin Index the Category information'); ?>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                        <a href="javascript:;" class="reload"></a>
                        <a href="javascript:;" class="remove"></a>
                    </div>
                </div>
            </div>

            <div class="portlet-body">

                <div class="table-toolbar">
                    <?php echo $this->Html->link(
                        sprintf('%s <i class="fa fa-plus"></i>', __d('users', 'Add New')),
                        array('action' => 'add'),
                        array(
                            'escape' => false,
                            'class'  => 'btn green'
                        )
                    ); ?>
                </div>

                <table class="table table-hover table-bordered table-striped table-condensed flip-content"
                       data-model="Category">
                    <thead>
                    <tr>
                        <th><?php echo $this->Paginator->sort('id'); ?></th>
                        <th><?php echo $this->Paginator->sort('parent_id'); ?></th>
                        <th><?php echo $this->Paginator->sort('title'); ?></th>
                        <th><?php echo $this->Paginator->sort('slug'); ?></th>
                        <th><?php echo $this->Paginator->sort('movies_count'); ?></th>
                        <th class="actions"><?php echo __('Actions'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($categories as $category): ?>
                        <tr>
                            <td><?php echo h($category['Category']['id']); ?>&nbsp;</td>
                            <td>
                                <?php echo $this->Html->link($category['ParentCategory']['title'], array('controller' => 'categories', 'action' => 'view', $category['ParentCategory']['id'])); ?>
                            </td>
                            <td><?php echo h($category['Category']['title']); ?>&nbsp;</td>
                            <td><?php echo h($category['Category']['slug']); ?>&nbsp;</td>
                            <td><?php echo h($category['Category']['movie_count']); ?>&nbsp;</td>
                            <td class="actions">
                                <?php echo $this->Html->link(
                                    sprintf('<i class="fa fa-info"></i>&nbsp;&nbsp;%s</a>', __d('users', 'View')),
                                    array('action' => 'view', $category['Category']['id']),
                                    array(
                                        'escape'         => false,
                                        'data-id'        => $category['Category']['id'],
                                        'class'          => 'btn btn-sm blue btn-editable',
                                        'original-title' => __d('users', 'View')
                                    )
                                ); ?>
                                <?php echo $this->Html->link(sprintf('<i class="fa fa-edit"></i>&nbsp;&nbsp;%s</a>', __d('users', 'Edit')),
                                    array(
                                        'action' => 'edit',
                                        $category['Category']['id']
                                    ),
                                    array(
                                        'escape'         => false,
                                        'data-id'        => $category['Category']['id'],
                                        'class'          => 'btn btn-sm green btn-editable',
                                        'original-title' => __d('users', 'Edit')
                                    )
                                ); ?>
                                <?php echo $this->Form->postLink(
                                    sprintf('<i class="fa fa-times"></i>&nbsp;&nbsp;%s</a>', __d('users', 'Delete')),
                                    array(
                                        'action' => 'delete',
                                        $category['Category']['id']
                                    ),
                                    array(
                                        'escape'         => false,
                                        'data-id'        => $category['Category']['id'],
                                        'class'          => 'btn btn-sm red btn-removable',
                                        'data-gravity'   => 's',
                                        'original-title' => 'Remove'
                                    ),
                                    __('Are you sure you want to delete # %s?', $category['Category']['id'])
                                );
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>

                </table>

            </div>
        </div>
    </div>
</div>
