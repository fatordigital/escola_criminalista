<?php
/**
 * @var     $this       View
 *
 * @project
 * @package       app.View.Layouts
 * @since
 */

echo $this->Html->docType(); ?>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
    <?php echo $this->Html->charset(); ?>
    <title>Escola de Criminalistas</title>
    <!-- TAGS FACEBOOK -->
    <meta property="og:locale" content="pt_br">
    <meta property="og:type" content="website">
    <meta property="og:url" content="<?php echo $this->Html->Url(null,true); ?>">
    <meta property="og:title" content="<?php echo $title_for_layout; ?>">
    <meta property="og:site_name" content="<?php echo $project_prefix_title; ?>">
    <?php if(isset($meta_description_custom) && $meta_description_custom != ""): ?>
        <meta property="og:description" content="<?php echo $meta_description_custom; ?>">
    <?php else: ?>
        <meta property="og:description" content="<?php echo $meta_description; ?>">
    <?php endif; ?>
    <?php if (isset($movie['Movie']['thumb']['xbig']['url'])){ ?>
        <meta property="og:image" content="<?php echo $this->Html->Url("/".$movie['Movie']['thumb']['xbig']['url']); ?>">    
    <?php }elseif(isset($news['News']['image'])){ ?>
        <meta property="og:image" content="<?php echo $this->Html->Url('/files/news/image/'.$news['News']['id'].'/'.$news['News']['image'],true); ?>">
    <?php }else{ ?>
        <meta property="og:image" content="<?php echo $this->Html->Url("/imagens/logo-icdt-facebook.jpg",true); ?>">
    <?php } ?>
    <meta property="og:image:type" content="image/jpeg">
    <!-- FIM TAGS FACEBOOK -->
    <?php
        echo $this->Html->meta('icon');

        echo $this->Html->css('css/reset.css', ['inline' => false]);
        echo $this->Html->css('//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css', ['inline' => false]);
        echo $this->Html->css('css/animate.css', ['inline' => false]);
        echo $this->Html->css('css/style.css', ['inline' => false]);

        // echo $this->Html->css('css/theme.css', ['inline' => false]);

        echo $this->Html->css('//fonts.googleapis.com/css?family=Lato:300,400,700,900', ['inline' => false]);
        echo $this->Html->css('//fonts.googleapis.com/css?family=Roboto:400,700,300,500,500italic,300italic,100', ['inline' => false]);

        $this->Html->meta('keywords', $meta_keywords, array('inline' => false));
        $this->Html->meta('description', $meta_description, array('inline' => false));
        $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0'), null, array('inline' => false));
        $this->Html->meta(array('name' => 'X-UA-Compatible', 'content' => 'IE=ege,chrome=1'), null, array('inline' => false));

        echo $this->Html->script('//ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.0.min.js');

        //echo $this->fetch('script');
        echo $this->fetch('meta');
        echo $this->fetch('css');
    ?>
    <script>
        var WEBROOT_DIR = '<?php echo $this->webroot; ?>';
        var BASENAME = '<?php echo $this->Html->Url("/", true); ?>';
    </script>
</head>
<?php echo $this->Html->tag('body', null, ['class' => $body_class]); ?>

    <?php echo $this->fetch('video') ;?>

    <?php echo $this->element('header'); ?>

    <?php
        echo $this->Session->flash();
        echo $this->Session->flash('auth');
    ?>

    <?php echo $this->fetch('content'); ?>

    <?php 
        if(!$this->Session->check('Auth.User') && (!isset($facebook_session['id']) || $facebook_session['id'] == "")){
            //echo $this->element('footer-login');     
        }
    ?>

    <?php echo $this->element('footer'); ?>

    <?php $this->Html->scriptStart(); ?>
    var $buoop = {vs:{i:9,f:20,o:15,s:5.1,n:9}};
    $buoop.ol = window.onload;
    window.onload=function(){
        try {if ($buoop.ol) $buoop.ol();}catch (e) {}
        var e = document.createElement("script");
        e.setAttribute("type", "text/javascript");
        e.setAttribute("src", "//browser-update.org/update.js");
        document.body.appendChild(e);
    }

    function isMobile(){
        var userAgent = navigator.userAgent.toLowerCase();
        if( userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i)!= -1 )
            return true;
    }
    <?php echo $this->Html->scriptEnd(); ?>

    <?php $this->Html->scriptStart(); ?>
        var _gaq = _gaq || [];
          _gaq.push(['_setAccount', 'UA-47547332-1']);
          _gaq.push(['_trackPageview']);

          (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();
    <?php echo $this->Html->scriptEnd(); ?>

    <?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js'); ?>
    <?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/jquery-placeholder/2.0.7/jquery.placeholder.min.js'); ?>
    <?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js'); ?>
    <?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js'); ?>
    <?php //echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/jquery.mask/0.9.0/jquery.mask.min.js'); ?>
    <?php echo $this->Html->script('/js/jquery.mask.min'); ?>
    <?php echo $this->Html->script('/js/bootstrap'); ?>
    <?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.66.0-2013.10.09/jquery.blockUI.min.js'); ?>

    <?php echo $this->fetch('script'); ?>

    <?php echo $this->Facebook->init(); ?>

    <?php echo $this->element('lightbox-assinatura'); ?>

    <?php echo $this->element('sql_dump'); ?>
</body>
</html>
