<?php
/**
 *
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		//echo $this->Html->css('css/cake.generic');
		echo $this->Html->css('css/reset.css', ['inline' => false]);
        echo $this->Html->css('//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css', ['inline' => false]);
        echo $this->Html->css('css/style.css', ['inline' => false]);
        echo $this->Html->css('//fonts.googleapis.com/css?family=Lato:300,400,700,900', ['inline' => false]);
        echo $this->Html->css('//fonts.googleapis.com/css?family=Roboto:400,700,300,500,500italic,300italic,100', ['inline' => false]);

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<?php echo $this->Html->tag('body', null, ['class' => $body_class]); ?>
	<?php echo $this->element('header'); ?>
	<?php echo $this->Session->flash(); ?>
	<?php echo $this->fetch('content'); ?>
	<?php echo $this->element('footer'); ?>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
