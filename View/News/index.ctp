<?php /** @var $this View */ ?>

<?php /*
<?php
$bg = array('escritos01.jpg', 'escritos02.jpg');
$i = rand(0, count($bg)-1);


if (isset($_SESSION['back']) && $_SESSION['back'] == $i) {
    while($_SESSION['back'] == $i){
        $i = rand(0, count($bg)-1);            
    }
    $_SESSION['back'] = $i; 
}else{
    $_SESSION['back'] = $i; 
}
  
$selectedBg = $bg[$_SESSION['back']];
?>

<style>
section.ao-vivo-banner {
    background: url("<?php echo $this->Html->Url('/'); ?>/imagens/capas/<?php echo $selectedBg; ?>") no-repeat scroll center 0 #FFFFFF !important;
}
</style> 

<section class="ao-vivo-banner hidden-sm hidden-xs">
<div class="container">
    <div class="col-lg-7">
        <header>
            <h1>Confira a nossa seleção de escritos.</span></h1>
        </header>
        <p>Remeta seu texto. <span>Faça parte!</p>
    </div>
</div>
</section>

*/ ?>
<div class="escritos">
	
    <section class="topo-pagina">
        <div class="container">
            <div class="row">
                <header>
                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-9">
                        <h1>BLOG</h1>
                    </div>
					<?php if(1 == 2): ?>
						<div class="col-lg-4 pull-right hidden-xs">
							<form action="#" method="post">
								<input type="text" class="no-border-right" placeholder="Pesquisar...">
								<input type="submit" class="btn-default" value="">
							</form>
						</div>					
					<?php endIf; ?>
                </header>
            </div>
        </div>
    </section>

    <section class="lista-escritos destaque">
        <div class="container">

	        <?php if(isset($news) && count($news) > 0): ?>
	        	<?php $cont = 1; ?>
	            <?php foreach ($news as $i => $n) : ?>

	                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<article>
							<header>
								<?php if($n['News']['image'] != ""): ?>
									<figure>
										<a href="<?php echo $this->Html->Url('/escrito/'. $n['News']['slug']); ?>" title="<?php echo $n['News']['title']; ?>">
											<?php echo $this->Html->image('/files/news/image/'.$n['News']['id'].'/'.$n['News']['image'], ['alt' => $n['News']['title'], 'class' => 'img-responsive fullsize-images']); ?>
										</a>
									</figure>
								<?php endIf; ?>
								<a href="<?php echo $this->Html->Url('/escrito/'. $n['News']['slug']); ?>" title="<?php echo $n['News']['title']; ?>">
									<h2><?php echo $n['News']['title']; ?></h2>
								</a>
								<span class="data-publicacao"><em>Publicado em: <?php echo $this->String->DataFormatada("d/m/Y", $n['News']['created']); ?></em></span>
							</header>
							<p><?php echo substr(strip_tags($n['News']['text']), 0, 100); ?></p>
							<a href="<?php echo $this->Html->Url('/escrito/'. $n['News']['slug']); ?>" class="btn btn-default" title="Ler mais sobre <?php echo $n['News']['title']; ?>">Leia mais</a>
						</article>
	                </div>
					
					<?php if (($cont > 0) &&  ($cont % 3 == 0)) : ?>
                        <div class="clearfix"></div>
                    <?php endif; ?>
					<?php $cont++; ?>
	            <?php endforeach; ?>

	            <div class="row">
	                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center clear">
	                    <ul class="pagination customizada">
	                        <?php echo $this->Paginator->prev(
	                            $this->Html->image('imagens/voltar-pagina.png', ['alt' => 'Voltar Página']),
	                            ['escape' => false]
	                        ); ?>
	                        <?php echo $this->Paginator->numbers(['currentClass' => 'active']); ?>
	                        <?php echo $this->Paginator->next(
	                            $this->Html->image('imagens/avancar-pagina.png', ['alt' => 'Avançar Página']),
	                            ['escape' => false]
	                        ); ?>
	                    </ul>
	                </div>
	            </div>

	        <?php else: ?>

                <p>Nenhum Artigo Encontrado</p>
            
            <?php endif; ?>
			
        </div>
    </section>

<?php //echo $this->element('cliquesolidario'); ?>
