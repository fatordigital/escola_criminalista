<?php if(isset($news)): ?>

<div class="escritos">
	<section class="o-escrito">
		<div class="container">
			<div class="col-lg-12">
				<article>
					<header>
						<?php if($news['News']['image']): ?>
							<figure>
								<?php echo $this->Html->image('/files/news/image/'.$news['News']['id'].'/'.$news['News']['image'], ['alt' => $news['News']['title'], 'class' => 'img-responsive']); ?>
							</figure>
						<?php endIf; ?>
						<h1><?php echo $news['News']['title']; ?></h1>
						<span class="data-publicacao"><em>Publicado em: <?php echo $this->String->DataFormatada("d/m/Y", $news['News']['created']); ?></em></span>
					</header>
					<p><?php echo $news['News']['text']; ?></p>
				</article>
				
				<h2 style="color: #525252; font-size: 17px;">Comente com o Facebook:</h2>
				<div id="fb-root"></div>
				<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=728391253941963&version=v2.0";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>
				<div class="fb-comments" data-href="<?php echo $this->Html->Url('/escrito/'. $news['News']['slug'], true); ?>" data-width="100%" data-numposts="5" data-colorscheme="light"></div>

			</div>
		</div>
	</section>
</div>
<?php endIf; ?>

<?php if(isset($relatedNews) && count($relatedNews) > 0): ?>
	<div class="escritos">
		<section class="lista-escritos destaque">
			<div class="container">
				<h2>Escritos relacionados</h2>
				
				<div class="row-fluid">
					<?php $cont = 1; ?>
					<?php foreach($relatedNews as $relatedNew): ?>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<article>
								<header>
									<?php if($relatedNew['News']['image']): ?>
										<a href="<?php echo $this->Html->Url('/escrito/'. $relatedNew['News']['slug']); ?>" title="<?php echo $relatedNew['News']['title']; ?>">
											<figure>
												<?php echo $this->Html->image('/files/news/image/'.$relatedNew['News']['id'].'/'.$relatedNew['News']['image'], ['alt' => $relatedNew['News']['title'], 'class' => 'img-responsive']); ?>
											</figure>
										</a>
									<?php endIf; ?>
									<a href="<?php echo $this->Html->Url('/escrito/'. $relatedNew['News']['slug']); ?>" title="<?php echo $relatedNew['News']['title']; ?>">
										<h2><?php echo $relatedNew['News']['title']; ?></h2>
									</a>
								</header>
								<p><?php echo substr($relatedNew['News']['text'], 0, 100); ?></p>
								<a href="<?php echo $this->Html->Url('/escrito/'. $relatedNew['News']['slug']); ?>" class="btn btn-default" title="Ler mais sobre <?php echo $relatedNew['News']['title']; ?>">Leia mais</a>
							</article>
						</div>
						
						<?php if (($cont > 0) &&  ($cont % 3 == 0)) : ?>
							<div class="clearfix"></div>
						<?php endif; ?>
						<?php $cont++; ?>
					<?php endForeach; ?>
				</div>
			</div>
		</section>
	</div>
<?php endIf; ?>

<section class="clique-solidario hidden-sm hidden-xs">
	<div class="container">
		<div class="col-lg-8">
			<header>
				<h2>Num clique, você solidário. <br><span>Veja os vídeos e colabore com instituições.</span></h2>
			</header>
			<p>Nullam molestie enim leo. Aenean rhoncus sem metus, eu lobortis tellus tristique ac. Donec auctor nulla non turpis feugiat aliquam. Quisque nec ultrices ipsum. Maecenas dapibus risus sem. Quisque in suscipit nulla, non varius nisl.</p>
			<a href="#" class="btn btn-default" class="Saiba mais">Saiba mais</a>
		</div>
	</div>
</section>