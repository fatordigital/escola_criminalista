<?php /** @var $this View */ ?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-reorder"></i> Vendo dados do <?php echo $news['News']['title']; ?>
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" class="config">
            </a>
            <a href="javascript:;" class="reload">
            </a>
            <a href="javascript:;" class="remove">
            </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
		<div class="form-body">
			<h3 class="form-section">Informações</h3>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">Título:</label>
						<div class="col-md-9">
							<p class="form-control-static">
								<?php echo $news['News']['title']; ?>
							</p>
						</div>
					</div>
				</div>
			</div>
			<!--/row-->
			<h3 class="form-section">Texto</h3>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<div class="col-md-10 col-md-offset-1">
							<p class="form-control-static">
								<?php echo $news['News']['text']; ?>
							</p>
						</div>
					</div>
				</div>
			</div>
			<!--/row-->
			<h3 class="form-section">Imagem</h3>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<div class="col-md-10 col-md-offset-1">
							<p class="form-control-static">
								<img src="<?php echo $this->Html->Url('/files/news/image/'.$news['News']['id'].'/'.$news['News']['image'], true) ?>" alt="<?php echo $news['News']['title']; ?>" />
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="form-actions fluid">
			<div class="row">
				<div class="col-md-6">
					<div class="col-md-offset-3 col-md-9">
						<?php echo $this->Html->link(
							sprintf('%s <i class="fa fa-pencil"></i>', __d('users', "Edit News")),
							array('action' => 'edit', $news['News']['id']),
							array('escape' => false, 'class'  => 'btn green')
						); ?>
						<?php echo $this->Form->postLink(
							__d('users', "Delete News"),
							array('action' => 'delete', $news['News']['id']),
							array('escape' => false, 'class'  => 'btn red'),
							__d('users', 'Are you sure you want to delete # %s?', $news['News']['id']));
						?>
						<?php echo $this->Html->link(
							__d('users', 'Go back'),
							array('action' => 'index'),
							array('escape' => false, 'class' => 'btn default')
						); ?>
					</div>
				</div>
				<div class="col-md-6">
				</div>
			</div>
		</div>
        <!-- END FORM-->
    </div>
</div>
