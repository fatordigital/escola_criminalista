<?php

/**
 * @var $this View
 */
$this->Html->addCrumb(__d('users', 'News'));
$this->Html->addCrumb(__d('users', 'Listing'));
?>

<div class="row">
    <div class="col-md-12">
        <h2 class="page-title"><?php echo __d('users', "Admin Index News"); ?></h2>
        <?php echo $this->Html->getCrumbList(
            array(
                'class'     => 'breadcrumb',
                'separator' => '<i class="icon-angle-right"></i>',
                'escape'    => false
            ),
			'Home'
        );
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?php echo $this->Session->flash(); ?>
    </div>
    <div class="clearfix"></div>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>&nbsp;<?php echo __d('users', "Admin Index the News information"); ?>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                        <a href="javascript:;" class="reload"></a>
                        <a href="javascript:;" class="remove"></a>
                    </div>
                </div>
            </div>

            <div class="portlet-body">

                <div class="table-toolbar">
                    <?php echo $this->Html->link(
                        sprintf('%s <i class="fa fa-plus"></i>', __d('users', 'Add New')),
                        array('action' => 'add'),
                        array(
                            'escape' => false,
                            'class'  => 'btn green'
                        )
                    ); ?>
                </div>

                <?php if (count($news) > 0) : ?>

                <table class="table table-hover table-bordered table-striped table-condensed flip-content"
                       data-model="News">
                    <thead>
                    <tr>
                        <th><?php echo $this->Paginator->sort('id', 'Código'); ?></th>
                        <th><?php echo $this->Paginator->sort('title', 'Título'); ?></th>
                        <th class="actions" nowrap><?php echo __d('users', 'Actions'); ?></th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php foreach ($news as $n): ?>
                        <tr>
                            <td><?php echo h($n['News']['id']); ?>&nbsp;</td>
                            <td><?php echo h($n['News']['title']); ?>&nbsp;</td>
                            <td class="actions" nowrap>
                                <?php echo $this->Html->link(
                                    sprintf('<i class="fa fa-info"></i>&nbsp;&nbsp;%s</a>', __d('users', 'View')),
                                    array('action' => 'view', $n['News']['id']),
                                    array(
                                        'escape'         => false,
                                        'data-id'        => $n['News']['id'],
                                        'class'          => 'btn btn-sm blue btn-editable ajaxify',
                                        'original-title' => __d('users', 'View')
                                    )
                                ); ?>
                                <?php echo $this->Html->link(sprintf('<i class="fa fa-edit"></i>&nbsp;&nbsp;%s</a>', __d('users', 'Edit')),

                                    array(
                                        'action' => 'edit',
                                        $n['News']['id']
                                    ),
                                    array(
                                        'escape'         => false,
                                        'data-id'        => $n['News']['id'],
                                        'class'          => 'btn btn-sm green btn-editable ajaxify',
                                        'original-title' => __d('users', 'Edit')
                                    )
                                ); ?>
                                <?php echo $this->Form->postLink(
                                    sprintf('<i class="fa fa-times"></i>&nbsp;&nbsp;%s</a>', __d('users', 'Delete')),
                                    array(
                                        'action' => 'delete',
                                        $n['News']['id']),
                                    array(
                                        'escape'         => false,
                                        'class'          => 'btn btn-sm red btn-removable',
                                        'original-title' => __d('users', 'Delete')
                                    ),
                                    __d('users', 'Are you sure you want to delete # %s?', $n['News']['id'])
                                );
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>

                <?php if($this->params['paging']['News']['pageCount'] > 1): ?>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center clear">
                            <ul class="pagination customizada">
                                <?php echo $this->Paginator->prev( '«', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li' ) ); ?>
                                <?php echo $this->Paginator->numbers(['currentClass' => 'active', 'tag' => 'li']); ?>
                                <?php  echo $this->Paginator->next( '»', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li' ) ); ?>
                            </ul>
                        </div>
                    </div>
                    <p style="width: 100%; text-align: center;"><?php echo $this->Paginator->counter(array(
                        'format' => 'Página {:page} de {:pages}, exibindo {:current} registros de um total de {:count} registros.'
                    )); ?>
                    </p>
                <?php endIf; ?>

                <?php else: ?>

                    <p>Nenhum escrito encontrado.</p>

                <?php endif; ?>

            </div>
        </div>
    </div>
</div>
