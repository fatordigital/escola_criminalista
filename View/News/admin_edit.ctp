<?php

/**
 * @var $this View
 */

$this->Html->addCrumb(__d('users', 'News'));
$this->Html->addCrumb(__d('users', 'Admin Edit %s News', $this->Form->value('News.title')));

?>

<?php $this->Html->script("$cmsPluginName.assets/plugins/jquery-tags-input/jquery.tagsinput.min.js", ['inline' => false]); ?>
<?php $this->Html->css("$cmsPluginName.assets/plugins/jquery-tags-input/jquery.tagsinput.css", ['inline' => false]); ?>

<?php $this->Html->script("$cmsPluginName.assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js", ['inline' => false]); ?>
<?php $this->Html->css("$cmsPluginName.assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css", ['inline' => false]); ?>

<?php $this->Html->script("$cmsPluginName.assets/plugins/select2/select2.min.js", ['inline' => false]); ?>
<?php $this->Html->css("$cmsPluginName.assets/plugins/select2/select2.css", ['inline' => false]); ?>
<?php $this->Html->css("$cmsPluginName.assets/plugins/select2/select2-metronic.css", ['inline' => false]); ?>

<?php $this->Html->script("$cmsPluginName.assets/plugins/ckeditor/ckeditor", ['inline' => false]); ?>

<?php $this->Html->scriptStart(['inline' => false]) ?>
$('#NewsTags').tagsInput({
    width: 'auto'
});
<?php $this->Html->scriptEnd() ?>

<div class="row">
    <div class="col-md-12">
        <h2 class="page-title"><?php echo __d('users', 'Admin Add News'); ?></h2>
        <?php echo $this->Html->getCrumbList(
            array(
                'class'     => 'breadcrumb',
                'separator' => '<i class="icon-angle-right"></i>',
                'escape'    => false
            ),
            'Home'
        );
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <?php echo $this->Session->flash(); ?>
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <?php echo __d('users', 'Admin Edit the News information'); ?>
                </div>
            </div>

            <div class="portlet-body form movies">

                <?php echo $this->Form->create(
                    'News',
                    array(
                        'inputDefaults' => array(
                            'div'        => 'form-group',
                            'wrapInput'  => 'col col-md-9',
                            'label'      => array(
                                'class' => 'col col-md-3 control-label'
                            ),
                            'class'      => 'form-control',
                            'novalidate' => 'novalidate'
                        ),
                        'class'         => 'form-horizontal',
                        'type'       	=> 'file',
                    )
                ); ?>

                <div class="form-body">

                    <?php
					echo $this->Form->hidden('id');
                    echo $this->Form->input('title', ['label' => 'Titulo']);
                    echo $this->Form->input('text', ['label' => 'Conteúdo', 'class' => 'ckeditor']);
                    echo $this->Form->input('image', ['label' => 'Imagem', 'type' => 'file']);
					?>
					<?php
						if($this->data['News']['image'] != ""):
					?>
					<div class="form-group content-image-news">
						<label class="col col-md-3 control-label"></label>
						<div class="col col-md-9">
							<img src="<?php echo $this->Html->Url('/files/news/image/'.$this->data['News']['id'].'/'.$this->data['News']['image'], true); ?>" alt="" width="150" />
							<?php echo $this->Form->input('News.image.remove', array('type' => 'checkbox', 'label' => 'Remover imagem existente')); ?>
						</div>
					</div>
					<?php 
						endIf;
					?>
					<?php
					echo $this->Form->input('tags',[
                        'class' => 'form-control tags'
                    ]);

                    ?>

                    <div class="form-actions">

                        <?php echo $this->Form->submit(__d('users', 'Submit'), array(
                            'div'    => false,
                            'class'  => 'btn blue',
                            'escape' => false,
                        )); ?>


                    </div>


                </div>
                <?php echo $this->Form->end(); ?>

            </div>
        </div>
    </div>
</div>
