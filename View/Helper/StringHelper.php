<?php
App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class StringHelper extends Helper {
	
	const STATUS_ATIVO = 'Ativo';
	const STATUS_INATIVO = 'Inativo'; 
	
	public function getStatus($valor) {
	    switch ($valor) {
			case '1':
				return self::STATUS_ATIVO;
				break;
			
			default:
				return self::STATUS_INATIVO;
				break;
		}		
	} 
	
	/**
	* Formata data para a string date passada
	*
	* @param string Formato (utilizar nota��o do PHP), se n�o informada gera DateTime Sem Timezone
	* @param string Data
	* @param string Data relativa (quando necess�rio para c�lculo, como "pr�xima segunda")
	*/
	public function DataFormatada($formato=null,$data=null,$relative=null){
		if(is_null($data)) $data = time();
		if(is_null($formato)) $formato = 'Y-m-d H:i:s';
		return date($formato,$this->DataToTime($data,$relative));
	}
	
	/**
	* Retorna o timestamp da data informada, é uma versão otimizada do strtotime
	*
	* @param string Data
	* @param string Data relativa (quando necessário para cálculo, como "próxima segunda")
	*/
	private function DataToTime($data,$relative=null){
		if(is_numeric($data)) return $data;
		if(preg_match('/([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{4}|[0-9]{2})[ \t\n\r]*([0-9]{2})[ \t\n\r]*(:|h|hour|hours|horas|hr)[ \t\n\r]*([0-9]{2})[ \t\n\r]*(:|m|min|mins|minutos|minuto|mn)[ \t\n\r]*([0-9]{2})/i',$data,$m)){
			$hora = $m[4]; $minuto = $m[6]; $segundo = $m[8]; $dia = $m[1]; $mes = $m[2]; $ano = $m[3];
		}elseif(preg_match('/([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{4}|[0-9]{2})[ \t\n\r]*([0-9]{2})[ \t\n\r]*(:|h|hour|hours|horas|hr)[ \t\n\r]*([0-9]{2})[ \t\n\r]*(:|m|min|mins|minutos|minuto|mn)/i',$data,$m)){
			$hora = $m[4]; $minuto = $m[6]; $segundo = 0; $dia = $m[1]; $mes = $m[2]; $ano = $m[3];
		}elseif(preg_match('/([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{4}|[0-9]{2})[ \t\n\r]*([0-9]{2})[ \t\n\r]*(:|h|hour|hours|horas|hr)/i',$data,$m)){
			$hora = $m[4]; $minuto = 0; $segundo = 0; $dia = $m[1]; $mes = $m[2]; $ano = $m[3];
		}elseif(preg_match('/([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{4}|[0-9]{2})/',$data,$m)){
			$hora = 0; $minuto = 0; $segundo = 0; $dia = $m[1]; $mes = $m[2]; $ano = $m[3];
		}elseif(preg_match('/([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{1,2})/',$data,$m)){
			$hora = 0; $minuto = 0; $segundo = 0; $dia = $m[1]; $mes = $m[2]; $ano = date('Y',is_null($relative)?time():$relative);
		}elseif(preg_match('/([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{4})/',$data,$m)){
			$hora = 0; $minuto = 0; $segundo = 0; $dia = 1; $mes = $m[1]; $ano = $m[2];
		}else{
			$tr = array(
				'hoje'=>'today','dia'=>'day', 'semana'=>'week', 'hora'=>'hour', 'minuto'=>'minute', 'segundo'=>'second',
				'meses'=>'months','mês'=>'month','mes'=>'month', 'ano'=>'year',
				'proxima'=>'next', 'próxima'=>'next',  'próximo'=>'next',  'próximo'=>'next', 
				'Última'=>'last', 'ultima'=>'last','ultimo'=>'last','Último'=>'last',
				'segunda-feira'=>'monday','segunda'=>'monday','terça-feira'=>'tuesday','terça'=>'tuesday',
				'quarta-feira'=>'wednesday','quarta'=>'wednesday', 'quinta-feira'=>'thursday','quinta'=>'thursday', 
				'sexta-feira'=>'friday','sexta'=>'friday', 'sábado'=>'saturday','sabado'=>'saturday', 'domingo'=>'sunday',
				'janeiro'=>'january', 'jan'=>'january', 'fevereiro'=>'february','fev'=>'february', 
				'março'=>'march','mar'=>'march', 'abril'=>'april', 'abr'=>'april',
				'maio'=>'may','mai'=>'may', 'junho'=>'june', 'jun'=>'june', 
				'julho'=>'july','jul'=>'july', 'agosto'=>'august', 'ago'=>'august', 
				'setembro'=>'september','set'=>'september', 'outubro'=>'october', 'out'=>'october', 
				'novembro'=>'november','nov'=>'november', 'dezembro'=>'december','dez'=>'december',
				'depois de amanhã'=>'+2 day','depois de amanha'=>'+2 day',
				'anteontem'=>'-2 day',
				'amanhã'=>'tomorrow','amanha'=>'tomorrow','ontem'=>'yesterday',' de '=>''
			
			);
			return strtotime(str_ireplace(array_keys($tr),array_values($tr),$data),is_null($relative)?time():($this->DataToTime($relative)));		
		}
		return mktime($hora,$minuto,$segundo,$mes,$dia,$ano);
	}

	public function diasemana($diasemana) {
		switch($diasemana) {
			case"0": $diasemana = "Domingo";       break;
			case"1": $diasemana = "Segunda-Feira"; break;
			case"2": $diasemana = "Terça-Feira";   break;
			case"3": $diasemana = "Quarta-Feira";  break;
			case"4": $diasemana = "Quinta-Feira";  break;
			case"5": $diasemana = "Sexta-Feira";   break;
			case"6": $diasemana = "Sábado";        break;
		}
		return $diasemana;
	}

	public function mes($mes) {
		switch($mes) {
			case"1": $mes = "Janeiro"; 		break;
			case"2": $mes = "Fevereiro";   	break;
			case"3": $mes = "Março";  		break;
			case"4": $mes = "Abril";  		break;
			case"5": $mes = "Maio";   		break;
			case"6": $mes = "Junho";        break;
			case"7": $mes = "Julho";        break;
			case"8": $mes = "Agosto";       break;
			case"9": $mes = "Setembro";     break;
			case"10": $mes = "Outubro";     break;
			case"11": $mes = "Novembro";    break;
			case"12": $mes = "Dezembro";    break;
		}
		return $mes;
	}
	public function truncate_str($str, $maxlen) {
		if ( strlen($str) <= $maxlen ) return $str;

		$newstr = substr($str, 0, $maxlen);
		if ( substr($newstr,-1,1) != ' ' ) 
			$newstr = substr($newstr, 0, strrpos($newstr, " "));

		return $newstr.'...';
	}
}
