<?php /** @var $this View */ ?>
<?php /*
<?php
$bg = array('sobre01.jpg', 'sobre02.jpg');
$i = rand(0, count($bg)-1);


if (isset($_SESSION['back']) && $_SESSION['back'] == $i) {
    while($_SESSION['back'] == $i){
        $i = rand(0, count($bg)-1);            
    }
    $_SESSION['back'] = $i; 
}else{
    $_SESSION['back'] = $i; 
}
  
$selectedBg = $bg[$_SESSION['back']];
?>

<style>
section.assista-aulas {
    background: url("../imagens/capas/<?php echo $selectedBg; ?>") no-repeat scroll center 0 #FFFFFF !important;
}

.sobre section.assista-aulas h2 span {
    color: #000;
}

.sobre section.assista-aulas p {
    color: #000;
}

</style> 
<section class="assista-aulas">
    <div class="container">
        <div class="col-lg-7">
            <article>
                <h2><span>POR QUÊ? PORQUE NÓS QUEREMOS MUDAR O MUNDO. FAÇA PARTE!</h2>

                <p>O Instituto Casa da Tolerância defende a construção de uma escola voltada para a produção de uma cidadania crítica, pensante, ativa, tolerante, na qual a cumplicidade entre os agentes da produção do conhecimento seja o ponto de partida para o desafio do aprendizado mútuo.</p>
            </article>
        </div>
    </div>
</section>
*/ ?>

<section class="sobre">
    <div class="container">
        <div class-"alinha">
            <div class="col-xs-12">
                <h2 class="text-left">Escola de Criminalistas</h2><br>
                <p>
                    A Escola de Criminalistas é fundada sobre as bases de um fazer artesanal da advocacia criminal. Isto implica em uma perspectiva de ensino-aprendizagem em que o foco recai sobre a transmissão de um ofício, o de criminalista.
                </p>
                <p>
                    Na transmissão de um ofício, o saber-fazer é aprendido de maneira prática, formal, no espaço da oficina, no contato do aprendiz com a matéria-prima, na observação e escuta da instrução-chave que não está descrita em manual algum, mas que se dá a conhecer pela palavra-ato do artesão mais antigo.
                </p>
                <p>
                    A advocacia criminal como fazer artesanal, para além das instruções dos manuais, impõe conhecer em profundidade os ritmos, as pausas, os fluxos, inflexões e tons em jogo no processo penal, sua matéria-prima. E se o produto artesanal é sempre o resultado do ato do artesão e como tal traz impressa a marca de sua personalidade, o saber-fazer, no ofício do criminalista, também implica um saber-ser.
                </p>
                <p>
                    É assim que a Escola de Criminalistas propõe uma experiência de ensino-aprendizagem vivencial cujo fio condutor é necessariamente o processo penal e cuja metodologia dá acesso tanto às sutilezas da prática processual quanto ao desenvolvimento de competências comportamentais fundamentais para a excelência no exercício do ofício de criminalista.
                </p>
                <p>
                    Nesta construção, os participantes escolhem e são escolhidos pela Escola de Criminalistas segundo critérios que testemunham reciprocidade, admiração pessoal e profissional, afinidade de princípios, confiabilidade e potencial de transformação. A viga-mestra, um forte pacto de confidencialidade, já que as informações tratadas dizem respeito ao delicado campo da defesa, suas estratégias, às relações com os diversos atores envolvidos no processo, às angústias causadas pela solidão deste fazer na contramão da opinião pública, assim como pelas dificuldades da administração de uma carreira sustentável como criminalista.
                </p>
                <p>
                    Uma Escola de Criminalistas com a determinação de propor um corajoso percurso de estudo, debate e transformação sobre as questões mais tormentosas e delicadas que povoam o trabalho do advogado criminal.
                </p>
                <p>
                    Agora existe uma. E é toda nossa!
                </p>
                <p>
                    <i>Jader Marques</i>
                </p>

            </div>
        </div>
        <?php /*<div class="alinha">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <h2>Missão</h2>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <p>Promover a tolerância, enquanto respeito e aceitação da diferença, buscando a valorização dos direitos humanos por meio da defesa de políticas públicas e privadas que priorizem a educação como estratégia de prevenção à violência.</p>
                <hr>
            </div>
        </div>
        <div class="alinha">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <h2>Visão</h2>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <p>Ser uma instituição brasileira de referência na promoção da tolerância como proposta de educação e conscientização das pessoas, a fim de se tornarem protagonistas na construção de uma sociedade diversa, plural e baseada na cultura da paz.</p>
                <hr>
            </div>
        </div>
        <div class="alinha">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <h2>Objetivos</h2>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <p>Promover campanhas abertas de conscientização.</p>
                <p>Realizar palestras, cursos, aulas públicas, mobilizações, vídeos e transmissões via internet.</p>
                <p>Trabalhar na divulgação de práticas tolerantes no âmbito do poder público e do setor privado.</p>
                <hr>
            </div>
        </div>
        <div class="alinha">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <h2>Princípios</h2>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <p>1. Todos somos dignos.</p>
                <p>2. Todos merecemos respeito.</p>
                <p>3. Todos somos falíveis.</p>
                <p>4. Todos queremos visibilidade.</p>
                <p>5. Todos queremos amor.</p>
                <p>6. Todos temos habilidades.</p>
                <p>7. Todos queremos liberdade.</p>
                <p>8. Todos somos diferentes.</p>
                <p>9. Todos somos capazes.</p>
                <p>10. Todos podemos tudo.</p>
                <hr>
            </div>
        </div>
        <div class="alinha">
            <div class="col-lg-4 col-md-4 hidden-sm hidden-xs"></div>
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <h3>Faça parte!<br>Faça a sua parte!</h3>
            </div>
        </div>*/ ?>
    </div>
</section>

<?php /*<div class="container">
    <section class="tolerante">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <?php echo $this->Html->image('imagens/quote-cima.png', ['class' => 'quote-top', 'alt' => 'Quote']); ?>
            <article class="quote">
                <p>Tendência a admitir, nos outros, maneiras de pensar, de agir e de sentir diferentes ou mesmo
                    diametralmente opostas às adotadas por si mesmo.</p>
            </article>
            <?php echo $this->Html->image('imagens/quote-baixo.png', ['class' => 'quote-bot', 'alt' => 'Quote']); ?>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 clear">
            <header>
                <h2>Instituto Tolerância</h2>
            </header>
            <article>
                <p>O INSTITUTO TOLERÂNCIA é um portal de educação à distância diferente.</p>
                <p>Nosso objetivo é gerar conhecimento com entretenimento, crítica com respeito à diversidade, crescimento pessoal com valorização do coletivo.</p>
                <p>Queremos pessoas tolerantes que enfrentem o desafio do conhecimento com a coragem dos insatisfeitos, com a angústia dos esclarecidos, com a responsabilidade dos livres.</p>
                
				<p>O INSTITUTO TOLERÂNCIA é um espaço cultural autônomo voltado para a discussão de temas ligados ao Direito, a partir de uma regra fundamental: a tolerância!</p>
				<p>Nós acreditamos que o ato de conhecer pode e deve acontecer num ambiente capaz de proporcionar uma experiência desafiadora, crítica, de experimentação e exploração de novas possibilidades, sempre com respeito à diferença. Teoria prática e prática teórica.</p>
				<p>Nós entendemos ser indispensável romper com o modo tradicional de discutir a Ciência Jurídica, o que só pode acontecer pela aceitação da complexidade dos temas ligados ao Direito. Por isso, buscamos a noção de escola como vivência inovadora, como ambiente no qual o conhecer acontece como sensação única, desafiadora e reflexiva.</p>
				<p>O INSTITUTO TOLERÂNCIA é uma sociedade comercial que reverte sua arrecadação para o financiamento da própria atividade. Não estamos vinculados a qualquer órgão estatal de controle das entidades de ensino e não trabalhamos para fornecer certificados. Definitivamente, não estamos preocupados com formalidades ou outras questões diferentes da vivência do conhecimento.</p>
				<p>Um lugar onde o estudo do Direito seja uma experiência agradável aos sentidos e uma vivência desafiadora à inteligência.</p>
				<p>Um lugar onde o conhecimento seja crítico, tolerante e solidário.</p>
				
				<p>Faça parte!</p>
                <p>Jader Marques<br />Diretor</p>
            </article>
        </div>
    </section>
</div>*/ ?>

<?php /* 24/10
<div class="container">
    <section class="tolerante">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <?php echo $this->Html->image('imagens/quote-cima.png', ['class' => 'quote-top', 'alt' => 'Quote']); ?>
            <article class="quote">
                <p>Tendência a admitir, nos outros, maneiras de pensar, de agir e de sentir diferentes ou mesmo
                    diametralmente opostas às adotadas por si mesmo.</p>
            </article>
            <?php echo $this->Html->image('imagens/quote-baixo.png', ['class' => 'quote-bot', 'alt' => 'Quote']); ?>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 clear">
            <header>
                <h2>Instituto Tolerância</h2>
            </header>
            <article>
                <p>O Instituto Tolerância é um portal de educação à distância diferente e que
pretende fazer a diferença.</p>
                <p>O Instituto surgiu com o nome polêmico Casa da Tolerância e veio para fazer
um contraponto aos discursos punitivistas do tipo Tolerância Zero. Na
primeira temporada, buscamos estabelecer parcerias com instituições de
ensino, com diretórios acadêmicos, com a OAB, com professores e alunos de
várias localidades do País. O discurso da tolerância esteve presente em cada
vídeo produzido. A todas e a todos que nos ajudaram a dar esse primeiro
passo, o registro verdadeiro do nosso MUITO OBRIGADO.</p>
                <p>Na nova temporada, o projeto muda de nome. Passa a chamar-se INSTITUTO
TOLERÂNCIA. Nosso objetivo, agora, é discutir as possibilidades de redução
dos danos provocados pelo Estado em matéria criminal: em busca do mínimo
penal.</p>
                <p>Estamos organizando e pensando o que vem por aí. Uma vez por semana, uma
manifestação forte, marcando posição a respeito dos fatos que estejam na
pauta de discussões da imprensa e das redes sociais. Uma vez por mês, um
vídeo-documentário em série, com cenas da realidade da justiça criminal. A
qualquer tempo, vídeos de cursos sobre temas que façam a ligação da prática
com a teoria e vice-versa.</p>
                <p>Nosso objetivo é gerar conhecimento com entretenimento, crítica com respeito
à diversidade, crescimento pessoal com valorização do coletivo. Queremos
pessoas tolerantes que enfrentem o desafio do conhecimento com a coragem dos
insatisfeitos, com a angústia dos esclarecidos, com a responsabilidade dos
livres.</p>
                <p>Não entregamos certificados ou outros papéis, apenas vivências.</p>
                <p>Faça parte!</p>
                <p><b>Instituto Tolerância</b></p>
            </article>
        </div>
    </section>
</div>

*/ ?>

<?php /*
<div class="container">
    <section class="tolerante">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <?php echo $this->Html->image('imagens/quote-cima.png', ['class' => 'quote-top', 'alt' => 'Quote']); ?>
            <article class="quote">
                <p>Tendência a admitir, nos outros, maneiras de pensar, de agir e de sentir diferentes ou mesmo
                    diametralmente opostas às adotadas por si mesmo.</p>
            </article>
            <?php echo $this->Html->image('imagens/quote-baixo.png', ['class' => 'quote-bot', 'alt' => 'Quote']); ?>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 clear">
            <header>
                <h2>Casa da Tolerância</h2>
            </header>
            <article>
                <p>O Instituto Casa da Tolerância é um portal de educação à distância diferente e que pretende fazer a diferença.</p>
                <p>Nosso objetivo é gerar conhecimento com entretenimento, crítica com respeito à diversidade, crescimento pessoal com valorização do coletivo. Queremos pessoas tolerantes que enfrentem o desafio do conhecimento com a coragem dos insatisfeitos, com a angústia dos esclarecidos, com a responsabilidade dos livres.</p>
                <p>Não entregamos certificados ou outros papéis, apenas vivências.</p>
                <p>Nós queremos apenas mudar o mundo.</p>
                <p>Faça parte!</p>
                <p><b>Instituto Casa da Tolerância</b></p>
                <p><b>Direito e Ousadia.</b></p>
            </article>
        </div>
    </section>
</div>
*/ ?>
<?php /*
<section class="razoes destaque ">
    <div class="container">
        <div class="row-fluid">
            <article>
                <div class="col-lg-4">
                    <header>
                        <hgroup>
                            <h2>POR QUE UMA CASA DA TOLERÂNCIA?</h2>
                            <h4>Quem aprende também ensina e quem ensina também aprende.</h4>
                        </hgroup>
                    </header>
                </div>
                <div class="col-lg-4">
                    <p>
                        Paulo Freire, no livro Pedagogia da Tolerância (2005), fala de educação e
                        cidadania, destacando que o exercício da cidadania não é algo mágico, ou
                        seja, que não adianta, simplesmente, pronunciarmos a palavra cidadania para
                        sermos cidadãos. O exercício da cidadania é expressamente um ato político
                        e, neste contexto, os educadores devem ter a consciência de que sua ação é,
                        sempre, uma ação política comprometida com a produção da cidadania.
                    </p>
                    <p>
                        Para que a escola seja um espaço voltado para a produção de cidadania é
                        fundamental que lutemos pela mudança da maneira mecanicista de pensar o
                        aprender e o ensinar. Devemos buscar uma dialética, na qual ensinar não seja
                        uma mera transferência de dados, uma entrega do que está pré-constituído,
                        do que está velho, usado, surrado.
                    </p>
                    <p>
                        Os atos de ensinar e aprender se constituem numa relação que produz
                        conhecimento e não podem ser encarados como uma fórmula matemática,
                        separando teoria e prática, isolando o mundo da vida, sempre tão complexo
                        e tão vasto para caber no discurso abstrato e purificado do mundo dos
                        conceitos.
                    </p>
                </div>
                <div class="col-lg-4">
                    <p>
                        Devemos discutir a separação entre ensinar os conhecimentos existentes e
                        produzir novos conhecimentos, para aceitarmos a necessária ligação do ato
                        de ensinar/aprender com o ato de investigar. A investigação deve ser tomada
                        como parte necessária e indispensável do ato de ensinar. É pela investigação
                        que se torna possível a produção do novo.
                    </p>
                    <p>
                        O Instituto Casa da Tolerância defende a construção de uma escola voltada
                        para a produção de uma cidadania crítica, pensante, ativa, tolerante, na qual a
                        cumplicidade entre os agentes da produção do conhecimento seja o ponto de
                        partida para o desafio do aprendizado mútuo.
                    </p>
                    <p>
                        Nessa relação de cúmplices, há um verdadeiro ato de comunhão, onde
                        todas as partes vivem uma relação ativa de participação: todos são sujeitos
                        responsáveis pela construção do conhecimento. Essa comunhão não apaga
                        a diferença de papéis, mas torna legítima a autoridade e convoca o respeito
                        como pressuposto da relação de poder existente entre educadores e
                        educandos. A autoridade e o poder a ela imanente se manifestam a partir da
                        consciência de que todos os envolvidos são responsáveis pela construção do
                        conhecimento, que acontece quando a investigação coloca em curso o desafio
                        de buscar o novo em sua maravilhosa originalidade e plenitude.
                    </p>
                </div>
            </article>
        </div>
    </div>
</section>
*/ ?>

<?php /*
<section class="como-functiona destaque">
    <div class="container">
        <header>
            <h1>Como funciona?</h1>
        </header>
        <div class="row-fluid">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 margin-bottom-30">
                <header class="imagem">
                    <figure class="text-right">
                        <?php echo $this->Html->image('imagens/videos-gratuitos.jpg',
                            ['class' => 'img-responsive fullsize-images', 'alt' => 'Vídeos gratutios']); ?>
                            <span>
                                <?php echo $this->Html->image('imagens/categoria-video-preview-1.png',
                                    ['class' => 'img-responsive categoria-video', 'alt' => 'Tipo de vídeo']); ?>
                            </span>
                    </figure>
                </header>
                <h2 class="laranja">Livre</h2>

                <p>O Instituto Casa da Tolerância disponibilizará filmes produzidos especialmente para gerar discussão daqueles pontos mais polêmicos e espera contar com a participação de todos os assinantes, que poderão fazer sugestão de pautas e roteiros ou mandar seu material gravado para ser discutido na Casa.</p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 margin-bottom-30">
                <header class="imagem">
                    <figure class="text-right">
                        <?php echo $this->Html->image('imagens/videos-pagos.jpg',
                            ['class' => 'img-responsive fullsize-images', 'alt' => 'Vídeos pagos']); ?>
                        <span>
                                <?php echo $this->Html->image('imagens/categoria-video-preview-2.png',
                                    ['class' => 'img-responsive categoria-video', 'alt' => 'Tipo de vídeo']); ?>
                            </span>
                    </figure>
                </header>
                <h2 class="azul">Restrito</h2>

                <p>Alguns filmes serão disponibilizados exclusivamente para os assinantes, pois um projeto como este, para que tenha continuidade, necessita de alguma participação daqueles que acreditam na ideia. Os Cursos e os Eventos de outras entidades serão apresentados com descontos especiais para os assinantes.</p>
            </div>
            <div class="col-lg-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 margin-bottom-30">
                <header class="imagem">
                    <figure class="text-right">
                        <?php echo $this->Html->image('imagens/clique-solidario.jpg',
                            ['class' => 'img-responsive fullsize-images', 'alt' => 'Clique Solidário']); ?>
                        <span>
                                <?php echo $this->Html->image('imagens/categoria-video-preview-3.png',
                                    ['class' => 'img-responsive categoria-video', 'alt' => 'Tipo de vídeo']); ?>
                            </span>
                    </figure>
                </header>
                <h2 class="verde">Clique solidário</h2>

                <p>Num clique, você solidário. O Instituto Casa da Tolerância permite que você contribua com instituição conveniada ao escolher o filme do seu interesse. Nesta primeira temporada: FAESP – Fundação de Apoio ao Egresso do Sistema Penitenciário. </p>
            </div>
        </div>
        <div class="col-lg-12">
            <span class="detalhe"><strong>Observação:</strong> Nós não entregamos certificados ou outros papéis. Nós entregamos vivências.</span>
        </div>
    </div>
</section>
*/ ?>

