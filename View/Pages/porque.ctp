<section class="razoes destaque ">
    <div class="container">
        <div class="row-fluid">
            <article>
                <div class="col-lg-4">
                    <header>
                        <hgroup>
                            <h2>POR QUE UMA CASA DA TOLERÂNCIA?</h2>
                            <h4>Quem aprende também ensina e quem ensina também aprende.</h4>
                        </hgroup>
                    </header>
                </div>
                <div class="col-lg-4">
                    <p>
                        Paulo Freire, no livro Pedagogia da Tolerância (2005), fala de educação e
                        cidadania, destacando que o exercício da cidadania não é algo mágico, ou
                        seja, que não adianta, simplesmente, pronunciarmos a palavra cidadania para
                        sermos cidadãos. O exercício da cidadania é expressamente um ato político
                        e, neste contexto, os educadores devem ter a consciência de que sua ação é,
                        sempre, uma ação política comprometida com a produção da cidadania.
                    </p>
                    <p>
                        Para que a escola seja um espaço voltado para a produção de cidadania é
                        fundamental que lutemos pela mudança da maneira mecanicista de pensar o
                        aprender e o ensinar. Devemos buscar uma dialética, na qual ensinar não seja
                        uma mera transferência de dados, uma entrega do que está pré-constituído,
                        do que está velho, usado, surrado.
                    </p>
                    <p>
                        Os atos de ensinar e aprender se constituem numa relação que produz
                        conhecimento e não podem ser encarados como uma fórmula matemática,
                        separando teoria e prática, isolando o mundo da vida, sempre tão complexo
                        e tão vasto para caber no discurso abstrato e purificado do mundo dos
                        conceitos.
                    </p>
                </div>
                <div class="col-lg-4">
                    <p>
                        Devemos discutir a separação entre ensinar os conhecimentos existentes e
                        produzir novos conhecimentos, para aceitarmos a necessária ligação do ato
                        de ensinar/aprender com o ato de investigar. A investigação deve ser tomada
                        como parte necessária e indispensável do ato de ensinar. É pela investigação
                        que se torna possível a produção do novo.
                    </p>
                    <p>
                        O Instituto Casa da Tolerância defende a construção de uma escola voltada
                        para a produção de uma cidadania crítica, pensante, ativa, tolerante, na qual a
                        cumplicidade entre os agentes da produção do conhecimento seja o ponto de
                        partida para o desafio do aprendizado mútuo.
                    </p>
                    <p>
                        Nessa relação de cúmplices, há um verdadeiro ato de comunhão, onde
                        todas as partes vivem uma relação ativa de participação: todos são sujeitos
                        responsáveis pela construção do conhecimento. Essa comunhão não apaga
                        a diferença de papéis, mas torna legítima a autoridade e convoca o respeito
                        como pressuposto da relação de poder existente entre educadores e
                        educandos. A autoridade e o poder a ela imanente se manifestam a partir da
                        consciência de que todos os envolvidos são responsáveis pela construção do
                        conhecimento, que acontece quando a investigação coloca em curso o desafio
                        de buscar o novo em sua maravilhosa originalidade e plenitude.
                    </p>
                </div>
            </article>
        </div>
    </div>
</section>

<?php echo $this->element('banners'); ?>
