<section class="banner-curso hidden-sm hidden-xs">
	<div class="container">
		<div class="col-lg-7 col-md-7 text-right">
			<article>
				<h2>Cursos voltados para o conhecimento teórico a partir de alguma prática. </h2>
				<p>Deixar o “mundo da vida” sujar o discurso abstrato e purificado do “mundo dos conceitos”.</p>
			</article>
		</div>
	</div>
</section>
<section class="destaque cursos">
	<div class="container">
		<header class="">
			<h2>Curso de Prática da Advocacia Criminal <span>– Tribunal do Júri</span></h2>
		</header>
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center">
				<h3>Quatro módulos<br /><span>(em breve)</span></h3>
			</div>
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
				<div class="row">
					<div class="modulos">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<article>
								<span>1º.)</span> Local de crime, Inquérito, relacionamento com cliente e familiares; Pedido de Liberdade e Habeas Corpus
							</article>
							<article>
								<span>2º.)</span> Primeira fase do Procedimento - denúncia, resposta, instrução, decisão e recursos; 
							</article>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<article>
								<span>3º.)</span> Segunda fase do Procedimento - requerimentos para plenário, preparação do trabalho de plenário, apresentação da peroração (saudação, desenvolvimento, exposição da tese, estudo dos quesitos); incidentes de plenário, nulidades, arguições, apartes; 
							</article>
							<article>
								<span>4º.)</span> Estudo de situações práticas em aula presencial e on line.
							</article>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>