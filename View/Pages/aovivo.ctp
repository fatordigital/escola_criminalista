<section class="ao-vivo-banner hidden-sm hidden-xs">
    <div class="container">
        <div class="col-lg-7 col-lg-offset-5 text-right">
            <header>
                <h1>Acompanhe às transmissões <span>ao vivo</span><br>Acompanhe a agenda e veja os vídeos em tempo real</h1>
            </header>
            <p>O Instituto Casa da Tolerância possui um estúdio para as transmissões ao vivo</p>
        </div>
    </div>
</section>
<section class="destaque aulas-ao-vivo">
    <div class="container">
        <div class="row-fluid">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 box">
                <article>
                    <header>
                        <span>Quarta-feira,<br>12 de janeiro</span>

                        <h2>Título do filme</h2>
                    </header>
                    <p>Nullam molestie enim leo. Aenean rhoncus sem metus, eu lobortis tellus tristique ac. Donec auctor
                        nulla non turpis feugiat aliquam.</p>
                    <a href="#" class="btn btn-default comprar" title="Comprar">Comprar</a>
                    <a href="#" class="btn btn-default saiba-mais" title="Saiba mais">Saiba mais</a>
                </article>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 box">
                <article>
                    <header>
                        <span>Quarta-feira,<br>12 de janeiro</span>

                        <h2>Título do filme</h2>
                    </header>
                    <p>Nullam molestie enim leo. Aenean rhoncus sem metus, eu lobortis tellus tristique ac. Donec auctor
                        nulla non turpis feugiat aliquam.</p>
                    <a href="#" class="btn btn-default comprar" title="Comprar">Comprar</a>
                    <a href="#" class="btn btn-default saiba-mais" title="Saiba mais">Saiba mais</a>
                </article>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 box">
                <article>
                    <header>
                        <span>Quarta-feira,<br>12 de janeiro</span>

                        <h2>Título do filme</h2>
                    </header>
                    <p>Nullam molestie enim leo. Aenean rhoncus sem metus, eu lobortis tellus tristique ac. Donec auctor
                        nulla non turpis feugiat aliquam.</p>
                    <a href="#" class="btn btn-default comprar" title="Comprar">Comprar</a>
                    <a href="#" class="btn btn-default saiba-mais" title="Saiba mais">Saiba mais</a>
                </article>
            </div>
        </div>
        <div class="row-fluid">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 box">
                <article>
                    <header>
                        <span>Quarta-feira,<br>12 de janeiro</span>

                        <h2>Título do filme</h2>
                    </header>
                    <p>Nullam molestie enim leo. Aenean rhoncus sem metus, eu lobortis tellus tristique ac. Donec auctor
                        nulla non turpis feugiat aliquam.</p>
                    <a href="#" class="btn btn-default comprar" title="Comprar">Comprar</a>
                    <a href="#" class="btn btn-default saiba-mais" title="Saiba mais">Saiba mais</a>
                </article>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 box">
                <article>
                    <header>
                        <span>Quarta-feira,<br>12 de janeiro</span>

                        <h2>Título do filme</h2>
                    </header>
                    <p>Nullam molestie enim leo. Aenean rhoncus sem metus, eu lobortis tellus tristique ac. Donec auctor
                        nulla non turpis feugiat aliquam.</p>
                    <a href="#" class="btn btn-default comprar" title="Comprar">Comprar</a>
                    <a href="#" class="btn btn-default saiba-mais" title="Saiba mais">Saiba mais</a>
                </article>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 box">
                <article>
                    <header>
                        <span>Quarta-feira,<br>12 de janeiro</span>

                        <h2>Título do filme</h2>
                    </header>
                    <p>Nullam molestie enim leo. Aenean rhoncus sem metus, eu lobortis tellus tristique ac. Donec auctor
                        nulla non turpis feugiat aliquam.</p>
                    <a href="#" class="btn btn-default comprar" title="Comprar">Comprar</a>
                    <a href="#" class="btn btn-default saiba-mais" title="Saiba mais">Saiba mais</a>
                </article>
            </div>
        </div>
    </div>
</section>
