<?php
/**
 * Created by PhpStorm.
 * User: leonel
 * Date: 10/03/14
 * Time: 15:08
 */
?>

<?php echo $this->Html->scriptStart(['inline' => false]) ?>

function modalPagamento(id) {
    $.blockUI({
        css: {
            border: 'none',
            padding: '5px',
            width: '600px',
            backgroundColor: '#FFFFFF',
            '-webkit-border-radius': '5px',
            '-moz-border-radius': '5px',
        },
    message: $('#modalblock-'+id)
    });
}


function fechar() {
    $.unblockUI();
}

$(document).ready(function(){
	$('a.saiba-mais-solidariedade').click(function(){	
		$.blockUI({
	        css: {
	            border: 'none',
	            padding: '5px',
	            width: '600px',
	            backgroundColor: '#FFFFFF',
	            'border-radius': '5px',
	        },
	    	message: $('#mensagem-solidariedade')
	    });
	});

    $('.btn_fechar').click(function() {
        fechar();
    });
    
});

<?php echo $this->Html->scriptEnd() ?>
<?php /*
<?php
$bg = array('solidario.jpg');
$i = rand(0, count($bg)-1);


if (isset($_SESSION['back']) && $_SESSION['back'] == $i) {
    while($_SESSION['back'] == $i){
        $i = rand(0, count($bg)-1);            
    }
    $_SESSION['back'] = $i; 
}else{
    $_SESSION['back'] = $i; 
}
  
$selectedBg = $bg[$_SESSION['back']];
?>

<style>
section.clique-solidario {
    background: url("../imagens/capas/<?php echo $selectedBg; ?>") no-repeat scroll center 0 #FFFFFF !important;
}

</style> 
<section class="clique-solidario hidden-sm hidden-xs">
		<div class="container">
			<div class="col-lg-8">
				<header>
					<h2>Um clique pode ser muito, pode ser solidário.<br><span>Veja os vídeos e colabore com as instituições selecionadas.</span></h2>
				</header>
			</div>
		</div>
	</section>
*/ ?>
    <section class="faesp">
		<div class="container">
			<div class="row">
				<header>
					<h2 class="margin-left-15">Aguarde novidades.</h2>
				</header>
				<?php /*
				<header>
					<h2 class="margin-left-15">Veja os vídeos solidários e colabore com a <strong>FAESP</strong></h2>
				</header>
				<div class="col-lg-4">
					<figure>
						<div class="video-container">
							<iframe width="560" height="315" src="//www.youtube.com/embed/3p0F4zmcoWQ" frameborder="0" allowfullscreen></iframe>
						</div>
					</figure>
					
				</div>
				<div class="col-lg-4">
					<p>
						A FUNDAÇÃO DE APOIO AO EGRESSO DO SISTEMA PENITENCIÁRIO –
						FAESP foi criada em 1997, tendo como missão apoiar o egresso do Sistema
						Penitenciário na condição de autor de sua reintegração produtiva e contando
						com a participação da sociedade.
					</p>

					<p>
						A FAESP está apoiada nos princípios humanos da solidariedade e da
						humanidade, buscando dar a sua contribuição para a redução da violência,
						para a prevenção contra a reincidência e seu alto custo social, evitar o custo
						econômico, social e humano do retorno do apenado ao sistema prisional,
						trabalhar pela manutenção da integridade física, familiar e patrimonial das
						pessoas envolvidas na execução penal, promover a recuperação da autoestima
						da pessoa humana, tornar possível a coexistência e o retorno dos apenados ao
						convívio social.
					</p>
				</div>
				<div class="col-lg-4">
					<p>
						As políticas de ação da FAESP visam à reintegração social do egresso, o seu
						fortalecimento psicoemocional do indivíduo, o suporte básico em saúde
						física, o suporte material para minimizar privações vitais no convívio
						inicial com a liberdade, o suporte educacional para retorno ao mundo do
						trabalho, à seleção e indicação de oportunidades laborais, à apropriação pelo
						egresso de sua cidadania, à sensibilização da sociedade para os benefícios
						da reintegração social do egresso, à redução da reincidência criminal e
						minimização da violência social.
					</p>
					<p>
						Em 15 anos de atuação, a FAESP já atendeu mais de 1.200 apenados, atingindo
						a marca de 91,31% de NÃO REINCIDÊNCIA CRIMINAL no ano de 2012.
					</p>

					<p>
						<?php echo $this->Html->image('imagens/faesp-logo.jpg',
							['alt' => 'FAESP - Fundação de Apoio ao Egresso do Sistema Penitenciário', 'class' => 'hidden-sm hidden-xs']); ?>
					</p>
				</div>
				
				*/ ?>
			</div>
		</div>
	</section>
	<?php /*<section class="videos-solidarios">
		<div class="container">
            <?php echo $this->element('latest_movies'); ?>
		</div>
	</section> */ ?>



<div id="mensagem-solidariedade" class="modalblock" style="display: none">            
    <a href="javascript:void(0);" class="btn_fechar">
        <?php echo $this->Html->image('imagens/btn_fechar.png', ['alt' => 'Fechar', 'id' => 'btnFechar']); ?>
    </a>
    <div id="topomodal">
        <h1 class="custom">Escolha o valor da doação</h1>
        <h4 class="custom">A partir do valor mínimo do vídeo, você poderá escolher quanto deseja doar.</h4>
    </div>
    <div id="centromodal">
        <div class="row">
        	<div class="col-lg-6 col-md-6">
        		<p class="principal">Valor Mínimo</p>
        		<p class="valor">R$ 15,00</p>
        	</div>
        	<div class="col-lg-6 col-md-6">
        		<p class="principal">Valor da doação</p>
        		<input type="text" placeholder="R$ 15,00">
        	</div>
        </div>
    </div>
    <div id="rodapemodal">
        <b>Pague com o pagseguro</b>

        <?php echo $this->Html->image('imagens/rodape_pagseguro.jpg', ['alt' => 'Pagseguro']); ?>
        <div align="right">
            <?php echo $this->Form->postLink('Doar',
                array(
                    'controller' => 'movies',
                    'action'     => 'buy',
                ),
                array(
                    'class' => 'btn btn-default',
                    'title' => 'Doar'
                )
            ); ?>
        </div>
    </div>
</div>
