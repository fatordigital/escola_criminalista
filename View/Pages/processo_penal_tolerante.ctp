<div class="container">
    <section class="tolerante">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 clear">
			<figure class="text-center">
				<?php echo $this->Html->link($this->Html->image("imagens/processo_penal_tolerante.jpg", ['alt' => 'CURSO DE PROCESSO PENAL TOLERANTE', 'class' => 'img-responsive']), $this->Html->Url('/cursos', true), array('escape' => false)); ?>
			</figure>
			<br /><br />
            <header>
                <h2>CURSO DE PROCESSO PENAL TOLERANTE</h2>
            </header>
            <article>
                <p>Humanização do Processo Penal: processo para que(m)?</p>
                <p><b>Programa</b></p>
				<p class="data">Segunda, 17 de novembro - 19h e 30min</p>
				<p><b>Lenio Streck</b></p>
				<p>Doutor em Direito, com pós-doutorado em Direito do Estado. Procurador de Justiça Aposentado do MPRS. Advogado.</p> 
				<p>Tema: “Há espaço para a certeza no processo penal?”</p>
				<p><b>Salah H. Khaled Jr</b></p>
				<p>Jurista, Escritor, Professor de Direito Penal, Criminologia, Sistemas Processuais Penais, História das Ideias Jurídicas do Programa de Pós-Graduação em Direito e Justiça Social - Mestrado na Universidade Federal do Rio Grande - FURG	</p>
				<p>Tema: “A busca da verdade no processo penal: in dubio pro hell”</p>
				<p class="data">Quarta, 19 de novembro: 19h e 30min</p>
				<p><b>Amilton Bueno de Carvalho</b></p>
				<p>Jurista, Escritor, Desembargador Aposentado do TJRS</p>
				<p>Tema: "O juiz e a possível humanização do direito penal"</p>
				<p><b>Jader Marques</b></p>
				<p>Advogado Criminalista, Doutor em Direito.</p>
				<p>Tema: "Delinquência Processual”</p>
				<p class="data">Segunda, 24 de novembro: 19h e 30min</p>
				<p><b>Roberto Tardelli</b></p>
				<p>Jurista, Escritor, Procurador de Justiça Aposentado do MPSP. Atuou nos rumorosos casos Suzane Von Richtofen e Francisco de Assis Pereira, o "Maníaco do Parque".</p>
				<p>Tema: “Acusar e condenar, sem perder a ternura”</p>
				<p><b>Augusto Jobim</b></p>
				<p>Jurista, Escritor, Doutor em Altos Estudos Contemporâneos – Universidade de Coimbra, Professor da PUC-RS</p>
				<p>Tema: “O dispositivo inquisitivo no processo penal brasileiro”</p>
				<br />
				<p>PRESENCIAL (VAGAS LIMITADAS)</p>
				<p>NÃO PRESENCIAL (INTERNET)</p>
				
				<p><b>LOCAL (presencial):</b></p>
				<p>NÓS COWORKING</p>
				<p>Shopping Total</p>
				<p>Av. Cristóvão Colombo, 545 Prédio 2 - 5˚andar
								Alameda dos Escritores
								Bairro Floresta | Porto Alegre
								CEP: 90560-003</p>
				<p><b>INFORMAÇÕES</b></p>
				<p>(51) 3232-4749 e (51) 8157-4005</p>
				<a href="mailto:contato@itolerancia.com.br" title="'Envie um e-mail para contato@itolerancia.com.br'">contato@itolerancia.com.br</a>
				<p><b>INSCRIÇÕES</b></p>
				<p>Inscrições somente pela internet no endereço:</p>
				<p><a href="http://www.itolerancia.com.br/cursos" title="Inscrições somente pela internet no endereço: http://www.itolerancia.com.br/cursos">http://www.itolerancia.com.br/cursos</a></p>
				<br />
				<p>Coord. Jader Marques</p>
			<!-- 	<p><b>INTERNET (EAD)</b></p>
				<p><a href="http://www.itolerancia.com.br/cursos">http://www.itolerancia.com.br/cursos</a></p>
				<p><b>INSCRIÇÕES</b></p>
				<p>Inscrições somente pela internet via PAGSEGURO (cartão de crédito)</p>
				<p><a href="http://www.itolerancia.com.br/cursos">http://www.itolerancia.com.br/cursos</a></p>
				<p><b>CONTATO</b></p>
				<p>(51) 3232-4749 e (51) 9668-9292</p>
				<p>contato@itolerancia.com.br <mailto:contato@itolerancia.com.br> </p>
				<p><b>VAGAS LIMITADAS</b></p>
				<p>O curso deste semestre será na parte de Eventos do nos coworking do Shopping Total:</p>
				<p><a href="http://www.noscoworking.com.br/#infraestrutura" target="_blank">http://www.noscoworking.com.br/#infraestrutura</a></p>
				<p>Será nos dias 17 e 18, presencial e com transmissão pela internet.</p> -->
            </article>
			<br />
        </div>
    </section>
</div>

