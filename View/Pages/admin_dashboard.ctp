<?php /** @var $this View */ ?>

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            Painel <small>Estatísticas e mais</small>
        </h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="index.html">
                    Home
                </a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">
                    Painel
                </a>
            </li>
            <li class="pull-right">

            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <p class="lead">Use o menu à esquerda para a navegação</p>
    </div>
</div>

<?php $this->Html->scriptStart(['inline' => false]); ?>
jQuery(document).ready(function() {
    $('.page-sidebar .ajaxify.start').click();
});
<?php $this->Html->scriptEnd(); ?>