﻿<?php
/**
 *
 * @var $this View
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Pages
 * @since         CakePHP(tm) v 0.10.0.1076
 */
?>

<?php echo $this->Html->scriptStart(['inline' => false]) ?>

function modalPagamento(id) {
    $.blockUI({
        css: {
            border: 'none',
            padding: '5px',
            width: '760px',
            backgroundColor: '#FFFFFF',
            '-webkit-border-radius': '5px',
            '-moz-border-radius': '5px',
        },
    message: $('#modalblock-'+id)
    });
}


function fechar() {
    $.unblockUI();
}

function popbox_como_functiona(){

}

$(document).ready(function(){
    if(!isMobile()){    
        $('.tela-explicativa').delay(3000).fadeIn(500);   
    };
    
    $('.conteudo-explicativo > img').click(function(){
        $('.tela-explicativa').fadeOut(500);
    });



    $('a.saiba-mais-como-funciona').click(function(){   
        $.blockUI({
            css: {
                border: 'none',
                padding: '5px',
                left: '25%',
                top: '10% !important',
                width: '760px',
                backgroundColor: '#FFFFFF',
                'border-radius': '5px',
            },
            message: $('#mensagem-como-funciona')
        });
    });

    $('.btn_fechar').click(function() {
        fechar();
    });
    
});

<?php echo $this->Html->scriptEnd() ?>

    <?php $this->start('video'); ?>
	<?php //echo $this->element('banners_top'); ?>
	<?php //if(1 == 2): ?>
		<section class="video-topo">
			<video class="video-js vjs-default-skin hidden-xs hidden-sm" autoplay preload="auto" loop="">
				<source type="video/mp4" src="<?php echo $this->Html->Url('/webroot/video/escolacriminalistas.mp4'); ?>">
			</video>
            <?php /*<img src="<?php echo $this->Html->Url('/imagens/banners/banner-03.jpg'); ?>" class="img-responsive" /> */?>
		</section>
	<?php //endIf; ?>
    <?php $this->end(); ?>

    <div class="topo-video hide">
        <?php echo $this->Html->image('imagens/topo-video-preview.jpg', ['alt' => 'Video Preview']); ?>
    </div>

    <div class="container">
        <?php /*
        <section class="definicoes">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <article>
                        <header>
                            <h2>Tolerância</h2>
                        </header>
                        <blockquote class="no-border no-padding">
                            <p>
                                <?php echo $this->Html->image('imagens/quote-cima.png', ['alt' => 'Quote', 'class' => 'sub']); ?>
                                <em><span class="um">Tendência a admitir,</span> nos outros, maneiras de pensar, de agir e
                                    de sentir <span class="dois">diferentes</span> ou mesmo <span class="um">diametralmente opostas</span>
                                    às adotadas por si mesmo.</em>
                                <?php echo $this->Html->image('imagens/quote-baixo.png', ['alt' => 'Quote', 'class' => 'text-top']); ?>
                            </p>
                        </blockquote>
                    </article>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <article>
                        <header>
                            <h2>Conheça a Casa</h2>
                        </header>
                        <?php echo $this->Html->image('imagens/sobre-instituto-preview.jpg',
                            ['alt' => 'Sobre o instituto', 'class' => 'img-responsive preview']); ?>

                        <p class="margin-bottom">
                            O Instituto Casa da Tolerância é um portal de educação por internet que
                            disponibilizará cursos, aulas, palestras, documentários, debates, com
                            transmissão “ao vivo” e por filmes gravados.
                        </p>

                        <div class="hidden-lg hidden-md col-xs-6">
                            <a href="http://www.casatolerancia.com.br/pages/sobre">Leia mais</a>
                        </div>

                        <div class="col-lg-3 hidden-xs">
                            <a class="saiba-mais-como-funciona">Leia mais</a>
                        </div>

                        <div class="col-lg-9 no-padding">
                            <?php echo $this->Html->image('imagens/leia-mais.jpg',
                                ['alt' => 'Leia mais', 'class' => 'img-responsive separador']); ?>
                        </div>
                    </article>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <article>
                        <header>
                            <h2>Clique Solidário</h2>
                        </header>
                        <?php echo $this->Html->image('imagens/clique-solidario-preview.jpg',
                            ['alt' => 'Sobre o instituto', 'class' => 'img-responsive preview']); ?>

                        <p class="margin-bottom">
                            Num clique, você solidário. O Instituto Casa da Tolerância permite que
                            você contribua com instituição conveniada ao escolher o filme do seu
                            interesse. Nesta primeira temporada: FAESP.
                        </p>

                        <div class="col-lg-3">
                            <?php echo $this->Html->link('Leia mais',
                                ['controller' => 'pages', 'action' => 'cliquesolidario'],
                                ['title' => 'Leia mais']); ?>
                        </div>
                        <div class="col-lg-9 no-padding">
                            <?php echo $this->Html->image('imagens/leia-mais.jpg',
                                ['alt' => 'Leia mais', 'class' => 'img-responsive separador']); ?>
                        </div>
                    </article>
                </div>
            </div>
        </section>
        */ ?>		
		
		<?php /*<section class="novidades" style="padding: 20px 0 0px">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<?php echo $this->Html->image('imagens/SEMINARIO-MAIORIDADE-PENAL.jpg',
                                ['alt' => 'SEMINARIO MAIORIDADE PENAL', 'class' => 'img-responsive separador']); ?>	
			</div>
		</section>*/ ?>
		
        <section class="novidades sobre-e-blog">
			
			
		
            <div class="col-lg-4 col-md-4 col-sm-4 -xs-12">
                <h2 class="no-border">Sobre</h2>

                <div class="sobre">
                    <p>
                        A Escola de Criminalistas é fundada sobre as bases de um fazer artesanal da advocacia criminal, numa perspectiva de ensino-aprendizagem em que o foco recai sobre a transmissão de um ofício, o de criminalista, e isso acontece por um método, o estudo da prática.
                    </p>
                    <p>
                        Uma Escola de Criminalistas com a determinação de propor um corajoso percurso de estudo pela experimentação.
                    </p>
                   <p>Agora existe uma. E é toda nossa!</p>
                   <p><i>Jader Marques</i></p>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 -xs-12">

                <div class="blog">

                    <h2>BLOG</h2>

                    <div class="blog-home-content">
                    <?php foreach ($news as $key => $new): ?>
                            <div class="col-lg-6 col-md-6 col-sm-6 -xs-6">
                            <?php if($new['News']['image'] != ""): ?>
                                <?php echo $this->Html->image($this->Html->Url('/files/news/image/'.$new['News']['id'].'/thumb_'.$new['News']['image'], true), ['alt' => $new['News']['title'], 'class' => 'img-responsive preview']); ?> 
                            <?php else: ?>
                                <?php echo $this->Html->image($this->Html->Url('/imagens/novidade-default.png', true), ['alt' => $new['News']['title'], 'class' => 'img-responsive preview']); ?> 
                            <?php endIf; ?>
                            <a href="javascript:void(0);" title="<?php echo $new['News']['title']; ?>" class="title"><?php echo $new['News']['title']; ?></a>
                            <p><?php echo $this->String->truncate_str(strip_tags($new['News']['text']), 200); ?></p>

                            <a href="<?php echo $this->Html->Url('/escrito/'. $new['News']['slug']); ?>" class="btn btn-ler-mais" title="Ler mais sobre <?php echo $new['News']['title']; ?>">Leia mais</a>
                        </div>
                    <?php endforeach; ?>
                    </div>

                </div>

            </div>
        </section>
        <?php /* if(isset($novidades) && is_array($novidades) && count($novidades) > 0): ?>
            <section class="novidades">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2>Destaques</h2>
                </div>
                <?php /*
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <?php /*<a href="http://www.itolerancia.com.br/lives/view/online-1" title="ACESSE: CURSO TOLERANTE DE ORATÓRIA FORENSE E SUSTENTAÇÃO ORAL">
                        <?php echo $this->Html->image('BANNER.jpg', ['alt' => 'CURSO TOLERANTE DE ORATÓRIA FORENSE E SUSTENTAÇÃO ORAL']); ?>
                    </a>*//* ?>
                    <a href="<?php echo $this->Html->url('/pages/soucontra171'); ?>" title="MOVIMENTO TOLERANTE CONTRA A REDUÇÃO DA MAIORIDADE PENAL">
                        <?php echo $this->Html->image('/imagens/banner-movimento-tolerante.jpg', ['class' => 'img-responsive my-banners', 'alt' => 'MOVIMENTO TOLERANTE CONTRA A REDUÇÃO DA MAIORIDADE PENAL']); ?>
                    </a>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <a href="<?php echo $this->Html->url('/diadobem'); ?>" title="DIA DO BEM">
                        <?php echo $this->Html->image('/imagens/banner-dia-do-bem.jpg', ['class' => 'img-responsive my-banners', 'alt' => 'DIA DO BEM']); ?>
                    </a>
                </div>
                 ?>
                <?php foreach ($novidades as $key => $novidade): ?>
                    <div class="col-lg-4 col-md-4 col-sm-4 -xs-12">
                        <div class="fb-share pull-right">
                            <?php 
                                if(strlen($novidade['Novidade']['text']) > 150){
                                    $description = substr($novidade['Novidade']['text'], 0, 150).'...';
                                }else{
                                    $description = $novidade['Novidade']['text'];
                                }
                                $description = trim(strip_tags($description));

                                $url = $_SERVER['SERVER_NAME'].$this->Html->Url(null);

                                $link = 'share/?title='.urlencode($novidade['Novidade']['title']);
                                $link .= '&description='.urlencode($description);
                                $link .= '&image='.'files/novidade/image/'.$novidade['Novidade']['id'].'/face_'.$novidade['Novidade']['image'];
                                $link .= '&url='.$url;
                                $link .= '&time='.time();

                                // print($link);die;
                            ?>
                            <div id="fb-root"></div>
                            <script>(function(d, s, id) {
                              var js, fjs = d.getElementsByTagName(s)[0];
                              if (d.getElementById(id)) return;
                              js = d.createElement(s); js.id = id;
                              js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&appId=229696703898972&version=v2.0";
                              fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                            <div class="fb-share-button" data-href="<?php echo $this->Html->Url('/'.$link, true)?>"></div>

                            <?php if(1 == 3): ?>
                                 <?php echo $this->Html->link(
                                    $this->Html->image('imagens/facebook-share.png', ['alt' => 'Compartilhe no Facebook', 'class' => 'img-responsive']),
                                    '../'.$link,
                                    ['escape' => false]
                                ); ?>
                            <?php endIf; ?>
                        </div>
                        <?php if($novidade['Novidade']['image'] != ""): ?>
                            <?php echo $this->Html->image($this->Html->Url('/files/novidade/image/'.$novidade['Novidade']['id'].'/thumb_'.$novidade['Novidade']['image'], true), ['alt' => $novidade['Novidade']['title'], 'class' => 'img-responsive preview']); ?> 
                        <?php else: ?>
                            <?php echo $this->Html->image($this->Html->Url('/imagens/novidade-default.png', true), ['alt' => $novidade['Novidade']['title'], 'class' => 'img-responsive preview']); ?> 
                        <?php endIf; ?>
                        <a href="javascript:void(0);" title="<?php echo $novidade['Novidade']['title']; ?>" class="title"><?php echo $novidade['Novidade']['title']; ?></a>
                        <p><?php echo $novidade['Novidade']['text']; ?></p>
                        <?php if($novidade['Novidade']['link'] != ""): ?>
                            <div class="row">
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <a href="<?php echo $novidade['Novidade']['link']; ?>" title="Leia Mais" target="_blank">Leia mais</a>
                                </div>
                                <div class="col-lg-9 col-md-8 col-sm-6 hidden-xs">
                                    <div class="linha"></div>
                                </div>
                            </div>
                        <?php endIf; ?>
                    </div>
                <?php endforeach; ?>
            </section>
        <?php endIf; */ ?>
    </div>
   
    <div class="container">
        <?php /*<div class="col-lg-12 col-md-12 hidden-sm hidden-xs">
            <?php echo $this->Html->image('imagens/banner-curso.jpg', ['alt' => 'Curso de Prática da Advocacia Criminal: TRIBUNAL DO JÚRI', 'class' => 'seminario img-responsive']); ?>    
        </div> 

        <div class="col-lg-12 col-md-12 hidden-sm hidden-xs">
            <?php echo $this->Html->link($this->Html->image("imagens/processo_penal_tolerante.jpg", ['alt' => 'CURSO DE PROCESSO PENAL TOLERANTE', 'class' => 'seminario img-responsive']), $this->Html->Url('/lives/view/segunda-feira-24-11-1', true), array('escape' => false)); ?>
        </div>*/ ?>
    </div>
    <!--
    <section class="ultimos-videos destaque row-fluid">
    <div class="container">
        <header>
            <h2>Últimos vídeos.</h2>
        </header>
        <div class="row-fluid">
        <?php $i = 1; ?>
        <?php foreach ($recentMovies as $recentMovie) : ?>
            <?php if ($i == 1 || $i % 3 == 0) : ?>
                <div class="row  ultimos-filmes">
            <?php endif; ?>

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <figure class="text-right">
						<a href="<?php echo $this->Html->Url(array('controller' => 'movies', 'action' => 'view', $recentMovie['Movie']['slug'])); ?>" title="<?php echo $recentMovie['Movie']['title']; ?>">
                        <?php echo $this->Html->image($recentMovie['Movie']['thumb']['big']['url'],
                            ['alt' => 'Preview vídeo', 'class' => 'img-responsive fullsize-images']
                        ); ?>
						</a>
                        <span>
                            <?php echo $this->Html->image("imagens/{$recentMovie['Movie']['type']}-bottom-right.png", ['alt' => 'Tipo de vídeo', 'class' => 'img-responsive categoria-video']); ?>
                        </span>
                        <?php echo $this->Html->link(
                            $this->Html->image('imagens/abrir-video.png', ['alt' => 'Abrir vídeo', 'class' => 'img-responsive']),
                            ['controller' => 'movies', 'action' => 'view', $recentMovie['Movie']['slug']],
                            ['escape' => false, 'title' => 'Clique aqui para abrir o vídeo', 'class' => 'player']); ?>
                    </figure>
					<span class="category-movie"><?php echo $recentMovie['Category']['title'] ?></span>
                    <header>
                        <h2>
                            <?php echo $this->Html->link($recentMovie['Movie']['title'],
                                ['controller' => 'movies', 'action' => 'view', $recentMovie['Movie']['slug']],
                                ['escape' => false]
                            ); ?>
                        </h2>
                    </header>
                    
                    <p><?php echo $this->Text->truncate($recentMovie['Movie']['description']); ?></p>
                
                </div>
                    
            <?php if ($i % 3 == 0) : ?>
                </div>
            <?php endif; ?>

            <?php $i++; ?>
        <?php endforeach; ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <?php echo $this->Html->link('Veja todos os vídeos', ['controller' => 'movies', 'action' => 'index'], ['class' => 'todos-filmes']); ?>
            </div>
        </div>
        <?php //echo $this->element('latest_movies'); ?>
    </div>
    </section>
    -->
   
    <!--<?php if(count($count_cursos) > 0): ?>
        <section class="destaque-curso row-fluid">

            <div class="container">

                <h2><?php echo $count_cursos['Movie']['title']; ?></h2>

                <div class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1 col-xs-12 no-padding">
                    <div class="destaque-curso-descricao">
                        <?php echo $this->String->truncate_str(strip_tags($count_cursos['Movie']['description']), 400); ?>
                    </div>
                </div>

                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 no-padding">
                    <div class="destaque-curso-valores">
                        <?php if($count_cursos['Movie']['price'] > 0): ?>
                            <?php $price = explode('.', $count_cursos['Movie']['price']); ?>
                            <p><span class="descricao">Por apenas</span> <span class="alto">R$</span><span class="valor"><?php echo $price[0] ?>,</span><span class="baixo"><?php echo $price[1] ?></span></p>
                            <?php echo $this->Html->link('Saiba Mais', $this->Html->Url("/cursos/modulos/".$count_cursos['Movie']['slug'], true), ['class' => 'btn-comprar-curso']); ?>
                            <?php if($count_cursos['Movie']['off'] > 0): ?>
                                <span class="desconto"><?php echo $count_cursos['Movie']['off']; ?>% off para quem faz jus ao desconto especial</span>
                            <?php endIf; ?>
                        <?php else: ?>
                            <p class="text-center">Faça já sua inscrição.</p>
                            <?php //echo $this->Html->link('FAÇA SUA INSCRIÇÃO AQUI', $this->Html->Url("/cursos/modulos/".$count_cursos['Movie']['slug'], true), ['class' => 'btn-comprar-curso']); ?>
                            <?php echo $this->Html->link('Saiba Mais', $this->Html->Url("/cursos/modulos/".$count_cursos['Movie']['slug'], true), ['class' => 'btn-comprar-curso']); ?>
                        <?php endIf; ?>
                    </div>
                </div>

            </div>

        </section>
    -->
    <?php endIf; ?>
    <?php /*
    <section class="assista-aulas rodape hidden-sm hidden-xs">
        <div class="container">
            <div class="col-lg-7">
                <article>
                    <p><span>Nós queremos apenas mudar o mundo. Faça Parte!</span><br>
                    Faça a assinatura mensal e realize este projeto junto com a gente. <?php echo $this->Html->image('imagens/instituto-casa-da-tolerancia-logo-small.png', ['alt' => 'Instituto Casa da Tolerância'])?></p>
                </article>
            </div>
        </div>
    </section>
    */ ?>
    <?php //echo $this->element('banners'); ?>
    <?php /*
    <div id="mensagem-como-funciona" class="modalblock hidden-xs" style="display: none;">            
        <a href="javascript:void(0);" class="btn_fechar">
            <?php echo $this->Html->image('imagens/btn_fechar.png', ['alt' => 'Fechar', 'id' => 'btnFechar']); ?>
        </a>
        <div id="topomodal">

            <h1 class="custom">Como Funciona a Casa?</h1>
        </div>
        <div id="rodapemodal" class="outro">
            <p><?php echo $this->Html->image('imagens/icone-01.png', ['alt' => 'Gratuíto']); ?> Onde você encontrar esse selo, estará presente um vídeo gratuito, que foi produzido pela Casa ou que selecionamos exclusivamente para nossos usuários.</p>
            <hr>
            <p><?php echo $this->Html->image('imagens/icone-02.png', ['alt' => 'Exclusivo']); ?> Esse é o selo que indica um conteúdo diferenciado, produzido de forma exclusivo e comercializado através de nosso site.</p>
            <hr>
            <p><?php echo $this->Html->image('imagens/icone-03.png', ['alt' => 'Clique Solidário']); ?> Aqui você tem o “Clique Solidário”, um vídeo especial que ao ser adquirido terá a totalidade de seu valor revertido para umainstituição que a Casa da Tolerância ajuda. <a href="#" title="saiba mais">Saiba mais</a></p>
            <hr>
            <p><?php echo $this->Html->image('imagens/faca-parte.png', ['alt' => 'Faça Parte']); ?> Não quer comprar vídeos avulsos? Clique nesse link e associe-se à casa. Por um valor mensal fixo você terá acesso à muito conteúdo de qualidade!</p>
            <b>Pague com o pagseguro</b>&nbsp;

            <?php echo $this->Html->image('imagens/rodape_pagseguro.jpg', ['alt' => 'Pagseguro']); ?>
        </div>
    </div>
    


<?php if($cookieHelper->read('popbox_como_funciona')=='true'){ }else{ ?>
    <div class="tela-explicativa hidden-xs" style="display: none; position: relative; z-index: 9999">
        <div class="overlay-tela"></div>
        <div class="explicacao">
            <div class="conteudo-explicativo">
                <?php echo $this->Html->image('imagens/btn_fechar.png', ['alt' => 'Fechar', 'id' => 'fechar-box']); ?>
                <div class="col-lg-12 col-md-12 text-center">

                <div style="margin-top:-85px; padding-bottom: 20px;"><img <?php echo $this->Html->image('imagens/aviso.png', ['alt' => 'Aviso Importante', 'id' => '']); ?></div>
                
                    <h2>Como Funciona a Casa?</h2>
                    <h3><span>Seja bem-vindo!</span> O portal do Instituto Casa da Tolerância possui conteúdo livre, restrito a assinantes, cursos e eventos, além do Clique Solidário. Nós não entregamos certificados ou outros papéis, apenas vivências.</h3>
                </div>
                <div class="col-lg-12 col-md-12">
                    <article>
                        <p><?php echo $this->Html->image('imagens/icone-01.png', ['alt' => 'Gratuíto']); ?> Este selo indica um filme <strong>livre</strong>, produzido pela Casa da Tolerância ou selecionado de outras fontes especialmente para você.</p>
                    </article>
                    <article>
                        <p><?php echo $this->Html->image('imagens/icone-02.png', ['alt' => 'Exclusivo']); ?>  Este selo indica um filme <strong>restrito</strong>, produzido exclusivamente para nossos assinantes. Faça parte!</p>
                    </article>
                    <article>
                        <p><?php echo $this->Html->image('imagens/icone-03.png', ['alt' => 'Clique Solidário']); ?> Este selo indica um filme <strong>solidário</strong>, que permite a doação diretamente para uma instituição selecionada pela Casa da Tolerância.</p>
                    </article>
                </div>
                <div class="col-lg-12 col-md-12 text-center">
                    <?php echo $this->Html->link( 'Clique aqui',
                        ['plugin' => false, 'controller' => 'pages', 'action' => 'assine']
                    ) ?> e faça sua assinatura mensal. Nós queremos apenas mudar o mundo. <strong>Faça parte! </strong>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<?php $cookieHelper->write('popbox_como_funciona', 'true', false, 24*3600*7); ?>
*/ ?>