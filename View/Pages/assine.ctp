<section class="topo-pagina">
		<div class="container">
			<div class="row-fluid">
				<header>
					<div class="col-lg-12">
						<h1>Faça sua Assinatura</h1>
					</div>
				</header>
			</div>
		</div>
	</section>
	<section class="ultimos-videos destaque">
		<div class="container">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
	            <div class="row">
	                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
	                    <?php echo $this->Html->image('imagens/assine-por-apenas-1990.png', ['alt' => 'Assine a casa da tolerância por apenas R$ 19,90', 'class' => 'img-responsive assine-a-casa']); ?>
	                </div>
	            </div>
	            <p class="custom">Importante</p>
	            <p>Após o processo de pagamento, sua assinatura poderá levar até 1 dia útil para ser habilitada, dependendo da aprovação da instituição financeira. Para agilizar esse processo entre em contato conosco pelo telefone: <span class="telefone">(51) 3232-4749</span></p>

	            <?php echo $this->Html->image('imagens/rodape_pagseguro.jpg', ['alt' => 'Pagseguro', 'class' => 'img-responsive']); ?>
	            
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 pull-right">

				<?php if (!$this->Session->check('Auth.User')): ?>
	                <h2>Escolha como deseja assinar</h2>
	                <?php echo $this->Html->link('Faça seu cadastro!',
	                    ['plugin' => 'users', 'controller' => 'users', 'action' => 'add'],
	                    ['class' => 'btn btn-primary btn-lg btn-block menor']); ?>
	                <p>Você realiza o cadastro normalmente, usando seu endereço de e-mail e informações pessoais.</p>
	                <br />
	                <br />
	                <?php
	                if(!isset($facebook_session) || $facebook_session['id'] == ""){ 
	                    echo $this->Facebook->login(
	                        array(
	                            'custom' => true,
	                            'perms' => 'email,publish_stream',
	                            'img' => 'facebook-grande.png'
	                        )
	                    );
	                }
	                ?>
	                <p>Você realiza o cadastro autenticando sua conta do facebook e então preenchendo apenas os dados adicionais.<br>
	                <span class="facebook"><strong>Importante:</strong> <em>Caso o acesso ao facebook seja bloqueado em sua rede, não será possível realizar a autenticação.</em></span></p>
	            <?php  else: ?>
	            	<?php echo $this->Form->postLink('Assine e Faça Parte da Casa',
	                    array(
	                        'controller' => 'orders',
	                        'action'     => 'buy',
	                        'assinaturaictd',
	                        'true'
	                    ),
	                    array(
	                        'class' => 'btn btn-primary btn-lg btn-block menor assine',
	                        'title' => 'Assine e Faça Parte da Casa'
	                    )
	                ); ?>
	            <?php endIf; ?>
			</div>
		</div>
	</section>