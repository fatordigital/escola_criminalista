<section class="clique-solidario hidden-sm hidden-xs">
    <div class="container">
        <div class="col-lg-8">
            <header>
                <h2>Num clique, você solidário. <br><span>Veja os vídeos e colabore com instituições.</span></h2>
            </header>
            <p></p>
            <a href="/pages/cliquesolidario/" class="btn btn-default" class="Saiba mais">Saiba mais</a>
        </div>
    </div>
</section>