<section class="cadastro">
	<div class="container">
		<header>
			<h2>Assine Agora!</h2>
		</header>
		<p>O cadastro é prático e simples. Com ele você pode assistir diversos vídeos e interagir com os demais participantes do instituto. Teremos o maior prazer em ter você conosco!</p>
		<div class="row-fluid">
			<div class="col-lg-2 col-md-2 col-sm-12 text-right">
				
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 text-center">
				<div class="facebook-cadastro botao-cadastro">
					<?php echo $this->Html->image('imagens/facebook-logo.jpg', ['alt' => 'Facebook'])?>
					<?php /* echo $this->Facebook->login(
                        array(
                            'custom' => true,
                            'perms' => 'email,publish_stream',
                            'class' => 'btn btn-default custom-btn-2',
                            'label'	=> 'Conectar com o Facebook'
                        )
                    ); */?>
					<?php echo $this->Html->link('Conectar com o Facebook',
							['plugin' => false, 'controller' => 'pages', 'action' => 'assine'],
							['class' => 'btn btn-default custom-btn-2']
                    );?>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 text-right">
				<div class="meu-cadastro botao-cadastro">
					<?php /* echo $this->Html->link('Inscrição com o endereço de e-mail',
                                    ['plugin' => 'users', 'controller' => 'users', 'action' => 'login'],
                                    ['class' => 'btn btn-default custom-btn-3']
                                ); */ ?>
                    <?php echo $this->Html->link('Inscrição com o endereço de e-mail',
                                    ['plugin' => false, 'controller' => 'pages', 'action' => 'assine'],
                                    ['class' => 'btn btn-default custom-btn-3']
                                ); ?>
				</div>
			</div>
		</div>
	</div>
</section>