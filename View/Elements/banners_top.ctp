<?php
    $class_top = array('banner-01', 'banner-02', 'banner-03', 'banner-04', 'banner-05', 'banner-06', 'banner-07', 'banner-08');
    $i = rand(0, count($class_top)-1);

    if (isset($_SESSION['back_top'])) {
        while(in_array($i, $_SESSION['back_top'])){
            $i = rand(0, count($class_top)-1);
        }

        if(count($_SESSION['back_top']) == 5){
            array_shift($_SESSION['back_top']);
        }
        array_push($_SESSION['back_top'], $i);
    }else{
        $_SESSION['back_top'] = array($i); 
    }
	
	$_SESSION['back_top_last'] = $i;

    $selectedClass = $class_top[$i];
?>

<style>
	.banner-top.<?php echo $selectedClass ?> {
        display: block !important;
    }
</style>

<section class="banners-novos banner-top banner-01">
	<img src="<?php echo $this->Html->Url('/imagens/banners/banner-01.jpg'); ?>" class="img-responsive" />
    <?php /*
    <div class="container">
        <div class="col-lg-7 col-md-8 hidden-sm hidden-xs">
            Lorem ipsum dolor sit amet, consectetur nun
            <span>risus leo, dapibus id, sollicitudin nisi.</span>
        </div>
    </div>
    */ ?>
</section>
<section class="banners-novos banner-top banner-02">
	<img src="<?php echo $this->Html->Url('/imagens/banners/banner-02.jpg'); ?>" class="img-responsive" />
    <?php /*
    <div class="container">
        <div class="col-lg-7 col-md-8 hidden-sm hidden-xs">
            Lorem ipsum dolor sit amet, consectetur nun
            <span>risus leo, dapibus id, sollicitudin nisi.</span>
        </div>
    </div>
    */ ?>
</section>
<section class="banners-novos banner-top banner-03">
	<img src="<?php echo $this->Html->Url('/imagens/banners/banner-03.jpg'); ?>" class="img-responsive" />
    <?php /*
    <div class="container">
        <div class="col-lg-7 col-md-8 hidden-sm hidden-xs">
            Lorem ipsum dolor sit amet, consectetur nun
            <span>risus leo, dapibus id, sollicitudin nisi.</span>
        </div>
    </div>
    */ ?>
</section>
<section class="banners-novos banner-top banner-04">
	<img src="<?php echo $this->Html->Url('/imagens/banners/banner-04.jpg'); ?>" class="img-responsive" />
    <?php /*
    <div class="container">
        <div class="col-lg-7 col-md-8 hidden-sm hidden-xs">
            Lorem ipsum dolor sit amet, consectetur nun
            <span>risus leo, dapibus id, sollicitudin nisi.</span>
        </div>
    </div>
    */ ?>
</section>
<section class="banners-novos banner-top banner-05">
	<img src="<?php echo $this->Html->Url('/imagens/banners/banner-05.jpg'); ?>" class="img-responsive" />
    <?php /*
    <div class="container">
        <div class="col-lg-7 col-md-8 hidden-sm hidden-xs">
            Lorem ipsum dolor sit amet, consectetur nun
            <span>risus leo, dapibus id, sollicitudin nisi.</span>
        </div>
    </div>
    */ ?>
</section>
<section class="banners-novos banner-top banner-06">
	<img src="<?php echo $this->Html->Url('/imagens/banners/banner-06.jpg'); ?>" class="img-responsive" />
   <?php /*
    <div class="container">
        <div class="col-lg-7 col-md-8 hidden-sm hidden-xs">
            Lorem ipsum dolor sit amet, consectetur nun
            <span>risus leo, dapibus id, sollicitudin nisi.</span>
        </div>
    </div>
    */ ?>
</section>
<section class="banners-novos banner-top banner-07">
	<img src="<?php echo $this->Html->Url('/imagens/banners/banner-07.jpg'); ?>" class="img-responsive" />
    <?php /*
    <div class="container">
        <div class="col-lg-7 col-md-8 hidden-sm hidden-xs">
            Lorem ipsum dolor sit amet, consectetur nun
            <span>risus leo, dapibus id, sollicitudin nisi.</span>
        </div>
    </div>
    */ ?>
</section>
<section class="banners-novos banner-top banner-08">
	<img src="<?php echo $this->Html->Url('/imagens/banners/banner-08.jpg'); ?>" class="img-responsive" />
    <?php /*
    <div class="container">
        <div class="col-lg-7 col-md-8 hidden-sm hidden-xs">
            Lorem ipsum dolor sit amet, consectetur nun
            <span>risus leo, dapibus id, sollicitudin nisi.</span>
        </div>
    </div>
    */ ?>
</section>
<section class="banners-novos banner-top banner-09">
	<img src="<?php echo $this->Html->Url('/imagens/banners/banner-09.jpg'); ?>" class="img-responsive" />
   <?php /*
    <div class="container">
        <div class="col-lg-7 col-md-8 hidden-sm hidden-xs">
            Lorem ipsum dolor sit amet, consectetur nun
            <span>risus leo, dapibus id, sollicitudin nisi.</span>
        </div>
    </div>
    */ ?>
</section>
<section class="banners-novos banner-top banner-10">
	<img src="<?php echo $this->Html->Url('/imagens/banners/banner-10.jpg'); ?>" class="img-responsive" />
    <?php /*
    <div class="container">
        <div class="col-lg-7 col-md-8 hidden-sm hidden-xs">
            Lorem ipsum dolor sit amet, consectetur nun
            <span>risus leo, dapibus id, sollicitudin nisi.</span>
        </div>
    </div>
    */ ?>
</section>