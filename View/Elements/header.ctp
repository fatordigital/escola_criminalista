<?php /** @var $this View */ ?>

<?php
// $menu2 = [
//     $this->Html->link('Home', '/'),
//     $this->Html->link('Sobre', ['plugin'=> false, 'controller' => 'pages', 'action' => 'sobre']),
//     $this->Html->link('Solidário', ['plugin'=> false, 'controller' => 'pages', 'action' => 'cliquesolidario']),
//     $this->Html->link('Cursos', '/cursos'),
//     $this->Html->link('Vídeos', ['plugin'=> false, 'controller' => 'movies', 'action' => 'index']),
//     $this->Html->link('Ao vivo', ['plugin'=> false, 'controller' => 'lives', 'action' => 'index']),
//     $this->Html->link('Escritos', ['plugin'=> false, 'controller' => 'news', 'action' => 'index']),
//     $this->Html->link('Contato', ['plugin'=> false, 'controller' => 'contacts', 'action' => 'add']),
// ]
    $menu[] = $this->Html->link('Home', '/');
    $menu[] = $this->Html->link('Sobre', ['plugin'=> false, 'controller' => 'pages', 'action' => 'sobre']);
    if(count($count_cursos) > 0){
        $menu[] = $this->Html->link('Cursos', '/cursos');
    }
    $menu[] = $this->Html->link('Vídeos', ['plugin'=> false, 'controller' => 'movies', 'action' => 'index']);
    if($count_ao_vivos > 0){
        $menu[] = $this->Html->link('Ao vivo', ['plugin'=> false, 'controller' => 'lives', 'action' => 'index']);
    }

    $menu[] = $this->Html->link('Blog', ['plugin'=> false, 'controller' => 'news', 'action' => 'index']);
    $menu[] = $this->Html->link('Contato', ['plugin'=> false, 'controller' => 'contacts', 'action' => 'add']);
?>

<?php $this->Html->scriptStart(['inline' => false]) ?>
	jQuery(function($) {
		setTimeout( function(){
            animate('#btn-cadastre-se', 'swing animated');
            }, 2000
        )
    });

    function animate(element_ID, animation) {
        $(element_ID).addClass(animation);
        setTimeout( function(){
            $(element_ID).removeClass(animation);
                setTimeout( function(){
                    animate('#btn-cadastre-se', animation);
                }, 2000);
            }, 1000
        )
    }
<?php echo $this->Html->scriptEnd() ?>
<?php if (Configure::read('debug') > 0) : ?>
<?php $this->Html->scriptStart(['inline' => false]) ?>

    function MM_openBrWindow(theURL,winName,features) { //v2.0
        window.open(theURL,winName,features);
    }

    function isMobile(){
        return (
            (navigator.userAgent.match(/Android/i)) ||
            (navigator.userAgent.match(/webOS/i)) ||
            (navigator.userAgent.match(/iPhone/i)) ||
            (navigator.userAgent.match(/iPod/i)) ||
            (navigator.userAgent.match(/iPad/i)) ||
            (navigator.userAgent.match(/BlackBerry/))
        );
    } 
    jQuery(function($) {
        
        if(!isMobile()){
            $('.tela-explicativa').delay(3000).fadeIn(500);
        }else{
            $('.chat-footer').css({'display': 'none !important'});
        };

        $('.conteudo-explicativo > img').click(function(){
            $('.tela-explicativa').fadeOut(500);
        });

        $('.fechar-chat').click(function(){
            $('.chat-footer').fadeOut('slow');
        });
        
        $('.cake-sql-log').addClass('table table-striped table-hovered');

        $.fn.center = function () {
            this.css("position","fixed");
            this.css("left", ( $(window).width() - this.width() ) / 2+$(window).scrollLeft() + "px");
            return this;
        };

        $('.blockUI.blockMsg').center();

        $("iframe").each(function(){
            var ifr_source = $(this).attr('src');
            var wmode = "wmode=transparent";
            if(ifr_source.indexOf('?') != -1) {
                var getQString = ifr_source.split('?');
                var oldString = getQString[1];
                var newString = getQString[0];
                $(this).attr('src',newString+'?'+wmode+'&'+oldString);
            }
            else $(this).attr('src',ifr_source+'?'+wmode);
        });
        setTimeout( function(){
            animate('#btn-cadastre-se', 'swing animated');
            }, 2000
        )
    });

    function animate(element_ID, animation) {
        $(element_ID).addClass(animation);
        setTimeout( function(){
            $(element_ID).removeClass(animation);
                setTimeout( function(){
                    animate('#btn-cadastre-se', animation);
                }, 2000);
            }, 1000
        )
    }
<?php echo $this->Html->scriptEnd() ?>
<?php endif; ?>

    <header>
        <div class="navbar topo" role="navigation">
            <div class="container">
                <div class="row-fluid">
                    <div class="botao-menu">
                        <?php echo $this->Html->image('imagens/botao-menu.png', ['alt' => 'Botão menu']); ?>
                    </div>
                    <div class="menu">
                        <div class="col-lg-3 col-sm-2 col-md-2">
                            <?php if ($this->Session->check('Auth.User')): ?>
                                <span class="nome"><?php echo $this->Html->link(
                                        '',
                                        ['plugin' => false, 'controller' => 'users', 'action' => 'home'],
                                        [
                                            'escape' => false
                                        ]
                                    ); ?> Olá, <?php echo $this->Session->read('Auth.User.first_name'); ?> (
									<?php echo $this->Html->link(
                                        'Área Restrita',
                                        ['plugin' => false, 'controller' => 'users', 'action' => 'home'],
                                        [
                                            'escape' => false,
											'style'	=> 'color: #FFF'
                                        ]
                                    ); ?> )</span>
                                <?php
                                    if($facebook_session){ 
                                        echo $this->Facebook->logout(
                                            array(
                                                'redirect' => array(
                                                    'plugin' => 'users',
                                                    'controller' => 'users',
                                                    'action' => 'logout'
                                                ),
                                                 'class' => 'btn btn-default custom-btn-9',
                                                 'label' => 'Sair'
                                            )
                                        );
                                    }else{
                                        echo $this->Html->link('Sair',
                                        ['plugin' => 'users', 'controller' => 'users', 'action' => 'logout'],
                                        ['class' => 'btn btn-default custom-btn-9']);
                                    }
                                ?>
                            <?php else: ?>
                                <?php  echo $this->Html->link('Cadastre-se',
                                            // ['plugin' => false, 'controller' => 'pages', 'action' => 'assine'],
                                            ['plugin' => 'users', 'controller' => 'users', 'action' => 'add'],
                                            ['class' => 'btn btn-default custom-btn-1 btn-cadastre-se', 'id' => 'btn-cadastre-se']
                                        ); 
                                ?>

                                <br />

                                 <?php echo $this->Html->link('Fazer login',
                                    ['plugin' => 'users', 'controller' => 'users', 'action' => 'login'],
                                    ['style' => 'background: none; border: none; clear: both; float: left; color: #FFFFFF; width: 127px; text-align: center;']
                                ); ?>
                            <?php endif; ?>

                            <?php 
                                // $user = $this->Session->read('Auth.User');
                                // if(is_null($user) || (isset($user['subscriber']) && $user['subscriber'] == false)):
                                //     echo $this->Html->link('Assine agora',
                                //     ['plugin' => false, 'controller' => 'pages', 'action' => 'assine'],
                                //     ['class' => 'btn btn-default custom-btn-1', 'style' => 'margin-top: 10px; margin-left: 10px; float: left;']
                                // );
                                // endif; 
                            ?>

                            <?php echo $this->Html->scriptStart(['inline' => false]) ?>
                                function fechar() {
                                    $.unblockUI();
                                }

                                function modalAssinaturaGeneral(){
                                    $.blockUI({
                                        css: {
                                            border: 'none',
                                            padding: '5px',
                                            width: '600px',
                                            top: '25%',
                                            backgroundColor: '#FFFFFF',
                                            '-webkit-border-radius': '5px',
                                            '-moz-border-radius': '5px'
                                        },
                                    message: $('#modalblockassinantegeneral'),

                                    });
                                }
                                $(document).ready(function(){
                                    $('.btnFechar').click(function() {
                                        fechar();
                                    });
                                    $('#btnAssinarGeneral').click(function() {
                                        modalAssinaturaGeneral();
                                    });
                                });
                            <?php echo $this->Html->scriptEnd() ?>
                        </div>
                        <div class="col-lg-7 col-sm-4 col-md-8 hidden-xs my-navbar text-center">
                            <div class="navbar-collapse collapse">
                                <nav class="nav-default">

                                    <?php echo $this->Html->nestedList($menu, ['class' => 'nav navbar-nav']); ?>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6 col-md-2 pull-right">
                        <?php echo $this->Html->link(
                            $this->Html->image('imagens/logo.png', ['alt' => 'Escola Criminalista', 'class' => 'logo-tolerancia img-responsive']),
                            '/',
                            ['escape' => false, 'title' => 'Instituto Casa da Tolerância']
                        ); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="menu-responsivo">
            <ul>
                <li><?php echo $this->Html->link('Fazer login', ['plugin' => 'users', 'controller' => 'users', 'action' => 'login'], ['style' => 'color: #D9A300']); ?></li>
                <li><?php echo $this->Html->link('Home', '/') ?></li>
                <!-- <li><?php echo $this->Html->link('Cursos', ['plugin'=> false, 'controller' => 'pages', 'action' => 'cursos']); ?></li> -->
                <?php if(count($count_cursos) > 0): ?>
                    <li><?php echo $this->Html->link('Cursos', '/cursos'); ?></li>
                <?php endIf; ?>
                <li><?php echo $this->Html->link('Vídeos', ['plugin'=> false, 'controller' => 'movies', 'action' => 'index']); ?></li>
                <?php if($count_ao_vivos > 0): ?>
                    <li><?php echo $this->Html->link('Ao vivo', ['plugin'=> false, 'controller' => 'lives', 'action' => 'index']); ?></li>
                <?php endIf; ?>
                <li><?php echo $this->Html->link('Blog', ['plugin'=> false, 'controller' => 'news', 'action' => 'index']); ?></li>
                <li><?php echo $this->Html->link('Contato', ['plugin'=> false, 'controller' => 'contacts', 'action' => 'add']); ?></li>
            </ul>
        </div>
    </header>
    