<?php if(isset($recentMovies) && count($recentMovies) > 0): ?>
    <div class="row" style="clear: both;">
        <header>
            <h2 class="alinha-titulo"><?php echo isset($sectionTitle)?$sectionTitle:'Vídeos indicados'; ?></h2>
        </header>

        <?php $i = 1; ?>

        <?php foreach ($recentMovies as $recentMovie) : ?>
            <?php // var_dump($recentMovie); ?>

            <?php if ($i == 1 || $i % 3 == 0) : ?>
            <div class="row ultimos-filmes">
            <?php endif; ?>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                    <figure>
						<a href="<?php echo $this->Html->Url(array('controller' => 'movies', 'action' => 'view', $recentMovie['Movie']['slug'])); ?>" title="<?php echo $recentMovie['Movie']['title']; ?>">
                        <?php echo $this->Html->image($recentMovie['Movie']['thumb']['small']['url'],
                            ['alt' => 'Preview vídeo', 'class' => 'img-responsive fullsize-images']
                        ); ?>
						</a>
                        <?php echo $this->Html->link(
                            $this->Html->image('imagens/abrir-video.png', ['alt' => 'Abrir vídeo', 'class' => 'img-responsive']),
    						['controller' => 'movies', 'action' => 'view', $recentMovie['Movie']['slug']],
                            ['escape' => false, 'title' => 'Clique aqui para abrir o vídeo', 'class' => 'player']); ?>
                    </figure>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                    <h2>
                        <?php echo $this->Html->link($recentMovie['Movie']['title'],
                            ['controller' => 'movies', 'action' => 'view', $recentMovie['Movie']['slug']],
                            ['escape' => false]
                        ); ?>
                    </h2>

                    <p><?php echo $this->Text->truncate($recentMovie['Movie']['description']); ?></p>
                </div>

            <?php if ($i % 3 == 0) : ?>
            </div>
            <?php endif; ?>

            <?php $i++; ?>
        <?php endforeach; ?>

    </div>
    <?php echo $this->Html->link('Ver Todos',
                                    array('plugin' => false, 'controller' => 'movies', 'action' => 'index', 'miscellaneous'),
                                    array('class' => 'btn btn-default custom-btn-1')
                                ); ?>
<?php endif; ?>