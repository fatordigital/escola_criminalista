<?php if(isset($movie)): ?>

<?php if(!isset($movie_price)) $movie_price = 'false'; ?>

<div class="modalblock">
    <div id="topomodal">
        <h1><?php //echo $this->Time->nice(); ?>
        <?php echo $this->Html->image('imagens/logo_cdt_modal.jpg', ['alt' => 'Escola de Criminalistas']); ?></h1>
        <?php if($movie['Movie']['controller'] == 'courses'): ?>
            <b>Título do evento/curso</b><br />
        <?php else: ?>
            <b>Título do filme</b><br />
        <?php endIf; ?>
        
        <?php echo $movie['Movie']['title'] ?>
    </div>
    <div id="centromodal">

        <b>Importante</b><br />
        <?php if($movie['Movie']['price'] > 0): ?>
            Você será direcionado para o ambiente do PagSeguro, aonde poderá efetuar o pagamento com toda segurança. 
        <?php else: ?>
            Aceite os termos para confirmar sua inscrição gratuitamente.
        <?php endIf; ?>
        
    </div>
    <div id="rodapemodal">
        
        <?php if($movie['Movie']['price'] > 0): ?>

            <b>Compre com o pagseguro</b>

            <?php if($user && $user['subscriber'] == true && $movie['Movie']['controller'] == "courses" && $movie['Movie']['desconto'] > 0): ?>
                <p><?php echo $this->Number->currency($movie['Movie']['price']-$movie['Movie']['desconto']); ?></p>
            <?php elseif($movie['Movie']['controller'] == "courses" && (isset($off) && $off == 'true') ): ?>
                <p><?php echo $this->Number->currency($curso['Movie']['price']-($curso['Movie']['price']*($curso['Movie']['off']/100))); ?></p>
            <?php elseif($movie['Movie']['controller'] == "courses" && (isset($var_desconto) && $var_desconto == true) ): ?>
                <p><?php echo $this->Number->currency($curso['Movie']['price']-($curso['Movie']['price']*($curso['Movie']['off']/100))); ?></p>
            <?php elseif((isset($movie_price) && $movie_price != 'false') && count($movie['MoviePrice']) > 0): ?>

                <?php 
                    foreach($movie['MoviePrice'] as $price){
                        if($price['id'] == $movie_price){
                            $movie['Movie']['price'] = $price['price'];
                        }
                    }
                ?>
                <p><?php echo $this->Number->currency($movie['Movie']['price']); ?></p>
            <?php else: ?>
                <p><?php echo $this->Number->currency($movie['Movie']['price']); ?></p>
            <?php endIf; ?>

            <?php echo $this->Html->image('imagens/rodape_pagseguro.jpg', ['alt' => 'Pagseguro']); ?>

        <?php endIf; ?>

        <?php if($movie['Movie']['type'] == "donation"): ?>
            
            <?php 
                echo $this->Form->create(
                    'Order',
                            array(
                                    'inputDefaults' => array(
                                            'div' => 'form-group',
                                            'wrapInput' => 'col-md-4',
                                            'label' => array(
                                                    'class' => 'col col-md-3 control-label'
                                            ),
                                            'class' => 'form-control',
                                            'novalidate' => 'novalidate'
                                    ),
                                    'class' => 'form-horizontal',
                                    'url' => array('admin' => false, 'controller' => 'orders', 'action' => 'buy', $movie['Movie']['id'])
                            )
                ); 
            ?>
            <div class="form-donation">
                <div class="col-form">
                    <?php echo $this->Form->input('valor_minimo', array(
                                                                    'label' => 'Valor mínimo', 
                                                                    'value' => $this->Number->currency($movie['Movie']['price']), 
                                                                    'rel' => $this->Number->currency($movie['Movie']['price']), 
                                                                    'disabled' => 'disabled'
                                                                )); ?>
                </div>
                <div class="col-form">
                    <?php 
                        echo $this->Form->input('price', array(
                                                                'label' => 'Valor pago',
                                                                'value' => $this->Number->currency($movie['Movie']['price']), 
                                                                'placeholder' => $this->Number->currency($movie['Movie']['price']))); ?>
                </div>
                <div style="clear: both"></div>
                <input type="submit" class="btn btn-default" value="Doar" />
            </div>
            <?php
                echo $this->Form->end(); 
            ?>

        <?php else: ?>
			
			
		    <?php if($movie['Movie']['price'] > 0): ?>
            
                <?php if(isset($off) && $off == 'true' || isset($var_desconto) && $var_desconto == true): ?>
					
					<?php echo $this->Form->create('Comprar', 
						array('url' => array(
                            'controller' => 'orders',
                            'action'     => 'buy',
                            $movie['Movie']['id'],
                            'false',
                            'true',
                            $movie_price
                        ))
					); ?>
						<div style="float: left; margin-left: 150px;">
							<?php echo $this->Form->input('published', array('type' => 'checkbox', 'legend' => false, 'label' => 'Estou ciente das condições de compra.', 'required' => true, 'value' => true)); ?>
						</div>
						
						<div align="right">
							<input type="submit" class="btn btn-default" value="Comprar" />
							
						</div>
					<?php echo $this->Form->end(); ?>
					
                <?php else: ?>
				
					<?php echo $this->Form->create('Comprar', 
						array('url' => array(
                            'controller' => 'orders',
                            'action'     => 'buy',
                            $movie['Movie']['id'],
                            'false',
                            'false',
                            $movie_price
                        ))
					); ?>
						<div style="float: left; margin-left: 150px;">
							<?php echo $this->Form->input('published', array('type' => 'checkbox', 'legend' => false, 'label' => 'Estou ciente das condições de compra.', 'required' => true, 'value' => true)); ?>
						</div>
						
						<div align="right">
							<input type="submit" class="btn btn-default" value="Comprar" />
							
						</div>
					<?php echo $this->Form->end(); ?>
				
                <?php endIf; ?>

            <?php else: ?>

                    <?php echo $this->Form->create('Comprar', 
                        array('url' => array(
                            'controller' => 'orders',
                            'action'     => 'inscreva',
                            $movie['Movie']['id'],
                            'false'
                        ))
                    ); ?>
                        <div style="float: left; margin-left: 150px;">
                            <?php echo $this->Form->input('published', array('type' => 'checkbox', 'legend' => false, 'label' => 'Estou ciente das condições.', 'required' => true, 'value' => true)); ?>
                        </div>
                        
                        <div align="right">
                            <input type="submit" class="btn btn-default" value="Inscreva-se" />
                            
                        </div>
                    <?php echo $this->Form->end(); ?>

            <?php endIf; ?>

            
        <?php endIf; ?>
        
    </div>
</div>
<?php endIf; ?>