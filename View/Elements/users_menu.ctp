<?php
/** @var $this View */

$menu = [
        $this->Html->link('Meus dados', ['plugin' => false, 'controller' => 'users', 'action' => 'index'], ['class' => 'no-padding']),
        $this->Html->link('Meus v�deos', ['plugin' => false, 'controller' => 'movies', 'action' => 'mymovies'], ['class' => 'no-padding']),
        $this->Html->link('V�deos favoritos', ['plugin' => false, 'controller' => 'movie_favorites', 'action' => 'mymovies'], ['class' => 'no-padding']),
        $this->Html->link('Agenda', ['plugin' => false, 'controller' => 'lives', 'action' => 'mymovies'], ['class' => 'no-padding']),
		$this->Html->link('Meus V�deos', ['plugin' => false, 'controller' => 'movies', 'action' => 'mycourses'], ['class' => 'no-padding']),
    ];

?>

<section class="nav-interna">
    <div class="container">
        <div class="col-lg-12 col-md-12">
            <?php echo $this->Html->nestedList($menu, ['class' => 'nav navbar-nav']); ?>
        </div>
    </div>
</section>