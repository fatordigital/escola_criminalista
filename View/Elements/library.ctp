<section class="banner-biblioteca hidden-sm hidden-xs">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <header>
                    <h2>
                        <span>Uma biblioteca do seu jeito, com o seu conteúdo.</span><br />
                        Confira nossos escritos.
                    </h2>
                </header>
            </div>
        </div>
    </div>
</section>
