<?php	
	$class = array('banner-01', 'banner-02', 'banner-03', 'banner-04', 'banner-05', 'banner-06', 'banner-07', 'banner-08');
	function get_banner(){
		$class = array('banner-01', 'banner-02', 'banner-03', 'banner-04', 'banner-05', 'banner-06', 'banner-07', 'banner-08');
		$i = rand(0, count($class)-1);
		if (isset($_SESSION['back'])) {			
			while(in_array($i, $_SESSION['back'])){
				$i = rand(0, count($class)-1);
			}
			if(count($_SESSION['back']) == 5){
				array_shift($_SESSION['back']);
			}
			array_push($_SESSION['back'], $i);
		}else{
			$_SESSION['back'] = array($i); 
		}
		
		if($_SESSION['back_top_last'] == $i){
			get_banner();
		}
		
		return $i;
	}
    
	$i = get_banner();
    $selectedClass = $class[$i];
?>

<style>
	.banner-center.<?php echo $selectedClass ?> {
        display: block !important;
    }
</style>

<section class="banners-novos banner-center banner-01">
    <?php /*
    <div class="container">
        <div class="col-lg-7 col-md-8 hidden-sm hidden-xs">
            Lorem ipsum dolor sit amet, consectetur nun
            <span>risus leo, dapibus id, sollicitudin nisi.</span>
        </div>
    </div>
    */ ?>
</section>
<section class="banners-novos banner-center banner-02">
    <?php /*
    <div class="container">
        <div class="col-lg-7 col-md-8 hidden-sm hidden-xs">
            Lorem ipsum dolor sit amet, consectetur nun
            <span>risus leo, dapibus id, sollicitudin nisi.</span>
        </div>
    </div>
    */ ?>
</section>
<section class="banners-novos banner-center banner-03">
    <?php /*
    <div class="container">
        <div class="col-lg-7 col-md-8 hidden-sm hidden-xs">
            Lorem ipsum dolor sit amet, consectetur nun
            <span>risus leo, dapibus id, sollicitudin nisi.</span>
        </div>
    </div>
    */ ?>
</section>
<section class="banners-novos banner-center banner-04">
    <?php /*
    <div class="container">
        <div class="col-lg-7 col-md-8 hidden-sm hidden-xs">
            Lorem ipsum dolor sit amet, consectetur nun
            <span>risus leo, dapibus id, sollicitudin nisi.</span>
        </div>
    </div>
    */ ?>
</section>
<section class="banners-novos banner-center banner-05">
    <?php /*
    <div class="container">
        <div class="col-lg-7 col-md-8 hidden-sm hidden-xs">
            Lorem ipsum dolor sit amet, consectetur nun
            <span>risus leo, dapibus id, sollicitudin nisi.</span>
        </div>
    </div>
    */ ?>
</section>
<section class="banners-novos banner-center banner-06">
   <?php /*
    <div class="container">
        <div class="col-lg-7 col-md-8 hidden-sm hidden-xs">
            Lorem ipsum dolor sit amet, consectetur nun
            <span>risus leo, dapibus id, sollicitudin nisi.</span>
        </div>
    </div>
    */ ?>
</section>
<section class="banners-novos banner-center banner-07">
    <?php /*
    <div class="container">
        <div class="col-lg-7 col-md-8 hidden-sm hidden-xs">
            Lorem ipsum dolor sit amet, consectetur nun
            <span>risus leo, dapibus id, sollicitudin nisi.</span>
        </div>
    </div>
    */ ?>
</section>
<section class="banners-novos banner-center banner-08">
    <?php /*
    <div class="container">
        <div class="col-lg-7 col-md-8 hidden-sm hidden-xs">
            Lorem ipsum dolor sit amet, consectetur nun
            <span>risus leo, dapibus id, sollicitudin nisi.</span>
        </div>
    </div>
    */ ?>
</section>
<section class="banners-novos banner-center banner-09">
   <?php /*
    <div class="container">
        <div class="col-lg-7 col-md-8 hidden-sm hidden-xs">
            Lorem ipsum dolor sit amet, consectetur nun
            <span>risus leo, dapibus id, sollicitudin nisi.</span>
        </div>
    </div>
    */ ?>
</section>
<section class="banners-novos banner-center banner-10">
    <?php /*
    <div class="container">
        <div class="col-lg-7 col-md-8 hidden-sm hidden-xs">
            Lorem ipsum dolor sit amet, consectetur nun
            <span>risus leo, dapibus id, sollicitudin nisi.</span>
        </div>
    </div>
    */ ?>
</section>