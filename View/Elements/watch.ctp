<section class="assista-aulas rodape hidden-sm hidden-xs">
    <div class="container">
        <div class="col-lg-6">
            <article>
                <p><span>Assista suas aulas onde quiser e quando quiser.</span><br>
                    Uma nova maneira de estudar direito.
                    <?php echo $this->Html->image('imagens/instituto-casa-da-tolerancia-logo-small.png',
                        ['alt' => 'Instituto Casa da Tolerância', 'class' => '']); ?>
                </p>
            </article>
        </div>
    </div>
</section>