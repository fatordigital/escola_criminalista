<?php echo $this->Html->scriptStart(['inline' => false]) ?>
function modalAssinatura(){
    $.blockUI({
        css: {
            border: 'none',
            padding: '5px',
            width: '600px',
            top: '25%',
            backgroundColor: '#FFFFFF',
            '-webkit-border-radius': '5px',
            '-moz-border-radius': '5px'
        },
    message: $('#modalblockassinantegeneral'),

    });
}
<?php echo $this->Html->scriptEnd() ?>

<div id="modalblockassinantegeneral" class="modalblock" style="display: none">
        <a href="javascript:void(0);" class="btnFechar">
            <?php echo $this->Html->image('imagens/btn_fechar.png', ['alt' => 'Fechar', 'id' => 'btnFechar']); ?>
        </a>
        <div id="topomodal">
            <h1 class="custom">Faça Parte da Casa</h1>
            <h4 class="custom">Nullam molestie enim leo. Aenean rhoncus sem metus, eu lobortis tellus tristique ac. Donec auctor nulla non turpis feugiat aliquam. </h4>
        </div>
        <div id="centromodal">
            <div class="row">
                <div class="col-lg-6 col-md-6 text-center">
                    <?php echo $this->Html->image('imagens/5990.png', ['alt' => 'De R$ 59,90']); ?>
                </div>
                <div class="col-lg-6 col-md-6 text-center">
                    <?php echo $this->Html->image('imagens/1990.png', ['alt' => 'Por R$ 19,90']); ?>
                </div>
            </div>
        </div>
        <div id="rodapemodal">
            <b class="custom">Importante</b>
            <p>Após o processo de pagamento, sua assinatura poderá levar até 24 horas úteis para ser habilitada. Para agilizar esse processo entre em contato conosco pelo telefone: <span>(51) 3232-4749</span></p>

            <?php echo $this->Html->image('imagens/rodape_pagseguro.jpg', ['alt' => 'Pagseguro']); ?>
            <div align="right">
                <?php echo $this->Form->postLink('Assine e Faça Parte da Casa',
                    array(
                        'controller' => 'orders',
                        'action'     => 'buy',
                        'assinaturaictd',
                        'true'
                    ),
                    array(
                        'class' => 'btn btn-default',
                        'title' => 'Assine e Faça Parte da Casa'
                    )
                ); ?>
            </div>
        </div>

    </div>