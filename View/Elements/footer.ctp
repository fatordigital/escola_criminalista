<?php /** @var $this View */ ?>

<footer>
    <div class="container">
        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-6">
            Escola de Criminalistas<br>
            Todos os direitos reservados © 2014
            <?php echo $this->Html->link(
                'contato@escoladecriminalistas.com.br',
                'mailto:contato@escoladecriminalistas.com.br',
                ['title' => 'contato@escoladecriminalistas.com.br', 'class' => 'email']
            ); ?>
        </div>
        <div class="col-lg-5 col-md-5 hidden-sm hidden-xs text-right redes-sociais">
           <!-- <?php echo $this->Html->link(
                $this->Html->image('imagens/facebook-logo-link.jpg', ['alt' => 'Facebook']),
                'https://www.facebook.com/institutotolerancia',
                ['escape' => false, 'title' => 'Facebook']
            ) ?>
            <?php echo $this->Html->link(
                $this->Html->image('imagens/youtube-logo-link.jpg', ['alt' => 'YouTube']),
                'https://www.youtube.com/channel/UCfV-SWfVMxcXWeVRmlsbsow',
                ['escape' => false, 'title' => 'YouTube']
            ) ?>-->
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 pull-right">
            <?php echo $this->Html->image('imagens/logo-footer.png',
                ['class' => 'logo-rodape img-responsive', 'alt' => 'Escola Criminalista']); ?>
            <?php echo $this->Html->image('imagens/logo-footer.png',
                ['class' => 'logo-rodape-menor', 'alt' => 'Escola Criminalista']); ?>
                <br>
            <div class="pull-right">

                <?php echo $this->Html->link(
                    $this->Html->image('imagens/logo-icdt.png', ['class' => 'myIcdt', 'alt' => 'Instituto de Tolerância']),
                    'http://www.itolerancia.com.br/',
                    ['escape' => false, 'title' => 'Instituto de Tolerância','target'=>'_blank']
                ) ?>

                <?php echo $this->Html->link(
                    $this->Html->image('imagens/logo-fatordigital.png', ['alt' => 'Fator Digital']),
                    'http://fatordigital.com.br',
                    ['escape' => false, 'title' => 'Fator Digital', 'target'=>'_blank']
                ) ?>

            </div>
        </div>
    </div>
</footer>
<?php /*
<section class="chat-footer">
        <div class="mulher">
            <?php echo $this->Html->image('imagens/fechar-chat.png', ['alt' => 'Fechar chat', 'class' => 'fechar-chat']) ?>
        </div>
        <div class="link">
            <div class="container">
                <div class="text-center">
                    SEMINÁRIO INTERNACIONAL VIRTUAL – DIA 04/04. <a href="/siv" title="Clique para assiste ao seminário ao vivo" target="_blank">Clique aqui</a>
                </div>
            </div>
        </div>
</section>


<section class="chat-footer">
        <div class="mulher">
            <?php echo $this->Html->image('imagens/fechar-chat.png', ['alt' => 'Fechar chat']) ?>
        </div>
        <div class="link">
            <div class="container">
                <div class="text-center">
                    Acompanhe a agenda de conteúdo ao vivo. <a href="#" title="Clique aqui">Clique aqui!</a>
                </div>
            </div>
        </div>
</section> */ ?>

<?php echo $this->Html->scriptStart(['inline' => false]) ?>
    $(document).ready(function(){
        $('.fechar-chat').click(function(){
            $('.chat-footer').fadeOut('slow');
        });

        $('.botao-menu img').click(function(){
            $('.menu-responsivo').toggle('blind');
        });
    });
<?php echo $this->Html->scriptEnd() ?>
