<section class="cadastro">
    <div class="container">
        <header>
            <h2>Cadastre-se Agora!</h2>
        </header>
        <p>O cadastro é pratico e simples. Com ele você pode assistir diversos vídeos e interagir com os demais
            participantes do instituto. Teremos o maior prazer em ter você conosco!</p>

        <div class="row-fluid">
            <div class="col-lg-6 col-md-6 col-sm-12 text-center">
                <div class="facebook-cadastro botao-cadastro">

                    <?php
                    if (!$facebook) {
                        echo $this->Facebook->login(
                            array(
                                'custom' => true,
                                'perms' => 'email,publish_stream',
                                'img' => 'facebook-logo.jpg'
                            )
                        );
                    } else {
                        echo $this->Facebook->login(
                            array(
                                'perms' => 'email,publish_stream',
                            )
                        );
                        echo $this->Facebook->logout(
                            array(
                                'redirect' => array(
                                    'controller' => 'users',
                                    'action' => 'logout'
                                ),
                                'img' => 'facebook-logout.png'
                            )
                        );
                    }
                    ?>

                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 text-center">
                <div class="meu-cadastro botao-cadastro">
                    <?php echo $this->Html->link('Inscrição com o endereço de e-mail',
                    ['plugin' => 'Users', 'controller' => 'Users', 'action' => 'add'],
                    ['class' => 'btn btn-default custom-btn-3']); ?>
                </div>
            </div>
        </div>
    </div>
</section>