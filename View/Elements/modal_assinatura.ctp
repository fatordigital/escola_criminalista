<?php if(isset($movie)): ?>
<div class="modalblock">
    <div id="topomodal">
        <h1 class="custom">Faça Parte da Casa</h1>
        <!-- <h4 class="custom">Nullam molestie enim leo. Aenean rhoncus sem metus, eu lobortis tellus tristique ac. Donec auctor nulla non turpis feugiat aliquam. </h4> -->
    </div>
    <div id="centromodal">
        <div class="row">
            <div class="col-lg-6 col-md-6 text-center">
                <?php echo $this->Html->image('imagens/5990.png', ['alt' => 'De R$ 59,90']); ?>
            </div>
            <div class="col-lg-6 col-md-6 text-center">
                <?php echo $this->Html->image('imagens/1990.png', ['alt' => 'Por R$ 19,90']); ?>
            </div>
        </div>
    </div>
    <div id="rodapemodal">
        <b class="custom">Importante</b>
        <p>Após o processo de pagamento, sua assinatura poderá levar até 1 dia útil para ser habilitada, dependendo da aprovação da instituição financeira. Para agilizar esse processo entre em contato conosco pelo telefone: <span>(51) 3232-4749</span></p>

        <?php echo $this->Html->image('imagens/rodape_pagseguro.jpg', ['alt' => 'Pagseguro']); ?>
        <div align="right">
            <?php echo $this->Form->postLink('Assine e Faça Parte da Casa',
                array(
                    'controller' => 'orders',
                    'action'     => 'buy',
                    $movie['Movie']['id'],
                    'true'
                ),
                array(
                    'class' => 'btn btn-default',
                    'title' => 'Assine e Faça Parte da Casa'
                )
            ); ?>
        </div>
    </div>
</div>
<?php endIf; ?>