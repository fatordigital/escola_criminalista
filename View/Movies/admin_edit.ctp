<?php

/**
 * @var $this View
 */

$this->Html->addCrumb(__d('users', $movieType . 's'));
$this->Html->addCrumb(__d('users', "Admin Edit %s $movieType", $this->Form->value('Movie.id')));
?>


<?php $this->Html->script("$cmsPluginName.assets/plugins/jquery-tags-input/jquery.tagsinput.min.js", ['inline' => false]); ?>
<?php $this->Html->css("$cmsPluginName.assets/plugins/jquery-tags-input/jquery.tagsinput.css", ['inline' => false]); ?>

<?php $this->Html->script("$cmsPluginName.assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js", ['inline' => false]); ?>
<?php $this->Html->css("$cmsPluginName.assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css", ['inline' => false]); ?>

<?php $this->Html->script("$cmsPluginName.assets/plugins/select2/select2.min.js", ['inline' => false]); ?>
<?php $this->Html->css("$cmsPluginName.assets/plugins/select2/select2.css", ['inline' => false]); ?>
<?php $this->Html->css("$cmsPluginName.assets/plugins/select2/select2-metronic.css", ['inline' => false]); ?>


<?php $this->Html->script("$cmsPluginName.assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker", ['inline' => false]); ?>
<?php $this->Html->script("$cmsPluginName.assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min", ['inline' => false]); ?>
<?php $this->Html->script("$cmsPluginName.assets/plugins/clockface/js/clockface", ['inline' => false]); ?>

<?php $this->Html->css("$cmsPluginName.assets/plugins/bootstrap-datepicker/css/datepicker.css", ['inline' => false]); ?>
<?php $this->Html->css("$cmsPluginName.assets/plugins/clockface/css/clockface.css", ['inline' => false]); ?>

<?php $this->Html->scriptStart(['inline' => false]) ?>
$(function() {
    //alert($('#MovieRelatedMovieTmp').val());
    $('#MovieTags').tagsInput({
        width: 'auto'
    });

     //$("#MovieRelatedMovie").select2({tags:["red", "green", "blue"]});
    
    //$("#MovieRelatedMovie").select2({
        // tags: $('#MovieRelatedMovieTmp').val(),
        // data: [{id: 0, text: 'story'},{id: 1, text: 'bug'},{id: 2, text: 'task'}]
       //tags: $('#MovieRelatedMovieTmp').val()
    //});

    $('#MovieRelatedMovie').select2({
        placeholder: "Pesquise um filme relacionado",
        minimumInputLength: 3,
        multiple: true,
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: "<?php echo $this->Html->url(['controller' => $controller, 'action' => 'index', 'ext' => 'json', 'prefix' => 'admin']); ?>",
            data: function (term, page) {
                return {
                    q: term
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
            // since we are using custom formatting functions we do not need to alter remote JSON data
                return {
                    results: data.movies
                };
            }
        },
        initSelection: function(element, callback) {
            var data = [];
            $($.parseJSON(element.val())).each(function(i, item) {
                data.push({
                    id: item.id,
                    title: item.title,
                });
            });
            //$("MovieRelatedMovie").val("");
            callback(data);
        },
        closeOnSelect: false,
        formatResult: movieFormatResult, // omitted for brevity, see the source of this page
        formatSelection: movieFormatSelection, // omitted for brevity, see the source of this page
    });
});

function movieFormatResult(movie) {
    return movie.title;
}

function movieFormatSelection(movie) {
    return movie.title;
}

$(document).ready(function() {       
    $('.date-picker').datepicker({ format: 'd/m/yyyy' });
   
    $('.clockface_1').clockface();
}); 

/*BEGIN PRICE*/
esconde_preco($("[name='data[Movie][type]']").val());
$("[name='data[Movie][type]']").change(function(){
    esconde_preco($("[name='data[Movie][type]']").val());

    esconde_assinatura($("[name='data[Movie][type]']").val());
});
function esconde_preco( value ){
    if(value == "free"){
        $("[name='data[Movie][price]']").attr("disabled", "disabled").closest('.form-group').hide();

        $("[name='data[Movie][restriction]']").removeAttr("disabled").closest('.form-group').show();        
        $("[name='data[Movie][restriction]']").closest('.has-switch').removeClass('disabled');

        $("[name='data[Movie][via_venda_avulso]']").attr("disabled", "disabled").closest('.form-group').hide();
        $("[name='data[Movie][via_assinatura]']").attr("disabled", "disabled").closest('.form-group').hide();
		
		$(".input_prices").attr("disabled", "disabled");

    }else{
        $("[name='data[Movie][price]']").removeAttr("disabled").closest('.form-group').show();

        $("[name='data[Movie][restriction]']").attr("disabled", "disabled").closest('.form-group').hide();

        $("[name='data[Movie][via_venda_avulso]']").removeAttr("disabled").closest('.form-group').show();        
        $("[name='data[Movie][via_venda_avulso]']").closest('.has-switch').removeClass('disabled');

        $("[name='data[Movie][via_assinatura]']").removeAttr("disabled").closest('.form-group').show();
        $("[name='data[Movie][via_assinatura]']").closest('.has-switch').removeClass('disabled');
		
		$(".input_prices").removeAttr("disabled");
        if ($("[name='data[Movie][show_movie_prices]']").is(':checked')) {
            $('.movie_prices').show();
            
            $(".input_prices").removeAttr("disabled");
        }else{
            $('.movie_prices').hide();
            
            $(".input_prices").attr("disabled", "disabled");
        }
    }
}

function esconde_assinatura( value ){
    if(value == "donation"){
        $("[name='data[Movie][via_assinatura]']").attr("disabled", "disabled").closest('.form-group').hide();
    }else{
        $("[name='data[Movie][via_assinatura]']").removeAttr("disabled").closest('.form-group').show();
    }
}

/*END PRICE*/

function selectFieldsMoviesPrices(){
    //se for cliente
    if ($("[name='data[Movie][show_movie_prices]']").is(':checked')) {
        $('.movie_prices').show();
    }else{
        $('.movie_prices').hide();
    }
}
$(document).ready(function() {   

    selectFieldsMoviesPrices();
    $("[name='data[Movie][show_movie_prices]']").change(function() {
       selectFieldsMoviesPrices();
    });
    
    //begin add variacao
    $('a.btn-add-variacao').on('click', function() {
        $("[name*='[MoviePrice][0][title]']").parents('.control-group').clone(true).appendTo('.box_variacoes');


        $('.box_variacoes > .control-group').last().each(function(i, v) {
            i = $('.box_variacoes > .control-group').length;
            $('a.btn-add-variacao', this).each(function(a, b) {
                $(this).hide();
            });
            $('a.btn-remove-variacao', this).each(function(a, b) {
                $(this).show();
                $(this).attr('rel', '');
            });

            $('input', this).each(function(a, b) {
                str = $(this).attr('name').replace(/\[MoviePrice\]\[[0-9]\]+/g, '[MoviePrice]['+i+']');
                $(this).attr('name', str);
                $(this).val("");
            });

            $("input[name='data[MoviePrice]["+i+"][id]']").val('');
        });
        return false;
    });
    $('a.btn-remove-variacao').on('click', function() {

        if($(this).attr('rel') != ""){
            var obj_btn = $(this);
            $.post("/fatorcms/movies/delete_price/" + obj_btn.attr('rel') , function(retorno) {
                if (retorno == true) {
                    obj_btn.closest('.control-group').remove();
                }else{
                    alert('Houve um problema na hora da exclusão.');
                }
            }, 'json');
        }else{
            $(this).closest('.control-group').remove();
        }

        return false;
    });
    //end add variacao

});
<?php $this->Html->scriptEnd() ?>

<div class="row">
    <div class="col-md-12">
        <h2 class="page-title"><?php echo __d('users', "Admin Edit $movieType"); ?></h2>
        <?php echo $this->Html->getCrumbList(
            array(
                'class'     => 'breadcrumb',
                'separator' => '<i class="icon-angle-right"></i>',
                'escape'    => false
            ),
            'Home'
        );
        ?>    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <?php echo $this->Session->flash(); ?>
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <?php echo __d('users', "Admin Edit the $movieType information"); ?>
                </div>
            </div>

            <div class="portlet-body form movies">
                <div class="form-body">
                    <?php echo $this->Form->create(
                        'Movie',
                        array(
                            'inputDefaults' => array(
                                'div'        => 'form-group',
                                'wrapInput'  => 'col-md-4',
                                'label'      => array(
                                    'class' => 'col col-md-3 control-label'
                                ),
                                'class'      => 'form-control',
                                'novalidate' => 'novalidate'
                            ),
                            'class'         => 'form-horizontal',
                            'type'          => 'file',
                        )
                    ); ?>

                    <?php
                    echo $this->Form->hidden('id');
					echo $this->Form->input('status', array(
                            'class' => 'form-control make-switch',
                            'before' => '<label class="col-md-3 control-label">Status</label>',
                            'label' => false,
                            'checkboxDiv' => false,
                        ));

                    if($controller != "courses" && $controller != "modules"){
                        echo $this->Form->input('category_id', array('label' => 'Categoria'));
                        echo $this->Form->input('type', [
                            'label' => 'Tipo',
                            'options' => [
                                'free' => 'Gratuito',
                                'donation' => 'Clique Solidário',
                                'paid' => 'Pago',
                            ]
                        ]);
                    }else{
                        echo $this->Form->input('type', array('type' => 'hidden', 'value' => 'paid'));
                    }


                    echo $this->Form->input('title', array('label' => 'Título'));
                    echo $this->Form->input('description', array('label' => 'Descrição'));
                     echo $this->Form->input('price', array('label' => 'Preço'));

                    ?>

                    <?php
                        echo $this->Form->input('show_movie_prices', array(
                            'class' => 'form-control make-switch',
                            'before' => '<label class="col-md-3 control-label">Tabela de Preços</label>',
                            'label' => false,
                            'checkboxDiv' => false,
                            'default' => false
                        ));
                    ?>

                    <div class="col-md-12 movie_prices">
                        <div class="table-grade">
                            <div class="control-group">

                                <div class="col-md-3"></div>
								<div class="col-md-9 no-padding form-inline">	
                                    <?php echo $this->Form->hidden('MoviePrice.0.id'); ?>
                                    <?php echo $this->Form->input('MoviePrice.0.title', array('label' => 'Título', 'required' => true, 'class' => 'input_prices')); ?>
                                    <?php echo $this->Form->input('MoviePrice.0.price', array('label' => 'Preço', 'required' => true, 'escape' => false, 'after' => '', 'class' => 'input_prices')); ?>
									
									<div class="col-md-12">
										<?php if(isset($this->data['MoviePrice'][0]['id']) && $this->data['MoviePrice'][0]['id'] != ""): ?>
											<a href="javascript:void(0);" class="btn-remove-variacao pull-right" rel="'.$this->data['MoviePrice'][0]['id'].'">remover [x]</a> 
										<?php else: ?>
											<a href="javascript:void(0);" class="btn-remove-variacao pull-right">remover [x]</a> 
										<?php endIf; ?>
										<a href="javascript:void(0);" class="btn-add-variacao pull-right">adicionar [+]</a>
									</div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="box_variacoes movie_prices">
                        <?php if(isset($this->data['MoviePrice']) && count($this->data['MoviePrice']) > 1): ?>
                            <?php $first = true; ?>
                            <?php foreach($this->data['MoviePrice'] as $k => $grade): ?>
                                <?php if($first == true){ $first = false; continue; } ; ?>

                                <div class="control-group">

                                    <div class="col-md-3"></div>
									<div class="col-md-9 no-padding form-inline">	
                                        <?php echo $this->Form->hidden("MoviePrice.{$k}.id"); ?>
                                        <?php echo $this->Form->input("MoviePrice.{$k}.title", array('label' => 'Título', 'required' => true, 'class' => 'input_prices')); ?>
                                        <?php echo $this->Form->input("MoviePrice.$k.price", array('label' => 'Preço', 'required' => true, 'escape' => false, 'after' => '', 'class' => 'input_prices')); ?>
										
										<div class="col-md-12">
											<a href="javascript:void(0);" class="btn-remove-variacao pull-right" rel="'.$grade['id'].'">remover [x]</a> 
											<a href="javascript:void(0);" class="btn-add-variacao pull-right">adicionar [+]</a>
										</div>
                                    </div>

                                </div>

                            <?php endForeach; ?>
                        <?php endIf;  ?>
                    </div>

                    <?php
                    if($controller == "courses"){
                        //echo $this->Form->input('desconto', array('label' => 'Desconto'));
						echo $this->Form->input('off', array('label' => 'Percentual de Desconto', 'escape' => false, 'after' => 'Digite o percentual do desconto aplicável <br>para quem faz jus ao desconto especial.'));
                    }
                    
                    if($controller != 'lives' && $controller != 'courses'){
                        echo $this->Form->input('link', array('required' => false));
                        echo $this->Form->input('tags',[
                            'class' => 'form-control tags'
                        ]);

						if($controller != 'related'){
							echo $this->Form->input('Movie.RelatedMovie',[
								'label' => 'Filme Relacionado',
								'class' => 'form-control tags'
							]);
							echo $this->Form->hidden('Movie.RelatedMovieTmp');
						}
                    }

                    if($controller != "courses"){
                        echo $this->Form->input('comments', array(
                            'class' => 'form-control make-switch',
                            'before' => '<label class="col-md-3 control-label">Comentários</label>',
                            'label' => false,
                            'checkboxDiv' => false,
                            'default' => true
                        ));
                        echo $this->Form->input('restriction', array(
                            'class' => 'form-control make-switch',
                            'before' => '<label class="col-md-3 control-label">Restrição</label>',
                            'label' => false,
                            'checkboxDiv' => false
                        ));
                    }

                    if($controller == 'lives' || $controller == 'modules'){
                        echo $this->Form->input('date', array(
                            'class' => 'form-control date-picker',
                            'before' => '<label class="col-md-3 control-label">Data</label>',
                            'label' => false,
                            'checkboxDiv' => false,
                            'type' => 'text'
                        ));

                       echo $this->Form->input('hour_start', array(
                            'class' => 'form-control timepicker clockface_1',
                            'before' => '<label class="col-md-3 control-label">Hora de Inicio</label>',
                            'label' => false,
                            'checkboxDiv' => false,
                            'type' => 'text'
                        ));

                        echo $this->Form->input('hour_end', array(
                            'class' => 'form-control timepicker clockface_1',
                            'before' => '<label class="col-md-3 control-label">Hora de Termino</label>',
                            'label' => false,
                            'checkboxDiv' => false,
                            'type' => 'text'
                        ));
                    }

                    /*echo $this->Form->input('facebook_posting', array(
                        'class' => 'form-control make-switch',
                        'before' => '<label class="col-md-3 control-label">Facebook</label>',
                        'label' => false,
                        'checkboxDiv' => false
                    ));*/

					if($controller != "courses" && $controller != "modules"){
						echo $this->Form->input('via_venda_avulso', array(
							'class' => 'form-control make-switch',
							'before' => '<label class="col-md-3 control-label">Permite Venda Avulsa</label>',
							'label' => false,
							'checkboxDiv' => false,
							// 'default' => true
						));

                        echo $this->Form->input('via_assinatura', array(
                            'class' => 'form-control make-switch',
                            'before' => '<label class="col-md-3 control-label">Permite Assinatura</label>',
                            'label' => false,
                            'checkboxDiv' => false,
                            // 'default' => true
                        ));

                    }

                    echo $this->Form->input('parent_id', array('type' => 'hidden'));
                    echo $this->Form->input('controller', array('type' => 'hidden'));
                    ?>

                    <?php
                    //begin IMAGEM NO CURSO E NO MODELULO
                    if($controller == "courses" || $controller == 'modules' || $controller == 'lives'){
                        echo $this->Form->input('image', ['label' => 'Imagem', 'type' => 'file']);
                    }
                    ?>

                    <?php if($this->data['Movie']['image'] != ""): ?>
                    <div class="form-group content-image-news">
                        <label class="col col-md-3 control-label"></label>
                        <div class="col col-md-9">
                            <img src="<?php echo $this->Html->Url('/files/movie/image/'.$this->data['Movie']['id'].'/thumb_'.$this->data['Movie']['image'], true); ?>" alt="" width="150" />
                            <?php echo $this->Form->input('Movie.image.remove', array('type' => 'checkbox', 'label' => 'Remover imagem existente')); ?>
                        </div>
                    </div>
                    <?php endIf; 
                    //end IMAGEM NO CURSO E NO MODULO
                    ?>

                    <div class="form-actions">

                        <?php echo $this->Form->submit(__d('users', 'Submit'), array(
                            'div'    => false,
                            'class'  => 'btn blue',
                            'escape' => false,
                        )); ?>
                        <?php echo $this->Form->postLink(
                            __d('users', 'Delete %s', '<i class="fa fa-times"></i>'),
                            array(
                                'escape'         => false,
                                'class'          => 'btn btn-sm red btn-removable',
                                'action'         => 'delete',
                                $this->Form->value('Movie.id')
                            ),
                            array('class' => 'btn red', 'escape' => false),
                            __d('users', 'Are you sure you want to delete %s # %s?', __d('users', strtolower($movieType)), $this->Form->value('Movie.id'))
                        ); ?>

                    </div>

                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
.w200{
width: 300px;
background-color: #ffffff;
    border: 1px solid #e5e5e5;
    border-radius: 0;
    box-shadow: none;
    color: #333333;
    font-size: 14px;
    font-weight: normal;
    transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
	height: 30px;
}
.btn-add-variacao, .btn-remove-variacao{
	margin-top: 10px;
	margin-bottom: 10px;
}
.btn-add-variacao{
	margin-right: 10px;
}
.no-padding{
	padding: 0px !important;
}
</style>