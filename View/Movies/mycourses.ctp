<?php echo $this->element('users_menu'); ?>

<section class="topo-pagina">
    <div class="container">
        <div class="row-fluid">
            <header>
                <div class="col-lg-3">
					<h1 class="titulo">Meus Cursos</h1>
				</div>
            </header>
        </div>
    </div>
</section>
				

<section class="destaque modulos">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">
			
				<?php if(isset($orders) && count($orders) > 0): ?>
					<?php $c = 1; ?>
					<?php foreach ($orders as $y => $order) : ?>
					
						<?php if(count($order['modules']) > 0): ?>

							<?php $i = 1; ?>
							<?php foreach ($order['modules'] as $k => $movie) : ?>

								<h3><?php echo $order['course']['Movie']['title']; ?> - <?php echo $movie['Movie']['title']; ?></h3>
								
								<div class="buttons">							
									<?php 
										echo $this->Html->link('Ver Detalhes',
														['controller' => 'lives', 'action' => 'view', $movie['Movie']['slug']],
														['escape' => false, 'class' => 'btn btn-default detalhes vermelho']
														);		
									?>							
								</div>
								
								<?php $i++; ?>
							<?php endforeach; ?>
						
						<?php else: ?>
							<p>Nenhum módulo encontrado.</p>
						<?php endif; ?>
						
						<?php if($c < count($orders)): ?>
							<hr />
						<?php endif; ?>
					
					<?php $c++; ?>
					<?php endforeach; ?>
					
				<?php else: ?>
					<p>Nenhum curso encontrado.</p>
				<?php endif; ?>
				
            </div>
        </div>
    </div>
</section>