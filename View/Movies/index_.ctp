<?php /** @var $this View */ ?>

    <section class="topo-pagina">
        <div class="container">
            <div class="row-fluid">
                <header>
                    <div class="col-lg-3 col-md-4 col-sm-5 col-xs-12">
                        <h1>Todos os vídeos</h1>
                    </div>
                    <div class="col-lg-9 col-md-8 col-sm-7 col-xs-7 hidden-sm hidden-xs text-right">
                        <select name="categorias" id="categorias">
                            <option value="Selecione a categoria">Selecione a categoria</option>
                            <option value="Curso">Curso</option>
                            <option value="Filmes">Filmes</option>
                            <option value="Curtas">Curtas</option>
                            <option value="Ao vivo">Ao vivo</option>
                            <option value="Palestras">Palestras</option>
                            <option value="Escritos">Escritos</option>
                        </select>
                        <select name="duracao" id="duracao">
                            <option value="Duração">Duração</option>
                            <option value="10min">Até 10 minutos</option>
                            <option value="30min">Até 30 minutos</option>
                            <option value="60min">Até 1 hora</option>
                            <option value="+60min">Mais de 1 hora</option>
                        </select>
                        <select name="quantidade" id="quantidade">
                            <option value="Quantidade">Quantidade</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                        </select>
                    </div>
                </header>
            </div>
        </div>
    </section>

    <section class="ultimos-videos destaque">
        <div class="container">

            <?php $i = 1; ?>
            <?php foreach ($movies as $movie) : ?>

                <?php if ($i == 0 || $i % 3 == 0) : ?>
                    <div class="row-fluid">
                <?php endif; ?>

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-30">
                    <figure class="text-right">
                        <?php echo $this->Html->image($movie['Movie']['thumb']['big']['url'], ['alt' => $movie['Movie']['title'], 'class' => 'img-responsive fullsize-images']); ?>

                        <span>
                            <?php echo $this->Html->image('imagens/categoria-video-preview-1.png', ['alt' => 'Tipo de vídeo', 'class' => 'img-responsive categoria-video']); ?>
                        </span>
                        <?php echo $this->Html->link(
                            $this->Html->image('imagens/abrir-video.png', ['alt' => 'Abrir video', 'class' => 'img-responsive']),
                            ['controller' => 'movies', 'action' => 'view', $movie['Movie']['slug']],
                            ['escape' => false]
                        ); ?>
                    </figure>
                    <header>
                        <h3>
                            <?php echo $this->Html->link($movie['Movie']['title'],
                                ['controller' => 'movies', 'action' => 'view', $movie['Movie']['slug']],
                                ['escape' => false]
                            ); ?>
                        </h3>
                    </header>
                    <p>
                        <?php echo $this->Text->truncate($movie['Movie']['description']); ?>
                    </p>
                </div>

                <?php if ($i % 3 == 0) : ?>
                    <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php $i++; ?>
            <?php endforeach; ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center clear">
                    <ul class="pagination customizada">
                        <li class="img"><a href="#" title="Voltar página"><img
                                    src="<?php // echo SITE_BASE ?>/views/imagens/voltar-pagina.png"
                                    alt="Voltar página"></a>
                        </li>
                        <li><a href="#" title="Ir para a página 01" class="active">01</a></li>
                        <li><a href="#" title="Ir para a página 02">02</a></li>
                        <li><a href="#" title="Ir para a página 03">03</a></li>
                        <li><a href="#" title="Ir para a página 04">04</a></li>
                        <li><a href="#" title="Ir para a página 05">05</a></li>
                        <li><a href="#" title="Ir para a página 06">06</a></li>
                        <li><a href="#" title="Ir para a página 07">07</a></li>
                        <li class="img"><a href="#" title="Avançar página"><img
                                    src="<?php // echo SITE_BASE ?>/views/imagens/avancar-pagina.png"
                                    alt="Avançar página"></a>
                        </li>
                    </ul>
                </div>
            </div>

            <?php echo $this->element('latest_movies'); ?>

        </div>
    </section>

<?php echo $this->element('library'); ?>