
<?php



/**

 * @var $this View

 */


$this->Html->addCrumb(__d('SolutionsCMS', 'Movies'));

$this->Html->addCrumb(__d('SolutionsCMS', 'Edit %s Movie', $this->Form->value('Movie.id')));

?>

<div class="row">
    <div class="col-md-12">
        <h2 class="page-title"><?php echo __d('SolutionsCMS', 'Edit Movie'); ?></h2>
                <?php echo $this->Html->getCrumbList(
            array(
                'class' => 'breadcrumb',
                'separator' => '<i class="icon-angle-right"></i>',
                'escape' => false
            ),
            '<i class="icon-home"></i> Home'
        );
        ?>    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <?php echo $this->Session->flash(); ?>
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <?php echo __d('SolutionsCMS', 'Edit the Movie information'); ?>64                </div>
            </div>

            <div class="portlet-body form movies">
                <div class="form-body">
                    <?php echo $this->Form->create(

                        'Movie',

                        		array(

                            			'inputDefaults' => array(

                                				'div' => 'form-group',

                                				'wrapInput' => 'col-md-4',

                                				'label' => array(

                                    					'class' => 'col col-md-3 control-label'

                                				),

                                				'class' => 'form-control',

                                				'novalidate' => 'novalidate'

                            			),

                            			'class' => 'form-horizontal',

                        		)

                    ); ?>

                    	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('category_id');
		echo $this->Form->input('title');
		echo $this->Form->input('description');
		echo $this->Form->input('link');
		echo $this->Form->input('length');
		echo $this->Form->input('type');
		echo $this->Form->input('price');
		echo $this->Form->input('comments');
		echo $this->Form->input('restriction');
		echo $this->Form->input('facebook_posting');
		echo $this->Form->input('source');
		echo $this->Form->input('api_data');
	?>

                    <div class="form-actions">

                        <?php echo $this->Form->submit(__d('SolutionsCMS', 'Submit'), array(
                            'div' => false,
                            'class' => 'btn blue',
                            'escape' => false,
                        )); ?>
                        <?php echo $this->Form->postLink(
                            __d('SolutionsCMS', 'Delete %s', '<i class="fa fa-times"></i>'),
                            array(
                                'action' => 'delete',
                                $this->Form->value('Movie.id')
                            ),
                            array('class' => 'btn red', 'escape' => false),
                            __d('SolutionsCMS', 'Are you sure you want to delete %s # %s?', __d('SolutionsCMS', 'System User'), $this->Form->value('Movie.id'))
                        );

                    </div>

                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
