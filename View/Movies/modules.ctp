<?php echo $this->Html->scriptStart(['inline' => false]) ?>

function modalPagamento(id) {
    $.blockUI({
        css: {
            border: 'none',
            padding: '5px',
            width: '600px',
            backgroundColor: '#FFFFFF',
            '-webkit-border-radius': '5px',
            '-moz-border-radius': '5px',
        },
    message: $('#modalblock-'+id)
    });
}

function modalPagamentoDesconto(id) {
    $.blockUI({
        css: {
            border: 'none',
            padding: '5px',
            width: '600px',
            backgroundColor: '#FFFFFF',
            '-webkit-border-radius': '5px',
            '-moz-border-radius': '5px',
        },
    message: $('#modalblockdesconto-'+id)
    });
}

function fechar() {
    $.unblockUI();
}

$(document).ready(function(){
    
    $('.btn_fechar').click(function() {
        fechar();
    });

    if(!isMobile()){
        $('.quero-comprar, .comprar-curso').click(function() {
            modalPagamento($(this).attr('rel'));
            return false;
        });

        $('.comprar-curso-desconto').click(function() {
            modalPagamentoDesconto($(this).attr('rel'));
            return false;
        });
    } 

});


<?php echo $this->Html->scriptEnd() ?>

<section class="destaque modulos">
    <div class="container">
        <div class="row">
            <header>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h1 class="titulo">Módulos</h1>
                </div>
            </header>
        </div>
        <div class="row">
            <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">
                <h2><?php echo $curso['Movie']['title']; ?></h2>

                <?php //IMAGEM DESTAQUE DO MODULO ?>
                <?php if($curso['Movie']['image'] != ""): ?>
                    <img src="<?php echo $this->Html->Url('/files/movie/image/'.$curso['Movie']['id'].'/vga_'.$curso['Movie']['image'], true); ?>" alt=""  class="img-responsive preview" style="margin: 10px 0px;" />
                <?php endIf; ?>

                <p class="subtitulo"><?php echo $curso['Movie']['description']; ?></p>

                <?php if(count($movies) > 0): ?>

                    <?php $i = 1; ?>
                    <?php
                        $aovivo_hora_atual = date('H');
                        $dia_atual = date('d/m/Y');
                    ?>
                    <?php foreach ($movies as $k => $movie) : ?>

                        <h4>Módulo <?php echo $i; ?> - <?php echo $this->String->dataFormatada("d/m/Y", $movie['Movie']['date']); ?></h4>
                        <h3><?php echo $movie['Movie']['title']; ?></h3>
                        <?php if($movie['Movie']['image'] != ""): ?>
                            <img src="<?php echo $this->Html->Url('/files/movie/image/'.$movie['Movie']['id'].'/vga_'.$movie['Movie']['image'], true); ?>" alt="<?php echo $movie['Movie']['title']; ?>" class="img-responsive preview thumb-modulo" />
                        <?php endIf; ?>
                        <p>
                            <?php
                                // if(strlen($movie['Movie']['description']) > 255){
                                //  echo substr($movie['Movie']['description'],0,255).'...';
                                // }else{
                                //  echo substr($movie['Movie']['description'],0,255);
                                // }
                                echo $movie['Movie']['description'];
                            ?>
                        </p>
                        <div class="buttons">
                            <?php 
                                $aovivo_hora_inicio = $this->String->DataFormatada('H',$movie['Movie']['hour_start']);
                                $aovivo_hora_termino = $this->String->DataFormatada('H',$movie['Movie']['hour_end']);
                            ?>
                            <?php 
                                if(
                                    !in_array($movie['Movie']['id'], $comprados) 
                                    && $curso_integral == false 
                                ){

                                    echo $this->Html->link('Ver Detalhes/Assistir',
                                                ['controller' => 'lives', 'action' => 'view', $movie['Movie']['slug']],
                                                ['escape' => false, 'class' => 'btn btn-default detalhes']
                                            );

                                    $exibir_btn = false;
                                    if($movie['Movie']['date'] > $dia_atual){
                                        $exibir_btn = true;
                                    }elseif($dia_atual == $movie['Movie']['date'] && $aovivo_hora_atual < $aovivo_hora_inicio){
                                         $exibir_btn = true;
                                    }
									
									$exibir_btn = true;
									//var_dump($movie['Movie']['date'] > $dia_atual);

                                    if($exibir_btn){
                                        if($movie['Movie']['price'] > 0){
                                            echo $this->Html->link("Quero comprar somente esse módulo <span class=\"valor-curso\">{$this->Number->currency($movie['Movie']['price'])}</span>", array(
                                                            'controller' => 'movies', 'action' => 'mobile', 'compra', $movie['Movie']['id']), array('rel' => $movie['Movie']['id'],
                                                            'class' => 'btn btn-default quero-comprar', 'escape' => false)
                                                );
                                        }else{
                                            echo $this->Html->link("Quero me inscrever apenas nesse módulo", array(
                                                            'controller' => 'movies', 'action' => 'mobile', 'compra', $movie['Movie']['id']), array('rel' => $movie['Movie']['id'],
                                                            'class' => 'btn btn-default quero-comprar', 'escape' => false)
                                                );
                                        }

                                        if(count($movie['MoviePrice'])){
                                            foreach ($movie['MoviePrice'] as $key => $movie_price) {
                                                echo $this->Html->link("Quero comprar somente esse módulo como: {$movie_price['title']}<span class=\"valor-curso\">{$this->Number->currency($movie_price['price'])}</span>", array(
                                                            'controller' => 'movies', 'action' => 'mobile', 'compra', $movie['Movie']['id'], 'false', 'false', $movie_price['id']), array('rel' => $movie_price['id'],
                                                            'class' => 'btn btn-default quero-comprar', 'escape' => false, 'style' => 'margin-left: 171px; margin-top: 10px;')
                                                );

                                                ?>
                                                    <div id="modalblock-<?php echo $movie_price['id']; ?>" class="modalblock" style="display: none">                
                                                        <a href="javascript:void(0);" class="btnFechar">
                                                            <?php echo $this->Html->image('imagens/btn_fechar.png', ['alt' => 'Fechar', 'id' => 'btnFechar']); ?>
                                                        </a>
                                                        <?php echo $this->element('modal_compra', array('movie' => $movie, 'movie_price' => $movie_price['id'])); ?>
                                                    </div>
                                                <?php
                                            }
                                        }
                                    }
                                }else{
                                    echo $this->Html->link('Ver Detalhes/Assistir',
                                                ['controller' => 'lives', 'action' => 'view', $movie['Movie']['slug']],
                                                ['escape' => false, 'class' => 'btn btn-default detalhes vermelho']
                                            );
                                }
                            ?>
                        </div>

                        <hr />

                        <div id="modalblock-<?php echo $movie['Movie']['id']; ?>" class="modalblock" style="display: none">                
                            <a href="javascript:void(0);" class="btnFechar">
                                <?php echo $this->Html->image('imagens/btn_fechar.png', ['alt' => 'Fechar', 'id' => 'btnFechar']); ?>
                            </a>
                            <?php echo $this->element('modal_compra', array('movie' => $movie)); ?>
                        </div>

                        <?php $i++; ?>
                    <?php endforeach; ?>
                
                <?php else: ?>

                    <?php if($ctrl == "courses"): ?>
                        <p>Nenhum curso encontrado.</p>
                    <?php else: ?>
                        <!--<p>Nenhum módulo encontrado.</p>-->
                    <?php endif; ?>
                
                <?php endif; ?>

            </div>

            <?php if($curso_integral == false): ?>
                <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                    <div class="adquirir-cursos">
                        <div class="row">
                            <?php if($curso['Movie']['price'] > 0): ?>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="observacao text-right">
                                    Adquirir o curso completo.
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center">
                                <div class="valor-total-curso">
                                    <?php $price = explode('.', $curso['Movie']['price']); ?>
                                    <span class="superior">R$</span><?php echo $price[0]; ?><span>,<?php echo $price[1]; ?></span>
                                </div>
                                <?php echo $this->Html->link('Comprar', array('controller' => 'movies', 'action' => 'mobile', 'compra', $curso['Movie']['id']), array('rel' => $curso['Movie']['id'],
                                            'class' => 'btn btn-default comprar-curso')); ?>
                                <div id="modalblock-<?php echo $curso['Movie']['id']; ?>" class="modalblock" style="display: none">                
                                    <a href="javascript:void(0);" class="btnFechar">
                                        <?php echo $this->Html->image('imagens/btn_fechar.png', ['alt' => 'Fechar', 'id' => 'btnFechar']); ?>
                                    </a>
                                    <?php echo $this->element('modal_compra', array('movie' => $curso)); ?>
                                </div>
                                <div id="modalblockdesconto-<?php echo $curso['Movie']['id']; ?>" class="modalblock" style="display: none">                
                                    <a href="javascript:void(0);" class="btnFechar">
                                        <?php echo $this->Html->image('imagens/btn_fechar.png', ['alt' => 'Fechar', 'id' => 'btnFechar']); ?>
                                    </a>
                                    <?php echo $this->element('modal_compra', array('movie' => $curso, 'var_desconto' => true)); ?>
                                </div>
                            </div>
                            <?php else: ?>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="observacao text-right">
                                        Inscreva-se no curso.
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center">
                                    <?php echo $this->Html->link('Inscreva-se', array('controller' => 'movies', 'action' => 'mobile', 'compra', $curso['Movie']['id']), array('rel' => $curso['Movie']['id'],
                                                'class' => 'btn btn-default comprar-curso')); ?>
                                    <div id="modalblock-<?php echo $curso['Movie']['id']; ?>" class="modalblock" style="display: none">                
                                        <a href="javascript:void(0);" class="btnFechar">
                                            <?php echo $this->Html->image('imagens/btn_fechar.png', ['alt' => 'Fechar', 'id' => 'btnFechar']); ?>
                                        </a>
                                        <?php echo $this->element('modal_compra', array('movie' => $curso)); ?>
                                    </div>
                                    <div id="modalblockdesconto-<?php echo $curso['Movie']['id']; ?>" class="modalblock" style="display: none">                
                                        <a href="javascript:void(0);" class="btnFechar">
                                            <?php echo $this->Html->image('imagens/btn_fechar.png', ['alt' => 'Fechar', 'id' => 'btnFechar']); ?>
                                        </a>
                                        <?php echo $this->element('modal_compra', array('movie' => $curso, 'var_desconto' => true)); ?>
                                    </div>
                                </div>
                            <?php endIf; ?>
                        </div>
                    </div>
                </div>
                <?php if($curso['Movie']['desconto'] > 0 && $curso['Movie']['desconto'] < $curso['Movie']['price']): ?>
                    <div class="col-lg-2 col-md-4 col-sm-12 col-xs-12">
                        <div class="adquirir-cursos">
                            <div class="detalhe-assinante-desconto">
                                Usuários com desconto pagam apenas<br /><span><?php echo $this->Number->currency($curso['Movie']['price']-$curso['Movie']['desconto']); ?></span>
                                <?php echo $this->Html->link('Comprar', array('controller' => 'movies', 'action' => 'mobile', 'compra', $curso['Movie']['id']), array('rel' => $curso['Movie']['id'],
                                            'class' => 'btn btn-default comprar-curso comprar-curso-assinante')); ?>
                            </div>
                        </div>
                    </div>                
                <?php endif; ?> 
                <?php if($curso['Movie']['off'] > 0): ?>
                    <div class="col-lg-2 col-md-4 col-sm-12 col-xs-12">
                        <div class="adquirir-cursos">
                            <div class="detalhe-assinante-desconto">
                                <span><?php echo $this->Number->currency($curso['Movie']['price']-($curso['Movie']['price']*($curso['Movie']['off']/100))); ?>*</span> para quem faz jus à inscrição com desconto especial
                                <?php echo $this->Html->link('Comprar', array('controller' => 'movies', 'action' => 'mobile', 'compra', $curso['Movie']['id'], 'true'), array('rel' => $curso['Movie']['id'],
                                            'class' => 'btn btn-default comprar-curso-desconto comprar-curso-assinante')); ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?> 
				<div class="clear"></div>

                <?php if(count($curso['MoviePrice']) > 0): ?>
				
                    <?php foreach($curso['MoviePrice'] as $pricee): ?>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding" style="margin-top: 20px;">
							<div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
								<div class="adquirir-cursos" style="margin-top: 0px;">
									<div class="row">
										<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
											<div class="observacao text-right">
												Adquirir o curso completo como: <br /><?php echo $pricee['title']; ?>
											</div>
										</div>
										<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center">
											<div class="valor-total-curso">
												<?php $price = explode('.', $pricee['price']); ?>
												<span class="superior">R$</span><?php echo $price[0]; ?><span>,<?php echo $price[1]; ?></span>
											</div>
											<?php echo $this->Html->link('Comprar', array('controller' => 'movies', 'action' => 'mobile', 'compra', $curso['Movie']['id'], 'false', 'false', $pricee['id']), array('rel' => $pricee['id'],
														'class' => 'btn btn-default comprar-curso')); ?>
											<div id="modalblock-<?php echo $pricee['id']; ?>" class="modalblock" style="display: none">                
												<a href="javascript:void(0);" class="btnFechar">
													<?php echo $this->Html->image('imagens/btn_fechar.png', ['alt' => 'Fechar', 'id' => 'btnFechar']); ?>
												</a>
												<?php echo $this->element('modal_compra', array('movie' => $curso, 'movie_price' => $pricee['id'])); ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
                    <?php endforeach; ?>
                <?php endIf; ?>
				<?php if($curso['Movie']['desconto'] > 0 && $curso['Movie']['desconto'] < $curso['Movie']['price']): ?>
				<div class="col-lg-12 col-md-12 text-left legend-off">
						* O benefício do desconto especial depende de comprovação por email ou no local antes do início da palestra.
					</div>
                <?php endif; ?> 
				
				<?php if($curso['Movie']['off'] > 0): ?>
				<div class="col-lg-12 col-md-12 text-left legend-off">
						* O benefício do desconto especial depende de comprovação por email ou no local antes do início da palestra.
					</div>
                <?php endif; ?> 
				
                <?php if($curso['Movie']['price'] > 0): ?>
                <div class="col-lg-12 col-md-8 hidden-sm hidden-xs">
                    <img src="<?php echo $this->Html->Url('/imagens/pagseguro-ico.png') ?>" alt="" class="pag-seguro" />
                </div>   
                <?php endif; ?> 
            <?php endif; ?>
        </div>
    </div>
</section>