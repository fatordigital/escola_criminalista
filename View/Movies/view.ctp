<?php /** @var $this View */ ?>
<?php $this->Html->script("/js/jquery.price_format.1.4", ['inline' => false]); ?>
<?php echo $this->Html->scriptStart(['inline' => false]) ?>

function modalPagamento() {
    $.blockUI({
        css: {
            border: 'none',
            padding: '5px',
            width: '600px',
            top: '25%',
            backgroundColor: '#FFFFFF',
            '-webkit-border-radius': '5px',
            '-moz-border-radius': '5px'
        },
    message: $('#modalblock'),

    });
}

function modalAssinatura(){
    $.blockUI({
        css: {
            border: 'none',
            padding: '5px',
            width: '600px',
            top: '25%',
            backgroundColor: '#FFFFFF',
            '-webkit-border-radius': '5px',
            '-moz-border-radius': '5px'
        },
    message: $('#modalblockassinante'),

    });
}

function fechar() {
    $.unblockUI();
}

 $('.btnFechar').click(function() {
    fechar();
});

$('document').ready(function(){
    $('.sem-cadastro').click(function(){
        $('figure .video-container, figure span').css({'display':'block'});
        $('.cadastre-se').css({'display':'none'});
    });

    $('section.filme-detalhe .o-filme a.apagar-luz').click(function(){
        $('.overlay').fadeIn(500);
        $('section.filme-detalhe .o-filme figure').css({'z-index':'900'});
    });

    $('.overlay').click(function(){
        $(this).fadeOut(500);
        $('section.filme-detalhe .o-filme figure').css({'z-index':'100'});
    });

    $('.ler-mais').click(function(){
        if($(this).attr('rel') == 'hidden'){
            $('section.filme-detalhe .descricao-filme').animate({height:'100%'}, 500);
            $('.fadeout').fadeOut(500);
            $(this).attr('rel', 'show');
        }else{
            $('section.filme-detalhe .descricao-filme').animate({height:'100'});
            $('.fadeout').fadeIn();
            $(this).attr('rel', 'hidden');
        }
    });

    $('#OrderPrice').priceFormat({
        prefix: 'R$ ',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    $('iframe').each(function(){
      var url = $(this).attr("src");
      $(this).attr("src",url+"?wmode=transparent");
    });


    $('section.filme-detalhe .o-filme a.favoritos').click(function(){
        var link = $(this);
        link.html('Processando...');
        $.post(BASENAME + "movie_favorites/salvar/", {movie_id: link.data('movie-id')},
            function(response) {
                if(typeof(response.message) != "undefined"){
                    if(response.status == true){
                        if(response.message == "insert"){
                            link.addClass('ativo');
                            link.html('Adicionado aos favoritos.');
                        }else{
                            link.removeClass('ativo');
                            link.html('Adicionar aos favoritos.');
                        }
                    }else{
                        link.removeClass('ativo');
                        link.html('Ocorreu um erro. Tente novamente');
                    }
                }
            }, 
            "json"
        );
    });

});

<?php echo $this->Html->scriptEnd() ?>

<?php echo $this->Session->flash(); ?>
<section class="filme-detalhe">
    <div class="container">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="o-filme">
                <figure>
                    <div class="cadastre-se text-center" style="display: none;">
                        <h2>Cadastre-se</h2>
                        <p>A Casa da Tolerância é um ambiente diferenciado, disponível a qualquer pessoa e com muito conteúdo de qualidade distribuído gratuitamente. Apenas pedimos que cadastre-se.</p>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <?php 
                                    /* Fazer Cadastro */
                                    echo $this->Html->link('Quero fazer meu cadastro e assistir aos vídeos!', 
                                    ['plugin' => 'users', 'controller' => 'users', 'action' => 'add'],
                                    ['class' => 'btn btn-default fazer-cadastro botao', 'title' => 'Quero fazer meu cadastro e assistir aos vídeos!']); 
                                ?><br />

                                <?php echo $this->Html->link('Já sou cadastrado e quero fazer meu login',
                                    /* Já tenho cadastro e quero fazer meu login */
                                    array(
                                        'plugin'     => 'users',
                                        'controller' => 'users',
                                        'action'     => 'login'
                                )); ?>
                            </div>
                            <div class="col-sm-12 col-xs-12">
                                <a class="btn btn-default sem-cadastro botao" title="Continuar sem cadastro">Quero continuar sem cadastro</a>  
                            </div>
                        </div>
                    </div>
                    <div class="video-container">
                        <?php if ($paidMovie): ?>
                            <?php echo $embed['html']; ?>
                        <?php else: ?>
                            <?php echo $this->Html->scriptStart(['inline' => false]) ?>
                                $(document).ready(function() {
                                    <?php if ($movie['Movie']['restriction'] == true && $movie['Movie']['type'] == "free" && !$this->Session->check('Auth.User')) : ?>
                                    if(!isMobile()){
                                        $.blockUI({
                                            css: {
                                                border: 'none',
                                                padding: '5px',
                                                width: '960px',
                                                height: '400px',
                                                top: '25%',
                                                left: '30%',
                                                backgroundColor: '#FFFFFF',
                                                '-webkit-border-radius': '5px',
                                                '-moz-border-radius': '5px',
                                            },
                                            overlayCSS: {
                                            	backgroundColor: '#5E1A17',
                                            	opacity: 0.85
                                            },
                                            message: $('#modalfree')

                                        });
                                        //setTimeout($.unblockUI, 10000);
                                    }else {
                                        $('figure .video-container, figure span').css({'display':'none'});
                                        $('.cadastre-se').css({'display':'block'});
                                    }

                                    $('#btnSemCadastro').click(function() {
                                        fechar();
                                    });
                                    <?php else : ?>
                                    if(!isMobile()){
                                        $('#btnComprar').click(function() {
                                            modalPagamento();
                                            return false;
                                        });

                                        $('#btnAssinar').click(function() {
                                            modalAssinatura();
                                            return false;
                                        });
                                    }
                                    <?php endif; ?>
                                });
                            <?php echo $this->Html->scriptEnd() ?>

                            <?php if ($movie['Movie']['type'] == "donation"): ?>

                                <?php //BEGIN: SE FOR CLIQUE SOLIDARIO, EXIBO 30 SEGUNDOS DO VIDEO PARA PREVIEW, E DEPOIS REMOVO O EMBED ?>
                                <script src="http://a.vimeocdn.com/js/froogaloop2.min.js"></script>
                                <script type="text/javascript">
                                $(document).ready(function(){
                                    var iframe = $('.video-container iframe')[0];
                                    var player = $f(iframe);

                                    player.addEvent('ready', function() {
                                       player.addEvent('playProgress', onPlayProgress);
                                    });

                                    function onPlayProgress(data, id) {
                                        if(parseInt(data.seconds) >= 30){
                                            player.api('pause');
                                            $('.video-container iframe').fadeOut();
                                            $(".video-banner").fadeIn();
                                            $("#modalblock #centromodal").html("<b>Importante</b><br />Para continuar assistindo esse filme, você precisa realizar um clique solidário, doando um valor a partir de "+$("#OrderValorMinimo").attr("rel")+". Esse processo será realizado com toda a segurança através do PagSeguro");
                                            //$("#modalblock").fadeIn();
                                            modalPagamento();
                                            $('.video-container iframe').remove();
                                        }
                                    }
                                });
                                </script>
                                <?php echo $embed['html']; ?>
                                <div class="video-banner" style="display: none;">
                                    <?php
                                        if(file_get_contents($movie['Movie']['thumb']['xbig']['url']) == false){
                                            echo $this->Html->image(str_replace("_1280.jpg", "_640.jpg", $movie['Movie']['thumb']['xbig']['url']), ['class' => 'fullsize-images img-responsive']);
                                        }else{
                                            echo $this->Html->image($movie['Movie']['thumb']['xbig']['url'], ['class' => 'fullsize-images img-responsive']);
                                        }
                                    ?>
                                </div>
                                <?php //END: SE FOR CLIQUE SOLIDARIO, EXIBO 30 SEGUNDOS DO VIDEO PARA PREVIEW, E DEPOIS REMOVO O EMBED ?>

                            <?php elseif ($movie['Movie']['type'] == "free") : ?>
                                <?php echo $embed['html']; ?>
                            <?php else: ?>
                                <?php 
                                    //echo $this->Html->image($movie['Movie']['thumb']['xbig']['url'], ['class' => 'fullsize-images img-responsive']);
                                    if(file_get_contents($movie['Movie']['thumb']['xbig']['url']) == false){
                                        echo $this->Html->image(str_replace("_1280.jpg", "_640.jpg", $movie['Movie']['thumb']['xbig']['url']), ['class' => 'fullsize-images img-responsive']);
                                    }else{
                                        echo $this->Html->image($movie['Movie']['thumb']['xbig']['url'], ['class' => 'fullsize-images img-responsive']);
                                    }
                                ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                    <span>
                        <?php echo $this->Html->image("imagens/{$movie['Movie']['type']}-top-right.png", ['alt' => "{$types[$movie['Movie']['type']]}"]); ?>
                    </span>
                </figure>
                <?php if ($this->Session->check('Auth.User')): ?>
                    <a href="javascript:void(0);" class="favoritos <?php echo ($favorite) ? 'ativo' : '';  ?>" data-movie-id="<?php echo $movie['Movie']['id']; ?>" title="Adicionar aos favoritos"><?php echo ($favorite) ? 'Adicionado' : 'Adicionar';  ?> aos favoritos</a>
                <?php endIf; ?>
                <a class="apagar-luz" title="Apagar luz">Apagar luz</a>
                <div class="pull-right"><!-- AddThis Button BEGIN -->
                    <div class="addthis">
                        <div class="addthis_toolbox addthis_default_style addthis_16x16_style">
                        <a class="addthis_button_facebook"></a>
                        <a class="addthis_button_twitter"></a>
                        <a class="addthis_button_google_plusone_share"></a>
                        </div>
                        <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
                        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-51310f426b45af61"></script>
                        <!-- AddThis Button END -->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 clear">
            <header>
				<span class="category-movie"><?php echo $movie['Category']['title'] ?></span>
                <h1><?php echo $movie['Movie']['title'] ?></h1>
                <span class="data-publicacao">Publicado em <?php echo $this->Time->format('d/m/Y', $movie['Movie']['created']); ?></span>
                <span class="visualizacoes"><?php echo $movie['Movie']['view_count'] ?> visualizações</span>
            </header>
            <div class="descricao-filme">
                <?php echo $movie['Movie']['description'] ?>
                <div class="fadeout"></div>
            </div>
            <?php if(strlen($movie['Movie']['description']) > 255): ?>
                <?php echo $this->Html->link(
                    'Continuar lendo ' . $this->Html->image('imagens/continuar-lendo.gif', ['alt' => 'Continuar lendo']),
                    'javascript:;',
                    ['class' => 'ler-mais', 'escape' => false, 'rel' => 'hidden']
                ); ?>
            <?php endIf; ?>
            <hr />
            <div class="dados-filme">
                <p><strong>Categoria:</strong> <a href="#" title="<?php echo $types[$movie['Movie']['type']]; ?>"><?php echo $types[$movie['Movie']['type']]; ?></a></p>

                <p>
                    <strong>Tags:</strong>
                    <?php
                    $tags = [];
                    foreach ($movie['Tag'] as $tag) {
                        $tags[] = $this->Html->link($tag['name'], ['controller' => 'movies', 'action' => 'tag', $tag['keyname']]);
                    } ?>
                    <?php echo implode(', ', $tags) ?>
                </p>
            </div>
            <?php
            if(!$paidMovie && $movie['Movie']['type'] != 'free'): ?>
                <div class="comprar">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="preco">
                                <strong>Valor</strong><br>
                                <span><?php echo $this->Number->currency($movie['Movie']['price']); ?></span>
                            </div>
                        </div>

                        <?php if($movie['Movie']['via_venda_avulso'] == true): ?>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <?php echo $this->Html->link('Comprar', array('controller' => 'movies', 'action' => 'mobile', 'compra', $movie['Movie']['id']), array('id' => 'btnComprar',
                                        'class' => 'btn btn-default')); ?>
                            </div>
                        <?php endIf; ?>

                        <?php if($movie['Movie']['via_assinatura'] == true): ?>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <?php echo $this->Html->link('Faça Parte', array('controller' => 'movies', 'action' => 'mobile', 'assinatura', $movie['Movie']['id']), array('id' => 'btnAssinar',
                                        'class' => 'btn btn-default')); ?>
                            </div>
                        <?php endIf; ?>

                    </div>
                </div>
            <?php endIf; ?>
        </div>
    </div>

    <div id="modalfree" class="modal_cadastre modalblock" style="display: none">
        <div id="topomodal">
            <h1 class="text-center outro-titulo">Cadastre-se!</h1>
        </div>
        <div id="centromodal">
            <p class="text-center">
                A Casa da Tolerância é um ambiente diferenciado, disponível a qualquer pessoa e com muito conteúdo de qualidade distribuído gratuitamente. Apenas pedimos que cadastre-se.
            </p>
        </div>
        <div class="rodapemodal">
            <div id="fazercadastro">
                <?php echo $this->Html->link('Quero fazer meu cadastro e assistir aos vídeos!',
                        ['plugin' => 'users', 'controller' => 'users', 'action' => 'add'],
                        ['class' => 'btn btn-default', 'title' => 'Quero fazer meu cadastro e assistir aos vídeos!']); ?>
            </div>
            <div id="semcadastro">
                <?php echo $this->Form->button('Quero continuar sem cadastro',
                    array(
                        'id' => 'btnSemCadastro',
                        'class' => 'btn btn-default',
                        'title' => 'Quero continuar sem cadastro',
                        'type' => 'button'
                    )
                ); ?>

            </div>
            <div id="fazerlogin">
            <?php echo $this->Html->link('Já sou cadastrado e quero fazer meu login',
                array(
                    'plugin'     => 'users',
                    'controller' => 'users',
                    'action'     => 'login'
                )); ?>
            </div>    
        </div>
        
    </div>

    <div id="modalblock" class="modalblock" style="display: none">
    	<a href="javascript:void(0);" class="btnFechar">
        	<?php echo $this->Html->image('imagens/btn_fechar.png', ['alt' => 'Fechar', 'id' => 'btnFechar']); ?>
        </a>
        <?php echo $this->element('modal_compra', array('movie' => $movie)); ?>
    </div>

    <div id="modalblockassinante" class="modalblock" style="display: none">
        <a href="javascript:void(0);" class="btnFechar">
            <?php echo $this->Html->image('imagens/btn_fechar.png', ['alt' => 'Fechar', 'id' => 'btnFechar']); ?>
        </a>
        <?php echo $this->element('modal_assinatura', array('movie' => $movie)); ?>
    </div>
</section>

<section class="destaque comentarios-relacionados">

    <div class="container">

        <?php if($movie['Movie']['comments'] == true): ?>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <h2>Comentários</h2>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                        <div class="linha-vertical"></div>
                    </div>
                </div>

                <div id="fb-root"></div>
                <script>(function(d, s, id) {
                  var js, fjs = d.getElementsByTagName(s)[0];
                  if (d.getElementById(id)) return;
                  js = d.createElement(s); js.id = id;
                  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=728391253941963&version=v2.0";
                  fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
                <div class="fb-comments" data-href="<?php echo $this->Html->Url('/movies/view/'. $movie['Movie']['slug'], true); ?>" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
            </div>

        <?php endIf; ?>
        
        <?php if(isset($relatedMovies) && count($relatedMovies) > 0): ?>
            <div class="col-lg-4 col-md-8 col-sm-12 col-xs-12">
                <h2>Vídeos Relacionados</h2>

                <?php foreach ($relatedMovies as $relatedMovie) : ?>

                    <article class="videos-relacionados col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <figure>
                            <?php echo $this->Html->image($relatedMovie['Movie']['thumb']['big']['url'], ['alt' => $relatedMovie['Movie']['title'], 'class' => 'img-responsive fullsize-images']); ?>
                            <span>
                                <?php echo $this->Html->image("imagens/{$relatedMovie['Movie']['type']}-bottom-right.png", ['alt' => $relatedMovie['Movie']['title'], 'class' => 'img-responsive categoria-video']); ?>
                            </span>
                        </figure>
                        <header>
                            <?php echo $this->Html->link($relatedMovie['Movie']['title'],
                                ['controller' => 'movies', 'action' => 'view', $relatedMovie['Movie']['slug']],
                                ['escape' => false]
                            ); ?>
                        </header>
                        <p><?php echo $this->Text->truncate($relatedMovie['Movie']['description']); ?></p>
                    </article>

                <?php endforeach; ?>

            </div>
        <?php endIf; ?>
    </div>
</section>

<style type="text/css">
.modal_cadastre h1{
    font-family: Roboto, sans-serif;
    color:#671C19;
    font-size: 50px;
    font-weight: bold;
}

.modal_cadastre .message{
    margin:60px 0 0 0;
    font-color:#763B39;
    font-size: 27px;
}
.modal_cadastre #fazercadastro .btn{
    background-color:#9B2926;
    font-size:14px;
    padding:20px 20px;
    border-radius: 10px;
    margin:60px 0 0 140px;
    color:#fff;
}
.modal_cadastre #semcadastro .btn{
    background-color:#D4D4D4;
    font-size:14px;
    padding:20px 20px;
    border-radius: 10px;
    margin:60px 0 0 -100px;
    color:#9B2926;
}

.modal_cadastre #fazerlogin a{
    color:#9A2826;
    margin:0 0 0 155px;
}

</style>

<div class="overlay"></div>