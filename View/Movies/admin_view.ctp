<?php /** @var $this View */ ?>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-reorder"></i> Vendo dados do <?php echo __d('users', ucfirst(substr($movie['Movie']['controller'], 0, -1))); ?>: <?php echo $movie['Movie']['title']; ?>
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" class="config">
            </a>
            <a href="javascript:;" class="reload">
            </a>
            <a href="javascript:;" class="remove">
            </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form class="form-horizontal" role="form">
            <div class="form-body">
                <h3 class="form-section">Preview</h3>

                <div class="row">
                    <div class="col-md-12 text-center">
                        <?php echo $embed['html']; ?>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <h3 class="form-section">Informações</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Título:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $movie['Movie']['title']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Duração:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $this->Time->format('i:s', $movie['Movie']['length']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Link:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo $this->Text->autoLink($movie['Movie']['link']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Tipo:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo h($types[$movie['Movie']['type']]); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Preço:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo CakeNumber::currency($movie['Movie']['price']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Categoria:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo h($movie['Category']['title']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <h3 class="form-section">Descrição</h3>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-1">
                                <p class="form-control-static">
                                    <?php echo ($movie['Movie']['description']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-3 col-md-9">
                            <?php echo $this->Html->link(
                                sprintf('%s <i class="fa fa-pencil"></i>', __d('users', "Edit $movieType")),
                                array('controller' => $movieRoute, 'action' => 'edit', $movie['Movie']['id']),
                                array('escape' => false, 'class'  => 'btn green')
                            ); ?>
                            <?php echo $this->Form->postLink(
                                __d('users', "Delete $movieType"),
                                array('action' => 'delete', $movie['Movie']['id']),
                                array('escape' => false, 'class'  => 'btn red'),
                                __d('users', 'Are you sure you want to delete # %s?', $movie['Movie']['id']));
                            ?>
                            <?php echo $this->Html->link(
                                __d('users', 'Go back'),
                                array('action' => 'index'),
                                array('escape' => false, 'class' => 'btn default')
                            ); ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>
