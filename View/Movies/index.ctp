<?php echo $this->Html->script('/js/customselect.js'); ?>
<?php $this->Html->scriptStart(); ?>
$(document).ready(function(){
$('section.formulario-contato form select').customSelect({customClass:'customSelect'});
$('select#categorias, select#duracao, select.quantidade').customSelect({customClass:'customized'});
$('#limit').on('change', function() {
if ($(this).val() != "" && $(this).val() != "quantidade") {
window.location = $('#limit').attr('rel').replace(/\/limit:(.*)/g, '/') + '/' + $(this).val();
} else {
window.location = $('#limit').attr('rel').replace(/\/limit:(.*)/gi, '');
}
});
});
<?php echo $this->Html->scriptEnd(); ?>
<?php /** @var $this View */ ?>
<?php /*
    <section class="banner-curso filme hidden-sm hidden-xs">
        <div class="container">
            <div class="col-lg-8 col-md-8 text-right">
                <article>
                    <h2>Uma usina extraordinária de ideias montada para gerar conteúdo de alto valor.</h2>
                    <p>Mande seu vídeo para discutirmos na Casa. Participe!</p>
                </article>
            </div>
        </div>
    </section>
    */ ?>

<section class="topo-pagina">
        <div class="container">
            <div class="row-fluid">
                <header>
                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                        <?php if (isset($ctrl) && $ctrl == "miscellaneous"): ?>
                            <h1 class="especial">Vídeos Indicados</h1>
                        <?php else: ?>
                            <h1 class="especial">Últimos vídeos. </h1>
                            <?php /*<span class="hidden-sm hidden-xs">Nós começamos quando você relaxa: conteúdo novo sempre nas sextas!</span> */ ?>
                        <?php endIf; ?>
                    </div>
                    <div class="col-lg-5 col-md-5 pull-right text-right">
                        <?php
                            $itens = array(
                                'quantidade' => 'Quantidade',
                                'limit:9'    => '9',
                                'limit:12'   => '12',
                                'limit:18'   => '18',
                                'limit:30'   => '30'
                            );
                            echo $this->Form->input('limit', array(
                                    'div'     => false,
                                    'label'   => 'Vídeos por página: ',
                                    'id'      => 'limit',
                                    'class'   => 'quantidade',
                                    'name'    => 'quantidade',
                                    'rel'     => $this->Html->url(null, true),
                                    'value'   => (isset($this->params['named']['limit'])) ? 'limit:' . $this->params['named']['limit'] : 'limit:12',
                                    'options' => $itens
                                )
                            );
                        ?>
                    </div>

                </header>
            </div>
        </div>
    </section>

    <section class="ultimos-videos destaque">
        <div class="container">

            <?php if (count($movies) > 0): ?>
                <?php $i = 1; ?>
                <?php foreach ($movies as $movie) : ?>

                    <?php if ($i == 0 || $i % 3 == 0) : ?>
                        <div class="row-fluid">
                    <?php endif; ?>


                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-30">
                        <figure class="text-right">
							<a href="<?php echo $this->Html->Url(array('controller' => 'movies', 'action' => 'view', $movie['Movie']['slug'])); ?>"
                               title="<?php echo $movie['Movie']['title']; ?>">
                                <?php
                                    if (isset($movie['Movie']['thumb']['big']))
                                        echo $this->Html->image($movie['Movie']['thumb']['big']['url'], ['alt' => $movie['Movie']['title'], 'class' => 'img-responsive fullsize-images']);
                                    else {
                                        echo $this->Html->image('/img/video-privado.jpg');
                                    }
                                ?>
							</a>

                            <span>
                                <?php echo $this->Html->image("imagens/{$movie['Movie']['type']}-bottom-right.png", ['alt' => 'Tipo de vídeo', 'class' => 'img-responsive categoria-video']); ?>
                            </span>

                            <?php
                                if (isset($movie['Movie']['thumb']['big'])) {
                                    echo $this->Html->link(
                                        $this->Html->image('imagens/abrir-video.png', ['alt' => 'Abrir video', 'class' => 'img-responsive']),
                                        ['controller' => 'movies', 'action' => 'view', $movie['Movie']['slug']],
                                        ['escape' => false, 'class' => 'player']
                                    );
                                }
                            ?>
                        </figure>
    					<span class="category-movie"><?php echo $movie['Category']['title']; ?></span>
                        <header>
                        <h3>
                            <?php echo $this->Html->link($movie['Movie']['title'],
                                ['controller' => 'movies', 'action' => 'view', $movie['Movie']['slug']],
                                ['escape' => false]
                            );
                            ?>
                        </h3>
                    </header>
                    <p>
                        <?php echo $this->Text->truncate($movie['Movie']['description']); ?>
                    </p>
                </div>

                    <?php if ($i % 3 == 0) : ?>
                        <div class="clearfix"></div>
                        </div>
                    <?php endif; ?>
                    <?php $i++; ?>
                <?php endforeach; ?>
            <?php else: ?>
                <p>Nenhum registro encontrado.</p>
            <?php endif; ?>

            <?php if ($this->params['paging']['Movie']['pageCount'] > 1): ?>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center clear">
                        <ul class="pagination customizada">
                            <?php echo $this->Paginator->prev(
                                $this->Html->image('imagens/voltar-pagina.png', ['alt' => 'Voltar Página']),
                                ['escape' => false]
                            ); ?>
                            <?php echo $this->Paginator->numbers(['currentClass' => 'active']); ?>
                            <?php echo $this->Paginator->next(
                                $this->Html->image('imagens/avancar-pagina.png', ['alt' => 'Avançar Página']),
                                ['escape' => false]
                            ); ?>
                        </ul>
                    </div>
                </div>
            <?php endIf; ?>

            <?php echo $this->element('latest_movies'); ?>

        </div>
    </section>

<?php //echo $this->element('library'); ?>
