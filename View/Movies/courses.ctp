<?php echo $this->Html->scriptStart(['inline' => false]) ?>

function modalPagamento(id) {
    $.blockUI({
        css: {
            border: 'none',
            padding: '5px',
            width: '600px',
            backgroundColor: '#FFFFFF',
            '-webkit-border-radius': '5px',
            '-moz-border-radius': '5px',
        },
    message: $('#modalblock-'+id)
    });
}


function fechar() {
    $.unblockUI();
}

$(document).ready(function(){
    $('.btn_fechar').click(function() {
        fechar();
    });

    if(!isMobile()){
        //$('.comprar').click(function() {
        //    modalPagamento($(this).attr('rel'));
        //    return false;
        //});
    }    
});


<?php echo $this->Html->scriptEnd() ?>
<div class="cursos">
    <section class="destaque aulas-ao-vivo">
        <div class="container">
            <div class="row">
                <header>
                    <div class="col-lg-3 col-md-4 col-sm-5 col-xs-12">
                        <h1 class="titulo">Cursos</h1>
                    </div>
                </header>
            </div>
            <?php if(count($movies) > 0): ?>

                <?php $i = 1; ?>
                <?php foreach ($movies as $movie) : ?>

                    <?php if ($i == 0 || $i % 3 == 0) : ?>
                        <div class="row-fluid">
                    <?php endif; ?>

                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 box">
                        <article>
                            <?php //IMAGEM DESTAQUE DO MODULO ?>
                            <?php if($movie['Movie']['image'] != ""): ?>
                                <img src="<?php echo $this->Html->Url('/files/movie/image/'.$movie['Movie']['id'].'/vga_'.$movie['Movie']['image'], true); ?>" alt="<?php echo $movie['Movie']['title']; ?>" class="img-responsive preview" style="margin-bottom: 10px;" />
                            <?php endIf; ?>
                            <header>
                                <h2><?php echo $movie['Movie']['title']; ?></h2>
                            </header>
                            <p>
                                <?php
                                    if(strlen($movie['Movie']['description']) > 255){
                                     echo substr(strip_tags($movie['Movie']['description']),0,255).'...';
                                    }else{
                                     echo substr(strip_tags($movie['Movie']['description']),0,255);
                                    }
                                ?>
								
								<?php echo $this->Html->link('Leia mais',
                                        $this->Html->Url("/cursos/modulos/".$movie['Movie']['slug'], true),
                                        ['escape' => false, 'class' => 'btn btn-leia-mais-curso', 'style' => 'line-height: initial; padding-top: 0', 'escape' => false]
                                    ); ?>
                            </p>
                            <?php /* if($movie['Movie']['price'] > 0): ?>
                                <?php $price = explode('.', $movie['Movie']['price']); ?>
                                <p class="valor"><span class="alto">R$</span><?php echo $price[0] ?>,<span class="baixo"><?php echo $price[1] ?></span></p>
                            <?php endIf; */ ?>

                            <?php if($movie['Movie']['price'] > 0): ?>
                                <?php $price = explode('.', $movie['Movie']['price']); ?>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <p class="valor"><span class="alto">R$</span><?php echo $price[0] ?>,<span class="baixo"><?php echo $price[1] ?></span></p>      
                                    </div>
                                    <?php if($movie['Movie']['desconto'] > 0): ?>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="desconto text-right">
                                                Usuários com desconto pagam <span><?php echo $this->Number->currency($movie['Movie']['price']-$movie['Movie']['desconto']); ?></span>
                                            </div>
                                        </div>
                                    <?php endIf; ?>
                                    <?php if($movie['Movie']['off'] > 0): ?>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="desconto text-right">
                                                <span><?php echo $movie['Movie']['off']; ?>% off</span> para quem faz jus ao desconto especial</span>
                                            </div>
                                        </div>
                                    <?php endIf; ?>
                                </div>
                            <?php endIf; ?>

                            <?php 
                                //if(!in_array($movie['Movie']['id'], $comprados)){
                                    echo $this->Html->link('Saiba Mais', 
											$this->Html->Url("/cursos/modulos/".$movie['Movie']['slug'], true),
											array('rel' => $movie['Movie']['id'],
                                            'class' => 'btn btn-default comprar'));
                                //}
                            ?>
                            <?php 
								if(in_array($movie['Movie']['id'], $com_modulos)){
									echo $this->Html->link('Comprar <br />módulo',
                                        $this->Html->Url("/cursos/modulos/".$movie['Movie']['slug'], true),
                                        ['escape' => false, 'class' => 'btn btn-default saiba-mais', 'style' => 'line-height: initial; padding-top: 0', 'escape' => false]
                                    ); 
								}
							?>
                        </article>
                    </div>

                    <?php if ($i % 3 == 0) : ?>
                        <div class="clearfix"></div>
                        </div>
                    <?php endif; ?>

                    <div id="modalblock-<?php echo $movie['Movie']['id']; ?>" class="modalblock" style="display: none">                
                        <a href="javascript:void(0);" class="btnFechar">
                            <?php echo $this->Html->image('imagens/btn_fechar.png', ['alt' => 'Fechar', 'id' => 'btnFechar']); ?>
                        </a>
                        <?php echo $this->element('modal_compra', array('movie' => $movie)); ?>
                    </div>

                    <?php $i++; ?>
                <?php endforeach; ?>
            
            <?php else: ?>

                <?php if($ctrl == "courses"): ?>
                    <p>Nenhum curso encontrado.</p>
                <?php else: ?>
                    <p>Nenhum módulo encontrado.</p>
                <?php endif; ?>
            
            <?php endif; ?>

            <?php if($this->params['paging']['Movie']['pageCount'] > 1): ?>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center clear">
                        <ul class="pagination customizada">
                            <?php echo $this->Paginator->prev(
                                $this->Html->image('imagens/voltar-pagina.png', ['alt' => 'Voltar Página']),
                                ['escape' => false]
                            ); ?>
                            <?php echo $this->Paginator->numbers(['currentClass' => 'active']); ?>
                            <?php echo $this->Paginator->next(
                                $this->Html->image('imagens/avancar-pagina.png', ['alt' => 'Avançar Página']),
                                ['escape' => false]
                            ); ?>
                        </ul>
                    </div>
                </div>
            <?php endIf; ?>
        </div>
    </section>
</div>
