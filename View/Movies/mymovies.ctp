<?php echo $this->element('users_menu'); ?>

<?php echo $this->Session->flash(); ?>

<section class="topo-pagina">
    <div class="container">
        <div class="row-fluid">
            <header>
                <div class="col-lg-3">
                    <h1>Meus vídeos</h1>
                </div>
            </header>
        </div>
    </div>
</section>

<section class="ultimos-videos destaque">
    <div class="container">
        <div class="row-fluid">

            <?php if(isset($orders) && count($orders) > 0): ?>

                <?php foreach ($orders as $order) : ?>

                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <figure class="text-right">
                            <?php echo $this->Html->image($order['Movie']['thumb']['big']['url'],
                                ['class' => 'img-responsive fullsize-images']); ?>
                           
                            <?php echo $this->Html->link(
                                $this->Html->image('imagens/abrir-video.png', ['alt' => 'Abrir vídeo', 'class' => 'img-responsive']),
                                ['plugin' => false, 'controller' => 'movies', 'action' => 'view', $order['Movie']['slug']],
                                ['escape' => false, 'title' => 'Clique aqui para abrir o vídeo']
                            ); ?>
                        </figure>
                        <header>
                            <h3>
                                <?php echo $this->Html->link(
                                    $order['Movie']['title'],
                                    ['plugin' => false, 'controller' => 'movies', 'action' => 'view', $order['Movie']['slug']],
                                    ['title' => 'Clique aqui para assistir o vídeo']
                                ); ?>
                            </h3>
                        </header>
                        <p><?php echo $this->Text->truncate($order['Movie']['description']); ?></p>
                    </div>

                <?php endforeach; ?>

            <?php else: ?>

                <p>Nenhum Vídeo Encontrado</p>
            
            <?php endif; ?>

        </div>
</section>