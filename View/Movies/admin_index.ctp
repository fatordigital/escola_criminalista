<?php

/**
 * @var $this View
 */
$this->Html->addCrumb(__d('users', $movieType . 's'));
$this->Html->addCrumb(__d('users', 'Listing'));
?>

<div class="row">
    <div class="col-md-12">
        <h2 class="page-title"><?php echo __d('users', "Admin Index $movieType"); ?></h2>
        <?php echo $this->Html->getCrumbList(
            array(
                'class'     => 'breadcrumb',
                'separator' => '<i class="icon-angle-right"></i>',
                'escape'    => false
            ),
            'Home'
        );
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?php echo $this->Session->flash(); ?>
    </div>
    <div class="clearfix"></div>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>&nbsp;<?php echo __d('users', "Admin Index the $movieType information"); ?>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                        <a href="javascript:;" class="reload"></a>
                        <a href="javascript:;" class="remove"></a>
                    </div>
                </div>
            </div>

            <div class="portlet-body">

                <div class="table-toolbar">
                    <?php 
                    if($movieRoute != "modules"){
                        echo $this->Html->link(
                            sprintf('%s <i class="fa fa-plus"></i>', __d('users', 'Add New')),
                            array('controller' => $movieRoute, 'action' => 'add'),
                            array(
                                'escape' => false,
                                'class'  => 'btn green'
                            )
                        ); 
                    }elseif(isset($this->params['named']['curso'])){
                        echo $this->Html->link(
                            sprintf('%s <i class="fa fa-plus"></i>', __d('users', 'Add New')),
                            array('controller' => $movieRoute, 'action' => 'add', 'curso' => $this->params['named']['curso']),
                            array(
                                'escape' => false,
                                'class'  => 'btn green'
                            )
                        ); 
                    }
                    ?>
                </div>

                <?php if (count($movies) > 0) : ?>

                <table class="table table-hover table-bordered table-striped table-condensed flip-content"
                       data-model="Movie">
                    <thead>
                    <tr>
                        <?php if($movieRoute != "courses" && $movieRoute != "modules"){ ?>
                            <th><?php echo $this->Paginator->sort('category_id', 'Categoria'); ?></th>
                        <?php } ?>
                        <th><?php echo $this->Paginator->sort('title', 'Título'); ?></th>
                        <th><?php echo $this->Paginator->sort('description', 'Descrição'); ?></th>
                        <?php if($movieRoute != "lives" && $movieRoute != "courses"){ ?>
                            <th><?php echo $this->Paginator->sort('link', 'Link'); ?></th>
                        <?php } ?>
                        <?php if($movieRoute != "courses"){ ?>
                            <th><?php echo $this->Paginator->sort('length', 'Duração'); ?></th>
                            <th><?php echo $this->Paginator->sort('type', 'Tipo'); ?></th>
                        <?php } ?>
                        <th><?php echo $this->Paginator->sort('price', 'Preço'); ?></th>
                        <?php if($movieRoute != "lives" && $movieRoute != "courses"){ ?>
                            <th><?php echo $this->Paginator->sort('source', 'Origem'); ?></th>
                        <?php } ?>
						<th><?php echo $this->Paginator->sort('status', 'Status'); ?></th>
                        <th class="actions" nowrap><?php echo __d('users', 'Actions'); ?></th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php foreach ($movies as $movie): ?>
                        <tr>
                            <?php if($movieRoute != "courses" && $movieRoute != "modules"){ ?>
                                <td>
                                    <?php echo $this->Html->link(
                                        $movie['Category']['title'],
                                        array('controller' => 'categories', 'action' => 'view', $movie['Category']['id'])
                                    ); ?>
                                </td>
                            <?php } ?>
                            <td><?php echo h($movie['Movie']['title']); ?>&nbsp;</td>
                            <td><?php echo $this->Text->truncate($movie['Movie']['description']); ?>&nbsp;</td>
                            <?php if($movieRoute != "lives" && $movieRoute != "courses"){ ?>
                                <td><?php echo ($movie['Movie']['link'] != "") ? $this->Text->autoLink($movie['Movie']['link']) : "Não informado"; ?>&nbsp;</td>
                            <?php } ?>
                            <?php if($movieRoute != "courses"){ ?>
                                <td><?php echo h($movie['Movie']['length']); ?>s&nbsp;</td>
                                <td><?php echo h($types[$movie['Movie']['type']]); ?>&nbsp;</td>
                            <?php } ?>
                            <td><?php echo CakeNumber::currency($movie['Movie']['price']); ?>&nbsp;</td>
                            <?php if($movieRoute != "lives" && $movieRoute != "courses"){ ?>
                                <td><?php echo ($movie['Movie']['source'] != "") ? h($sources[$movie['Movie']['source']]) : "Não informado"; ?>&nbsp;</td>
                            <?php } ?>
							<td><?php echo ($movie['Movie']['status'] == true) ? 'Ativo' : 'Inativo'; ?>&nbsp;</td>
                            <td class="actions" nowrap>
                                 <?php 
                                    if($movieRoute == "courses"){
                                         echo $this->Html->link(sprintf('<i class="fa fa-plus"></i>&nbsp;&nbsp;%s</a>', __d('users', 'Listar Módulos ')),
                                                array(
                                                    'controller' => 'modules',
                                                    'action' => 'index',
                                                    'curso' => $movie['Movie']['id']
                                                ),
                                                array(
                                                    'escape'         => false,
                                                    'data-id'        => $movie['Movie']['id'],
                                                    'class'          => 'btn btn-sm purple btn-editable ajaxify',
                                                    'original-title' => __d($cmsPluginName, 'Edit')
                                                )
                                            ); 
                                         echo "&nbsp;";
                                         echo $this->Html->link(sprintf('<i class="fa fa-plus"></i>&nbsp;&nbsp;%s</a>', __d('users', 'Adicionar Módulo')),
                                                array(
                                                    'controller' => 'modules',
                                                    'action' => 'add',
                                                    'curso' => $movie['Movie']['id']
                                                ),
                                                array(
                                                    'escape'         => false,
                                                    'data-id'        => $movie['Movie']['id'],
                                                    'class'          => 'btn btn-sm green btn-editable ajaxify',
                                                    'original-title' => __d($cmsPluginName, 'Edit')
                                                )
                                            );
                                    }
                                ?>
                                <?php echo $this->Html->link(
                                    sprintf('<i class="fa fa-info"></i>&nbsp;&nbsp;%s</a>', __d('users', 'View')),
                                    array('controller' => $movieRoute, 'action' => 'view', $movie['Movie']['id']),
                                    array(
                                        'escape'         => false,
                                        'data-id'        => $movie['Movie']['id'],
                                        'class'          => 'btn btn-sm blue btn-editable ajaxify',
                                        'original-title' => __d($cmsPluginName, 'View')
                                    )
                                ); ?>
                                <?php echo $this->Html->link(sprintf('<i class="fa fa-edit"></i>&nbsp;&nbsp;%s</a>', __d('users', 'Edit')),
                                    array(
                                        'controller' => $movieRoute,
                                        'action' => 'edit',
                                        $movie['Movie']['id']
                                    ),
                                    array(
                                        'escape'         => false,
                                        'data-id'        => $movie['Movie']['id'],
                                        'class'          => 'btn btn-sm green btn-editable ajaxify',
                                        'original-title' => __d($cmsPluginName, 'Edit')
                                    )
                                ); ?>
                                <?php echo $this->Form->postLink(
                                    sprintf('<i class="fa fa-times"></i>&nbsp;&nbsp;%s</a>', __d('users', 'Delete')),
                                    array(
                                        'controller' => $movieRoute,
                                        'action' => 'delete',
                                        $movie['Movie']['id']),
                                    array(
                                        'escape'         => false,
                                        'class'          => 'btn btn-sm red btn-removable',
                                        'original-title' => __d('users', 'Delete')
                                    ),
                                    __d('users', 'Are you sure you want to delete # %s?', $movie['Movie']['id'])
                                );
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>

                <?php if($this->params['paging']['Movie']['pageCount'] > 1): ?>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center clear">
                            <ul class="pagination customizada">
                                <?php echo $this->Paginator->prev( '«', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li' ) ); ?>
                                <?php echo $this->Paginator->numbers(['currentClass' => 'active', 'tag' => 'li']); ?>
                                <?php  echo $this->Paginator->next( '»', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li' ) ); ?>
                            </ul>
                        </div>
                        <p style="width: 100%; text-align: center;"><?php echo $this->Paginator->counter(array(
                            'format' => 'Página {:page} de {:pages}, exibindo {:current} registros de um total de {:count} registros.'
                        )); ?>
                        </p>
                    </div>
                <?php endIf; ?>

                <?php else: ?>

                    <p>Nenhum filme encontrado.</p>

                <?php endif; ?>

            </div>
        </div>
    </div>
</div>
