<section class="dia-do-bem">
	<div class="container">
		<div class="header"></div><!-- header vazio -->
		<div class="content" style="height:800px;">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<div class="data-evento">
						<div class="dt-day">
							<h2>24/11</h2>
						</div>
						<h4>Paz, amor e tolerância!</h4>
					</div>
					<div class="col-xs-12 texto">
						<h2>Um dia diferente...</h2>
						<p>
							Diariamente, quando você acorda e entra em contato com as primeiras notícias da manhã, pelo jornal, rádio, celular, já percebe que as coisas continuam complicadas. Violência, insegurança, corrupção, egoísmo, enfim, as notícias não são nada boas e você ainda nem tomou café e já está atrasado...
						</p>
						<p>Agora imagine que você pudesse acordar em um dia especial.</p>
						<p>
							Um dia em que novamente fosse natural e educado dar bom dia. Um dia no qual, um grupo de pessoas com cartazes, em plena rua, distribuísse abraços. Mais adiante, uma loja inteira mobilizada para a arrecadação de fundos destinados a entidade de amparo a crianças, idosos, pessoas especiais. Gente recolhendo lixo da rua. Na padaria, pessoas pagando um café a um morador de rua. Nas escolas, as crianças realizando atividades de valorização do ser humano, de aceitação da diferença, da diversidade.<br/>
							Imagine você acordar em um dia, no qual as pessoas estivessem mobilizadas para a paz, para o amor e para a tolerância.
						</p>
						<p>Já imaginou? Pois bem, agora pare de imaginar e ajude-nos a construir este dia.</p>
						<p><span>DIA DO BEM: paz, amor e tolerância.</span></p>
						<p>							
							No dia <span>24 de novembro de 2015</span>, você pode acordar em um dia diferente. Para que isto aconteça, basta estar disposto a colaborar com a sua ação positiva, dar a sua contribuição, fazer a sua parte. Basta acreditar que é possível, porque quando você acredita no potencial transformador da sua própria mudança de atitude, o mundo, imediatamente, muda.
							Para participar, pense em uma iniciativa positiva, individual ou em grupo, e inscreva a atividade, preenchendo as informações ao lado.
						</p>						
						<p>
							Um DIA DO BEM pode ser construído com simplicidade, humildade e vontade de solidariedade. Pagar um café para alguém ou uma passagem de ônibus. Doar livros. Distribuir flores. Organizar uma visita a um asilo, creche, hospital. Aderir a uma das tantas campanhas de arrecadação de entidades beneficentes. Mobilizar o pessoal do trabalho para uma doação em grupo. Promover atividade de conscientização das pessoas para a valorização dos direitos humanos. Todas as atitudes são válidas. Mas é muito importante que a ação seja compartilhada com todos, para que sirva de incentivo aos demais, formando uma verdadeira corrente positiva.<br/>
							Na prática, simples gestos de carinho, boas ações, gentilezas geram um impacto social inestimável, porque se tornam fatores multiplicadores de novas ações do mesmo gênero.
						</p>
						<p>
							Faça parte. Faça sua parte.
						</p>

					</div><!-- end texto -->
					<div class="col-xs-12">
						<?php echo $this->Html->image('imagens/parceiros.gif', ['class' => 'img-responsive img-parceiro', 'alt' => 'Parceiros']); ?>
						<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12 my-btns"><br/>
							<a href="http://www.integraleventos.com.br/evento/seminario-tolerante/" target="_blank"><?php echo $this->Html->image('imagens/btn-seminario.gif', ['class' => 'img-responsive', 'alt' => 'Seminario']); ?></a>
						</div>
						<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12 my-btns"><br/>
						    <a href="http://minhaentrada.com.br/evento/festa-dia-do-bem--3828" target="_blank"><?php echo $this->Html->image('imagens/btn-festa.gif', ['class' => 'img-responsive ', 'alt' => 'Festa']); ?></a>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 colunas my-first">
						<h3>Manhã</h3>
						<p>Atividades do<br/> DIA DO BEM</p>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 colunas">
						<h3>Tarde</h3>
						<p> Seminário Tolerante</p>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 colunas">
						<h3>Noite</h3>
						<p>Festa do Bem<br/> no Bar<br/> Opinião</p>
					</div>
					<div class="col-xs-12 eventos">						
						<?php 
							if(isset($eventos[0]['Diadobem']['entidade'])):
								echo '<h4>Alguns eventos já cadastrados</h4>';
							endif;
							$i = 0;
						    foreach ($eventos as $item):?>
								<div class="item <?php if(($i%2)==0): echo 'black'; endif; ?>">
								  <?php if($item['Diadobem']['imagem']!=""): ?>
									<figure class="pull-right img-evento">
										<?php echo $this->Html->image('files/'.$item['Diadobem']['imagem'].'', ['class' => 'img-responsive', 'alt' => 'Dia do Bem']); ?>										
									</figure>
								 <?php endif;?>
									
										<p><span>Entidade: </span><?php echo $item['Diadobem']['entidade'];  ?></p>
										<p><span>Cidade: </span> <?php echo $item['Diadobem']['cidade'];  ?></p>
										<p><span>Horário: </span><?php echo $item['Diadobem']['horario'];  ?></p>
										<p class="desc">
											<span>Descrição: </span>
											<?php echo $item['Diadobem']['descricao'];  ?>
										</p>									
									
								</div><!-- end item -->								
						<?php
						   $i++;
						   endforeach; ?>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div class="sidebar">
						<h4>Faça parte. Faça a sua parte.</h4>
						<h3>Divulgue o Bem que você fará no <strong>Dia do Bem</strong></h3>
						<p>
							Doar um livro, entregar uma flor, plantar uma árvore, limpar uma praça, participar da campanha de arrecadação de alguma entidade beneficente são alguns exemplos simples de atitudes positivas que transformam a vida das pessoas envolvidas.
						</p>
						<p>Inscreva aqui a sua ação positiva para que ela sirva de motivação para outras pessoas.</p>
						
						<form action="diadobem/add" method="post" enctype="multipart/form-data">
							<div class="form-group">
								<label>NOME</label>
								<input type="hidden" name="data[DiaDobem][teste]" id="nome-diadobem" class="form-control" required>
								<input type="text" name="data[DiaDobem][entidade]" id="nome-diadobem" class="form-control" required >
							</div>
							<div class="form-group">
								<label>CIDADE</label>
								<input type="text" name="data[DiaDobem][cidade]" id="cidade-diadobem" class="form-control" required>
							</div>							
							<div class="form-group">
								<label>DESCRIÇÃO DO EVENTO</label>
								<textarea class="form-control" rows="4" name="data[DiaDobem][descricao]" id="form-descricao" required></textarea>
							</div>
							<div class="form-group">
								<label>HORÁRIO DE INÍCIO</label>
								<input type="text" name="data[DiaDobem][horario]" id="horario" class="form-control" required>
							</div>
							<div class="form-group">
								<label>DADOS PARA CONTATO <span>(e-mail/telefone)</span></label>
								<input type="text" name="data[DiaDobem][contato]" id="dados" class="form-control" required>
							</div>
							<div class="form-group">
								<input type="file" name="data[DiaDobem][imagem]" id="arquvio" class="form-control file">
								<div class="my-file"><p id="my-label"><span class="pull-left">+</span> Anexar uma imagem</p></div>
							</div>
							<div class="form-group">
								<button type="submit" class="btn">Enviar</button>
							</div>
							<div class="form-group">
								<br/>
								<h5>QUER SER APOIADOR DO DIA DO BEM?</h5>
								<p>
									<strong>Informações:</strong> contato@itolerância.com.br <br/>
									<strong>Telefone:</strong> (51) 9903-7703
								</p>
							</div>
						</form>
					</div><!-- end sidebar -->				
				</div><!-- end col-4 -->		
			</div>
		</div>
	</div>
</section>
<script>
	$("#arquvio").change(function(){
		var arquivo = this.files[0];
		$("#my-label").html( "<span class='pull-right myClose' id='myClose'>x</span>"+ arquivo.name);
		$("#myClose").click(function(){
			$("#my-label").html( "<span class='pull-left'>+</span> Anexar uma imagem");
			$("#arquvio").val('');
		});
	});
	
</script>

<?php echo $this->element('banners'); ?>