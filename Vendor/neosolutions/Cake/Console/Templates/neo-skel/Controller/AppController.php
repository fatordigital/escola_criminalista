<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $components = array(
        'DebugKit.Toolbar'
    );

    public function beforeFilter()
    {
        parent::beforeFilter();
        // Dinamycally enable the DebugKit Toolbar component.
        if (Configure::read('debug') > 0) {
            $this->components[] = 'DebugKit.Toolbar';
        }
        $this->cmsSettings();
    }

    public function cmsSettings()
    {
        global $cmsName;
        $cmsName = Configure::read('Admin.name');

        $this->set('cmsName', $cmsName);
        $this->cmsName = $cmsName;
    }

    public function seoSettings() {
        $meta_keywords = '';
        $meta_description = '';
        $project_prefix_title = '';

        $this->set(compact('meta_keywords'));
        $this->set(compact('meta_description'));
        $this->set(compact('project_prefix_title'));
    }

    /**
     * MÃ©todo pra chamar dinamicamente o layout do SolutionsCMS caso seja uma aÃ§Ã£o administrativa.
     */
    public function beforeRender()
    {
        parent::beforeRender();
        $this->_setErrorLayout();
	$this->seoSettings();
    }

    protected function _setErrorLayout()
    {

        $cmsName = $this->cmsName;

        if ($this->name == 'CakeError') {
            $this->layout = 'error';
            return;
        }

        if (strpos($this->here, strtolower($cmsName)) > 0 && strpos($this->here, 'login') > 0) {
            $this->layout = "$cmsName.login";
        } else if (strpos($this->here, strtolower($cmsName)) !== false) {
            $this->layout = "$cmsName.default";
        }

        /* if this is 404 error
        {
            $this->layout = 'error';
        }*/

    }

}
