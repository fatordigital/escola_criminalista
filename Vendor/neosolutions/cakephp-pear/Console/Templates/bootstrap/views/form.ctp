<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * @var $pluralVar           string   The model, camelCased and plural, i.e. "UsersTags"
 * @var $singularName        string   The model, camelCased and singular, i.e. "SystemConfig"
 * @var $singularHumanName   string   The model, singular, separated by spaces, first letter uppercased, i.e. "Cms User"
 * @var $pluralHumanName     string   The model, plural, separated by spaces, first letter uppercased, i.e. "Cms User"
 * @var $action              string   The action that is being requested, like "view"
 * @var $fields              array    Array containing each column of the database table
 * @var $primaryKey          string   Column name of the primary key from database table
 * @var $modelClass          string   The model, camelCased and plural, i.e. "UsersTags"
 * @var $modelObj            Model
 */

echo "<?php

/**
 * @var \$this View
 */
\$this->Html->addCrumb(__d('admin', '{$pluralHumanName}'));\n
\$this->Html->addCrumb(__d('admin', '" . Inflector::humanize($action) . " %s $singularHumanName', \$this->Form->value('{$modelClass}.{$primaryKey}')));\n
?>";

?>

<div class="row">
    <div class="col-md-12">
        <h2 class="page-title"><?php printf("<?php echo __d('admin', '%s %s'); ?>", Inflector::humanize($action), $singularHumanName); ?></h2>
        <?php echo <<<PHP
        <?php echo \$this->Html->getCrumbList(
            array(
                'class' => 'breadcrumb',
                'separator' => '<i class="icon-angle-right"></i>',
                'escape' => false
            ),
            '<i class="icon-home"></i> Home'
        );
        ?>
PHP;
?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <?php echo "<?php echo \$this->Session->flash(); ?>"; ?>

        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <?php echo printf("<?php echo __d('admin', '%s the %s information'); ?>", Inflector::humanize($action), $singularHumanName); ?>
                </div>
            </div>

            <div class="portlet-body form <?php echo $pluralVar; ?>">
                <div class="form-body">
                    <?php echo "<?php echo \$this->Form->create(
                        '{$modelClass}',
                        array(
                            \t'inputDefaults' => array(
                                \t'div' => 'form-group',
                                \t'wrapInput' => 'col-md-4',
                                \t'label' => array(
                                    \t\t'class' => 'col col-md-3 control-label'
                                \t),
                                \t'class' => 'form-control',
                                \t'novalidate' => 'novalidate'
                            \t),
                        'class' => 'form-horizontal',
                        )
                    ); ?>\n"; ?>

                    <?php
                    echo "\t<?php\n";
                    foreach ($fields as $field) {
                        if (strpos($action, 'add') !== false && $field == $primaryKey) {
                            continue;
                        } elseif (!in_array($field, array('created', 'modified', 'updated'))) {
                            echo "\t\techo \$this->Form->input('{$field}',
                            \t\t\tarray(
                                \t\t\t\t'label' => array(
                                    \t\t\t\t\t'text' => '" . Inflector::humanize($field) . "'
                                \t\t\t\t)
                                \t\t\t)
                            \t\t\t);\n";
                        }
                    }
                    if (!empty($associations['hasAndBelongsToMany'])) {
                        foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
                            echo "\t\t\t\techo \$this->Form->input('{$assocName}');\n";
                        }
                    }
                    echo "\t?>\n";
                    ?>

                    <div class="form-actions">

                        <?php echo "<?php echo \$this->Form->submit(__d('admin', 'Submit'), array(
                            'div' => false,
                            'class' => 'btn blue',
                            'escape' => false,
                        )); ?>" ?>

                        <?php echo "<?php echo \$this->Form->postLink(
                            __d('admin', 'Delete %s', '<i class=\"fa fa-times\"></i>'),
                            array(
                                'action' => 'delete',
                                \$this->Form->value('{$modelClass}.{$primaryKey}')
                            ),
                            array('class' => 'btn red', 'escape' => false),
                            __d('admin', 'Are you sure you want to delete %s # %s?', __d('admin', '{$modelClass}'), \$this->Form->value('{$modelClass}.{$primaryKey}'))
                        ); ?>" ?>


                    </div>

                    <?php echo "<?php echo \$this->Form->end(); ?>" ?>

                </div>
            </div>
        </div>
    </div>
</div>
