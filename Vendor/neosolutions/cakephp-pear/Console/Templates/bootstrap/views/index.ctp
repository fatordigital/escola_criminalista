<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * @var $pluralVar           string   The model, camelCased and plural, i.e. "UsersTags"
 * @var $singularName        string   The model, camelCased and singular, i.e. "SystemConfig"
 * @var $singularHumanName   string   The model, singular, separated by spaces, first letter uppercased, i.e. "Cms User"
 * @var $pluralHumanName     string   The model, plural, separated by spaces, first letter uppercased, i.e. "Cms User"
 * @var $action              string   The action that is being requested, like "view"
 * @var $fields              array    Array containing each column of the database table
 * @var $primaryKey          string   Column name of the primary key from database table
 * @var $modelClass          string   The model, camelCased and plural, i.e. "UsersTags"
 * @var $modelObj            Model
 * @var $this View
 */

App::uses('HtmlHelper', 'View/Helper');

echo "
<?php

/**
 * @var \$this View
 */
\$this->Html->addCrumb(__d('admin', '{$pluralHumanName}'));\n
?>\n";
?>

<div class="row">
    <div class="col-md-12">
        <h2 class="page-title"><?php printf("<?php echo __d('admin', '%s %s'); ?>", Inflector::humanize($action), $singularHumanName); ?></h2>
        <?php echo <<<PHP
        <?php echo \$this->Html->getCrumbList(
                    array(
                        'class' => 'breadcrumb',
                        'separator' => '<i class="icon-angle-right"></i>',
                        'escape' => false
                    ),
                    '<i class="icon-home"></i> Home'
                );
        ?>
PHP;
        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <?php echo "<?php echo \$this->Session->flash(); ?>\n"; ?>

        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list">&nbsp;<?php echo printf("<?php echo __d('admin', '%s the %s information'); ?>", Inflector::humanize($action), $singularHumanName); ?>

                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                        <a href="javascript:;" class="reload"></a>
                        <a href="javascript:;" class="remove"></a>
                    </div>
                </div>
            </div>

            <div class="portlet-body">

                <div class="table-toolbar">
                    <?php echo "<?php echo \$this->Html->link(
                        sprintf('%s <i class=\"fa fa-plus\"></i>', __d('admin', 'Add New')),
                        array('action' => 'add'),
                        array(
                            'escape' => false,
                            'class'  => 'btn green'
                        )
                    ); ?>\n"; ?>
                </div>

                <table class="table table-hover table-bordered table-striped table-condensed flip-content"
                       data-model="<?php echo $modelClass ?>">
                    <thead>
                    <tr>
                        <?php foreach ($fields as $field): ?>
                        <th><?php echo "<?php echo \$this->Paginator->sort('{$field}'); ?>"; ?></th>
                        <?php endforeach; ?>
                        <th class="actions"><?php echo "<?php echo __d('admin', 'Actions'); ?>"; ?></th>
                    </tr>
                    </thead>

                    <?php
                    echo "<?php foreach (\${$pluralVar} as \${$singularVar}): ?>\n";
                    echo "\t<tr>\n";
                    foreach ($fields as $field) {
                        $isKey = false;
                        if (!empty($associations['belongsTo'])) {
                            foreach ($associations['belongsTo'] as $alias => $details) {
                                if ($field === $details['foreignKey']) {
                                    $isKey = true;
                                    echo "\t\t<td>\n\t\t\t<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t</td>\n";
                                    break;
                                }
                            }
                        }
                        if ($isKey !== true) {
                            echo "\t\t<td><?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>&nbsp;</td>\n";
                        }
                    }

                    echo "\t\t<td class=\"actions\">\n";

                    echo "\t\t\t<?php echo \$this->Html->link(\n
                            \t\t\t\tsprintf('<i class=\"fa fa-info\"></i>&nbsp;&nbsp;%s</a>', __d('admin', 'View')),\n
                             \t\t\t\tarray('action' => 'view', \${$singularVar}['{$modelClass}']['{$primaryKey}']),\n
                             \t\t\t\tarray(\n
                                    \t\t\t\t\t'escape'         => false,\n
                                    \t\t\t\t\t'data-id'        => \${$singularVar}['{$modelClass}']['id'],\n
                                    \t\t\t\t\t'class'          => 'btn btn-sm blue btn-editable',\n
                                    \t\t\t\t\t'original-title' => __d('admin', 'View')\n
                                \t\t\t\t)
                             \t\t\t\t); ?>\n";

                    echo "\t\t\t<?php echo \$this->Html->link(sprintf('<i class=\"fa fa-edit\"></i>&nbsp;&nbsp;%s</a>', __d('admin', 'Edit')),\n
                        \t\t\t\tarray(\n
                            \t\t\t\t\t'action' => 'edit',\n
                            \t\t\t\t\t\${$singularVar}['{$modelClass}']['{$primaryKey}']\n
                            \t\t\t\t),\n
                            \t\t\t\tarray(\n
                                \t\t\t\t\t'escape'         => false,\n
                                \t\t\t\t\t'data-id'        => \${$singularVar}['{$modelClass}']['{$primaryKey}'],\n
                                \t\t\t\t\t'class'          => 'btn btn-sm green btn-editable',\n
                                \t\t\t\t\t'original-title' => __d('admin', 'Edit')\n
                            \t\t\t\t)\n
                            ); ?>\n";

                    echo "\t\t\t<?php echo \$this->Form->postLink(
                        \t\t\t\tsprintf('<i class=\"fa fa-times\"></i>&nbsp;&nbsp;%s</a>', __d('admin', 'Delete')),\n
                        \t\t\t\tarray(\n
                            \t\t\t\t\t'action' => 'delete',\n
                            \t\t\t\t\t\${$singularVar}['{$modelClass}']['{$primaryKey}']),\n
                            \t\t\t\t\tarray(
                            \t\t\t\t\t\t'escape'         => false,\n
                            \t\t\t\t\t\t'class'          => 'btn btn-sm red btn-removable',\n
                            \t\t\t\t\t),\n
                            \t\t\t\t\t__d('admin', 'Are you sure you want to delete # %s?', \${$singularVar}['{$modelClass}']['{$primaryKey}'])
                        \t\t\t\t);\n

                        \t\t\t\t?>\n";

                    echo "\t\t</td>\n";
                    echo "\t</tr>\n";

                    echo "<?php endforeach; ?>\n";

                    ?>

               </table>

            </div>
        </div>
    </div>
</div>
