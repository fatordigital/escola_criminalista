<?php /** @var $this View */ ?>

<?php if (Configure::read('debug') > 0) : ?>
    <?php $this->Html->scriptStart(['inline' => false]) ?>
    jQuery(function($) {
        $('.cake-sql-log').addClass('table table-striped table-hovered');
    });
    <?php echo $this->Html->scriptEnd() ?>
<?php endif; ?>