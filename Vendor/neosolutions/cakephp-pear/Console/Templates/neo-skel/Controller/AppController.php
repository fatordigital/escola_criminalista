<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $components = array(
        'Session',
        'Auth'
    );


    public $helpers = array(
        'Session',
        'Html'      => array('className' => 'BoostCake.BoostCakeHtml'),
        'Form'      => array('className' => 'BoostCake.BoostCakeForm'),
        'Paginator' => array('className' => 'BoostCake.BoostCakePaginator'),
    );

    public function beforeFilter()
    {
        parent::beforeFilter();
        // Dinamycally enable the DebugKit Toolbar component.
        if (Configure::read('debug') > 0) {
            $this->components[] = 'DebugKit.Toolbar';
        }
        $this->cmsSettings();
        $this->cmsVariables();
        $this->autoBodyClass();
    }


    /**
     * Método pra chamar dinamicamente o layout do SolutionsCMS caso seja uma aÃ§Ã£o administrativa.
     */
    public function beforeRender()
    {
        parent::beforeRender();
        $this->_setErrorLayout();
        $this->seoSettings();
        if ($this->request->is('ajax')) {
            $this->layout = 'ajax';
        }
    }

    public function autoBodyClass() {
        if (!isset($this->viewVars['variables']['content']['body_class'])) {
            $this->set('body_class',
                sprintf(
                    '%s %s %s %s',
                    strtolower($this->name),
                    $this->action,
                    $this->request->params['action'],
                    @$this->request->params['pass'][0]
                )
            );
        }
    }

    public function cmsVariables() {
        $adminSettings = Configure::read('Admin');

        foreach ($adminSettings as $key => $value) {
            $_key = 'cms' . Inflector::camelize($key);
            $this->set($_key, $value);
            $this->$_key = $value;
        }
    }

    public function cmsSettings()
    {
        if (isset($this->request->params['prefix']) && $this->request->params['prefix'] == 'admin') {
            AuthComponent::$sessionKey = 'Auth.Admin';// solution from http://bit.ly/1pMoolz

            $this->Auth->loginAction    = array(
                'plugin'     => 'Admin',
                'controller' => 'CmsUsers',
                'action'     => 'login'
            );
            $this->Auth->loginRedirect  = array(
                'plugin'     => 'Admin',
                'controller' => 'CmsUsers',
                'action'     => 'index'
            );
            $this->Auth->logoutRedirect = array(
                'plugin'       => false,
                'solutionscms' => false,
                'controller'   => 'pages',
                'action'       => 'display',
                'home'
            );
            $this->Auth->authenticate   = array(
                'Form' => array(
                    'userModel' => 'CmsUser',
                )
            );
            $this->Auth->authError      = 'Você não tem permissão para acessar esta página!';
            $this->Auth->flash          = array(
                'params'  => array(
                    'class' => 'alert alert-warning'
                ),
                'key'     => 'auth',
                'element' => 'default'
            );
            $this->Auth->allow('login');
        } else {
            $this->Auth->allow();
        }
    }

    public function seoSettings() {
        $meta_keywords = '';
        $meta_description = '';
        $project_prefix_title = '';

        $this->set(compact('meta_keywords'));
        $this->set(compact('meta_description'));
        $this->set(compact('project_prefix_title'));
    }

    protected function _setErrorLayout()
    {

        if ($this->name == 'CakeError') {
            $this->layout = 'error';
            return;
        }

        if ($this->request->prefix == $this->cmsRoutingPrefix) {
            $this->layout = "{$this->cmsPluginName}.default";
            if (strpos($this->request->action, 'login')) {
                $this->layout = "{$this->cmsPluginName}.login";
            }
        }

        /* if this is 404 error
        {
            $this->layout = 'error';
        }*/

    }

}
