



$(document).ready(function (){
	/* Colocar uma function por vez 
	
	$('video#header').on("loadedmetadata", redimencionaVideo);
	$(window).on("resize", redimencionaVideo);

	function redimencionaVideo(){

		var	alturaJanela = $(window).height();
		var	larguraJanela = $(window).width();

		var larguraNativaVideo = $('video#header')[0].videoWidth;
		var alturaNativaVideo = $('video#header')[0].videoHeight;

		var fatorEscalaAltura = alturaJanela / alturaNativaVideo;
		var fatorEscalaLargura = larguraJanela / larguraNativaVideo;

		if(fatorEscalaLargura > fatorEscalaAltura){
			var scale = fatorEscalaLargura;
		}else{
			var scale = fatorEscalaAltura;
		}

		var alturaVideoRedimencionado = alturaNativaVideo * scale;
		var larguraVideoRedimencionado = larguraNativaVideo * scale;

		$('video#header').height(alturaVideoRedimencionado);
		$('video#header').width(larguraVideoRedimencionado);
	}*/
	 

	$('section.formulario-contato form select').customSelect({customClass:'customSelect'});
	$('select#categorias, select#duracao, select.quantidade').customSelect({customClass:'customized'});

	$('section.filme-detalhe a.ler-mais').click(function(){
		$('section.filme-detalhe .descricao-filme .fadeout').toggle('blind', 500);
	});

	$('.botao-menu img').click(function(){
		$('.menu-responsivo').toggle('blind');
	});

});