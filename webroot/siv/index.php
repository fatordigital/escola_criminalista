<!DOCTYPE html>
    <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
    <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
    <!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
    <!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <?php require_once 'includes/head.php' ?>
    <body>
        <?php require_once 'includes/header.php' ?>
        <section class="principal">
        	<div class="container">
        		<div class="col-xs-12 text-center">
	        		<h1>Seminário Internacional Virtual Casa da<br> Tolerância - RedeJur</h1>
	        		<h2>VI Seminário <span>RedeJur</span></h2>
	        		<h3>Dia 04/04 • Sexta-feira • Londres • Inglaterra</h3><br>
	        	</div>
	        	<div class="col-xs-8 text-center col-xs-offset-2">
	        		<p>O Instituto Casa da Tolerância é um portal de educação por internet que disponibilizará cursos, aulas, palestras, documentários, debates, com transmissão “ao vivo” e por filmes gravados. 
<br>A RedeJur é uma associação civil sem fins lucrativos constituída por sociedades de advogados com escritórios localizados nas capitais e principais cidades do Brasil e associados no Exterior, que atua desde 2003,  com o objetivo de oferecer aos associados o apoio necessário para o bom exercício da advocacia empresarial em rede de cooperação. 
	        	</div>
        	</div>
        </section>
        <section class="videos">
        	<div class="container">
        		<div class="video">
        			<figure>
        				<a href="http://www.casadatolerancia.com.br/lives/view/seminario-internacional-virtual-casa-da-tolerancia-redejur" title="link da stream">
        					<img src="<?php echo $baseurl ?>/imagens/link-video.jpg" alt="">
        				</a>
        			</figure>
        		</div>
        		<div class="col-xs-12 text-center">
        			<a href="http://www.casadatolerancia.com.br/lives/view/seminario-internacional-virtual-casa-da-tolerancia-redejur" title="Clique aqui para ver a transmissão ao vivo!">
        				<img src="<?php echo $baseurl ?>/imagens/ver-transmissao.png" alt="Clique aqui para ver a transmissão ao vivo!">
        			</a>
        		</div>
        	</div>
        </section>
		<section class="outros">
			<div class="container">
				<div class="col-xs-4">
					<h2 class="primeiro">O Instituto Casa da Tolerância e a Redejur unem-se para transmitir o primeiro Seminário Internacional Virtual da Casa da Tolerância e o VI Seminário RedeJur, que será transmitido diretamente de Londres para o seu computador, tablet ou celular. </h2>
					<p>No evento, os palestrantes farão uma explanação inicial livre, havendo, em seguida, um debate aberto sobre os principais desafios e dificuldades da advocacia nos seus países de origem.</p>
					<p>Faça sua assinatura mensal no portal do Instituto Casa da Tolerância e participe deste importante debate sobre a realidade da advocacia em diversos países do mundo. Pelo valor promocional de R$ 19,90 (mensal), você poderá ter acesso a toda a programação da Casa da Tolerância e, ainda, ter descontos especiais nos Cursos e Eventos. Caso não queira fazer a assinatura, o evento estará disponível para aquisição avulsa. </p>
					<p>Instituto Casa de Tolerância<br /><em>Direito e ousadia</em></p>
				</div>
				<div class="col-xs-4 text-center">
					<h2>Panorama Internacional dos<br /><span>Desafios da Advocacia</span></h2>
					<p class="small">
						<strong>Horários</strong><br />
						<em>Horário no Brasil: 9h e 30min</em><br />
					</p>
					<p>
						<strong>Thomas Larry</strong><br />
						Washington/EUA
					</p>
					<p>
						<strong>Massimo Penco</strong><br />
						Milão/Itália
					</p>
					<p>
						<strong>Tiago Rodrigues Bastos</strong><br />
 						Lisboa/Portugal
					</p>
					<p>
						<strong>Sérgio Vital Moreira</strong><br />
 						Lisboa/Portugal
					</p>
					<p>
						<strong>Luis González Lanuza</strong><br />
						Buenos Aires/Argentina
					</p>
					<p>
						<strong>Vitória Nabas</strong><br />
						Londres/Inglaterra
					</p>
					<p>
						<strong>Jean Louis Ducharne</strong><br />
						Paris/França
					</p>
					<h2 class="outro">Execução de Sentenças Estrangeiras<br />e Execução de Sentenças Arbitrais Estrangeiras <span>nos Estados Unidos</span></h2>
					<p class="small">
						<strong>Horários</strong><br />
						<em>Horário no Brasil: 10h e 30min</em>
					</p>
					<p>
						<strong>Palestrante: Larry W. Thomas</strong><br />
						The Thomas Law Firm/USA
					</p>
				</div>
				<div class="col-xs-4 text-center">
					<h2>A Advocacia em<br /><span>Rede no Brasil</span></h2>
					<p class="small">
						<strong>Horários</strong><br />
						<em>Horário no Brasil: 11h</em><br />
					</p>
					<p>
						<strong>Vinicius de Figueiredo Teixeira</strong><br />
						Brasília/DF
					</p>
					<p>
						<strong>Felipe Lollato</strong><br />
						Caçador/SC
					</p>
					<p>
						<strong>Rodrigo Falconi Camargos</strong><br />
						Natal/RN
					</p>
					<p>
						<strong>Maurício Faria da Silva</strong><br />
						São Paulo/SP
					</p>
					<p>
						<strong>Marcelo Ricardo Grünwald</strong><br />
						São Paulo/SP
					</p>
					<p>
						<strong>Marco Antônio Aparecido de Lima</strong><br />
						Porto Alegre/RS
					</p>
					<p>
						<strong>Cesar Peduti</strong><br />
						São Paulo/SP
					</p>
					<p>
						<strong>Rochilmer Rocha Filho</strong><br />
						Porto Velho/RO
					</p>
					<p>
						<strong>Marina Emilia Baruffi Valente Baggio</strong><br />
						Ribeirão Preto/SP
					</p>
				</div>
			</div>
		</section>
        <?php require_once 'includes/footer.php' ?>
    </body>
</html>
