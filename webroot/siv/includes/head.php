<?php $baseurl = '//'.$_SERVER["HTTP_HOST"].'/siv'; ?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Instituto Casa da Tolerância | SIV</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <link rel="stylesheet" href="<?php echo $baseurl ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo $baseurl ?>/css/style.css">
    <script src="<?php echo $baseurl ?>/js/vendor/modernizr-2.6.2.min.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    <!--[if lt IE 9]>
        <script src="//raw.github.com/mylovecompany/ie9-js/master/ie9.min.js">IE7_PNG_SUFFIX=".png";</script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
    <![endif]-->
</head>