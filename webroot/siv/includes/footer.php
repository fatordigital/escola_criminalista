<footer>
	<div class="container">
		<div class="col-xs-12 text-center">
			<h2>Reserve seu lugar neste Seminário agora mesmo!</h2>
			<h3>
				Clique no link abaixo e conheça em detalhes como será o<br />
				Seminário Internacional Virtual.
			</h3>
			<a href="http://www.casadatolerancia.com.br/lives/view/seminario-internacional-virtual-casa-da-tolerancia-redejur" title="Clique aqui para ver a transmissão ao vivo!">
				<img src="<?php echo $baseurl ?>/imagens/ver-transmissao.png" alt="Clique aqui para ver a transmissão ao vivo!" class="link">
			</a>
		</div>
		<div class="col-xs-8 col-xs-offset-2">
			<hr>
		</div>
		<div class="col-xs-12 text-center">
			<img src="<?php echo $baseurl ?>/imagens/icdt-redejur.gif" alt="Instituto Casa da Tolerância | RedeJur" class="logo">
		</div>
	</div>
</footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="<?php echo $baseurl ?>/js/plugins.js"></script>
<script src="<?php echo $baseurl ?>/js/main.js"></script>