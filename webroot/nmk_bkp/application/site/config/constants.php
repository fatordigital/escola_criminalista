<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * MK CodeIgniter
 * fb.com/mkethur
 *
 * constants.php
 *
 * Version: 1.0.0
 *
 * Date: 27 / 02 / 2014
 */

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');


/*
  |--------------------------------------------------------------------------
  | FD Default defines
  |--------------------------------------------------------------------------
  | Markus Ethur
 */


define('LAYOUTPATH', $application_folder.'/layouts/');
define('UPLOADSPATH', 'files/uploads/');
define('JSPATH', $files_folder . '/js/');
define('CSSPATH', $files_folder.'/css/');
define('VENDORPATH', $files_folder.'/vendor_packages/');
define('IMGPATH', $files_folder.'/imagens/');
define('TEMPFOLDER', $files_folder.'/temp/');
define('FILESFOLDER', $files_folder);
define('FONTSFOLDER', $files_folder.'/font/');
define('HELPERSFOLDER', $application_folder.'/helpers');

// Define Ajax Request
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

define('ABSOLUTEPATH', '/Applications/XAMPP/xamppfiles/htdocs/fatorcms2');


/* End of file constants.php */
/* Location: ./application/config/constants.php */