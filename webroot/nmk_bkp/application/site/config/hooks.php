<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * MK CodeIgniter
 * fb.com/mkethur
 *
 * hooks.php
 *
 * Version: 1.0.0
 *
 * Date: 27 / 02 / 2014
 */

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/

/*
$hook['display_override'][] = array('class' => 'Layout',
                                    'function' => 'init',
                                    'filename' => 'layout.php',
                                    'filepath' => 'hooks');
*/

/* End of file hooks.php */
/* Location: ./application/config/hooks.php */