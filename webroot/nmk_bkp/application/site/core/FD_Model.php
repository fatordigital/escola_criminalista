<?php
/**
 * MK CodeIgniter
 * fb.com/mkethur
 *
 * FD_Model.php
 *
 * Version: 1.0.0
 *
 * Date: 27 / 02 / 2014
 */

if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class FD_Model extends CI_Model {

  public function __construct() {
    parent::__construct();
  }


    /**
     * updateById
     * Atualiza dados de uma tabela utilizando o id como referência.
     * Array como indice => dados de acordo com a tabela.
     *
     *
     * @param $id String indice da tabela, id
     * @param $updateData Array
     * @return bool
     */
    public function updateById($id, $updateData) {
    $this->db->where('id', $id);
    $this->db->update($this->table, $updateData);
    if ($this->db->affected_rows() > 0) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

    /**
     * insert
     * Insere os dados em uma tabela e retorna true ou false
     * 
     * @param $table String
     * @param $insertData Array
     * @return bool
     */
    protected function insert($table,$insertData) {
    if ($this->db->insert($table, $insertData)) {
      return TRUE;
    } else {
      return FALSE;
    }
  }


}