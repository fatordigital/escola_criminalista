<?php
/**
 * MK CodeIgniter
 * fb.com/mkethur
 *
 * FD_Controller.php
 *
 * Version: 1.0.1
 *
 * Date: 27 / 02 / 2014
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class FD_Controller extends CI_Controller {


    public function __construct() {
        parent::__construct();        
    }

    public function setHead($head,$args=null){
        $this->load->view((string)$head.'.php',$args);
    }

    public function setHeader($header,$args=null){
        $this->load->view((string)$header.'.php',$args);
    }

    public function setFooter($footer,$args=null){
        $this->load->view((string)$footer.'.php',$args);
    }

    public function setContent($content,$args=null){
        $this->load->view((string)$content.'.php',$args);
    }


}