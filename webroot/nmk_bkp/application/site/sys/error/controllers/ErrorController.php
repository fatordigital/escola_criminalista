<?php

class ErrorController extends FD_Controller {

  public function __construct() {
    parent::__construct();
  }

  public function index(){

    $head['title'] = "Página não encontrada";
    $this->setHead('default_head',$head);

    $header['title_interna'] = 'Página não encontrada';
    $header['body_class'] = 'novidades';
    $this->setHeader('default_header',$header);

    $this->setContent('error_404_view');

    $this->setFooter('default_footer');
  }

}
