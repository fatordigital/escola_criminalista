<header class="header-main">
  <a href="<?php echo base_url() ?>">
    <img src="files/site/imagens/logo-mini.svg" alt="Nunca Mais Kiss">
  </a>

  <a class="burger-icon" href="#">
    <span class="burger-bun-top"></span>
    <span class="burger-middle"></span>
    <span class="burger-bun-bottom"></span>
  </a>
</header>

<nav class="navigation-main">
  <a href="sobre" class="navigation-main--item ripple-dark">O projeto</a>
  <a href="denuncia" class="navigation-main--item ripple-dark">Fazer denúncia</a>
</nav>