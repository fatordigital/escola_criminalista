<?php

class AboutController extends FD_Controller {
  
  public function __construct() {
    parent::__construct();
  }
  
  public function about(){

    ini_set('allow_url_fopen',true);
    $head['title'] = null;

    $this->setHead('default_head',$head);
    $this->setContent('about',$data);
    $this->setFooter('default_footer');
  }


}