<?php

class novidades_model extends FD_Model {

  private $table;

  function __construct() {
    $this->table = 'fd_novidades';
    parent::__construct();
  }

  public function getAllNovidades($categoria=null,$limit_x=null,$limit_y=null){
    if($categoria!=null){
      $this->db->where($this->table.'.categoria_id',$categoria);
    }
    if($limit_x!=null){
      $this->db->limit($limit_x,$limit_y);
    }
    $this->db->where($this->table.'.status',1);
    $this->db->order_by($this->table.'.data','DESC');
    return $this->db->get($this->table)->result();
  }

  public function getNovidade($id){
    $this->db->where($this->table.'.id',$id);
    return $this->db->get($this->table)->row();
  }

  public function getTotalNovidades($categoria=null,$limit_x=null,$limit_y=null){
    if($categoria!=null){
      $this->db->where($this->table.'.categoria_id',$categoria);
    }
    if($limit_x!=null){
      $this->db->limit($limit_x,$limit_y);
    }
    $this->db->where($this->table.'.status',1);
    $this->db->order_by($this->table.'.data','DESC');
    return $this->db->get($this->table)->num_rows();
  }



} 