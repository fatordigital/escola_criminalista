<body class="body--home">

  <main class="page--home">
    <div class="cover-logo">
      <div class="logo-ribbon"></div>
      <?php include "files/site/imagens/logo.svg" ?>
    </div>
    
    <h2 class="home-subtitle">Você pode salvar vidas.</h2>
    
    <img src="files/site/imagens/icons/extinguisher.svg" alt="Extintor de Incêndio">
    <img class="middle-icon" src="files/site/imagens/icons/exit.svg" alt="Saída de Emergência">
    <img src="files/site/imagens/icons/downstairs.svg" alt="Indicação de Escada">

    <nav class="navigation-home">
      <a href="sobre" class="navigation-home--item">Conheça o <span class="black">projeto</span></a>
      <a href="denuncia" class="navigation-home--item">Faça uma <span class="black">denúncia</span></a>
    </nav>
  </main>