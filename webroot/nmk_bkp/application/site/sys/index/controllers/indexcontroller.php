<?php

class IndexController extends FD_Controller {
  
  public function __construct() {
    parent::__construct();
  }
  
  public function index(){

    ini_set('allow_url_fopen',true);
    $head['title'] = null;

    $this->setHead('default_head',$head);
    $this->setContent('home',$data);
    $this->setFooter('default_footer');
  }

}
