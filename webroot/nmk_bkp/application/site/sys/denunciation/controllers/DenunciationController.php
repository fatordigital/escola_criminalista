<?php

class DenunciationController extends FD_Controller {
  
  public function __construct() {
    parent::__construct();
  }
  
  public function denunciation(){

    ini_set('allow_url_fopen',true);
    $head['title'] = null;

    $this->setHead('default_head',$head);
    $this->setContent('denunciation',$data);
    $this->setFooter('default_footer');
  }

  public function send_denunciation(){

      $this->load->model('denunciations_model');

      $data['denunciations'] = array(
        'st_name'                     => $this->input->post("name"),  
        'st_email'                    => $this->input->post("email"),    
        'st_telephone'                => $this->input->post("telephone"), 
        'st_other_issues_description' => $this->input->post('other-issues-description'),
        'bl_in_place'                 => $this->input->post("bl_in_place"),
        'bl_fire_extinguishers'       => $this->input->post("bl_fire_extinguishers"),
        'bl_emergency_exits'          => $this->input->post("bl_emergency_exits"),
        'bl_fire_door'                => $this->input->post("bl_fire_door"),
        'bl_maximum_capacity'         => $this->input->post("bl_maximum_capacity"),
        'bl_lamp_emergency'           => $this->input->post("bl_lamp_emergency"),
        'st_name_place'               => $this->input->post("name-place"),
        'st_address'                  => $this->input->post("address"),
        'st_others_information'       => $this->input->post('othres-information'),
        'dt_register'                 => date('Y-m-d H:m:s')
      );
      
      if($this->denunciations_model->insert('denunciation',$data['denunciations'])):
          $this->session->set_flashdata('success_msg',true);
      else:
          $this->session->set_flashdata('error_msg',true);
      endif;

      $erro_envio = false;
      $this->load->library('upload', $config);

        if($_FILES['imagem1']['name']!=""):        
          $extensao = strrchr($_FILES['imagem1']['name'], '.');
          $file_name = md5($_FILES['imagem1']['name']."1".date('Y-m-d H:m:s')).$extensao;
          $config['file_name'] = $file_name;
          $config['overwrite'] = TRUE;
          $config['allowed_types'] = 'jpg|jpeg|gif|png';
          $config['max_size'] = 5000;
          $config['upload_path'] = "./files/uploads/imagens";
          $this->upload->initialize($config);
           if(!$this->upload->do_upload('imagem1')):
            $data['upload_error_msg'] = true;
            $erro_envio = true;
          endif;  
          $id = $this->denunciations_model->get_max();
          $data['photo'] = array(
            'id_denunciation' => $id->id,  
            'st_photo_name'   => $file_name,
          );
         $this->denunciations_model->insert('photos',$data['photo']);

        endif;

        if($_FILES['imagem2']['name']!=""):          
          $extensao = strrchr($_FILES['imagem2']['name'], '.');
          $file_name = md5($_FILES['imagem2']['name']."4".date('Y-m-d H:m:s')).$extensao;
          $config['file_name'] = $file_name;
          $config['overwrite'] = TRUE;
          $config['allowed_types'] = 'jpg|jpeg|gif|png';
          $config['max_size'] = 5000;
          $config['upload_path'] = "./files/uploads/imagens";
          $this->upload->initialize($config);
          if(!$this->upload->do_upload('imagem2')):
            $data['upload_error_msg'] = true;
            $erro_envio = true;
          endif;
          $id = $this->denunciations_model->get_max();
          $data['photo'] = array(
            'id_denunciation' => $id->id,  
            'st_photo_name'   => $file_name,
          );
         $this->denunciations_model->insert('photos',$data['photo']);
        endif;
        if($_FILES['imagem3']['name']!=""):
          $extensao = strrchr($_FILES['imagem3']['name'], '.');
          $file_name = md5($_FILES['imagem3']['name']."3".date('Y-m-d H:m:s')).$extensao;
          $config['file_name'] = $file_name;
          $config['overwrite'] = TRUE;
          $config['allowed_types'] = 'jpg|jpeg|gif|png';
          $config['max_size'] = 5000;
          $config['upload_path'] = "./files/uploads/imagens";
          $this->upload->initialize($config);
          if(!$this->upload->do_upload('imagem3')):
            $data['upload_error_msg'] = true;
            $erro_envio = true;
          endif;
          $x = +1;
          $id = $this->denunciations_model->get_max();
          $data['photo'] = array(
            'id_denunciation' => $id->id,  
            'st_photo_name'   => $file_name,
          );
         $this->denunciations_model->insert('photos',$data['photo']);
        endif; 

      if($this->input->post("bl_in_place")==1): $lugar ="Sim"; else: $lugar ="Não";  endif;
      if($this->input->post("bl_fire_extinguishers")==1): $extintor = "Sim"; else: $extintor = "Não";  endif;
      if($this->input->post("bl_emergency_exits")==1): $saida = "Sim"; else: $saida = "Não";  endif;
      if($this->input->post("bl_fire_door")==1): $porta = "Sim"; else: $porta = "Não";  endif;
      if($this->input->post("bl_maximum_capacity")==1): $capacidade = "Sim"; else: $capacidade = "Não";  endif;
      if($this->input->post("bl_lamp_emergency")==1): $lampada = "Sim"; else: $lampada = "Não";  endif;

      require_once(APPPATH.'/libraries/phpMailer/class.phpmailer.php');
      $mail = new PHPMailer();

      $mail->IsSMTP(); // Define que a mensagem será SMTP
      $mail->Host = "smtp.mandrillapp.com:587"; // Endereço do servidor SMTP
      $mail->SMTPAuth = true; // Usa autenticação SMTP? (opcional)
      $mail->Username = 'mandrill@fatordigital.com.br'; // Usuário do servidor SMTP
      $mail->Password = 'GW15myLkhDJoQjsji8L-1g'; // Senha do servidor SMTP

      $mail->From = "nao-responder@nuncamaiskiss.com.br"; // Seu e-mail
      
      for ($i=1; $i < 4 ; $i++) { 
         $mail->AddAttachment($_FILES['imagem'.$i]['tmp_name'], $_FILES['imagem'.$i]['name']  );
      }
      //$mail->FromName = "Nunca mais Kiss"; // Seu nome
      $mail->AddAddress('nmk@defensoria.rs.gov.br', 'Nunca mais Kiss');
      $mail->AddAddress('nmk@itolerancia.com.br', 'Nunca mais Kiss');      
      $mail->AddBCC('rosalba.monteiro@fatordigital.com.br', 'Nunca mais Kiss');

      $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
      $mail->CharSet = 'UTF-8'; // Charset da mensagem (opcional)

      $mail->Subject  = 'Contato - Nunca mais Kiss'; // Assunto da mensagem

      $msg = "<table width='100%' height='100%' border='0'><tr><td style='font-family: Arial, Helvetica, sans-serif;font-size: 13px;line-height: 150%;'>";
      $msg .= "<b><span id='titulo'>Foi preenchido o formulário de Contato:</span><br><br></b>";
      $msg .= "<b>Nome: </b> ".$this->input->post("name")."<br>";
      $msg .= "<b>Email: </b> ".$this->input->post("email")."<br>";
      $msg .= "<b>Telefone: </b> ".$this->input->post("telephone")."<br>";
      $msg .= "<b>Você está no local? </b> ".$lugar."<br>";
      $msg .= "<b>EXTINTORES DE INCÊNDIO: </b> ".$extintor."<br>";
      $msg .= "<b>SAÍDAS DE EMERGÊNCIA:  </b> ".$saida."<br>";
      $msg .= "<b>ALVARÁ DE PREVENÇÃO E PROTEÇÃO CONTRA INCÊNDIO: </b> ".$porta."<br>";
      $msg .= "<b>LOTAÇÃO MÁXIMA: </b> ".$capacidade."<br>";
      $msg .= "<b>LUZES E SINALIZAÇÃO DE EMERGÊNCIA: </b> ".$lampada."<br>";

      if($this->input->post('other-issues-description')!=""):
        $msg .= "<b>OUTRAS QUESTÕES QUE PERCEBI: </b> ".$this->input->post('other-issues-description')."<br>";
      endif;
      
      $msg .= "<b>Nome do local: </b> ".$this->input->post("name-place")."<br>";
      $msg .= "<b>Endereço: </b> ".$this->input->post("address")."<br>";

      if($this->input->post('othres-information')!=""):
        $msg .= "<b>Outras Informações (opcional): </b> ".$this->input->post('othres-information')."<br>";
      endif;
      $msg .= "</td></tr></table>";

      $mail->Body = $msg;

      if($mail->Send()){
        $this->session->set_flashdata('form_newsletter_sucesso',true);
        redirect(base_url().'denuncia');
      }

      $mail->ClearAllRecipients();
      $mail->ClearAttachments();

  }




}