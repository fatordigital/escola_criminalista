<body class="body--denuncia">
  
  <main class="page--denuncia">
    <?php include "header-main.php" ?>
    <?php if($this->session->flashdata('success_msg')){ ?>
      <div class="alert alert-success fade in">
          <button data-dismiss="alert" class="close close-sm" type="button">
              <i class="fa fa-times">X</i>
          </button>
          <strong>Sucesso!</strong> Mensagem enviada com sucesso.
      </div>
    <?php } ?>
    <?php if($this->session->flashdata('error_msg')){ ?>
      <div class="alert alert-danger fade in">
          <button data-dismiss="alert" class="close close-sm" type="button">
              <i class="fa fa-times">X</i>
          </button>
          <strong>Erro!</strong> Ocorreu um erro ao efetuar o envio.
      </div>
    <?php } ?>
    <form class="form--denuncia" method="post" action="<?php echo base_url('envio') ?>" enctype="multipart/form-data">

      <fieldset class="content content--identificacao">
        <legend class="title-section">Identificação</legend>
        <div class="page-content">
          <label for="nome">Seu Nome</label>
          <input type="text" name="name" id="nome" required>
          <label for="email">Seu E-mail</label>
          <input type="email" name="email" id="email" required>
          <label for="telefone">Seu Telefone (opcional)</label>
          <input type="phone" class="input--telefone" id="telefone" name="telephone">
        </div>
        <hr>
        <div class="page-content">
          <label class="switch switch-no-local">
            <input type="checkbox" class="switch-input" id="teste"  name="bl_in_place" value="0">
            <span class="switch-label" data-off="Não" data-on="Sim" ></span>
            <span class="switch-handle"></span>
          </label>
          <span class="label">Você está no local agora?</span>
        </label>
        </div>
      </fieldset>

      <fieldset class="content--local-obs local-obs--perception">
        <legend class="title-section">O que você percebeu no local?</legend>
        <section class="page-content perception-box">
          <h3 class="side-title">
            <span class="side-icon">
              <?php include "files/site/imagens/icons/extinguisher.svg" ?>
            </span>
            <span class="perception--title-text">Problemas com extintores de incêndio (ou falta deles)</span>
          </h3>
          <label class="switch switch-local-obs">
            <input type="checkbox" class="switch-input switch-with-boxes" value="0" name="bl_fire_extinguishers">
            <span class="switch-label"></span>
            <span class="switch-handle"></span>
          </label>
          <p class="small-note">Extintores são obrigatórios e devem ser checados uma vez por ano. Quem calcula a quantidade do dispositivo e onde ele deve ser instalado também é o especialista técnico. Segundo as normas do Inmetro, existem três tipos de fogo: o fogo gerado por combustíveis líquidos, por líquidos inflamáveis e por equipamentos elétricos. Cada extintor traz as suas especificações.</p>
        </section>
        <hr>
        <section class="page-content perception-box">
          <h3 class="side-title">
            <span class="side-icon"> <?php include "files/site/imagens/icons/exit.svg" ?>
            </span>
            <span class="perception--title-text"> Não encontrei saídas de emergência ou elas estão bloqueadas</h3>
          <label class="switch switch-local-obs">
            <input type="checkbox" class="switch-input switch-with-boxes" value="0" name="bl_emergency_exits">
            <span class="switch-label"></span>
            <span class="switch-handle"></span>
          </label>
          <p class="small-note">Saídas de Emergência são obrigatórias. Deve ser localizada imediatamente acima da porta centralizada a uma altura de 1,8 metro de altura medida do piso à base da sinalização.</p>
          <p class="small-note">Deve ser localizada de modo que a distância de percurso de qualquer ponto da rota de saída até a sinalização seja de, no máximo, 15 metros. A sinalização complementar de rotas de saída é facultativa e deve ser aplicada sobre o piso.</p>
        </section>
        <hr>
        <section class="page-content perception-box">
          <h3 class="side-title">
            <span class="side-icon"> <?php include "files/site/imagens/icons/downstairs.svg" ?>
            </span>
            <span class="perception--title-text"> Não encontrei porta corta-fogo ou ela está bloqueada</h3>
          <label class="switch switch-local-obs">
            <input type="checkbox" class="switch-input switch-with-boxes" value="0" name="bl_fire_door">
            <span class="switch-label"></span>
            <span class="switch-handle"></span>
          </label>
          <p class="small-note">Protege a saída de emergência, que não pode ter materiais inflamáveis. Para segurança, ela deve possuir uma barra “antipânico” como sistema de abertura. Só com a pressão das mãos sobre a barra a pessoa destrava e abre a porta. Deve ser usada em áreas que comunicam diretamente com as rotas de fuga.</p>
        </section>
        <hr>
        <section class="page-content perception-box">
          <h3 class="side-title">
            <span class="side-icon">
              <?php include "files/site/imagens/icons/group.svg" ?>
            </span>
            <span class="perception--title-text"> O local parece estar superlotado ou não há indicações de lotação máxima</span>
            </h3>
          <label class="switch switch-local-obs">
            <input type="checkbox" class="switch-input switch-with-boxes" value="0" name="bl_maximum_capacity">
            <span class="switch-label"></span>
            <span class="switch-handle"></span>
          </label>
          <p class="small-note">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi dapibus sollicitudin turpis, in porta erat cursus ut. In commodo nunc vitae nulla maximus scelerisque. Nulla nulla erat, lacinia eu nisi eu, convallis tempus sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. </p>
        </section>
        <hr>
        <section class="page-content perception-box">
          <h3 class="side-title">
            <span class="side-icon"> <?php include "files/site/imagens/icons/lights.svg" ?>
            </span>
            <span class="perception--title-text">  Há problemas com as luzes de emergência ou elas não existem</span>
          </h3>
          <label class="switch switch-local-obs">
            <input type="checkbox" class="switch-input switch-with-boxes" value="0" name="bl_lamp_emergency">
            <span class="switch-label"></span>
            <span class="switch-handle"></span>
          </label>
          <p class="small-note">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi dapibus sollicitudin turpis, in porta erat cursus ut. In commodo nunc vitae nulla maximus scelerisque. Nulla nulla erat, lacinia eu nisi eu, convallis tempus sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. </p>
        </section>
        <hr>
        <section class="page-content perception-box">
          <h3 class="side-title">
            <span class="side-icon"> <?php include "files/site/imagens/icons/note.svg" ?>
            </span>
            <span class="perception--title-text">Outras questões que percebi (descreva)</span>
          </h3>
          <p class="small-note">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi dapibus sollicitudin turpis, in porta erat cursus ut. In commodo nunc vitae nulla maximus scelerisque. Nulla nulla erat, lacinia eu nisi eu, convallis tempus sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. </p>
          <textarea placeholder="Informações adicionais" name="other-issues-description" rows="5"></textarea>
        </section>
        <hr>
        <section class="page-content perception-box">
          <h3 class="side-title">
            <span class="side-icon">
              <?php include "files/site/imagens/icons/camera.svg" ?>
            </span>
            <span class="perception--title-text my-anexo">Anexar pelo menos uma foto do local</span>
          </h3>
          <p class="small-note">Para melhorar a identificação do local ou do problema, você precisa anexar pelo menos uma foto do local nessa denúncia.</p>

          <label class="input-file" for="file1">
            <span class="input-file--feedback"><img src="files/site/imagens/icons/clip.svg"></span>
            <input type="file" id="file1" name="imagem1"  class="file-upload" required></label>
          </label>

          <label class="input-file" for="file2">
            <span class="input-file--feedback"><img src="files/site/imagens/icons/clip.svg"></span>
            <input type="file" id="file2" name="imagem2" class="file-upload"></label>
          </label>

          <label class="input-file" for="file3">
            <span class="input-file--feedback"><img src="files/site/imagens/icons/clip.svg"></span>
            <input type="file" id="file3" name="imagem3" class="file-upload"></label>
          </label>
        </section>
      </fieldset>

      <fieldset class="content--local">
        <legend class="title-section">Ajude-nos a identificar o local</legend>
        <div class="page-content">
          <label for="nome-local">Nome do local</label>
          <input type="text" id="nome-local" name="name-place" placeholder="Digite aqui o nome do local que você está" required>
        </div>
        <hr>
        <div class="page-content">
          <label for="endereco">Endereço</label>
          <input type="text" id="endereco" name="address" placeholder="Informe o endereço ou localização aproximada" required>
        </div>
        <hr>
        <div class="page-content">
          <label for="telefone">Outras Informações (opcional)</label>
          <textarea placeholder="Deseja deixar algum comentário?" rows="6" name="othres-information"></textarea>
        </div>
      </fieldset>

      <fieldset class="section-backgrounded">
        <button type="submit" class="button-green">Enviar Denúncia</button>
      </fieldset>
    </form>
  </main>