<!DOCTYPE html>
<html lang="pt-br">
<head>

  <meta charset="utf-8">
  <link type="text/plain" rel="author" href="humans.txt">

  <title>Nunca Mais Kiss</title>

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Info for robots -->
  <meta name="description" content="">
  <meta name="google-site-verification" content="">

  <!-- [TODO] Load favicons (non-Retina) -->
  <link rel="shortcut icon" href="files/site/imagens/favicon.png">

  <!-- Load styles -->
  <link href="files/site/css/main.css" rel="stylesheet">
  
  <!-- Load fonts -->
  <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">

</head>