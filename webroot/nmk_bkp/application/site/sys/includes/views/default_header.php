<header class="header-main">
  <a href="index.php">
    <img src="assets/images/logo-mini.svg" alt="Nunca Mais Kiss">
  </a>

  <a class="burger-icon" href="#">
    <span class="burger-bun-top"></span>
    <span class="burger-middle"></span>
    <span class="burger-bun-bottom"></span>
  </a>
</header>

<nav class="navigation-main">
  <a href="sobre.php" class="navigation-main--item ripple-dark">O projeto</a>
  <a href="denuncia.php" class="navigation-main--item ripple-dark">Fazer denúncia</a>
</nav>