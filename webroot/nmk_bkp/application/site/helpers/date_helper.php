<?php
/**
 * Created by PhpStorm.
 * User: markus.ethur
 * Date: 7/18/14
 * Time: 7:28 PM
 */



function novidade_date($creation){
  //$only_date = trim(substr($creation, 0, -8));
  $only_date = $creation;
  $date = explode("/",$only_date);
  $meses = array('01'=>'Jan','02'=>'Fev','03'=>'Mar','04'=>'Abr','05'=>'Mai','06'=>'Jun','07'=>'Jul','08'=>'Ago','09'=>'Set','10'=>'Out','11'=>'Nov','12'=>'Dez');
  return $date[0]." ".$meses[$date[1]].", ".$date[2];

}