<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * MK CodeIgniter
 * fb.com/mkethur
 *
 * getlink_helper.php
 *
 * Version: 1.0.0
 *
 * Date: 27 / 02 / 2014
 */


/**
 * getLink
 * Retorna echo da URL completa /{parametro}
 *
 * @param $string String
 */
function getLink($string='') {
  return base_url().$string;
}

/**
 * getIimg
 * Retorna echo da URL completa da imagem na pasta /files/{modulo}/img
 *
 * @param $img String
 */
function getImg($img){
  return base_url().IMGPATH.$img;
}

/**
 * getUplodadedFile
 * Retorna echo da URL completa do arquivo na pasta /files/uploads
 *
 * @param $file String
 */
function getUploadedFile($file){
	if(file_exists(UPLOADSPATH.$file) == FALSE || $file == null){
		return '';
	}
  	return base_url().UPLOADSPATH.$file;
}

/**
 * getCss
 * Retorna echo da URL completa do arquivo stylesheet de /files/{modulo}/css/{parametro}.css
 *
 * @param $css String
 */
function getCss($css){
    return base_url().CSSPATH.$css.'.css';
}

/**
 * getJs
 * Retorna echo da URL completa do arquivo javascript de /files/{modulo}/js/{parametro}.js
 *
 * @param $js String
 */
function getJs($js){
    return base_url().JSPATH.$js.'.js';
}

?>
