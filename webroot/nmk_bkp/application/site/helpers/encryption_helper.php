<?php
/**
 * MK CodeIgniter
 * fb.com/mkethur
 *
 * encryption_helper.php
 *
 * Version: 1.0.0
 *
 * Date: 27 / 02 / 2014
 */

/**
 * Encrypt some String into sha1 of md5
 *
 * @param $pwd String
 * @return string encrypted
 */
function encrypt_password($pwd){
    return sha1(md5($pwd));
}