<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->            <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a href="<?php getLink('dashboard'); ?>" <?=$this->uri->segment(2)=='dashboard' ? 'class="active"':''; ?>>
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;" <?=$this->uri->segment(2)=='novidades' ? 'class="active"':''; ?>>
                        <i class="fa fa-edit"></i>
                        <span>Denuncias</span>
                    </a>
                    <ul class="sub">
                        <li <?=$this->uri->uri_string()=='fatorcms/denuncias' ? 'class="active"':''; ?>><a href="<?php getLink('denuncias') ?>">Listar Novidades</a></li>
                    </ul>
                </li>               
                <li>
                    <a href="javascript:;" <?=$this->uri->segment(2)=='usuarios' ? 'class="active"':''; ?>>
                        <i class="fa fa-users"></i>
                        <span>Usuários</span>
                    </a>
                    <ul class="sub">
                      <li <?=$this->uri->uri_string()=='fatorcms/usuarios' ? 'class="active"':''; ?>><a href="<?php getLink('usuarios') ?>">Listar Usuários</a></li>
                      <li <?=$this->uri->uri_string()=='fatorcms/usuarios/adicionar' ? 'class="active"':''; ?>><a href="<?php getLink('usuarios/adicionar/') ?>">Cadastrar Usuários</a></li>
                    </ul>
                </li>
                <li>
                    <a href="<?php getLink('logout'); ?>">
                        <i class="fa fa-key"></i>
                        <span>Logout</span>
                    </a>
                </li>
            </ul></div>
        <!-- sidebar menu end-->
    </div>
</aside>