<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="icon" type="image/png" href="<?php getImg('favicon.png'); ?>">

    <title>{title_for_layout}</title>

    <!--Core CSS -->
    {css_for_layout}

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="<?php getJs('ie8/ie8-responsive-file-warning'); ?>"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- First of all, jQuery -->
    <script type="text/javascript" src="<?php getJs('lib/jquery'); ?>"></script>
    {js_for_layout}
</head>

{content_for_layout}



</body>
{js_for_layout_bottom}

</html>