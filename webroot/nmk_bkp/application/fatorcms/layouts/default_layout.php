<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="icon" type="image/png" href="<?php getImg('favicon.png'); ?>">

    <title>{title_for_layout}</title>

    <!-- blueimp Gallery styles -->
    <link rel="stylesheet" href="<?=base_url().'files/fatorcms/vendor/mini-upload-form/assets/css/bucketmin.css';?>">

    <!--Core CSS -->
    {css_for_layout}


    <!-- Generic page styles -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>files/fatorcms/vendor_packages/fileupload/css/style.css">
    <!-- blueimp Gallery styles -->
    <link rel="stylesheet" href="http://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>files/fatorcms/vendor_packages/fileupload/css/jquery.fileupload.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>files/fatorcms/vendor_packages/fileupload/css/jquery.fileupload-ui.css">
    <!-- CSS adjustments for browsers with JavaScript disabled -->
    <noscript><link rel="stylesheet" href="<?php echo base_url(); ?>files/fatorcms/vendor_packages/fileupload/css/jquery.fileupload-noscript.css"></noscript>
    <noscript><link rel="stylesheet" href="<?php echo base_url(); ?>files/fatorcms/vendor_packages/fileupload/css/jquery.fileupload-ui-noscript.css"></noscript>

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="<?php getJs('ie8/ie8-responsive-file-warning'); ?>"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- First of all, jQuery -->
    <script type="text/javascript" src="<?php getJs('lib/jquery'); ?>"></script>
    {js_for_layout}
</head>

<body>

<section id="container">
<!--header start-->
<header class="header fixed-top clearfix">
<!--logo start-->
<div class="brand">

    <a href="<?php getLink(); ?>" class="logo">
        <img src="<?php getImg('logo_fatorcms_234x55_white.png'); ?>" width="180" alt="">
    </a>
    <div class="sidebar-toggle-box">
        <div class="fa fa-bars"></div>
    </div>
</div>
<!--logo end-->
<div class="top-nav clearfix">
    <!--search & user info start-->
    <ul class="nav pull-right top-menu">
        <!-- user login dropdown start-->
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <img alt="" src="<?php getImg('avatar1_small.jpg'); ?>">
                <span class="username">{user_name}</span>
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu extended logout">
                <li><a href="#"><i class=" fa fa-suitcase"></i>Perfil de Usuário</a></li>
                <li><a href="<?php getLink('logout'); ?>"><i class="fa fa-key"></i> Log Out</a></li>
            </ul>
        </li>
    </ul>
    <!--search & user info end-->
</div>
</header>
<!--header end-->
{menu_for_layout}
<!--sidebar end-->

<!--main content start-->
{content_for_layout}
<!--main content end-->

</section>

{js_for_layout_bottom}
<script type="text/javascript" src="<?php getJs('scripts'); ?>"></script>

</body>
</html>