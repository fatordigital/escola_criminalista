<body class="login-body">

<div class="container">

    <form class="form-signin" method="post" action="<?php getLink('login'); ?>">
        <h2 class="form-signin-heading"><img src="<?php getImg('logo_fatorcms_234x55.png'); ?>" /></h2>
        <div class="login-wrap">
            <div class="user-login-info">
                <input type="text" class="form-control" name="user" placeholder="Usuário" autofocus>
                <input type="password" class="form-control" name="password" placeholder="Senha">
            </div>
            <?php if($this->session->flashdata('login_error')){ ?>
            <div class="alert alert-danger">Usuário ou senha incorretos.</div>
            <?php } ?>
            <?php if($this->session->flashdata('logout_error')){ ?>
            <div class="alert alert-warning">Você foi desconectado do sistema.</div>
            <?php } ?>
            <label class="checkbox">
                <!--<input type="checkbox" value="remember-me"> Lembrar de mim-->
                <span class="pull-right">
                    <a data-toggle="modal" href="#myModal"> Esqueceu a Senha?</a>

                </span>
            </label>
            <button class="btn btn-lg btn-login btn-block" type="submit">Entrar</button>

            <!--
            <div class="registration">
                Don't have an account yet?
                <a class="" href="registration.html">
                    Create an account
                </a>
            </div>
            -->

        </div>

        <!-- Modal -->
        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Esqueceu sua Senha ?</h4>
                    </div>
                    <div class="modal-body">
                        <p>Coloque seu email abaixo para resetarmos sua senha.</p>
                        <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                        <button class="btn btn-success" type="button">Enviar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- modal -->

    </form>

</div>