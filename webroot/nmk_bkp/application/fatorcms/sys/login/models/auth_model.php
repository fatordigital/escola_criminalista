<?php

class auth_model extends FD_Model {

    function __construct() {
        parent::__construct();
    }

    public function checkAuth($user, $password){
        $this->db->select('fd_users.user,fd_users.password,fd_users.pager');
        $this->db->where('fd_users.user',$user);
        $this->db->where('fd_users.password',$password);
        if($this->db->get('fd_users')->num_rows()===1){
            return true;
        }else{
            return false;
        }
    }

    public function getIdByUser($user){
        $this->db->select('fd_users.id');
        $this->db->where('fd_users.user',$user);
        $row = $this->db->get('fd_users')->row_array();
        return $row['id'];
    }

    public function getNameByUser($user){
        $this->db->select('fd_users.name');
        $this->db->where('fd_users.user',$user);
        $row = $this->db->get('fd_users')->row_array();
        return $row['name'];
    }

    public function getPagerByUser($user){
        $this->db->select('fd_users.pager');
        $this->db->where('fd_users.user',$user);
        $row = $this->db->get('fd_users')->row_array();
        return $row['pager'];
    }

} 