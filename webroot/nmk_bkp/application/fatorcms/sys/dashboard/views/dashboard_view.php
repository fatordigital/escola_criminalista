<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-sm-12">

        <section class="panel">
          <header class="panel-heading">
            FatorCMS - Dashboard
          </header>
          <div class="panel-body">

            <div class="panel-body">
              <p>Bem-vindo ao FatorCMS</p>
              <p>O FatorCMS é um sistema de gerenciamento de conteúdo (CMS - Content Management System) utilizado para tornar dinâmicas, as funcionalidades do seu site, portal ou blog.
                Você pode começar acessando os módulos do seu CMS no menu a esquerda.</p>
            </div>

          </div>
        </section>
      </div>
    </div>
    <!-- page end-->
  </section>
</section>
