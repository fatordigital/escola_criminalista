<?php

class DashboardController extends FD_Controller {

    public function __construct() {
        $this->setMenu('default_menu');
        parent::__construct();
        if(!$this->isLoggedIn()){ redirect(base_url().'fatorcms');}
    }

    public function index(){
      $this->load->view('dashboard_view');
    }

}