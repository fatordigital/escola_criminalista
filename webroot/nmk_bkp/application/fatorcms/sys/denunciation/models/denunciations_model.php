<?php

class denunciations_model extends FD_Model {

  private $table;

  function __construct() {
    $this->table = 'denunciation';
    parent::__construct();
  }

  public function getAll(){
    $this->db->order_by($this->table.'.dt_register','DESC');
    return $this->db->get($this->table)->result();
  }

  public function getDenunciation($id){
    return $this->db->get_where($this->table, array('id' => $id))->row();
  }

   public function getPhoto($id_denunciatio){
    return $this->db->get_where('photos', array('id_denunciation' => $id_denunciatio))->result();
  }
  

} 