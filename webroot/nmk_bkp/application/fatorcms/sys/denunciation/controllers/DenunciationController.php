<?php

class DenunciationController extends FD_Controller {

  public function __construct() {
    $this->setMenu('default_menu');
    parent::__construct();
    if(!$this->isLoggedIn()){ redirect(base_url().'fatorcms');}
  }

  public function index(){

    /* Dynamic Tables Block */
    $this->loadCSSVendor(vendor_dynamic_tables_css());
    $this->loadJSVendorBottom(vendor_dynamic_tables_js());
    $this->loadJSBottom(dynamic_tables_js());
    /* End - Dynamic Tables Block */

    $data = array();
    $this->load->model('denunciations_model');
    $data['denunciations'] = $this->denunciations_model->getAll();
    $this->load->view('denunciations_view',$data);
  }

  public function detail($id){
    $data = array();
    $this->load->model('denunciations_model');
    $data['denunciation'] = $this->denunciations_model->getDenunciation($id);
    $data['photos'] = $this->denunciations_model->getPhoto($id);
    $this->load->view('denunciation_detail_view',$data);

  }

}