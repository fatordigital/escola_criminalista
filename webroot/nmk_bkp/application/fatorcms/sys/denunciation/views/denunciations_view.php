<?php //debugVarAndDie($galerias); ?>

<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">

                <ul class="breadcrumb">
                    <li><a href="<?php getLink(); ?>"><i class="fa fa-home"></i>Home</a></li>
                    <li><a href="<?php getLink('denuncias'); ?>">Gerenciar Denúncias</a></li>
                    <li class="active">Lista de Denúncias</li>
                </ul>

                <?php if($this->session->flashdata('error_msg')){ ?>
                <div class="alert alert-danger fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="fa fa-times"></i>
                    </button>
                    <strong>Erro!</strong> Ocorreu um erro ao efetuar esta operação.
                </div>
                <?php } ?>

                <section class="panel">
                    <header class="panel-heading">
                        Gerenciamento de Denúncias
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                         </span>
                    </header>
                    <div class="panel-body">
                        <div class="panel-body">
                            <div class="adv-table">
                                <table  class="display table table-bordered table-striped" id="dynamic-table">
                                    <thead>
                                        <tr>
                                            <th width="150">Data de Registro</th>
                                            <!--<th width="15%">Imagem</th>-->
                                            <th width="">Nome</th>
                                            <th width="100px">E-mail</th>
                                            <th>Local</th>
                                            <th width="14%">Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody id="content">
                                        <?php foreach ($denunciations as $d): ?>
                                          <tr class="content-loading">
                                            <td><?php echo date("d/m/Y h:m:s", strtotime($d->dt_register)) ?></td>
                                            <td><?php echo $d->st_name ?></td>
                                            <td><?php echo $d->st_email ?></td>
                                            <td><?php echo $d->st_name_place ?></td>
                                            <td><a href="<?php getLink('denuncias/detalhe/'.$d->id.'') ?>" class="btn btn-warning btn-xs">VER</a></td>
                                          </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>

