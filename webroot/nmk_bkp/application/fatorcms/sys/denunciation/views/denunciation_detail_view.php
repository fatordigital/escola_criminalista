<?php if(isset($_GET['dev'])){ debugVarAndDie($this->session->userdata('campos_preenchidos')); } ?>

<section id="main-content">
  <section class="wrapper">
    <!-- page start-->

    <div class="row">
      <div class="col-sm-12">

        <ul class="breadcrumb">
          <li><a href="<?php getLink(); ?>"><i class="fa fa-home"></i>Home</a></li>
          <li><a href="<?php getLink('denuncias'); ?>">Gerenciar Denúncias</a></li>
          <li class="active">Detalhe da Denúncia</li>
        </ul>
        <section class="panel">
          <div class="panel-body">
            <div class="tab-content">
              <div id="galeria" class="tab-pane active">
                <div class="col-xs-12">
                  <div class="form-group">
                    <label for="data">Data Denúncia:</label>
                    <p><?php echo isset($denunciation) ? date("d/m/Y h:m:s", strtotime($denunciation->dt_register)) : '' ?></p>
                  </div>
                </div>
                <div class="col-xs-6">
                  <div class="form-group">
                    <label for="data">Nome:</label>
                    <p><?php echo isset($denunciation) ? $denunciation->st_name : ''; ?></p>
                  </div>
                  <div class="form-group">
                    <label for="">E-mail:</label>
                    <p><?php echo isset($denunciation) ? $denunciation->st_email : ''; ?></p>
                  </div>
                  <div class="form-group">                 
                    <label for="">Telefone</label>
                    <p><?php echo isset($denunciation) ? $denunciation->st_telephone : ''; ?></p>
                  </div>
                  <div class="form-group"> 
                    <label>Nome do local</label>
                    <p><?php echo isset($denunciation) ? $denunciation->st_name_place : ''; ?></p>
                  </div> 
                  <div class="form-group"> 
                    <label>Endereço</label>
                    <p><?php echo isset($denunciation) ? $denunciation->st_address : ''; ?></p>
                  </div>
                  <div class="form-group"> 
                    <label>Outras questões que percebi (descreva)</label>
                    <p><?php echo isset($denunciation) ? $denunciation->st_other_issues_description : ''; ?></p>
                  </div> 
                </div>
                <div class="col-xs-6">
                  <div class="form-group"> 
                    <label>Você está no local agora?</label>
                    <p><?php if(isset($denunciation)): if($denunciation->bl_in_place==1): echo "SIM"; else: echo "NÃO"; endif; else: echo ""; endif; ?></p>
                  </div>
                  <div class="form-group"> 
                    <label>Problemas com extintores de incêndio (ou falta deles)?</label>
                    <p><?php if(isset($denunciation)): if($denunciation->bl_fire_extinguishers==1): echo "SIM"; else: echo "NÃO"; endif; else: echo ""; endif; ?></p>
                  </div>
                  <div class="form-group"> 
                    <label>Não encontrei saídas de emergência ou elas estão bloqueadas?</label>
                    <p><?php if(isset($denunciation)): if($denunciation->bl_emergency_exits==1): echo "SIM"; else: echo "NÃO"; endif; else: echo ""; endif; ?></p>
                  </div>
                  <div class="form-group"> 
                    <label>Não encontrei porta corta-fogo ou ela está bloqueada?</label>
                    <p><?php if(isset($denunciation)): if($denunciation->bl_fire_door==1): echo "SIM"; else: echo "NÃO"; endif; else: echo ""; endif; ?></p>
                  </div>
                  <div class="form-group"> 
                    <label>O local parece estar superlotado ou não há indicações de lotação máxima?</label>
                    <p><?php if(isset($denunciation)): if($denunciation->bl_maximum_capacity==1): echo "SIM"; else: echo "NÃO"; endif; else: echo ""; endif; ?></p>
                  </div>
                  <div class="form-group"> 
                    <label>Há problemas com as luzes de emergência ou elas não existem?</label>
                    <p><?php if(isset($denunciation)): if($denunciation->bl_lamp_emergency==1): echo "SIM"; else: echo "NÃO"; endif; else: echo ""; endif; ?></p>
                  </div>
                </div>  
                <div class="col-xs-12">
                  <div class="form-group">
                    <label>Outras Informações (opcional)</label>
                    <p><?php echo isset($denunciation) ? $denunciation->st_others_information : ''; ?></p>
                  </div>
                  <div class="form-group">
                    <?php foreach ($photos as $photo):?>
                      <div class="col-xs-4">
                        <img src="<?php echo base_url('files/uploads/imagens/'.$photo->st_photo_name.'') ?>" class="img-responsive">
                      </div>
                    <?php endforeach ?>
                  </div>
                </div>          
              </div>
            </div>
          </div>
        </section>






      </div>
    </div>
    <!-- page end-->
  </section>
</section>

<script type="text/javascript" src="<?php echo getJs('tiny_mce/tiny_mce'); ?>"></script>
<script type="text/javascript">
  $(document).ready(function(){
    // Datepicker
    $('.default-date-picker').datepicker({
      format: 'dd/mm/yyyy'
    });
  });

  tinyMCE.init({
    mode : 'exact',
    elements : 'conteudo',
    entity_encoding : 'raw',
    theme : 'advanced',
    width : '97.5%',
    height : '350px',
    language : 'br',
    plugins : 'searchreplace,contextmenu,paste,filemanager,imagemanager, youtubeIframe',
    paste_auto_cleanup_on_paste : true,
    theme_advanced_buttons1 : 'bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,pasteword,removeformat,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,link,unlink,anchor,|,insertimage,insertfile, youtubeIframe',
    theme_advanced_buttons2 : '',
    theme_advanced_buttons3 : '',
    theme_advanced_toolbar_location : 'top',
    theme_advanced_toolbar_align : 'center',
    theme_advanced_statusbar_location : 'none',
    theme_advanced_resizing : false,
    relative_urls : false
    // remove_script_host : false
  });


  $('#select_publicacao').prop('selectedIndex',0);
  $('#download_cadastro').prop('selectedIndex',0);

  $('#select_publicacao').change(function(){
    if($('#select_publicacao').val()=='1'){
      $('#form_download').slideDown();
    }else{
      $('#form_download').slideUp();
    }
  });

  $('#select_download').change(function(){
    if($('#select_download').val()=='1'){
      $('#form_cadastro').slideDown();
    }else{
      $('#form_cadastro').slideUp();
    }
  });

  <?php if(isset($campos_preenchidos['publicacao']) AND $campos_preenchidos['publicacao']==1): ?>
  $('#select_publicacao option[value="1"]').attr('selected', 'selected');
  <?php endif; ?>
  <?php if(isset($campos_preenchidos['publicacao']) AND $campos_preenchidos['publicacao']==0): ?>
  $('#select_publicacao option[value="0"]').attr('selected', 'selected');
  <?php endif; ?>

  <?php if(isset($campos_preenchidos['download_cadastro']) AND $campos_preenchidos['download_cadastro']==1): ?>
  $('#select_download option[value="1"]').attr('selected', 'selected');
  <?php endif; ?>
  <?php if(isset($campos_preenchidos['download_cadastro']) AND $campos_preenchidos['download_cadastro']==0): ?>
  $('#select_download option[value="0"]').attr('selected', 'selected');
  <?php endif; ?>

</script>