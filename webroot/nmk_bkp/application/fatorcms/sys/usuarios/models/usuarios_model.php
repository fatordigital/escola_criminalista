<?php

class usuarios_model extends FD_Model {

    function __construct() {
        parent::__construct();
    }

    public function getAllUsuarios(){
        return $this->db->get('fd_users')->result();
    }

    public function getUsuario($id){
      $this->db->where('fd_users.id',$id);
      return $this->db->get('fd_users')->row();
    }

    public function hasUser($user){
      $this->db->where('fd_users.user',$user);
      return $this->db->get('fd_users')->row();
    }

    public function insertUsuario($data){
        if($this->db->insert('fd_users',$data)){
            return true;
        }else{
            return false;
        }
    }

    public function updateUsuario($id,$data){
        $this->db->where('id',$id);
        if($this->db->update('fd_users',$data)){
            return true;
        }else{
            return false;
        }
    }

    public function deleteUsuario($id){
        if($this->db->delete('fd_users', array('id' => $id))){
            return true;
        }
        return false;
    }


} 