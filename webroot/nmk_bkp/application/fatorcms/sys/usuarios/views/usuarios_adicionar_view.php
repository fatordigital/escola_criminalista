<section id="main-content">
  <section class="wrapper">
    <!-- page start-->

    <div class="row">
      <div class="col-sm-12">

        <ul class="breadcrumb">
          <li><a href="<?php getLink(); ?>"><i class="fa fa-home"></i>Home</a></li>
          <li><a href="<?php getLink('usuarios'); ?>">Gerenciar Usuários</a></li>
          <li class="active">Cadastrar Usuário</li>
        </ul>

        <?php if($this->session->flashdata('duplicated_msg')){ ?>
          <div class="alert alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
              <i class="fa fa-times"></i>
            </button>
            <strong>Erro!</strong> Este usuário já esta cadastrado, por favor, escolha outro nome de usuário.
          </div>
        <?php } ?>

        <?php
          if($this->session->userdata('fields')){
            $fields = $this->session->userdata('fields');
          }
        ?>

        <section class="panel">
          <div class="panel-body">
            <div class="tab-content">
              <div id="galeria" class="tab-pane active">
                <form role="form" method="post" action="" enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="">Usuário</label>
                    <input type="text" name="user" maxlength="50" class="form-control" value="<?php echo isset($fields['user']) ? $fields['user'] : ''; ?>" required>
                    <div>* Não deve conter espaços ou caracteres especiais, apenas letras e números.</div>

                    <label for="">Nome</label>
                    <input type="text" name="name" maxlength="50" class="form-control" value="<?php echo isset($fields['name']) ? $fields['name'] : ''; ?>" required>

                    <label for="">E-mail</label>
                    <input type="text" name="email" maxlength="50" class="form-control" value="<?php echo isset($fields['email']) ? $fields['email'] : ''; ?>" required>

                    <label for="">Senha</label>
                    <input type="text" name="password" maxlength="50" class="form-control" value="<?php echo isset($fields['password']) ? $fields['password'] : ''; ?>" required>
                  </div>
                  <div class="form-group">
                    <input type="hidden" name="usuario_adicionar" value="true" />
                    <button type="submit" class="btn btn-info">Salvar</button>
                  </div>
                </form>
              </div>

            </div>
          </div>
        </section>



        <!-- old code down here -->





      </div>
    </div>
    <!-- page end-->
  </section>
</section>