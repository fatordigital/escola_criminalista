<?php if(isset($_GET['dev'])){ debugVarAndDie($categoria); }?>
<section id="main-content">
  <section class="wrapper">
    <!-- page start-->

    <div class="row">
      <div class="col-sm-12">

        <ul class="breadcrumb">
          <li><a href="<?php getLink(); ?>"><i class="fa fa-home"></i>Home</a></li>
          <li><a href="<?php getLink('categorias'); ?>">Gerenciar Categorias</a></li>
          <li class="active">Cadastrar Categoria</li>
        </ul>

        <section class="panel">
          <div class="panel-body">
            <div class="tab-content">
              <div id="galeria" class="tab-pane active">
                <form role="form" method="post" action="" enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="">Nome</label>
                    <input type="text" name="nome" maxlength="50" class="form-control" value="<?php if(isset($categoria)){ echo $categoria->nome; } ?>">
                  </div>
                  <div class="form-group">
                    <input type="hidden" name="categoria_editar" value="true" />
                    <button type="submit" class="btn btn-info">Salvar</button>
                  </div>
                </form>
              </div>

            </div>
          </div>
        </section>



        <!-- old code down here -->





      </div>
    </div>
    <!-- page end-->
  </section>
</section>