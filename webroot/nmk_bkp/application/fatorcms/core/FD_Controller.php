<?php
/**
 * MK CodeIgniter
 * fb.com/mkethur
 *
 * FD_Controller.php
 *
 * Version: 1.0.2
 *
 * Date: 28 / 02 / 2014
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class FD_Controller extends CI_Controller {

    public $layout;
    public $title;
    public $menu;
    public $head;
    public $footer;
    public $css;
    public $css_vendor = array();
    public $js = array();
    public $js_bottom = array();
    public $js_vendor = array();
    public $js_vendor_bottom = array();


    public function __construct() {
        parent::__construct();        
        $this->siteInitialize();
    }
    
    
    /**
     * siteInitialize
     *
     * - Carrega o título padão do site, css, js, css vendor, js vendor do layout padrao;
     * - Carrega o bloco {menu_for_layout} e {footer_for_layout};
     * - Sobrescreve a instancia do CodeIgniter rescrevendo a leitura dos blocos ao site,
     * proporcionando maior dinâmica ao layout;
     *
     */
    public function siteInitialize(){        
        $this->title = 'FatorCMS';
        $this->css = array(
            'bs3/bootstrap.min',
            'bootstrap-reset',
            'style',
            'style-responsive',
            'font-awesome/css/font-awesome',
            'iCheck-master/skins/minimal/minimal'
        );
        $this->js = array(
            'bs3/bootstrap.min',
            'accordion-menu/jquery.dcjqaccordion.2.7',
            'scrollTo/jquery.scrollTo.min',
            'jQuery-slimScroll-1.3.0/jquery.slimscroll',
            'nicescroll/jquery.nicescroll',
            'easypiechart/jquery.easypiechart',
            'sparkline/jquery.sparkline',
            'flot-chart/jquery.flot',
            'flot-chart/jquery.flot.tooltip.min',
            'flot-chart/jquery.flot.resize',
            'flot-chart/jquery.flot.pie.resize'
        );
        $this->css_vendor = array(
            'bootstrap-switch-master/build/css/bootstrap3/bootstrap-switch.min'
        );
        $this->js_vendor = array(
            'bootstrap-switch-master/build/js/bootstrap-switch.min'
        );
        
        $CI = & get_instance();
        
        if(!empty($this->menu)){
          $this->extra = array(
              '{menu_for_layout}' => $CI->load->file(APPPATH.'layouts/menu/'.$this->menu.'.php', true)
          );          
        }
        if(!empty($this->footer)){
            $this->extra = array_merge($this->extra, array(
                '{footer_for_layout}' => $CI->load->file(APPPATH.'layouts/footer/'.$this->footer.'.php', true)
            ));
        }
    }

    /**
     * LoadJS
     * Carrega os javascripts da pasta /js ao topo do html
     *
     * @param $js String/Array
     */
    protected function loadJS($js){
        if(is_array($js)){
            foreach($js as $jsAdd){            
                array_push($this->js, $jsAdd);
            }
        } else {             
            array_push($this->js, $js);
        }
    }

    /**
     * LoadJSVendor
     * Carrega os javascripts da pasta /vendor_packages ao topo do html
     *
     * @param $js String/Array
     */
    protected function loadJSVendor($js){
        if(is_array($js)){
            foreach($js as $jsAdd){
                array_push($this->js_vendor, $jsAdd);
            }
        } else {
            array_push($this->js_vendor, $js);
        }
    }

    /**
     * LoadJSVendorBottom
     * Carrega os javascripts da pasta /vendor_packages ao bottom do html
     *
     * @param $js String/Array
     */
    protected function loadJSVendorBottom($js){
        if(is_array($js)){
            foreach($js as $jsAdd){
                array_push($this->js_vendor_bottom, $jsAdd);
            }
        } else {
            array_push($this->js_vendor_bottom, $js);
        }
    }

    /**
     * LoadJSBottom
     * Carrega os javascripts da pasta /js ao bottom do html
     *
     * @param $js String/Array
     */
    protected function loadJSBottom($js){
        if(is_array($js)){
            foreach($js as $jsAdd){
                array_push($this->js_bottom, $jsAdd);
            }
        } else {
            array_push($this->js_bottom, $js);
        }
    }

    /**
     * loadCSS
     * Carrega os stylesheets da pasta /css ao html
     *
     * @param $css String/Array
     */
    protected function loadCSS($css){
        if(is_array($css)){
            foreach($css as $cssAdd){
                array_push($this->css, $cssAdd);
            }
        } else {         
            array_push($this->css, $css);
        }
    }

    /**
     * loadCSS
     * Carrega os stylesheets da pasta /vendor_packages ao html
     *
     * @param $css String/Array
     */
    protected function loadCSSVendor($css){
        if(is_array($css)){
            foreach($css as $cssAdd){
                array_push($this->css_vendor, $cssAdd);
            }
        } else {
            array_push($this->css_vendor, $css);
        }
    }

    /**
     * setTitle
     * Define título agregado ao padrao do site.
     * Modelo: {titulo} - {titulo_padrao}
     *
     * @param $title String
     */
    protected function setTitle($title){
        $this->title = $title . ' - ' . $this->title;
    }


    /**
     * setMenu
     * Define a view a ser carregada como menu, da pasta layouts/menu/
     *
     * @param $menu String
     */
    protected function setMenu($menu){
      $this->menu = $menu;
    }

    /**
     * setFooter
     * Define a view a ser carregada como footer, da pasta layouts/footer
     *
     * @param $footer String
     */
    protected function setFooter($footer){
        $this->footer = $footer;
    }

    /**
     * setLayout
     * Define view a ser carregada como layout, da pasta layouts/
     *
     * @param $layout String
     */
    protected function setLayout($layout){
      $this->layout = $layout;
    }

    /**
     * isAjax
     *
     * Define a view do layout como nula, permitindo html em branco, normalmente utilizado em pagecall de ajax.
     */
    protected function isAjax() {
      $this->layout = '';
    }

    /**
     * isLoggedIn
     * Verifica se o visitante está autenticado como usuário ao sistema
     *
     * @return bool
     */
    protected function isLoggedIn(){
        if($this->session->userdata('auth')==='true'){
            return true;
        }else{
            return false;
        }
    }

}