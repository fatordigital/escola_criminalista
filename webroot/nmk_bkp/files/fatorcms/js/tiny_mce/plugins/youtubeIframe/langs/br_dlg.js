tinyMCE.addI18n('br.youtube_dlg',{
	title: 'Inserir/editar youtube videos',
    url_field: 'URL do YouTube ou Código:',
    url_example1: 'Exemplo de URL',
    url_example2: 'Examplo de Código',
    choose_size: 'Escolha do Tamanho',
    custom: 'Personalizado',
    Width: 'Largura',
    Height: 'Altura',
    iframe: 'Novo iFrame',
    embed: 'Antigo Embeded'
});
