<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 */

/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
Router::connect('/', array('controller' => 'pages', 'action' => 'home'));

/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
Router::connect('/pages/:action', array('controller' => 'pages'));

Router::mapResources('movies');
Router::parseExtensions();

Router::connect('/filmes/meus-filmes', ['controller' => 'movies', 'action' => 'mymovies']);
Router::connect('/filmes/favoritos', ['controller' => 'movie_favorites', 'action' => 'mymovies']);
Router::connect('/filmes/meus-cursos', ['controller' => 'movies', 'action' => 'mycourses']);

Router::connect(
    '/movies/buy/:movie_id', ['controller' => 'orders', 'action' => 'buy'],
    ['pass' => ['movie_id']]
);

Router::connect(
    '/movies/view/:slug', ['controller' => 'movies', 'action' => 'view'],
    ['pass' => ['slug']]
);

Router::connect(
    '/escrito/:slug', ['controller' => 'news', 'action' => 'view'],
    ['pass' => ['slug']]
);


Router::connect('/users/index', ['plugin' => 'users', 'controller' => 'users', 'action' => 'index']);
Router::connect('/users/login', ['plugin' => 'users', 'controller' => 'users', 'action' => 'login']);
Router::connect('/users/home', ['plugin' => 'users', 'controller' => 'users', 'action' => 'home']);
// Router::connect('/usuarios/dados', ['plugin' => 'users', 'controller' => 'users', 'action' => 'index']);
Router::connect('/escritos/*', ['controller' => 'news', 'action' => 'index']);
Router::connect('/filmes/*', ['controller' => 'movies', 'action' => 'index']);
Router::connect(
    '/cursos/modulos/:slug', ['controller' => 'movies', 'action' => 'index', 'modules']
);
Router::connect('/cursos/*', ['controller' => 'movies', 'action' => 'index', 'courses']);
//Router::connect('/filmes/cursos/*', ['controller' => 'movies', 'action' => 'index', 'prefix' => 'courses']);

Router::connect('/contato/*', ['controller' => 'contacts', 'action' => 'add']);

Router::connect('/aovivo/*', ['controller' => 'lives', 'action' => 'index']);
//Router::connect('/diadobem', ['controller' => 'diadobem', 'action' => 'index']);
//Router::connect('/contato/*', ['controller' => 'movies', 'action' => 'mymovies']);

Router::connect('/processo-penal-tolerante', ['controller' => 'pages', 'action' => 'processo_penal_tolerante']);
Router::connect('/processo-penal-tolerante/*', ['controller' => 'pages', 'action' => 'processo_penal_tolerante']);

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
require CAKE . 'Config' . DS . 'routes.php';
